// ModelProcessor.cpp : Defines the entry point for the console application.
//

#include <iostream>
#include <algorithm>
#include <unordered_map>
#include <tchar.h>
#include <string>
#include <fstream>
#include <stdint.h>
#include <limits>
#include "lz4\lz4.h"
#include "lz4\lz4hc.h"
#include "modelprocessor.h"
#include "vcacheopt.h"
using namespace std;

#pragma comment(lib, "libfbxsdk-mt.lib")
#ifdef NDEBUG
#pragma comment(lib, "lz4c64.lib")
#else
#pragma comment(lib, "lz4c64d.lib")
#endif
#pragma comment(lib, "libfbxsdk-mt.lib")


struct VertexFormat
{
	static const int typeNumber = 1;
	float position[3];
	float normal[3];
	float tangent[4];
	float binormal[4];
	float texture[2];
};

bool operator==(const VertexFormat& lhs, const VertexFormat& rhs)
{
	if(memcmp(&lhs, &rhs, sizeof(VertexFormat)))
		return false;
	else
		return true;
}

struct CollisionVertexFormat
{
	static const int typeNumber = 2;
	float position[3];

	bool operator==(const CollisionVertexFormat& rhs){ return (position[0] == rhs.position[0]) && (position[1] == rhs.position[1]) && (position[2] == rhs.position[2]); }
};

FbxManager* lSdkManager = nullptr;
char lInputFilename[512];
char lOutputFilename[512];


bool isCollisionModel = false;
bool waitForInput = false;

int _tmain(int argc, _TCHAR* argv[])
{
	if (argc == 1)
		FailAndExit("Arguments are: <inputfilename> <outputfilename>\nOptions are -c for collision meshes\n");
	else if (argc == 2)
	{
		waitForInput = true;
		wcstombs(lInputFilename, argv[1], sizeof(lInputFilename));
		wcstombs(lOutputFilename, argv[1], sizeof(lOutputFilename));
		string outName(lOutputFilename);
		size_t lastof;
		lastof = outName.find_last_of('.');
		outName = outName.substr(0, lastof);
		outName += ".pbm";
		strncpy(lOutputFilename, outName.c_str(), outName.length());
	}
	else if (argc > 2)
	{
		wcstombs(lInputFilename, argv[1], sizeof(lInputFilename));
		wcstombs(lOutputFilename, argv[2], sizeof(lOutputFilename));
	}
	if (argc > 3)
	{
		char optionBuffer[3];
		wcstombs(optionBuffer, argv[3], 3);
		if (strcmp(optionBuffer, "-c") == 0)
			isCollisionModel = true;
	}

	ConvertMeshToPBM(lInputFilename, lOutputFilename, isCollisionModel);


	if(waitForInput)
		system("pause");

	return 0;
}

void ProcessNodes( FbxNode* current )
{
	// check node attributes for mesh
	for(int i = 0; i < current->GetNodeAttributeCount(); i++)
	{
		FbxNodeAttribute* currentAttribute = current->GetNodeAttributeByIndex(i);

		FbxNodeAttribute::EType currentType = currentAttribute->GetAttributeType();
		if(currentType == FbxNodeAttribute::eMesh)
		{
			const float maxFloat = numeric_limits<float>::min();
			const float minFloat = numeric_limits<float>::max();
			float max_extents[3] = {maxFloat, maxFloat, maxFloat};
			float min_extents[3] = {minFloat, minFloat, minFloat};
			if(!isCollisionModel)
			{
				vector<VertexFormat> currentModel;
				vector<uint16_t> indexBuffer;
				cout << endl << "Node name: " << currentAttribute->GetNameOnly() << endl;
				ProcessMesh(currentAttribute, currentModel, indexBuffer, min_extents, max_extents);
				cout << "Mesh Processed" << endl;
				GeneratePBM<VertexFormat>(string(currentAttribute->GetNameOnly()), currentModel, indexBuffer, min_extents, max_extents);
			}
			else if(isCollisionModel)
			{
				vector<CollisionVertexFormat> currentModel;
				vector<uint16_t> indexBuffer;
				cout << endl << "Node name: " << currentAttribute->GetNameOnly() << endl;
				ProcessCollisionMesh(currentAttribute, currentModel, min_extents, max_extents);
				cout << "Mesh Processed" << endl;
				GeneratePBM(string(currentAttribute->GetNameOnly()), currentModel, indexBuffer, min_extents, max_extents);
			}
		}
	}
	
	for(int i = 0; i < current->GetChildCount(); i++)
	{
		ProcessNodes(current->GetChild(i));
	}
}

void ProcessCollisionMesh( FbxNodeAttribute* meshAttribute, std::vector<CollisionVertexFormat>& model, float* min_extents, float* max_extents )
{
	FbxMesh* pMesh = (FbxMesh*)meshAttribute;

	string uvName;
	ValidateInputMesh(pMesh, uvName);

	const FbxVector4 * lControlPoints = pMesh->GetControlPoints();

	int lPolygonIndexCount = pMesh->GetPolygonCount();
	uint32_t currentVertId = 0;


	for (int lPolygonIndex = 0; lPolygonIndex < lPolygonIndexCount; lPolygonIndex++)
	{

		for (int lVerticeIndex = 0; lVerticeIndex < 3; ++lVerticeIndex)
		{
			CollisionVertexFormat currentVertex;
			const int lControlPointIndex = pMesh->GetPolygonVertex(lPolygonIndex, lVerticeIndex);
			if(lControlPointIndex == -1)
			{
				FailAndExit("invalid control point index");
			}
			

			FbxVector4 lCurrentControlPointPosition = lControlPoints[lControlPointIndex];
			currentVertex.position[0] = static_cast<float>(lCurrentControlPointPosition.mData[0]);
			currentVertex.position[1] = static_cast<float>(lCurrentControlPointPosition.mData[1]);
			currentVertex.position[2] = static_cast<float>(lCurrentControlPointPosition.mData[2]);

			max_extents[0] = std::max(max_extents[0], currentVertex.position[0]);
			max_extents[1] = std::max(max_extents[1], currentVertex.position[1]);
			max_extents[2] = std::max(max_extents[2], currentVertex.position[2]);

			min_extents[0] = std::min(min_extents[0], currentVertex.position[0]);
			min_extents[1] = std::min(min_extents[1], currentVertex.position[1]);
			min_extents[2] = std::min(min_extents[2], currentVertex.position[2]);

			//auto currentTuple = make_tuple(currentVertex.position[0], currentVertex.position[1], currentVertex.position[2]);
			//uint32_t hashValue = hash_point(currentVertex.position[0], currentVertex.position[1], currentVertex.position[2]);
			//auto found = vertMap.find(hashValue);
			//if(found == vertMap.end())
			//{
			//	vertMap[hashValue] = true;
			//	model.push_back(currentVertex);
			//}
			
			auto it = find(model.begin(), model.end(), currentVertex);
			if(it == model.end())
				model.push_back(currentVertex);

			
			++currentVertId;
		}
	}

	cout << "Binary Collision Model Generated" << endl;
}

void ProcessMesh( FbxNodeAttribute* meshAttribute, vector<VertexFormat>& model, vector<uint16_t>& ib , float* min_extents, float* max_extents)
{	
	FbxMesh* pMesh = (FbxMesh*)meshAttribute;
	
	string uvName;
	ValidateInputMesh(pMesh, uvName);

	GenerateTangents(pMesh);

	VerifyFBXMappingModes(pMesh);

	const FbxVector4 * lControlPoints = pMesh->GetControlPoints();

	unsigned reuseCount = 0;

	int lPolygonIndexCount = pMesh->GetPolygonCount();
	uint32_t currentVertId = 0;
	
	for (int lPolygonIndex = 0; lPolygonIndex < lPolygonIndexCount; lPolygonIndex++)
	{
		
		for (int lVerticeIndex = 0; lVerticeIndex < 3; ++lVerticeIndex)
		{
			bool success = false;
			VertexFormat currentVertex;
			const int lControlPointIndex = pMesh->GetPolygonVertex(lPolygonIndex, lVerticeIndex);
			if(lControlPointIndex == -1)
			{
				FailAndExit("invalid control point index");
			}
			FbxVector4 lCurrentControlPointPosition = lControlPoints[lControlPointIndex];
			currentVertex.position[0] = static_cast<float>(lCurrentControlPointPosition.mData[0]);
			currentVertex.position[1] = static_cast<float>(lCurrentControlPointPosition.mData[1]);
			currentVertex.position[2] = static_cast<float>(lCurrentControlPointPosition.mData[2]);

			max_extents[0] = std::max(max_extents[0], currentVertex.position[0]);
			max_extents[1] = std::max(max_extents[1], currentVertex.position[1]);
			max_extents[2] = std::max(max_extents[2], currentVertex.position[2]);

			min_extents[0] = std::min(min_extents[0], currentVertex.position[0]);
			min_extents[1] = std::min(min_extents[1], currentVertex.position[1]);
			min_extents[2] = std::min(min_extents[2], currentVertex.position[2]);

			FbxVector4 lCurrentNormal;
			success = pMesh->GetPolygonVertexNormal(lPolygonIndex, lVerticeIndex, lCurrentNormal);
			if(!success)
				FailAndExit("Vertex does not have valid normal");
			currentVertex.normal[0] = static_cast<float>(lCurrentNormal[0]);
			currentVertex.normal[1] = static_cast<float>(lCurrentNormal[1]);
			currentVertex.normal[2] = static_cast<float>(lCurrentNormal[2]);

			FbxGeometryElementTangent* leTangent = pMesh->GetElementTangent(0);
			if(leTangent->GetReferenceMode() !=  FbxLayerElement::EReferenceMode::eDirect) 
				FailAndExit("Tangents have unsupported reference mode, tell poop to fix");
			FbxVector4 lCurrentTangent;
			lCurrentTangent = leTangent->GetDirectArray().GetAt(currentVertId);
			currentVertex.tangent[0] = static_cast<float>(lCurrentTangent[0]);
			currentVertex.tangent[1] = static_cast<float>(lCurrentTangent[1]);
			currentVertex.tangent[2] = static_cast<float>(lCurrentTangent[2]);
			currentVertex.tangent[3] = static_cast<float>(lCurrentTangent[3]);

			FbxGeometryElementBinormal* leBinormal = pMesh->GetElementBinormal(0);
			if(leBinormal->GetReferenceMode() !=  FbxLayerElement::EReferenceMode::eDirect) 
				FailAndExit("Binormals have unsupported reference mode, tell poop to fix");
			FbxVector4 lCurrentBinormal;
			lCurrentBinormal = leBinormal->GetDirectArray().GetAt(currentVertId);
			currentVertex.binormal[0] = static_cast<float>(lCurrentBinormal[0]);
			currentVertex.binormal[1] = static_cast<float>(lCurrentBinormal[1]);
			currentVertex.binormal[2] = static_cast<float>(lCurrentBinormal[2]);
			currentVertex.binormal[3] = static_cast<float>(lCurrentBinormal[3]);

			FbxVector2 lCurrentUV;
			bool lUnmappedUV;
			success = pMesh->GetPolygonVertexUV(lPolygonIndex, lVerticeIndex, uvName.c_str(), lCurrentUV, lUnmappedUV);
			if(!success)
				FailAndExit("Vertex does not have valid UV");
			currentVertex.texture[0] = static_cast<float>(lCurrentUV[0]);
			currentVertex.texture[1] = static_cast<float>(lCurrentUV[1]);

			int modelSize = model.size();
			bool vertExists = false;
			for(int i = 0; i < modelSize; ++i)
			{
				if (model[i] == currentVertex)
				{
					vertExists = true;
					ib.push_back(i);
					reuseCount++;
					break;
				}
			}
			if(!vertExists)
			{
				ib.push_back(model.size());
				model.push_back(currentVertex);
			}
			++currentVertId;
		}
	}

	if(model.size() > 65535)
		FailAndExit("too many verts for 16 bit index, try to split mesh or ask poop to implement optional 32 bit indicies");

	cout << reuseCount << " Verts reused out of " << reuseCount+model.size() << endl;
	vector<int> expandedIB(ib.begin(), ib.end());
	int triangleCount = expandedIB.size() / 3;
	VertexCache vCache;
	int misses = vCache.GetCacheMissCount(&expandedIB[0], triangleCount);

	printf("*** Before optimization ***\n");
	printf("Cache misses\t: %d\n", misses);
	printf("ACMR\t\t: %f\n", (float)misses / (float)triangleCount);

	VertexCacheOptimizer vco;
	printf("Optimizing ... \n");
	VertexCacheOptimizer::Result res = vco.Optimize(&expandedIB[0], triangleCount);
	if (res)
		FailAndExit("Error optimizing indicies\n");

	misses = vCache.GetCacheMissCount(&expandedIB[0], triangleCount);

	printf("*** After optimization ***\n");
	printf("Cache misses\t: %d\n", misses);
	printf("ACMR\t\t: %f\n", (float)misses / (float)triangleCount);

	ib.clear();
	ib.insert(ib.begin(), expandedIB.begin(), expandedIB.end());

	cout << "Binary Model Generated" << endl;
}

void ValidateInputMesh( FbxMesh* pMesh,  string& uvName)
{
	if(!pMesh)
		FailAndExit("ProcessMesh requires a valid mesh reference");

	if(!isCollisionModel)
	{

		if(pMesh->GetElementNormalCount() < 1)
			FailAndExit("model requires normals");

		if(pMesh->GetElementUVCount() < 1)
			FailAndExit("model requires UV\'s");

		FbxStringList lUVNames;
		pMesh->GetUVSetNames(lUVNames);
		if(lUVNames.GetCount() > 1)
			FailAndExit("Too many UV\'s");
		// should never get here due to earlier check
		else if(lUVNames.GetCount() < 1)
			FailAndExit("Not enough UV\'s");

		uvName = lUVNames.GetItemAt(0)->mString;
	}
}

void GenerateTangents( FbxMesh* pMesh )
{
	if(pMesh->GetElementTangentCount())
		cout << "Using tangents from file" << endl;
	else
	{
		bool success = pMesh->GenerateTangentsDataForAllUVSets(false);
		if(!success)
			FailAndExit("Could not generate tangents");
		cout << "tangents sucessfully generated" << endl;
	}
}

void VerifyFBXMappingModes( FbxMesh* pMesh )
{
	FbxGeometryElement::EMappingMode lNormalMappingMode = FbxGeometryElement::eNone;
	lNormalMappingMode = pMesh->GetElementNormal(0)->GetMappingMode();
	if(lNormalMappingMode != FbxGeometryElement::eByPolygonVertex)
		FailAndExit("unsupported normal mapping mode, tell poop to fix");

	FbxGeometryElement::EMappingMode lUVMappingMode = FbxGeometryElement::eNone;
	lUVMappingMode = pMesh->GetElementUV(0)->GetMappingMode();
	if(lUVMappingMode != FbxGeometryElement::eByPolygonVertex)
		FailAndExit("unsupported UV mapping mode, tell poop to fix");

	FbxGeometryElement::EMappingMode lTangentMappingMode = FbxGeometryElement::eNone;
	lTangentMappingMode = pMesh->GetElementTangent(0)->GetMappingMode();
	if(lTangentMappingMode != FbxGeometryElement::eByPolygonVertex)
		FailAndExit("unsupported Tangent mapping mode, tell poop to fix");

	FbxGeometryElement::EMappingMode lBinormalMappingMode = FbxGeometryElement::eNone;
	lTangentMappingMode = pMesh->GetElementBinormal(0)->GetMappingMode();
	if(lTangentMappingMode != FbxGeometryElement::eByPolygonVertex)
		FailAndExit("unsupported Binormal mapping mode, tell poop to fix");
}

void FailAndExit(string str)
{
	cout << "Failed: " << str << endl;
	
#ifdef _DEBUG
	system("pause");
	__debugbreak();
#endif 
	if(waitForInput)
		system("pause");
	exit(-1);
}

void GeneratePBMVersion1( string name, vector<VertexFormat>& model )
{
	FailAndExit("Shouldn't be generating pbm version 1 anymore");

	ofstream outFile;
	outFile.open (lOutputFilename, ios::binary | ios::out);
	if(!outFile.is_open())
		FailAndExit("unable to open " + name + "for output");

	outFile.write("POOP", sizeof("POOP"));
	const uint32_t version = 1;
	outFile.write(reinterpret_cast<const char*>(&version), sizeof (version));
	uint32_t size = static_cast<uint32_t>(model.size());
	outFile.write(reinterpret_cast<const char*>(&size), sizeof (size));
	for(uint32_t i = 0; i < size; ++i)
	{
		outFile.write(reinterpret_cast<const char*>(&model[i]), sizeof (model[i]));
	}
}

template<typename T> 
void GeneratePBM( std::string name, std::vector<T>& model, std::vector<uint16_t>& ib, float* min_extents, float* max_extents)
{
	ofstream outFile;
	outFile.open (lOutputFilename, ios::binary | ios::out);
	if(!outFile.is_open())
		FailAndExit("unable to open " + name + "for output");

	char* dataChunk = new char[model.size() * sizeof(model[0]) + ib.size() * sizeof(ib[0])];
	memcpy(dataChunk, &model[0],  model.size() * sizeof(model[0]));
	memcpy(dataChunk + model.size() * sizeof(model[0]), &ib[0], ib.size() * sizeof(ib[0]));


	int worstCase = LZ4_compressBound(model.size() * sizeof(model[0]) + ib.size() * sizeof(ib[0]));
	char* dest = new char[worstCase];

	int compressedSize = LZ4_compressHC(dataChunk, dest, model.size() * sizeof(model[0]) + ib.size() * sizeof(ib[0]));

	cout << "shrunk down to " + to_string((float)compressedSize / (model.size() * sizeof(model[0]) + ib.size() * sizeof(ib[0])) * 100) + "%" << endl;

	cout << "extents: " << min_extents[0] << " " << min_extents[1] << " " << min_extents[2] <<  " " << max_extents[0] <<  " " << max_extents[1] <<  " " << max_extents[2] << endl;

	float halfSizes[3];
	float centers[3];

	halfSizes[0] = (max_extents[0] - min_extents[0]) / 2.0f;
	halfSizes[1] = (max_extents[1] - min_extents[1]) / 2.0f;
	halfSizes[2] = (max_extents[2] - min_extents[2]) / 2.0f;
	centers[0] = min_extents[0] + halfSizes[0];
	centers[1] = min_extents[1] + halfSizes[1];
	centers[2] = min_extents[2] + halfSizes[2];

	const uint32_t version = 7;
	const uint32_t typeNum = T::typeNumber;
	const uint32_t vertexCount = static_cast<uint32_t>(model.size());
	const uint32_t indexCount = static_cast<uint16_t>(ib.size());
	outFile.write("POOP",											sizeof("POOP"));
	outFile.write(reinterpret_cast<const char*>(&version),			sizeof (version));
	outFile.write(reinterpret_cast<const char*>(&typeNum),			sizeof (typeNum));
	outFile.write(reinterpret_cast<const char*>(&compressedSize),	sizeof (compressedSize));
	outFile.write(reinterpret_cast<const char*>(&vertexCount),		sizeof (vertexCount));
	outFile.write(reinterpret_cast<const char*>(&indexCount),		sizeof (indexCount));
	outFile.write(reinterpret_cast<const char*>(centers),		sizeof(float) * 3);
	outFile.write(reinterpret_cast<const char*>(halfSizes),		sizeof(float) * 3);
	outFile.write(dest, compressedSize);

	outFile.close();

	cout << "Success";

	delete[] dest;
}

void ConvertMeshToPBM(string input, string output, bool collisionMesh)
{
	waitForInput = true;
	isCollisionModel = collisionMesh;

	string outName(lOutputFilename);
	size_t lastof;
	lastof = outName.find_last_of('.');
	outName = outName.substr(0, lastof);
	outName += ".pbm";
	strncpy(lInputFilename, input.c_str(), output.length());
	strncpy(lOutputFilename, outName.c_str(), output.length());





	// Initialize the SDK manager. This object handles memory management.
	lSdkManager = FbxManager::Create();

	// Create the IO settings object.
	FbxIOSettings *ios = FbxIOSettings::Create(lSdkManager, IOSROOT);
	lSdkManager->SetIOSettings(ios);

	// Create an importer using the SDK manager.
	FbxImporter* lImporter = FbxImporter::Create(lSdkManager, "");

	// Use the first argument as the filename for the importer.
	if (!lImporter->Initialize(lInputFilename, -1, lSdkManager->GetIOSettings()))
	{
		string err = "Call to FbxImporter::Initialize() failed.\n Error returned: " + string(lImporter->GetStatus().GetErrorString());
		FailAndExit(err);
	}

	cout << lInputFilename << " opened." << endl;

	// Create a new scene so that it can be populated by the imported file.
	FbxScene* lScene = FbxScene::Create(lSdkManager, "myScene");

	// Import the contents of the file into the scene.	
	bool success = lImporter->Import(lScene);

	if (!success)
		FailAndExit("Error returned: " + string(lImporter->GetStatus().GetErrorString()));

	cout << "scene imported" << endl;

	// The file is imported, so get rid of the importer.
	lImporter->Destroy();

	FbxDocumentInfo* sceneInfo = lScene->GetSceneInfo();
	if (sceneInfo)
	{
		FBXSDK_printf("\n\n--------------------\nMeta-Data\n--------------------\n\n");
		FBXSDK_printf("    Title: %s\n", sceneInfo->mTitle.Buffer());
		FBXSDK_printf("    Subject: %s\n", sceneInfo->mSubject.Buffer());
		FBXSDK_printf("    Author: %s\n", sceneInfo->mAuthor.Buffer());
		FBXSDK_printf("    Keywords: %s\n", sceneInfo->mKeywords.Buffer());
		FBXSDK_printf("    Revision: %s\n", sceneInfo->mRevision.Buffer());
		FBXSDK_printf("    Comment: %s\n", sceneInfo->mComment.Buffer());
	}

	FbxAxisSystem sceneAxisSystem = lScene->GetGlobalSettings().GetAxisSystem();
	//FbxSystemUnit sceneSystemUnit = lScene->GetGlobalSettings().GetSystemUnit();

	FbxAxisSystem engineAxisSystem(FbxAxisSystem::DirectX);
	//FbxSystemUnit engineSystemUnit(FbxSystemUnit::cm);

	// NOTE:  You must set the System Units BEFORE the System Axis
	//if (sceneSystemUnit != engineSystemUnit)
	//{
	//	engineSystemUnit.ConvertScene(lScene);
	//}

	if (sceneAxisSystem != engineAxisSystem)
	{
		engineAxisSystem.ConvertScene(lScene);     // convert the scene to engine specs
	}

	FbxGeometryConverter converter(lSdkManager);
	converter.Triangulate(lScene, true);

	// Print the nodes of the scene and their attributes recursively.
	// Note that we are not printing the root node because it should
	// not contain any attributes.
	FbxNode* lRootNode = lScene->GetRootNode();
	if (lRootNode)
	{
		for (int i = 0; i < lRootNode->GetChildCount(); i++)
		{
			ProcessNodes(lRootNode->GetChild(i));
		}
	}

	// Destroy the SDK manager and all the other objects it was handling.
	lSdkManager->Destroy();
}

