#ifndef _MODEL_PROCESSOR_H_
#define _MODEL_PROCESSOR_H_

#include "fbxsdk.h"
#include <vector>
struct VertexFormat;
struct CollisionVertexFormat;

extern "C" __declspec(dllexport) void ConvertMeshToPBM(std::string input, std::string output, bool collisionMesh);
void ProcessNodes(FbxNode* current);
void ProcessMesh(FbxNodeAttribute* mesh, std::vector<VertexFormat>& model, std::vector<uint16_t>& ib, float* min_extents, float* max_extents);
void ProcessCollisionMesh(FbxNodeAttribute* mesh, std::vector<CollisionVertexFormat>& model, float* min_extents, float* max_extents);
void ValidateInputMesh( FbxMesh* pMesh, std::string& uvName );
void GenerateTangents( FbxMesh* pMesh );
void VerifyFBXMappingModes( FbxMesh* pMesh );
void FailAndExit(std::string str);
void GeneratePBM(std::string name, std::vector<VertexFormat>& model);

// don't call these directly
void GeneratePBMVersion1(std::string name, std::vector<VertexFormat>& model);
template<typename T> void GeneratePBM(std::string name, std::vector<T>& model, std::vector<uint16_t>& ib, float* min_extents, float* max_extents);

#endif // !_MODEL_PROCESSOR_H