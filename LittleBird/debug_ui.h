#ifndef _DEBUG_UI_H_
#define _DEBUG_UI_H_

#include <memory>
#include <string>

#include "ui_var_type.h"

class DebugUI;
struct ID3D11Device;

class DebugUI
{
public:
	DebugUI(ID3D11Device* device, int width, int height);
	DebugUI(const DebugUI&);
	~DebugUI();

	bool GetEnabled(){return m_Enabled;}
	void SetEnabled(bool enabled);

	void CreateMenu(std::string name);
	void SetMenuRefreshRate(std::string name, float rate);
	void SetMenuSize(std::string name, int x, int y);

	void AddReadOnlyVariable(std::string menuName, std::string varName, DEBUG_UI_TYPE type, void* data);
	void AddReadWriteFloat(std::string menuName, std::string varName, void* data, float min, float max, float step);
	void AddReadWriteColorFloat3( std::string menuName, std::string varName, void* data );
	void AddReadWriteArrow(std::string menuName, std::string varName, void* data);
	void AddReadWriteBool(std::string menuName, std::string varName, void* data);
	void AddReadWriteUInt(std::string menuName, std::string varName, void* data);

	void Draw();
private:
	void Initialize(ID3D11Device* device, int width, int height );

	void Shutdown();

private:
	bool m_Enabled;

};


#endif