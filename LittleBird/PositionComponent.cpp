
#include "PositionComponent.h"

void PositionComponent::NetworkSerialize( RakNet::BitStream& bs )
{
	bs.WriteBits((unsigned char*)&Position, 128);
	bs.WriteBits((unsigned char*)&Orientation, 128);
}

void PositionComponent::NetworkDeserialize( RakNet::BitStream& bs )
{
	bs.ReadBits((unsigned char*)&Position, 128);
	bs.ReadBits((unsigned char*)&Orientation, 128);
}
