#ifndef _TIMER_H_
#define _TIMER_H_

class Timer
{
public:
	Timer();
	~Timer();

	// time since last call to deltatime;
	double DeltaTime(); // in seconds

	// delta time since last call to DeltaTime;
	double CheckDeltaTime() const;
	
	// time since clock was created
	double TotalTime()const; // in seconds

private:
	double m_SecondsPerCount;
	__int64 m_StartTime;
	__int64 m_PrevTime;
	double m_PreviousDelta;

};

#endif