
#ifndef _PLAYER_SYSTEM_H_
#define _PLAYER_SYSTEM_H_

#include "component_system.h"

class PlayerComponent;
class RigidBodyComponent;
class Camera;

class PlayerSystem: public ComponentSystem
{
public:
	PlayerSystem();
	void AddEntry(unsigned ID, PlayerComponent *player, PositionComponent* position);
	void Run( float delta, Camera* camera);

private:
	std::wstring Console_SetUseCamera( std::wstring args );


private:
	std::vector<PlayerComponent*> m_Players;
	std::vector<PositionComponent*> m_Positions;

	bool m_UsePlayerCam;
};

#endif