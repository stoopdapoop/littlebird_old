
#include "EntityFactory.h"
#include "Entity.h"
#include "ModelComponent.h"
#include "PositionComponent.h"
#include "RigidBodyComponent.h"
#include "component.h"
#include "services.h"

Entity* EntityFactory::CreatePropEntity(std::string modelName, std::string textureName, PositionComponent *pos)
{
	using namespace DirectX;

	ResourceManager* res = Services::GetResourceManager();

	Entity* ent = new Entity(ReplicationFlags::LocalOnly);
	ResourceHandle modelHandle = res->GetModelHandle(modelName);
	ResourceHandle textureHandle = res->GetTextureHandle(textureName);
	//ModelComponent* modelcomp = new ModelComponent(modelHandle, textureHandle, );
	
	XMFLOAT3 pos3;
	XMStoreFloat3(&pos3, pos->Position);
	RigidBodyComponent* rigidComp = new RigidBodyComponent(res->GetCollisionShapeHandle("sks_col.pbm"), pos, 1.0f, LocalOnly);

	//ent->AddComponent(modelcomp);
	ent->AddComponent(pos);
	ent->AddComponent(rigidComp);
	
	return ent;
}
