#ifndef _POSITION_COMPONENT_H_
#define _POSITION_COMPONENT_H_

#include "component.h"
#include <DirectXMath.h>

class PositionComponent : public Component
{
public:
	PositionComponent(DirectX::XMVECTOR pos, DirectX::XMVECTOR orient, char replicationFlags) : Position(pos), Orientation(orient), Component(ComponentID::Position, replicationFlags) {}
	PositionComponent(char replicationFlags) : Component(ComponentID::Position, replicationFlags) {}

	void NetworkSerialize(RakNet::BitStream& bs);
	void NetworkDeserialize(RakNet::BitStream& bs);

	DirectX::XMVECTOR Position;
	DirectX::XMVECTOR Orientation;
};

#endif