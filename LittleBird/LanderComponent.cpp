

#include "LanderComponent.h"

LanderComponent::LanderComponent( float liftStrength, float tiltSpeed, float fuelAmount, bool playerControlled, char replicationFlags ) : Component(ComponentID::Lander, replicationFlags), LiftStrength(liftStrength),
	TiltSpeed(tiltSpeed), FuelAmount(fuelAmount), PlayerControlled(playerControlled)
{

}
