
#ifndef _PLAYER_COMPONENT_H_
#define _PLAYER_COMPONENT_H_

#include "component.h"
#include <DirectXMath.h>

class btKinematicCharacterController;

class PlayerComponent : public Component
{
public:
	PlayerComponent(float moveSpeed, float accel, float viewHeight, DirectX::XMVECTOR startPos, char replicationFlags);

	void NetworkSerialize(RakNet::BitStream& bs);
	void NetworkDeserialize(RakNet::BitStream& bs);

	float MaxMoveSpeed;
	float CurrentVelocity[3];
	float Accelleration;
	float ViewHeight;

	btKinematicCharacterController* kinematicController;
};

#endif