#ifndef _FPSCOUNTER_H_
#define _FPSCOUNTER_H_


#pragma comment(lib, "winmm.lib")


#include <windows.h>


class FPSCounter
{
public:
	FPSCounter();
	FPSCounter(const FPSCounter&);
	~FPSCounter();
	void Initialize();

	void Frame();
	int GetFps() const;

private:
	
	void CreateDebugMenu();

private:
	int m_fps, m_count;
	unsigned long m_startTime;
};

#endif