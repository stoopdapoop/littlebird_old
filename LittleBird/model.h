#ifndef _MODEL_H_
#define _MODEL_H_

#include "VertexFormats.h"
#include <vector>
#include <fstream>


struct ID3D11Device;
struct ID3D11DeviceContext;
struct ID3D11Buffer;
struct ID3D11ShaderResourceView;


class Model
{

	friend class ResourceManager;	

public:
	Model(const Model&);
	~Model();

	void Shutdown();
	void Render(ID3D11DeviceContext*)const;

	int GetIndexCount()const { return m_indexCount; }
	int GetVertexCount()const { return m_vertexCount; }
	const std::vector<VertexType>& GetVerts() { return m_Verts; }

	ID3D11Buffer* GetVertexBuffer() const { return m_vertexBuffer; }
	ID3D11Buffer* GetIndexBuffer() const { return m_indexBuffer; }

private:
	Model(ID3D11Device* device, std::string modelFilename);
	void Initialize(ID3D11Device*, std::string modelFilename);

	void InitializeBuffers(ID3D11Device*, std::vector<VertexType>&);
	void ShutdownBuffers();
	void RenderBuffers(ID3D11DeviceContext*)const;

	void LoadModelOBJ(std::string, std::vector<VertexType>&);
	void LoadModelPBM(std::string, std::vector<VertexType>&, std::vector<uint16_t>&);

	void ReleaseModel();

public:
	float m_BoundingBoxCenter[3];
	float m_BoundingBoxHalfExtents[3];
	
private:
	int m_vertexCount, m_indexCount;
	ID3D11Buffer *m_vertexBuffer, *m_indexBuffer;
	std::vector<VertexType> m_Verts;
	std::vector<uint16_t> m_Indicies;
	
};

class CollisionModel
{
	friend class ResourceManager;	
public:
	~CollisionModel();
	uint32_t GetVertexCount() { return m_VertexCount; }
	const CollisionVertexType* GetVertexArray() { return m_Verticies; }
private:
	CollisionModel(std::string modelName);	
private:
	CollisionVertexType* m_Verticies;
	uint32_t m_VertexCount;
};

#endif