#include <DirectXCollision.h>
#include "game.h"
#include "renderer.h"
#include "timer.h"
#include "input.h"
#include "texture.h"
#include "debug_ui.h"
#include "services.h"
#include "profiler.h"
#include "memory_utilities.h"
#include "math_utilities.h"
#include "camera.h"
#include "terrain.h"

#include "entity.h"
#include "developer_console.h"
#include "terrain_manager.h"
#include "EntityFactory.h"
#include <random>
#include "position_component.h"
#include "RigidBodyComponent.h"
#include "HelicopterComponent.h"

#include "ModelRenderSystem.h"
#include "HelicopterSystem.h"

#include "string_utilities.h"

#include "DebugDrawer.h"
#include "physics.h"
using namespace std;

using namespace DirectX;

Game::Game(int screenheight, int screenwidth, HWND hwnd, HINSTANCE hinstance, bool fullScreen, bool vSync) : m_UsingController(false), m_ViewSensitivity(1.0f)
{	
	CreateSubsystems(screenheight, screenwidth, hwnd, hinstance, fullScreen, vSync);
	InitializeSubsystems();
	CreateDebugObjects(screenwidth, screenheight);

	INIReader* iniReader = Services::GetINIReader();
	iniReader->SetSection("Controls");
	m_UsingController = iniReader->GetValue("Use_Controller", false);
	m_ViewSensitivity = iniReader->GetValue("View_Sensitivity", 4.0f);
}

Game::~Game()
{
	SafeDelete(m_TerrainManager);
	SafeDelete(m_ResourceManager);
	SafeDelete(m_Profiler);
	SafeDelete(m_DebugUI);
	SafeDelete(m_DeveloperConsole);
	SafeDelete(m_Camera);
	SafeDelete(m_DebugDrawer);

	for(auto it = m_Entities.begin(); it != m_Entities.end(); ++it)
	{
		delete (*it);
	}

}

void Game::CreateSubsystems( int screenheight, int screenwidth, HWND hwnd, HINSTANCE hinstance, bool fullScreen, bool vSync )
{
	m_Physics = new Physics(false);
	Services::ProvidePhysics(m_Physics);
	m_Renderer.reset(new Renderer(screenheight, screenwidth, hwnd, fullScreen, vSync));
	m_DeveloperConsole = new DeveloperConsole(m_Renderer.get(), screenwidth, screenheight);
	Services::ProvideDeveloperConsole(m_DeveloperConsole);
	m_DebugUI = new DebugUI(m_Renderer->GetDevice(), screenwidth, screenheight);
	Services::ProvideDebugUI(m_DebugUI);
	m_Input.reset(new Input(hinstance, hwnd, screenwidth, screenheight));
	m_Profiler = new Profiler(m_Renderer->GetDevice(), m_Renderer->GetImmediateContext());
	m_GameTimer.reset(new Timer);
	m_ResourceManager = new ResourceManager(m_Renderer.get(), m_Physics);
	Services::ProvideResourceManager(m_ResourceManager);
	m_TerrainManager = new TerrainManager("testisland", 9, 15, m_Renderer->GetDevice());
	
	m_ModelRenderSystem = new ModelRenderSystem(m_Renderer.get());
	m_TardMovementSystem = new TardMovementSystem();
	m_HelicopterSystem = new HelicopterSystem(m_Input.get());

	m_DebugDrawer = new DebugDrawer(m_Renderer.get());
	Services::ProvideDebugDrawer(m_DebugDrawer);
	m_Physics->SetDebugDrawer(m_DebugDrawer);
}

// called after create
void Game::InitializeSubsystems()
{
	m_Renderer->Initialize();
	m_Input->Frame();
}

const int texcount = 6;

void Game::CreateDebugObjects( int screenwidth, int screenheight )
{
	float aspectRatio = (float)screenwidth / screenheight;

	INIReader* ini = Services::GetINIReader();

	ini->SetSection("Camera");
	float fov = ini->GetValue("Camera_Vertical_Fov", XMConvertToRadians(40.0f));
	fov = XMConvertToRadians(fov);

	m_Camera = new FirstPersonCamera(aspectRatio, fov, 0.1f, 10000.0f);
	//m_Camera = new PerspectiveCamera(aspectRatio, fov, 0.1f, 10000.0f);

	XMVECTOR startPos = XMVectorSet(120.0f, 15.0f, 120.5f, 0);
	m_Camera->SetPosition(startPos);
	//XMVECTOR up = XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);
	
	//m_Camera->SetLookAt(startPos, XMVectorSet(0.0f,0.0f,0.0f,0.0f), up);

	//for(int i = 0; i < 200; ++i)
	//{
	//	PositionComponent* piss = new PositionComponent(XMVectorSet(rand() % 2000, rand() % 2000, rand() % 2000, 0.0f), XMQuaternionRotationRollPitchYaw(rand(),rand(),rand()));
	//	Entity* tempEnt = EntityFactory::CreatePropEntity("sks.pbm", "dirt.jpg", piss);
	//	m_Entities.push_back(tempEnt);
	//}

	//PositionComponent* piss = new PositionComponent(XMVectorSet(rand() % 2000 + 5000, rand() % 20 + 300, rand() % 2000 + 5000, 0.0f), XMQuaternionRotationRollPitchYaw(0, 0, 0));
	//Entity* tempEnt = EntityFactory::CreatePropEntity("sponza.pbm", "grass_bc7.dds", piss);
	//tempEnt->AddComponent(new HelicopterComponent(25000.f, 10000.0f, true));
	//m_Entities.push_back(tempEnt);

	//m_TerrainManager->Update(m_Camera->GetPosition());
}

//todo: remove this
extern XMFLOAT4 lightPosition;
extern XMFLOAT4 skycolor;
extern XMFLOAT4 groundcolor;
extern XMFLOAT4 horizoncolor;
extern XMFLOAT4 fogColor;
extern XMFLOAT4 sunFogColor;
extern XMFLOAT4 coefecients;

bool Game::Frame()
{
	Timer t;

	float delta = static_cast<float>(m_GameTimer->DeltaTime());

	if(m_Input->KeyPressed(DIK_TAB))
		m_Input->ToggleCursor();

	if(m_Input->KeyPressed(DIK_ESCAPE))
		return false;

	if(m_UsingController)
		HandleControllerInput(delta);

	HandlePCInput(delta);

	DispatchEntities();

	m_TardMovementSystem->Run(delta, m_Camera->GetPosition());

	m_HelicopterSystem->Run(m_Camera);

	m_DebugDrawer->SetupCamera(m_Camera, m_Renderer.get());
	m_Physics->Update(delta);

	m_TerrainManager->Update(m_Camera->GetPosition());
		
	m_Input->Frame();
	
	double frametime = t.TotalTime();
	Services::GetDebugDrawer()->drawText(30,30, to_string(frametime));

	Render();

	return true;
}

void Game::Render()
{
	m_Renderer->BeginFrame(0.45f, 0.8f, 1.0f, 1.0f);
	m_Renderer->BeginSingleLightShader();
	
	m_Renderer->UpdateLightBuffer();	
	m_Renderer->UpdateAmbientBuffer(skycolor, groundcolor, horizoncolor, fogColor, sunFogColor, coefecients);

	//m_ModelRenderSystem->Run(m_Camera);

	RenderTerrain();

	m_DebugDrawer->Present(m_Renderer.get());

	m_Profiler->StartProfile("UI");
	m_DebugUI->Draw();
	m_Profiler->EndProfile("UI");

	m_DeveloperConsole->Display();

	m_Profiler->EndFrame();
	m_Renderer->EndFrame();
}

void Game::HandleControllerInput( float delta )
{
	ControllerState& conState = m_Input->GetControllerState();

	if(conState.ButtonDown(LEFT_STICK))
	{
		conState.LeftVibration = conState.RightTrigger;
		conState.RightVibration = conState.LeftTrigger;
	}

	const float cameraSpeed = 200.0f;
	if(conState.ButtonDown(LEFT_BUMPER))
	{
		XMVECTOR lightDir = XMLoadFloat4(&lightPosition);

		float leftright = conState.RightStick[0] * conState.RightXSensitivity * delta;
		float updown = conState.RightStick[1] * conState.RightYSensitivity * delta;

		XMVECTOR rot = XMQuaternionRotationRollPitchYaw(updown, leftright, 0.0f);
		lightDir = XMVector3Rotate(lightDir, rot);

		XMStoreFloat4(&lightPosition, lightDir);
	}
	else
	{
		//static float pitch = 0.0f;
		//static float yaw = 0.0f;
		//yaw += conState.RightStick[0] * conState.RightXSensitivity * delta;
		//pitch += conState.RightStick[1] * conState.RightYSensitivity * delta;
		//m_Camera->SetOrientation(XMQuaternionRotationRollPitchYaw(pitch, yaw, 0.0f));
		//m_Camera->SetXRotation(m_Camera->GetXRotation() + conState.RightStick[0] * conState.RightXSensitivity * delta);
		//m_Camera->SetYRotation(m_Camera->GetYRotation() + conState.RightStick[1] * conState.RightYSensitivity * delta);
	}


	float rightDelta = conState.LeftStick[0] * cameraSpeed * delta;
	float forwardDelta = conState.LeftStick[1] * cameraSpeed * delta;
	float upDelta = (conState.RightTrigger - conState.LeftTrigger) * cameraSpeed * delta;
	XMVECTOR rightPos = XMVectorScale(m_Camera->GetRightVector(), rightDelta);
	XMVECTOR forwardPos = XMVectorScale(m_Camera->GetForwardVector(), forwardDelta);
	XMVECTOR upPos = XMVectorScale(m_Camera->GetUpVector(), upDelta);
	XMVECTOR& newpos = rightPos;
	newpos = XMVectorAdd(forwardPos, rightPos);
	newpos = XMVectorAdd(newpos, upPos);
	newpos = XMVectorAdd(m_Camera->GetPosition(), newpos);
	m_Camera->SetPosition(newpos);

	if(conState.ButtonPressed(X))
		m_Renderer->ToggleWireframe();		

	if(conState.ButtonPressed(RIGHT_BUMPER))
		m_DebugUI->SetEnabled(!m_DebugUI->GetEnabled());
}

void Game::HandlePCInput( float delta )
{
	
	if(m_Input->KeyPressed(DIK_GRAVE))
		m_DeveloperConsole->ToggleEnabled();
	
	if(m_DeveloperConsole->IsEnabled())
	{
		wstring bufferedInput = to_wstring(m_Input->GetBufferedChars());

		if(bufferedInput.length() > 0)
			m_DeveloperConsole->AddToInputBuffer(bufferedInput);

		if(m_Input->KeyPressed(DIK_UPARROW))
			m_DeveloperConsole->ShowPreviousCommand();
	}
	else
	{
		HandlePCGameInput(delta);
	}
}

void Game::HandlePCGameInput( float delta )
{
	int x, y, z;
	static float mupdown, mrightleft = 0.0f;
	m_Input->GetMouseDelta(x, y, z);
	if(m_Input->KeyDown(DIK_LCONTROL))
	{
		XMVECTOR lightDir = XMLoadFloat4(&lightPosition);

		float leftright = XMConvertToRadians(x/50.f) * m_ViewSensitivity;
		float updown =  XMConvertToRadians(y/50.f) * m_ViewSensitivity;

		XMVECTOR rot = XMQuaternionRotationRollPitchYaw(updown, leftright, 0.0f);
		lightDir = XMVector3Rotate(lightDir, rot);

		XMStoreFloat4(&lightPosition, lightDir);
	}
	else if(!m_Input->IsCursorEnabled())
	{
		mrightleft += XMConvertToRadians(x/50.f) * m_ViewSensitivity;
		mupdown += XMConvertToRadians(y/50.f) * m_ViewSensitivity;
		m_Camera->SetOrientation(XMQuaternionRotationRollPitchYaw(mupdown, mrightleft, 0.0f));
	}

	static float cameraSpeed = 30.0f;

	cameraSpeed = max<float>(0.0f, cameraSpeed += z/50.0f);
	float rightDelta = 0.0f;
	float forwardDelta = 0.0f;
	float upDelta = 0.0f;
	if(m_Input->KeyDown(DIK_D))
		rightDelta += 1.0f;
	if(m_Input->KeyDown(DIK_A))
		rightDelta -= 1.0f;
	if(m_Input->KeyDown(DIK_W))
		forwardDelta += 1.0f;
	if(m_Input->KeyDown(DIK_S))
		forwardDelta -= 1.0f;
	if(m_Input->KeyDown(DIK_E))
		upDelta += 1.0f;
	if(m_Input->KeyDown(DIK_Q))
		upDelta -= 1.0f;
	if(m_Input->KeyPressed(DIK_T))
	{
		//todo: remove
		static bool debugdraw = false;
		debugdraw = !debugdraw;
		if(debugdraw)
			m_DebugDrawer->setDebugMode(DebugDrawModes::DBG_DrawWireframe);
		else
			m_DebugDrawer->setDebugMode(DebugDrawModes::DBG_NoDebug);
	}


	rightDelta = rightDelta * cameraSpeed * delta;
	forwardDelta = forwardDelta * cameraSpeed * delta;
	upDelta = upDelta * cameraSpeed * delta;
	XMVECTOR rightPos = XMVectorScale(m_Camera->GetRightVector(), rightDelta);
	XMVECTOR forwardPos = XMVectorScale(m_Camera->GetForwardVector(), forwardDelta);
	XMVECTOR upPos = XMVectorScale(m_Camera->GetUpVector(), upDelta);
	XMVECTOR& newpos = rightPos;
	newpos = XMVectorAdd(forwardPos, rightPos);
	newpos = XMVectorAdd(newpos, upPos);
	newpos = XMVectorAdd(m_Camera->GetPosition(), newpos);
	m_Camera->SetPosition(newpos);

	if(m_Input->KeyPressed(DIK_I))
		m_Renderer->ToggleWireframe();

	if(m_Input->KeyPressed(DIK_F))
		m_DebugUI->SetEnabled(!m_DebugUI->GetEnabled());
}

void Game::DispatchEntities()
{
	size_t entityCount = m_Entities.size();
	for (size_t i = 0; i < entityCount; ++i)
	{
		Entity* ent = m_Entities[i];
		unsigned entID = ent->GetID();
		unsigned currentMask = ent->GetComponentMasks();
		unsigned testingFor = (unsigned)ComponentID::Position + (unsigned)ComponentID::Model;
		if((currentMask & testingFor) == testingFor)
		{
			PositionComponent* entPos = (PositionComponent*)ent->GetComponent(ComponentID::Position);
			ModelComponent* entModel = (ModelComponent*)ent->GetComponent(ComponentID::Model);
			m_ModelRenderSystem->AddEntry(entID, entPos, entModel );
		}

		testingFor = (unsigned)ComponentID::TardMovement + (unsigned)ComponentID::Position;
		if((currentMask & testingFor) == testingFor)
		{
			PositionComponent* entPos = (PositionComponent*)ent->GetComponent(ComponentID::Position);
			m_TardMovementSystem->AddEntry(entID, entPos);
		}

		testingFor = (unsigned)ComponentID::RigidBody + (unsigned)ComponentID::Helicopter;
		if((currentMask & testingFor) == testingFor)
		{
			// HACKHACK!
			RigidBodyComponent* entRigid = (RigidBodyComponent*)ent->GetComponent(ComponentID::RigidBody);
			HelicopterComponent* entHeli = (HelicopterComponent*)ent->GetComponent(ComponentID::Helicopter);
			m_HelicopterSystem->AddEntry(entID, entHeli, entRigid);
		}
	}
}

void Game::RenderTerrain()
{
	XMMATRIX modelMatrix = XMMatrixIdentity();
	m_Renderer->UpdateCameraBuffer(m_Camera, &modelMatrix);
	m_Renderer->BeginTerrainShader();
	
	ID3D11ShaderResourceView* terrtex[texcount];
	ResourceHandle temp = m_ResourceManager->GetTextureHandle("grass_bc7.dds");
	terrtex[0] = m_ResourceManager->GetTexture(temp)->GetResourceView();
	temp = m_ResourceManager->GetTextureHandle("grass_bc7.dds");
	terrtex[1] = m_ResourceManager->GetTexture(temp)->GetResourceView();
	temp = m_ResourceManager->GetTextureHandle("grass_bc7.dds");
	terrtex[2] = m_ResourceManager->GetTexture(temp)->GetResourceView();
	temp = m_ResourceManager->GetTextureHandle("pondnormal.png");
	terrtex[3] = m_ResourceManager->GetTexture(temp)->GetResourceView();
	temp = m_ResourceManager->GetTextureHandle("grass_bc7.dds");
	terrtex[5] = m_ResourceManager->GetTexture(temp)->GetResourceView();

	temp = m_ResourceManager->GetTextureHandle("testcube_bc7.dds");
	ID3D11ShaderResourceView* cube = m_ResourceManager->GetTexture(temp)->GetResourceView();
	m_Renderer->GetImmediateContext()->PSSetShaderResources(7,1, &cube);

	int x,z;
	m_TerrainManager->CalculateTilePosition(m_Camera->GetPosition(), x, z);
	int startOffset = -m_TerrainManager->GetTerrainStride() / 2;
	int endOffset = m_TerrainManager->GetTerrainStride() / 2;

	BoundingFrustum bf;
	BoundingFrustum::CreateFromMatrix(bf, m_Camera->GetProjectionMatrix());
	bf.Transform(bf, 1.0f, m_Camera->Orientation(), m_Camera->GetPosition());
	int tileCount = 0;

	for(int terrainWidth = startOffset; terrainWidth <= endOffset; ++terrainWidth)
	{
		for(int terrainLength = startOffset; terrainLength <= endOffset; ++terrainLength)
		{
			int currentx = x+terrainWidth;
			int currentz = z+terrainLength;
			Terrain* currentTerrain = m_TerrainManager->GetTerrain(currentx, currentz);
			if(currentTerrain)
			{
				float terX, terY, terZ;
				currentTerrain->GetTerrainPosition(terX, terY, terZ);

				float HalfWidth = (currentTerrain->GetTerrainWidth() * currentTerrain->GetTileScale()) * 0.5f;
				XMFLOAT3 tPos(terX + HalfWidth, ((currentTerrain->GetMaxHeight() - currentTerrain->GetMinHeight()) * 0.5f) + currentTerrain->GetMinHeight(), terZ + HalfWidth);

				float halfHeight = (currentTerrain->GetMaxHeight() - currentTerrain->GetMinHeight()) * 0.5f;
				XMFLOAT3 halfExtents(HalfWidth, halfHeight, HalfWidth);
				BoundingBox bb(tPos, halfExtents);

				if(bf.Contains(bb))
				{
					++tileCount;
					modelMatrix = XMMatrixTranslation(terX, terY, terZ);
					m_Renderer->UpdateCameraBuffer(m_Camera, &modelMatrix);
					temp = currentTerrain->GetSplatTextureHandle();
					terrtex[4] = m_ResourceManager->GetTexture(temp)->GetResourceView();
					ID3D11ShaderResourceView* tempHeights = currentTerrain->GetHeightMapTexture()->GetResourceView();

					m_Renderer->GetImmediateContext()->PSSetShaderResources(1,texcount, terrtex);
					m_Renderer->GetImmediateContext()->VSSetShaderResources(0, 1, &tempHeights);
					m_Renderer->GetImmediateContext()->VSSetShaderResources(1, texcount, terrtex);
					

					currentTerrain->Render(m_Renderer->GetImmediateContext());

					m_Renderer->GetImmediateContext()->DrawIndexed(currentTerrain->GetIndexCount(), 0, 0);
				}
			}
		}
	}
	m_DebugDrawer->drawText(300, 300, to_string(tileCount));
}
