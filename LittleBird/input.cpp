
#include "Input.h"
#include "MathUtilities.h"
#include "services.h"

using namespace std;

/////////////
// LINKING //
/////////////
#pragma comment(lib, "dinput8.lib")
#pragma comment(lib, "dxguid.lib")
#pragma comment(lib, "XINPUT9_1_0.lib")

Input::Input( HINSTANCE hinstance, HWND hwnd, int screenWidth, int screenHeight ): m_DirectInput(nullptr), m_Keyboard(nullptr),m_Mouse(nullptr),m_CursorEnabled(true)
{
	ZeroMemory(reinterpret_cast<void*>(&m_ControlerState), sizeof(ControllerState));
	m_ControlerState.ValidRange = 0.95f;
	m_ControlerState.DeadZone = 0.3f;
	m_ControlerState.StickLeftXSensitivity = 5.0f;
	m_ControlerState.StickLeftYSensitivity = 5.0f;
	m_ControlerState.StickRightXSensitivity = 4.0f;
	m_ControlerState.StickRightYSensitivity = 4.0f;
	m_ControlerState.VibrationLeft = 0.0f;
	m_ControlerState.VibrationRight = 0.0f;

	Initialize(hinstance, hwnd, screenWidth, screenHeight);
}

Input::~Input()
{
	Shutdown();
}

void Input::Initialize(HINSTANCE hinstance, HWND hwnd, int screenWidth, int screenHeight)
{
	HRESULT result;


	// Store the screen size which will be used for positioning the mouse cursor.
	m_ScreenWidth = screenWidth;
	m_ScreenHeight = screenHeight;

	// Initialize the location of the mouse on the screen.
	m_MouseX = 0;
	m_MouseY = 0;
	
	// Initialize the main direct input interface.
	result = DirectInput8Create(hinstance, DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)&m_DirectInput, NULL);
	if(FAILED(result))
		throw L"Could not create Input Interface";
	
	// Initialize the direct input interface for the keyboard.
	result = m_DirectInput->CreateDevice(GUID_SysKeyboard, &m_Keyboard, NULL);
	if(FAILED(result))
		throw L"Could not create Keyboard Interface";

	// Set the data format.  In this case since it is a keyboard we can use the predefined data format.
	result = m_Keyboard->SetDataFormat(&c_dfDIKeyboard);
	if(FAILED(result))
		throw L"Could not set keyboard dataformat";

	// Set Keyboard buffer size.
	DIPROPDWORD  dipdw;  
	dipdw.diph.dwSize = sizeof(DIPROPDWORD); 
	dipdw.diph.dwHeaderSize = sizeof(DIPROPHEADER); 
	dipdw.diph.dwObj = 0; 
	dipdw.diph.dwHow = DIPH_DEVICE; 
	dipdw.dwData = KB_BUFFER_SIZE; 
	result = m_Keyboard->SetProperty(DIPROP_BUFFERSIZE, &dipdw.diph); 
	if(FAILED(result))
		throw L"Couldn't set up keyboard buffer";

	m_Hwnd = hwnd;
	// Set the cooperative level of the keyboard to not share with other programs.
	result = m_Keyboard->SetCooperativeLevel(m_Hwnd, DISCL_FOREGROUND | DISCL_NONEXCLUSIVE);
	if(FAILED(result))
		throw L"Failed to set keyboard cooperation";
	
	// Now acquire the keyboard.
	result = m_Keyboard->Acquire();
	//if(FAILED(result))
		//throw L"Failed to acquire keyboard";
	
	// Initialize the direct input interface for the mouse.
	result = m_DirectInput->CreateDevice(GUID_SysMouse, &m_Mouse, NULL);
	if(FAILED(result))
		throw L"Could not create mouse interface.";

	// Set the data format for the mouse using the pre-defined mouse data format.
	result = m_Mouse->SetDataFormat(&c_dfDIMouse);
	if(FAILED(result))
		throw L"could not set mouse Data Format";

	// Set mouse buffer size.
	DIPROPDWORD  dipdwm;  
	dipdwm.diph.dwSize = sizeof(DIPROPDWORD); 
	dipdwm.diph.dwHeaderSize = sizeof(DIPROPHEADER); 
	dipdwm.diph.dwObj = 0; 
	dipdwm.diph.dwHow = DIPH_DEVICE; 
	dipdwm.dwData = KB_BUFFER_SIZE; 
	result = m_Mouse->SetProperty(DIPROP_BUFFERSIZE, &dipdwm.diph); 
	if(FAILED(result))
		throw L"could not set mouse buffer size";

	// Set the cooperative level of the mouse to share with other programs.
	if(m_CursorEnabled)
		result = m_Mouse->SetCooperativeLevel(m_Hwnd, DISCL_FOREGROUND |  DISCL_NONEXCLUSIVE);
	else
		result = m_Mouse->SetCooperativeLevel(m_Hwnd, DISCL_FOREGROUND |  DISCL_EXCLUSIVE);
	if(FAILED(result))
		throw L"could not set mouse cooperation level";

	
	// Acquire the mouse.
	result = m_Mouse->Acquire();
	if(FAILED(result))
	{
		//throw L"could not acquire mouse";
	}

	Frame();

	CreateControllerMenu();
}

void Input::Frame()
{
	bool result;
	
	// Read the current state of the keyboard.
	result = ReadKeyboard();
	if(!result)
	{
		// maybe throwing an exception if we lose the keyboard for a second is a tad extreme
		//throw L"Could not Read Keyboard";
	}

	// Read the current state of the mouse.
	result = ReadMouse();
	if(!result)
	{
		// TODO: do something productive when can't get mouse.
		// return false;
	}

	ReadController();

	ProcessInput();
}

void Input::Shutdown()
{
	// Release the mouse.
	if(m_Mouse)
	{
		m_Mouse->Unacquire();
		m_Mouse->Release();
		m_Mouse = nullptr;
	}

	// Release the keyboard.
	if(m_Keyboard)
	{
		m_Keyboard->Unacquire();
		m_Keyboard->Release();
		m_Keyboard = nullptr;
	}

	// Release the main interface to direct input.
	if(m_DirectInput)
	{
		m_DirectInput->Release();
		m_DirectInput = nullptr;
	}

	//XInputEnable(false);
}

bool Input::ReadKeyboard()
{
	HRESULT result;


	// Read the keyboard device.
	result = m_Keyboard->GetDeviceState(sizeof(m_KeyboardState), (LPVOID)&m_KeyboardState);
	if(FAILED(result))
	{
		// If the keyboard lost focus or was not acquired then try to get control back.
		if((result == DIERR_INPUTLOST) || (result == DIERR_NOTACQUIRED))
		{
			m_Keyboard->Acquire();
		}
		else
		{
			return false;
		}
	}

	// Clear old buffers
	ZeroMemory(&m_KeyboardPressed,256);
	ZeroMemory(&m_KeyboardReleased,256);

	m_KeyboardBufferedEventCount = KB_BUFFER_SIZE;
	result = m_Keyboard->GetDeviceData(sizeof(DIDEVICEOBJECTDATA),m_KeyboardBuffer,&m_KeyboardBufferedEventCount,0);
	if(FAILED(result))
	{
		// The buffer will be garbage, don't try to read it.
		m_KeyboardBufferedEventCount = 0;
		// Don't crash if lose focus.
		if(result != DIERR_NOTACQUIRED)
			return false;
	}
	return true;
}

bool Input::ReadMouse()
{
	HRESULT result;


	// Read the mouse device.
	result = m_Mouse->GetDeviceState(sizeof(DIMOUSESTATE), (LPVOID)&m_MouseState);
	if(FAILED(result))
	{
		// If the mouse lost focus or was not acquired then try to get control back.
		if((result == DIERR_INPUTLOST) || (result == DIERR_NOTACQUIRED))
		{
			m_Mouse->Acquire();
		}
		else
		{
			return false;
		}
	}

	ZeroMemory(&m_MousePressed,4);
	ZeroMemory(&m_MouseReleased,4);

	m_MouseBufferedEventCount = KB_BUFFER_SIZE;
	result = m_Mouse->GetDeviceData(sizeof(DIDEVICEOBJECTDATA),m_MouseBuffer,&m_MouseBufferedEventCount,0);
	if(FAILED(result))
	{
		// The buffer will be garbage, don't try to read it.
		m_KeyboardBufferedEventCount = 0;
		// Don't crash if lose focus.
		if(result != DIERR_NOTACQUIRED)
			return false;
	}

	return true;
}

void Input::ProcessInput()
{
	ProcessMouse();
	ProcessKeyboard();
	if(m_ControlerState.controllerConnected)
		ProcessController();
}

void Input::GetMouseLocation(int& mouseX, int& mouseY) const
{
	mouseX = m_MouseX;
	mouseY = m_MouseY;
}

bool Input::KeyDown(unsigned char key, bool consume)
{
	if(m_KeyboardState[key] & 0x80)
	{
		if(consume)
			m_KeyboardState[key] = 0;
		return true;
	}

	return false;
}

bool Input::KeyPressed(const unsigned char key, bool consume)
{
	if(m_KeyboardPressed[key] & 0x80)
	{
		if(consume)
			m_KeyboardPressed[key] = 0;
		return true;
	}
	return false;
}

bool Input::KeyReleased(unsigned char key, bool consume)
{
	if(m_KeyboardReleased[key] & 0x80)
	{
		if(consume)
			m_KeyboardReleased[key] = 0;
		return true;
	}
	return false;
}

void Input::GetMouseDelta(int& x, int& y, int& z) const
{
	x = m_MouseState.lX;
	y = m_MouseState.lY;
	z = m_MouseState.lZ;
}

void Input::GetMouseDelta(int& x, int& y) const
{
	x = m_MouseState.lX;
	y = m_MouseState.lY;
}

int Input::GetMouseWheelDelta() const
{
	return m_MouseState.lZ;
}

bool Input::MouseButtonDown(unsigned char button) const
{
	if(m_MouseState.rgbButtons[button] & 0x80)
		return true;

	return false;
}

bool Input::MouseButtonPressed( unsigned char button ) const
{
	if(m_MousePressed[button] & 0x80)
		return true;

	return false;
}

bool Input::MouseButtonReleased( unsigned char button ) const
{
	if(m_MouseReleased[button] & 0x80)
		return true;

	return false;
}

void Input::ToggleCursor()
{
	m_Mouse->Unacquire();
	m_CursorEnabled = !m_CursorEnabled;
	
	HRESULT result;
	// Set the cooperative level of the mouse to share with other programs.
	if(m_CursorEnabled)
		result = m_Mouse->SetCooperativeLevel(m_Hwnd, DISCL_FOREGROUND |DISCL_NONEXCLUSIVE);
	else
		result = m_Mouse->SetCooperativeLevel(m_Hwnd, DISCL_FOREGROUND |DISCL_EXCLUSIVE);

	m_Mouse->Acquire();
}
#include "winerror.h"

void Input::ReadController()
{
	DWORD dwResult;
	dwResult = XInputGetState(0, &m_Controller);

	if(dwResult == ERROR_DEVICE_NOT_CONNECTED)
	{
		if(m_ControlerState.controllerConnected == true)
		{
			m_ControlerState.controllerConnected = false;
			m_ControlerState.CurrentButtons = m_ControlerState.PreviousButtons = 0;
			m_ControlerState.StickRightCircular[0] = 0.0f;
			m_ControlerState.StickRightCircular[1] = 0.0f;
			m_ControlerState.StickLeftCircular[0] = 0.0f;
			m_ControlerState.StickLeftCircular[1] = 0.0f;
			m_ControlerState.TriggerRight = m_ControlerState.TrifferLeft = 0.0f;
			m_ControlerState.StickLeftMagnitude = m_ControlerState.StickRightMagnitude = 0.0f;
		}
	}
	else if(dwResult == ERROR_SUCCESS)
	{
		m_ControlerState.controllerConnected = true;
	}
	
}


void Input::NormalizeStick(float inX, float inY, float& outXCircular, float& outYCircular, float& outXRectangular, float& outYRectangular)
{
	float maxValue = 32767 * m_ControlerState.ValidRange;
	float controllerDeadZone = maxValue * m_ControlerState.DeadZone;
	float magnitude = sqrt(inX*inX + inY*inY);


	//determine the direction the controller is pushed
	float normalizedX = 0.0f;
	float normalizedY = 0.0f;
	// don't want no float exceptions... right?
	if(!magnitude == 0.0f)
	{
		normalizedX = inX / magnitude;
		normalizedY = inY / magnitude;
	}

	outXRectangular = max(min(abs(inX), maxValue) - controllerDeadZone, 0.0f) * sign(normalizedX) / (maxValue - controllerDeadZone);
	outYRectangular = max(min(abs(inY), maxValue) - controllerDeadZone, 0.0f) * sign(normalizedY) / (maxValue - controllerDeadZone);
	
	float normalizedMagnitude = 0;
	//check if the controller is outside a circular dead zone
	if (magnitude > controllerDeadZone)
	{
		//clip the magnitude at its expected maximum value
		if (magnitude > maxValue) 
			magnitude = maxValue;
		//adjust magnitude relative to the end of the dead zone
		magnitude -= controllerDeadZone;

		//optionally normalize the magnitude with respect to its expected range
		//giving a magnitude value of 0.0 to 1.0
		normalizedMagnitude = magnitude / (maxValue - controllerDeadZone);
	}
	else //if the controller is in the dead-zone zero out the magnitude
	{
		magnitude = 0.0;
		normalizedMagnitude = 0.0;
	}

	outXCircular = normalizedX * normalizedMagnitude;
	outYCircular = normalizedY * normalizedMagnitude;
}

void Input::ProcessMouse()
{
	// check buffered mouse stuffs
	for(unsigned int i = 0; i < m_MouseBufferedEventCount; ++i)
	{
		DIDEVICEOBJECTDATA current = m_MouseBuffer[i];
		if(current.dwOfs == DIMOFS_BUTTON0)
		{
			if(current.dwData & 0x80)
				m_MousePressed[0] = 0x80;
			else
				m_MouseReleased[0] = 0x80;
		}
		else if(current.dwOfs == DIMOFS_BUTTON1)
		{
			if(current.dwData & 0x80)
				m_MousePressed[1] = 0x80;
			else
				m_MouseReleased[1] = 0x80;
		}
	}

	// Update the location of the mouse cursor based on the change of the mouse location during the frame.
	m_MouseX += m_MouseState.lX;
	m_MouseY += m_MouseState.lY;

	// Ensure the mouse location doesn't exceed the screen width or height.
	if(m_MouseX < 0)  
		m_MouseX = 0;
	if(m_MouseY < 0)  
		m_MouseY = 0;

	if(m_MouseX > m_ScreenWidth)  
		m_MouseX = m_ScreenWidth; 
	if(m_MouseY > m_ScreenHeight)
		m_MouseY = m_ScreenHeight; 
}

void Input::ProcessKeyboard()
{
	m_BufferedChars.clear();

	// Check to see which keys were pressed and released this poll.
	for(unsigned int i = 0; i < m_KeyboardBufferedEventCount; ++i)
	{
		DIDEVICEOBJECTDATA current = m_KeyboardBuffer[i];
		if(current.dwData & 0x80)
		{
			m_KeyboardPressed[current.dwOfs] = 0x80;
			m_BufferedChars+=DIKToChar(current.dwOfs);
		}
		else
		{
			m_KeyboardReleased[current.dwOfs] = 0x80;
		}
	}
}

void Input::ProcessController()
{
	// normalize inputs to 0-1 range and validate/set vibration.
	XINPUT_STATE state = m_Controller;
	short X = state.Gamepad.sThumbLX;
	short Y = state.Gamepad.sThumbLY;
	NormalizeStick(X, Y, m_ControlerState.StickLeftCircular[0], m_ControlerState.StickLeftCircular[1], m_ControlerState.StickLeftRectangular[0], m_ControlerState.StickLeftRectangular[1]);
	
	X = state.Gamepad.sThumbRX;
	Y = state.Gamepad.sThumbRY;
	NormalizeStick(X, Y, m_ControlerState.StickRightCircular[0], m_ControlerState.StickRightCircular[1], m_ControlerState.StickRightRectangular[0], m_ControlerState.StickRightRectangular[1]);
	
	m_ControlerState.StickLeftMagnitude = sqrt((m_ControlerState.StickLeftCircular[0] * m_ControlerState.StickLeftCircular[0]) + (m_ControlerState.StickLeftCircular[1] * m_ControlerState.StickLeftCircular[1]));
	m_ControlerState.StickRightMagnitude = sqrt((m_ControlerState.StickRightCircular[0] * m_ControlerState.StickRightCircular[0]) + (m_ControlerState.StickRightCircular[1] * m_ControlerState.StickRightCircular[1]));

	m_ControlerState.TrifferLeft = state.Gamepad.bLeftTrigger/255.0f;
	m_ControlerState.TriggerRight = state.Gamepad.bRightTrigger/255.0f;
	m_ControlerState.PreviousButtons = m_ControlerState.CurrentButtons;
	m_ControlerState.CurrentButtons = state.Gamepad.wButtons;
	m_ControlerState.VibrationLeft = saturate(m_ControlerState.VibrationLeft);
	m_ControlerState.VibrationRight = saturate(m_ControlerState.VibrationRight);

	XINPUT_VIBRATION vib;
	vib.wLeftMotorSpeed = static_cast<WORD>(m_ControlerState.VibrationLeft * 65535);
	vib.wRightMotorSpeed = static_cast<WORD>(m_ControlerState.VibrationRight * 65535);
	XInputSetState(0, &vib);
}

void Input::CreateControllerMenu()
{
	string name = "ControllerInput";
	DebugUI* dGUI = Services::GetDebugUI();
	dGUI->CreateMenu(name);
	dGUI->AddReadOnlyVariable(name, "LXCir", DEBUG_UI_TYPE::FLOAT,  &m_ControlerState.StickLeftCircular[0]);
	dGUI->AddReadOnlyVariable(name, "LYCir", DEBUG_UI_TYPE::FLOAT,  &m_ControlerState.StickLeftCircular[1]);
	dGUI->AddReadOnlyVariable(name, "LXRec", DEBUG_UI_TYPE::FLOAT, &m_ControlerState.StickLeftRectangular[0]);
	dGUI->AddReadOnlyVariable(name, "LYRec", DEBUG_UI_TYPE::FLOAT, &m_ControlerState.StickLeftRectangular[1]);
	dGUI->AddReadOnlyVariable(name, "LStickMag", DEBUG_UI_TYPE::FLOAT,  &m_ControlerState.StickLeftMagnitude);
	dGUI->AddReadOnlyVariable(name, "RXCir", DEBUG_UI_TYPE::FLOAT,  &m_ControlerState.StickRightCircular[0]);
	dGUI->AddReadOnlyVariable(name, "RYCir", DEBUG_UI_TYPE::FLOAT,  &m_ControlerState.StickRightCircular[1]);
	dGUI->AddReadOnlyVariable(name, "RXRec", DEBUG_UI_TYPE::FLOAT, &m_ControlerState.StickRightRectangular[0]);
	dGUI->AddReadOnlyVariable(name, "RYRec", DEBUG_UI_TYPE::FLOAT, &m_ControlerState.StickRightRectangular[1]);
	dGUI->AddReadOnlyVariable(name, "RStickMag", DEBUG_UI_TYPE::FLOAT,  &m_ControlerState.StickRightMagnitude);

	dGUI->AddReadOnlyVariable(name, "LeftTrigger", DEBUG_UI_TYPE::FLOAT,  &m_ControlerState.TrifferLeft);
	dGUI->AddReadOnlyVariable(name, "RightTrigger", DEBUG_UI_TYPE::FLOAT,  &m_ControlerState.TriggerRight);

	dGUI->AddReadOnlyVariable(name, "Buttons", DEBUG_UI_TYPE::UINT16, &m_ControlerState.CurrentButtons);

	dGUI->AddReadWriteFloat(name, "DeadZone", &m_ControlerState.DeadZone, 0.0f, 1.0f, 0.01f);
	dGUI->AddReadWriteFloat(name, "ValidRange", &m_ControlerState.ValidRange, 0.0f, 1.0f, 0.01f);

	const float maxStickSensitivity = 10.0f;
	dGUI->AddReadWriteFloat(name, "LeftXSensitivity", &m_ControlerState.StickLeftXSensitivity, 0.0f, maxStickSensitivity, 0.01f);
	dGUI->AddReadWriteFloat(name, "LeftYSensitivity", &m_ControlerState.StickLeftYSensitivity, 0.0f, maxStickSensitivity, 0.01f);
	dGUI->AddReadWriteFloat(name, "RightXSensitivity", &m_ControlerState.StickRightXSensitivity, 0.0f, maxStickSensitivity, 0.01f);
	dGUI->AddReadWriteFloat(name, "RightYSensitivity", &m_ControlerState.StickRightYSensitivity, 0.0f, maxStickSensitivity, 0.01f);

	dGUI->SetMenuRefreshRate(name, 0.1f);
	dGUI->SetMenuSize(name, 180, 190);
}

char Input::DIKToChar(DWORD scanCode)
{
	//obtain keyboard information
	static HKL layout = GetKeyboardLayout(0);
	static UCHAR keyboardState[256];
	if (GetKeyboardState(keyboardState) == false)
		return 0;

	//translate keyboard press scan code identifier to a char
	UINT vk = MapVirtualKeyEx(scanCode, 1, layout);
	USHORT asciiValue;
	ToAscii(vk, scanCode, keyboardState, &asciiValue, 0);

	return static_cast<char>(asciiValue);
}