
#ifndef _NETWORKING_H_
#define _NETWORKING_H_

#pragma once
#ifdef _DEBUG
#pragma comment(lib, "RakNet_VS2008_LibStatic_Debug_x64.lib")
#else
#pragma comment(lib, "RakNet_VS2008_LibStatic_Release_x64.lib")
#endif

#include <string>
#include <functional>
#include "component.h"
#pragma warning(disable:4005)
#include "RakPeerInterface.h"
#include "NetworkIDObject.h"
#include "MessageIdentifiers.h"
#include "RakNetTypes.h"
#pragma warning(default:4005)
#include "ReplicationFlags.h"
#include "BitStream.h"

namespace RakNet {
	struct Packet;
	enum ConnectionAttemptResult;
	enum StartupResult;
	class RakPeerInterface;
	class NetworkIDManager;
};

class Entity;

enum PoopMessageIDs : unsigned char {
	POOP_CHATMESSAGE = ID_USER_PACKET_ENUM,
	POOP_REPLICATE_ENTITY,
	POOP_REPLICATE_COMPONENT,
};

enum PacketChannels : char {
	CHAT_CHANNEL = 0,
	ENTITY_CHANNEL = 1,
};

typedef std::function<void(uint64_t id, bool spawning, char replicationFlags )> ReplicateEntityFunction;
typedef std::function<void(ComponentID_t idType, Entity* ent, Component* com)> ReplicateComponentFunction;

class Networking
{
public:
	Networking();

	void HostGame();
	void Connect(std::wstring ip);
	void Update();
	
	// wabba wabba wabba
	RakNet::NetworkIDManager* GetNetworkIDManager() { if(m_NetworkIDManager) return m_NetworkIDManager; else throw std::string("Need to create NetworkIDManager");}
	bool IsHost() { return m_IsHost; }
	bool IsConnected();
	void SetReplicateEntityCallback(ReplicateEntityFunction func);
	void SetReplicateComponentCallback(ReplicateComponentFunction func);
	void SendReplicateEntity(RakNet::NetworkID id, char replicationFlags);
	
	template<class T>
	void SendReplicateComponent(RakNet::NetworkID entityID, T* comp);

	void HandleReplicateEntity(RakNet::Packet* p);
	void HandleReplicateComponent(RakNet::Packet* p);

private:
	std::wstring Console_Say(std::wstring args);

	unsigned char GetPacketIdentifier(RakNet::Packet *p);
	void DisplayConnectionAttemptResult(RakNet::ConnectionAttemptResult* res);
	void DisplayStartupAttemptResult(RakNet::StartupResult* res);
	void HandleNetworkLayer( unsigned char packetIdentifier, RakNet::Packet* p );
	void HandleApplicationLayer( unsigned char packetIdentifier, RakNet::Packet* p );

	void HandleChat(RakNet::Packet* p);
	void SendChat(std::wstring str);
	
private:
	bool m_IsHost;
	RakNet::SystemAddress m_HostAddress;
	RakNet::NetworkIDManager* m_NetworkIDManager;
	RakNet::RakPeerInterface* m_Peer;
	ReplicateEntityFunction m_ReplicateObjectCallback;
	ReplicateComponentFunction m_ReplicateComponentCallback;
};

template<class T>
void Networking::SendReplicateComponent(RakNet::NetworkID entityID, T* comp)
{
	RakNet::BitStream bs;
	bs.Write((RakNet::MessageID)POOP_REPLICATE_COMPONENT);
	bs.Write(entityID);
	bs.Write(comp->GetComponentID());
	bs.WriteBitsFromIntegerRange<unsigned char>(comp->GetReplicationFlags(), 0, (unsigned char)ReplicationFlagCount, false);
	bs.Write1();
	comp->NetworkSerialize(bs);
	// why
	if(m_IsHost)
		m_Peer->Send(&bs, MEDIUM_PRIORITY, RELIABLE, ENTITY_CHANNEL, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
	else
		m_Peer->Send(&bs, MEDIUM_PRIORITY, RELIABLE, ENTITY_CHANNEL, m_HostAddress, false);
}

#endif