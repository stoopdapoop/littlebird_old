#ifndef _TEXTURE_EXCEPTION_H_
#define _TEXTURE_EXCEPTION_H_

#include <exception>
#include <string>

class TextureException : public std::exception
{
public:
	TextureException(std::string texname) : m_TexName(texname) {}

	virtual const char* what() const throw()
	{
		return m_TexName.c_str();
	}

private:
	std::string m_TexName;
};

#endif