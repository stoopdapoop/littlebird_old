#ifndef _RIGID_BODY_SYSTEM_H_
#define _RIGID_BODY_SYSTEM_H_

#include "component_system.h"
class Physics;
class RigidBodyComponent;

class RigidBodySystem : public ComponentSystem
{
public:
	RigidBodySystem(Physics* phys);
	void AddEntry(int ID, RigidBodyComponent* rigid, PositionComponent* pos);
	void Run();
private:
	Physics* m_Physics;
	std::vector<RigidBodyComponent*> m_RigidBodies;
	std::vector<PositionComponent*> m_Positions;
};

#endif