#ifndef _STRING_UTILITIES_H_
#define _STRING_UTILITIES_H_

#include <algorithm>
#include <vector>

inline void RemoveSpaces(std::string& str)
{
	str.erase(remove_if(str.begin(), str.end(), isspace), str.end());
}

std::vector<std::string> inline StringSplit(const std::string &source, const char *delimiter = " ", bool keepEmpty = false)
{
	std::vector<std::string> results;

	size_t prev = 0;
	size_t next = 0;

	while ((next = source.find_first_of(delimiter, prev)) != std::string::npos)
	{
		if (keepEmpty || (next - prev != 0))
		{
			results.push_back(source.substr(prev, next - prev));
		}
		prev = next + 1;
	}

	if (prev < source.size())
	{
		results.push_back(source.substr(prev));
	}

	return results;
}

std::vector<std::wstring> inline StringSplit(const std::wstring &source, const wchar_t *delimiter = L" ", bool keepEmpty = false)
{
	std::vector<std::wstring> results;

	size_t prev = 0;
	size_t next = 0;

	while ((next = source.find_first_of(delimiter, prev)) != std::string::npos)
	{
		if (keepEmpty || (next - prev != 0))
		{
			results.push_back(source.substr(prev, next - prev));
		}
		prev = next + 1;
	}

	if (prev < source.size())
	{
		results.push_back(source.substr(prev));
	}

	return results;
}

std::wstring inline wstringToLower(std::wstring str)
{	
	std::transform(str.begin(), str.end(), str.begin(), ::tolower);
	return str;
}

std::wstring inline to_wstring(std::string str)
{
	std::wstring wstr(str.begin(), str.end());
	return wstr;
}
#endif