#include "timer.h"
#include <windows.h>

Timer::Timer() : m_SecondsPerCount(0), m_PrevTime(0), m_PreviousDelta(0.0)
{
	__int64 countsPerSec;
	QueryPerformanceFrequency((LARGE_INTEGER*)&countsPerSec);
	m_SecondsPerCount = 1.0 / (double)countsPerSec;

	QueryPerformanceCounter((LARGE_INTEGER*)&m_StartTime);
	m_PrevTime = m_StartTime;
}

double Timer::DeltaTime()
{
	__int64 current;

	QueryPerformanceCounter((LARGE_INTEGER*)&current);

	double delta = (current - m_PrevTime) * m_SecondsPerCount;
	
	// clock can supposedly move backwards when entering power saving mode
	if(delta < 0.0)
		delta = 0.0;

	m_PrevTime = current;
	m_PreviousDelta = delta;

	return delta;
}

double Timer::TotalTime()const
{
	__int64 current;
	QueryPerformanceCounter((LARGE_INTEGER*)&current);

	return (current - m_StartTime) * m_SecondsPerCount;
}


double Timer::CheckDeltaTime() const
{
	return m_PreviousDelta;
}

Timer::~Timer()
{

}