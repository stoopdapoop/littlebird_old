
#ifndef _HELICOPTER_COMPONENT_H_
#define _HELICOPTER_COMPONENT_H_

#include "component.h"

class LanderComponent : public Component
{
public:
	LanderComponent(float liftStrength, float tiltSpeed, float fuelAmount, bool playerControlled, char replicationFlags);
	
	float LiftStrength;
	float TiltSpeed;
	float Throttle = 0.0f;
	float FuelAmount;
	bool PlayerControlled;
};

#endif