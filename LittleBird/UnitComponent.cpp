
#include "services.h"
#include "BulletCollision/CollisionDispatch/btGhostObject.h"
#include "UnitComponent.h"
using namespace DirectX;

UnitComponent::UnitComponent(XMVECTOR startPos, float unitHeight, float moveSpeed) : UnitHeight(unitHeight), MoveSpeed(moveSpeed), Component(ComponentID::Unit, LocalOnly)
{
	Physics* phys = Services::GetPhysics();
	ResourceManager* res = Services::GetResourceManager();
	ResourceHandle hand = res->GetCollisionShapeHandle(CAPSULE, 1.0f, unitHeight);
	btConvexShape* shape = (btConvexShape*)res->GetCollisionShape(hand);
	if (shape == nullptr)
		throw string("player shape is not derived from btConvexShape");

	btTransform startTransform;
	startTransform.setIdentity();
	startTransform.setOrigin(*(btVector3*)&startPos);
	btPairCachingGhostObject* spooky = new btPairCachingGhostObject();
	spooky->setWorldTransform(startTransform);

	phys->GetWorld()->getBroadphase()->getOverlappingPairCache()->setInternalGhostPairCallback(new btGhostPairCallback());
	spooky->setCollisionShape(shape);
	spooky->setCollisionFlags(btCollisionObject::CF_CHARACTER_OBJECT);

	kinematicController = new btKinematicCharacterController(spooky, shape, unitHeight * 0.1f);

	phys->GetWorld()->addCollisionObject(spooky, btBroadphaseProxy::CharacterFilter, btBroadphaseProxy::StaticFilter | btBroadphaseProxy::DefaultFilter | btBroadphaseProxy::CharacterFilter);

	phys->GetWorld()->addAction(kinematicController);
}
