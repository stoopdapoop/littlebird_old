
#include "Networking.h"
#include "NetworkIDManager.h"
#include "StringCompressor.h"

#include <DirectXColors.h>

#include <string>
#include "StringUtilities.h"
#include "services.h"

#include "ComponentFactory.h"

#include "Entity.h"

using namespace std;
using namespace RakNet;

Networking::Networking() : m_Peer(nullptr), m_IsHost(false)
{
	m_Peer = RakNet::RakPeerInterface::GetInstance();

	m_NetworkIDManager = new NetworkIDManager;

	DECLARE_CONSOLECOMMAND("say", Networking::Console_Say);
}

unsigned char Networking::GetPacketIdentifier(RakNet::Packet *p)
{
	if (p==0)
		return 255;

	if ((unsigned char)p->data[0] == ID_TIMESTAMP)
	{
		RakAssert(p->length > sizeof(RakNet::MessageID) + sizeof(RakNet::Time));
		return (unsigned char) p->data[sizeof(RakNet::MessageID) + sizeof(RakNet::Time)];
	}
	else
		return (unsigned char) p->data[0];
}

void Networking::Update()
{
	if (!m_Peer || !m_Peer->IsActive())
		return;

	RakNet::Packet* p = nullptr;
	unsigned char packetIdentifier;
	for (p=m_Peer->Receive(); p; m_Peer->DeallocatePacket(p), p=m_Peer->Receive())
	{
		// We got a packet, get the identifier with our handy function
		packetIdentifier = GetPacketIdentifier(p);


		if(packetIdentifier < ID_USER_PACKET_ENUM)
			HandleNetworkLayer(packetIdentifier, p);

		else
			HandleApplicationLayer(packetIdentifier, p);
	}
}

void Networking::HandleNetworkLayer( unsigned char packetIdentifier, RakNet::Packet* p )
{
	DeveloperConsole* dev = Services::GetDeveloperConsole();

	// Check if this is a network message packet
	switch (packetIdentifier)
	{
	case ID_DISCONNECTION_NOTIFICATION:
		// Connection lost normally
		dev->WriteToConsole(L"Disconnected from server", DirectX::Colors::Red);
		throw string("disconnected from server");
		break;
	case ID_ALREADY_CONNECTED:
		dev->WriteToConsole(L"Already Connected to server", DirectX::Colors::Blue);
		break;
	case ID_INCOMPATIBLE_PROTOCOL_VERSION:
		dev->WriteToConsole(L"Incompatible protocol version", DirectX::Colors::Red);
		break;
	case ID_REMOTE_DISCONNECTION_NOTIFICATION: // Server telling the clients of another client disconnecting gracefully.  You can manually broadcast this in a peer to peer enviroment if you want.
		dev->WriteToConsole(L"Another player has disconnected");
		break;
	case ID_REMOTE_CONNECTION_LOST: // Server telling the clients of another client disconnecting forcefully.  You can manually broadcast this in a peer to peer enviroment if you want.
		dev->WriteToConsole(L"Another player timed out");
		break;
	case ID_REMOTE_NEW_INCOMING_CONNECTION: // Server telling the clients of another client connecting.  You can manually broadcast this in a peer to peer enviroment if you want.
		dev->WriteToConsole(L"New player is connecting");
		break;
	case ID_NEW_INCOMING_CONNECTION: ///// RakPeer - The system we attempted to connect to is not accepting new connections.
		dev->WriteToConsole(L"New player is connecting to your server");
		break;
	case ID_CONNECTION_BANNED: // Banned from this server
		dev->WriteToConsole(L"We are banned from this server.", DirectX::Colors::Red);
		break;			
	case ID_CONNECTION_ATTEMPT_FAILED:
		dev->WriteToConsole(L"Connection attempt failed", DirectX::Colors::Red);
		break;
	case ID_NO_FREE_INCOMING_CONNECTIONS:
		// Sorry, the server is full.  I don't do anything here but A real app should tell the user
		dev->WriteToConsole(L"Server is full", DirectX::Colors::Red);
		break;

	case ID_INVALID_PASSWORD:
		dev->WriteToConsole(L"Incorrect Password", DirectX::Colors::Red);
		break;

	case ID_CONNECTION_LOST:
		// Couldn't deliver a reliable packet - i.e. the other system was abnormally terminated
		dev->WriteToConsole(L"Connection was lost", DirectX::Colors::Red);
		throw string("connection lost");
		break;

	case ID_CONNECTION_REQUEST_ACCEPTED:
		// This tells the client they have connected
		dev->WriteToConsole(std::wstring(L"Connection request accepted to") + to_wstring(p->systemAddress.ToString(true)), DirectX::Colors::Blue);
		dev->WriteToConsole(std::wstring(L"Connecting as ") + to_wstring(m_Peer->GetExternalID(p->systemAddress).ToString(true)),  DirectX::Colors::Blue);
		m_HostAddress = p->systemAddress;
		break;
	case ID_CONNECTED_PING:
		break;
	case ID_UNCONNECTED_PING:
		break;
	}
}

const unsigned short portNumber = 19404;

void Networking::HostGame()
{
	SocketDescriptor sockDesk(portNumber, 0);

	DeveloperConsole* dev = Services::GetDeveloperConsole();

	auto startResult = m_Peer->Startup(8,&sockDesk, 1);
		
	DisplayStartupAttemptResult(&startResult);
	if(startResult)
		return;
	m_Peer->SetMaximumIncomingConnections(4);

	m_Peer->SetTimeoutTime(30000,RakNet::UNASSIGNED_SYSTEM_ADDRESS);

	dev->WriteToConsole(L"RakNet started...", DirectX::Colors::Blue);

	m_IsHost = true;
}

void Networking::Connect(wstring ip)
{
	SocketDescriptor sockDesk;

	DeveloperConsole* dev = Services::GetDeveloperConsole();
	
	
	auto startResult = m_Peer->Startup(8, &sockDesk, 1);
	DisplayStartupAttemptResult(&startResult);
	if(startResult)
		return;
	
	m_Peer->SetMaximumIncomingConnections(4);

	dev->WriteToConsole(L"RakNet started...", DirectX::Colors::Blue);

	m_Peer->SetOccasionalPing(true);

	string narrowIP(ip.begin(), ip.end());
	
	ConnectionAttemptResult result = m_Peer->Connect(narrowIP.c_str(), portNumber, 0,0);
	DisplayConnectionAttemptResult(&result);
	
}

void Networking::DisplayConnectionAttemptResult( RakNet::ConnectionAttemptResult* res )
{
	DeveloperConsole* dev = Services::GetDeveloperConsole();
	
	switch (*res)
	{
	case CONNECTION_ATTEMPT_STARTED:
		dev->WriteToConsole(L"CONNECTION_ATTEMPT_STARTED", DirectX::Colors::Blue);
		break;
	case INVALID_PARAMETER:
		dev->WriteToConsole(L"INVALID_PARAMETER whatever that means", DirectX::Colors::Blue);
		break;
	case CANNOT_RESOLVE_DOMAIN_NAME:
		dev->WriteToConsole(L"CANNOT_RESOLVE_DOMAIN_NAME", DirectX::Colors::Blue);
		break;
	case ALREADY_CONNECTED_TO_ENDPOINT:
		dev->WriteToConsole(L"ALREADY_CONNECTED_TO_ENDPOINT", DirectX::Colors::Blue);
		break;
	case CONNECTION_ATTEMPT_ALREADY_IN_PROGRESS:
		dev->WriteToConsole(L"CONNECTION_ATTEMPT_ALREADY_IN_PROGRESS", DirectX::Colors::Blue);
		break;
	case SECURITY_INITIALIZATION_FAILED:
		dev->WriteToConsole(L"SECURITY_INITIALIZATION_FAILED", DirectX::Colors::Blue);
		break;
	}
}

void Networking::DisplayStartupAttemptResult( RakNet::StartupResult* res )
{
	DeveloperConsole* dev = Services::GetDeveloperConsole();
	
	switch(*res)
	{
	case RAKNET_STARTED:
		dev->WriteToConsole(L"RAKNET_STARTED", DirectX::Colors::Blue);
		break;
	case RAKNET_ALREADY_STARTED:
		dev->WriteToConsole(L"RAKNET_ALREADY_STARTED", DirectX::Colors::Blue);
		break;
	case INVALID_SOCKET_DESCRIPTORS:
		dev->WriteToConsole(L"INVALID_SOCKET_DESCRIPTORS", DirectX::Colors::Blue);
		break;
	case INVALID_MAX_CONNECTIONS:
		dev->WriteToConsole(L"INVALID_MAX_CONNECTIONS", DirectX::Colors::Blue);
		break;
	case SOCKET_FAMILY_NOT_SUPPORTED:
		dev->WriteToConsole(L"SOCKET_FAMILY_NOT_SUPPORTED", DirectX::Colors::Blue);
		break;
	case SOCKET_PORT_ALREADY_IN_USE:
		dev->WriteToConsole(L"SOCKET_PORT_ALREADY_IN_USE", DirectX::Colors::Blue);
		break;
	case SOCKET_FAILED_TO_BIND:
		dev->WriteToConsole(L"SOCKET_FAILED_TO_BIND", DirectX::Colors::Blue);
		break;
	case SOCKET_FAILED_TEST_SEND:
		dev->WriteToConsole(L"SOCKET_FAILED_TEST_SEND", DirectX::Colors::Blue);
		break;
	case PORT_CANNOT_BE_ZERO:
		dev->WriteToConsole(L"PORT_CANNOT_BE_ZERO", DirectX::Colors::Blue);
		break;
	case FAILED_TO_CREATE_NETWORK_THREAD:
		dev->WriteToConsole(L"FAILED_TO_CREATE_NETWORK_THREAD", DirectX::Colors::Blue);
		break;
	case COULD_NOT_GENERATE_GUID:
		dev->WriteToConsole(L"COULD_NOT_GENERATE_GUID", DirectX::Colors::Blue);
		break;
	case STARTUP_OTHER_FAILURE:
		dev->WriteToConsole(L"STARTUP_OTHER_FAILURE", DirectX::Colors::Blue);
		break;
	default:
		dev->WriteToConsole(L"Really bad error", DirectX::Colors::Blue);
		break;
	}
}

std::wstring Networking::Console_Say( std::wstring args )
{
	SendChat(args);
	return args;
}

void Networking::HandleApplicationLayer( unsigned char packetIdentifier, RakNet::Packet* p )
{

	switch (packetIdentifier)
	{
	case POOP_CHATMESSAGE:
		HandleChat(p); 
		break;		
	case POOP_REPLICATE_ENTITY:
		HandleReplicateEntity(p);
		break;
	case POOP_REPLICATE_COMPONENT:
		HandleReplicateComponent(p);
	default:
		break;
	}
}

const int maxChatLength = 256;
void Networking::HandleChat(  RakNet::Packet* p )
{
	BitStream bs(p->data, p->length, false);
	char mesg[maxChatLength];
	bs.Read(mesg, 1);
	if(StringCompressor::Instance()->DecodeString(mesg, maxChatLength, &bs))
	{
		string nmsg(mesg);
		wstring wmsg = to_wstring(nmsg);
		Services::GetDeveloperConsole()->WriteToConsole(wmsg);
		if(m_IsHost)
		{
			const char* d = (char*)p->data;
			m_Peer->Send(d, p->length, MEDIUM_PRIORITY, RELIABLE_ORDERED_WITH_ACK_RECEIPT, CHAT_CHANNEL, p->systemAddress, true);
		}
	}
	else
	{
		Services::GetDeveloperConsole()->WriteToConsole(L"Failed to decompress string");
	}
		
}

void Networking::SendChat( std::wstring str )
{
	if (!m_Peer || !m_Peer->IsActive())
		return;

	string st(str.begin(), str.end());
	const RakString rs(st.c_str());
	BitStream bs;
	bs.Write((unsigned char)POOP_CHATMESSAGE);
	SystemAddress* sa = new SystemAddress[4];
	unsigned short num = 4;
	m_Peer->GetConnectionList(sa, &num);
	if(num == 0)
	{
		Services::GetDeveloperConsole()->WriteToConsole(L"You do not appear to be connected to another player");
		return;		
	}
	
	StringCompressor::Instance()->EncodeString(rs, maxChatLength, &bs);
	m_Peer->Send(&bs, MEDIUM_PRIORITY, RELIABLE_ORDERED_WITH_ACK_RECEIPT, CHAT_CHANNEL , UNASSIGNED_SYSTEM_ADDRESS, true);
		

}

bool Networking::IsConnected()
{
	SystemAddress sa;
	unsigned short num = 1;
	m_Peer->GetConnectionList(&sa, &num);
	if(num > 0)
		return true;
	else
		return false;
}

void Networking::SetReplicateEntityCallback( ReplicateEntityFunction func )
{
	m_ReplicateObjectCallback = func;
}

void Networking::SetReplicateComponentCallback(ReplicateComponentFunction func)
{
	m_ReplicateComponentCallback = func;
}

void Networking::SendReplicateEntity( RakNet::NetworkID id, char replicationFlags )
{
	if(replicationFlags == ReplicationFlags::LocalOnly)
	{
		throw string("Should not attempt to replicate localonly entities, poop made a mistake");
	}

	BitStream bs;
	bs.Write((unsigned char)POOP_REPLICATE_ENTITY);
	bs.Write1();
	bs.Write(id);
	bs.Write(replicationFlags);

	if(m_IsHost)
		m_Peer->Send(&bs, MEDIUM_PRIORITY, RELIABLE, ENTITY_CHANNEL, UNASSIGNED_SYSTEM_ADDRESS, true);
	else
		m_Peer->Send(&bs, MEDIUM_PRIORITY, RELIABLE, ENTITY_CHANNEL, m_HostAddress, false);
}


// read packet, send it to everyone if we're server, make sure to change authority flags
void Networking::HandleReplicateEntity( RakNet::Packet *p )
{
	if(!m_ReplicateObjectCallback)
		throw string("replicate entity callback not assigned");
		
	BitStream bs(p->data, p->length, false);

	bs.IgnoreBytes(1);
	bool spawning = bs.ReadBit();
	NetworkID entID;
	char repFlags;
	bs.Read(entID);
	bs.Read(repFlags);


	if(repFlags == ReplicationFlags::Authoritative)
		repFlags = ReplicationFlags::Replicated;

	if(m_IsHost)
	{
		BitStream relay;
		relay.Write((MessageID)POOP_REPLICATE_ENTITY);
		relay.Write(spawning);
		relay.Write(entID);
		relay.Write(repFlags);
		m_Peer->Send(&relay, MEDIUM_PRIORITY, RELIABLE, ENTITY_CHANNEL, p->systemAddress, true);
	}

	m_ReplicateObjectCallback(entID, spawning, repFlags);
}

void Networking::HandleReplicateComponent( RakNet::Packet* p )
{
	BitStream bs(p->data, p->length, false);
	bs.IgnoreBytes(1);
	
	NetworkID entID;
	ComponentID_t idType;
	unsigned char replicateBits;
	bool spawning;

	bs.Read(entID);	
	bs.Read(idType);
	bs.ReadBitsFromIntegerRange<unsigned char>(replicateBits, 0, (unsigned char)ReplicationFlagCount, false);
	bs.Read(spawning);

	Entity* ent = m_NetworkIDManager->GET_OBJECT_FROM_ID<Entity*>(entID);
	if(ent == nullptr)
		throw string("Tried to add component to entity that does not yet exist, if this happens it's probably because poop is bad hasn't yet ordered this networking channel");

	// we "correct" the replication flags in this manner so that the server does not have to modify existing packets to relay to other clients.
	if(replicateBits == ReplicationFlags::Authoritative)
		replicateBits = ReplicationFlags::Replicated;

	//todo: use replicationbits
	Component* com = GetComponentFromBitstream(idType, bs);
	m_ReplicateComponentCallback(idType, ent, com);

	if(m_IsHost)
	{
		BitStream relay(p->data, p->length, false);
		m_Peer->Send(&relay, MEDIUM_PRIORITY, RELIABLE, ENTITY_CHANNEL, p->systemAddress, true);
	}
	
	DeveloperConsole* dev = Services::GetDeveloperConsole();
	dev->WriteToConsole(L"pls");
}

