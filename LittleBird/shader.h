#ifndef _SHADER_H_
#define _SHADER_H_

#include <string>

struct ID3D11VertexShader;
struct ID3D11PixelShader;
struct ID3D11InputLayout;

class Shader
{
public:
	Shader(std::string name): m_Name(name) {}
	std::string			GetName()			{ return m_Name; }
	ID3D11VertexShader* GetVertexShader()	{ return m_vertexShader; }
	ID3D11PixelShader*	GetPixelShader()	{ return m_pixelShader; }
	ID3D11InputLayout*	GetInputLayout()	{ return m_layout; }
	void SetVertexShader(ID3D11VertexShader* vShader);
	void SetPixelShader(ID3D11PixelShader* pShader);
	void SetInputLayout(ID3D11InputLayout* layout);
protected:
	std::string m_Name;
	ID3D11VertexShader* m_vertexShader;
	ID3D11PixelShader* m_pixelShader;
	ID3D11InputLayout* m_layout;
};


#endif