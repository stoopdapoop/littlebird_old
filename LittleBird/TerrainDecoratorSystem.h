#ifndef TerrainDecoratorSystem_h__
#define TerrainDecoratorSystem_h__


#include <unordered_map>
#include <vector>

class Camera;
class Renderer;
class Terrain;
class TerrainManager;

struct ID3D11Buffer;
struct TerrainCollisionPair;

struct DecorationInstanceData{
	DirectX::XMMATRIX worldMatrix;
};

class TerrainDecorationList
{
public:
	TerrainDecorationList(ResourceHandle handle) : modelHandle(handle) {}
	ResourceHandle modelHandle;
	std::vector<DecorationInstanceData> instances;
};

class TileDecorationPair
{
public:
	TileDecorationPair(Terrain* ter) : terrainTile(ter) {}
	Terrain* terrainTile;
	std::vector<TerrainDecorationList> tileDecorationLists;
};

class TerrainDecoratorSystem
{
public:
	TerrainDecoratorSystem(Renderer* rend, TerrainManager* terr);
	void Run(Camera* cam);
	void RenderToShadowMap();
	void RenderToScreen();

private:
	void GenerateDecorationListForTile(const TerrainCollisionPair* tile);
	void PerformBroadPhaseTileFiltering(std::vector<TerrainCollisionPair>& narrowPhaseTiles, DirectX::XMVECTOR xmViewPos, DirectX::XMVECTOR xmOffset,
		DirectX::XMVECTOR xmDistCutoffSq, DirectX::BoundingFrustum& camFrustum);
	void CleaupOldDecorations();
	void PopulateInstanceBuffer(vector<TerrainCollisionPair>& narrowPhaseTiles, const DirectX::XMVECTOR xmViewPos, DirectX::XMVECTOR xmDistCutoff, Camera* cam);

private:
	Renderer* m_Renderer;
	TerrainManager* m_TerrainManager;
	std::unordered_map<size_t, TileDecorationPair> m_TileDecorationPairs;
	unsigned m_VisibleInstanceCount;
	ID3D11Buffer* m_InstanceBuffer;
};

#endif // TerrainDecoratorSystem_h__
