#ifndef _PHYSICS_H_
#define _PHYSICS_H_

#pragma warning(disable:4127)
#pragma warning(disable:4100)

#include "btBulletDynamicsCommon.h"

#pragma warning(default:4100)
#pragma warning(default:4127)
#include "BulletDynamics/Character/btKinematicCharacterController.h"

class	DebugDrawer;
class	btRigidBody;
class	btSequentialImpulseConstraintSolver;
class	btDiscreteDynamicsWorld;
class	btCollisionDispatcher;
class	btBroadphaseInterface;
class	btDefaultCollisionConfiguration;
class	btTransform;
class	btCollisionShape;

class RigidBodyComponent;

enum PHYSICS_GROUP : short
{
	AllFilter = -1,
	DefaultFilter = 1,
	StaticFilter = 2,
	KinematicFilter = 4,
	DebrisFilter = 8,
	SensorTrigger = 16,
	CharacterFilter = 32,
};



class Physics
{
public:
	Physics();
	~Physics();

	void Update(float delta);
	btRigidBody* CreateTerrainCollisionMesh(float* mesh, float maxHeight, float minHeight, float tileScale, int length, int width, int xpos, int zpos);
	void SetDebugDrawer(DebugDrawer* deb);

	btRigidBody* CreateRigidBody(float mass, RigidBodyComponent* rbc, PHYSICS_GROUP group, PHYSICS_GROUP col);
	btRigidBody* CreateRigidBody(float mass, RigidBodyComponent* rbc);

	// DO NOT USE THESE
	btRigidBody* CreateRigidBody(float mass, const btTransform& startTransform, btCollisionShape* shape, PHYSICS_GROUP group, PHYSICS_GROUP col);
	btRigidBody* CreateRigidBody(float mass, const btTransform& startTransform, btCollisionShape* shape);

	void ClosestCollision(btVector3 start, btVector3 end, bool& hit, btVector3& hitPoint);
	
	btDiscreteDynamicsWorld* GetWorld() { return m_DynamicsWorld; }

	void RemoveRigidBody(btRigidBody* body);

private:
	btRigidBody* InternalCreateRigidBody(float mass, RigidBodyComponent* rbc);
	btRigidBody* InternalCreateRigidBody(float mass, const btTransform& startTransform,btCollisionShape* shape);
	
	

private:
	btDiscreteDynamicsWorld* m_DynamicsWorld;
	btSequentialImpulseConstraintSolver* m_Solver;
	btBroadphaseInterface* m_OverlappingPairCache;
	btCollisionDispatcher* m_Dispatcher;
	btDefaultCollisionConfiguration* m_CollisionConfiguration;
};

class PoopEngineMotionState : public btMotionState
{
public:
	PoopEngineMotionState(RigidBodyComponent* rbc);

	void getWorldTransform( btTransform& worldTrans ) const;

	void setWorldTransform( const btTransform& worldTrans );

private:
	RigidBodyComponent* m_Owner;
	btTransform m_Transform;
};

#endif