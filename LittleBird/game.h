#ifndef _GAME_H_
#define _GAME_H_


#include <windows.h>
#include <memory>
#include "resource_manager.h"

class Entity;
class PerspectiveCamera;
class Camera;
class DebugUI;
class Input;
class Physics;
class Profiler;
class Renderer;
class TerrainManager;
class Timer;
class WorldQuery;
class DeveloperConsole;
class ModelRenderSystem;
class DebugDrawer;
class TardMovementSystem;
class RigidBodySystem;
class HelicopterSystem;


class Game
{
public:
	Game(int screenheight, int screewidth, HWND hwnd, HINSTANCE hinstance, bool fullScreen, bool vSync);
	~Game();

	void CreateDebugObjects( int screenwidth, int screenheight );
	bool Frame();

private:
	void CreateSubsystems(int screenheight, int screenwidth, HWND hwnd, HINSTANCE hinstance, bool fullScreen, bool vSync);
	void InitializeSubsystems();
	void Render();

	void RenderTerrain();

	void HandleControllerInput( float delta );
	void HandlePCInput( float delta );

	void HandlePCGameInput( float delta );
	void DispatchEntities();
private:
	
	std::unique_ptr<Input> m_Input;
	std::unique_ptr<Renderer> m_Renderer;
	std::unique_ptr<Timer> m_GameTimer;
	DeveloperConsole* m_DeveloperConsole;
	DebugUI* m_DebugUI;
	Profiler* m_Profiler;
	PerspectiveCamera* m_Camera;
	Physics* m_Physics;
	TerrainManager* m_TerrainManager;
	ResourceManager* m_ResourceManager;
	WorldQuery* m_WorldQuery;
	std::vector<Entity*> m_Entities;
	DebugDrawer* m_DebugDrawer;
	bool m_UsingController;
	float m_ViewSensitivity;

	ModelRenderSystem* m_ModelRenderSystem;
	TardMovementSystem* m_TardMovementSystem;
	RigidBodySystem* m_RigidBodySystem;
	HelicopterSystem* m_HelicopterSystem;

};


#endif