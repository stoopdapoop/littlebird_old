#ifndef _GUI_CALLBACKS_H_
#define _GUI_CALLBACKS_H_

#define DeclareGUICallback(name, valuetype, datatype, setfunction, getfunction) \
	void TW_CALL Get ## name ## Callback(void *value, void *clientData) \
{ \
	*(##valuetype *)value = static_cast<##datatype*>(clientData)->##getfunction(); \
} \
	void TW_CALL Set ## name ## Callback(const void *value, void *clientData) \
{ \
	static_cast<##datatype*>(clientData)->##setfunction(*(##valuetype *)value); \
} 

#endif