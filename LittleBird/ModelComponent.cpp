
#include "ModelComponent.h"
#include "services.h"

void ModelComponent::NetworkSerialize( RakNet::BitStream& bs )
{

}


void ModelComponent::NetworkDeserialize( RakNet::BitStream& bs )
{
	ResourceManager* res = Services::GetResourceManager();

	ModelHandle = res->GetModelHandle("rock.pbm");
	DiffuseTextureHandle = res->GetTextureHandle("rock_bc7.dds");
	NormalTextureHandle = res->GetTextureHandle("rock_normal_BC7.dds");
	RoughnessTextureHandle = res->GetTextureHandle("woodgrain_BC7.dds");
	SpecularTextureHandle = res->GetTextureHandle("subtlenormals_BC7.dds");
}
