#ifndef _TERRAIN_H_
#define _TERRAIN_H_

#include ".\Shaders\terrainConstants.h"
#include "ResourceHandle.h"


const int TEXTURE_SCALE = 16;
//const float HEIGHT_DIFFERENCE = 655.35f * 0.75f;
//const float HEIGHT_DIFFERENCE = 8000.00f;

#define LODCOUNT 8

struct ID3D11Device;
struct ID3D11DeviceContext;
struct ID3D11Buffer;

class Texture;
struct TerrainVertexType;

class Terrain
{
private:

public:
	Terrain(ID3D11Device* device, std::string heightMapFilename, ResourceHandle textureSplatHandle, ResourceHandle textureTintHandle, float tileScale, int xPos, int zPos, int lodLevel);
	~Terrain();

	int GetIndexCount(int lodLevel) const {return m_IndexCount[lodLevel];}
	int GetIndexCount() const	{return m_IndexCount[m_LodLevel];}
	float* GetHeights()	const	{return m_HeightmapHeights;}
	float GetMaxHeight()const	{return m_MaxHeight;}
	float GetMinHeight()const	{return m_MinHeight;}
	int GetTerrainWidth()const	{return m_terrainWidth;}
	int GetTerrainLength()const	{return m_terrainLength;}
	void GetTilePosition(int& x, int& z) const { x = m_xPosition; z = m_zPosition; }

	// returns the "lower left" corner of the tile with a height of zero, not the origin.
	void GetTerrainPosition(float& x, float& y, float& z)const;
	Texture* GetHeightMapTexture() { return m_HeightMapTexture; }

	void Render(ID3D11DeviceContext* deviceContext)const;
	static void Render(ID3D11DeviceContext* deviceContext, int lodLevel);
	static void PreRender( ID3D11DeviceContext* deviceContext );

	float GetTileScale()const {return m_TileScale;}
	void SetTileScale(float scale);
	void SetLod(unsigned lod);
	unsigned GetLod() const { return m_LodLevel;}
	static float CalculateTileTerrainSideLength() { return (SZ - 1) * tileDistance; }

	ResourceHandle GetSplatTextureHandle()const;
	ResourceHandle GetTintTextureHandle()const;

private:
	Terrain();
	void Initialize(ID3D11Device* device, std::string heightMapFilename);
	void Shutdown();

	void CalculateNormals();
	void InitializeBuffers(ID3D11Device*);
	void ShutdownBuffers();
	void LoadHeightMap(ID3D11Device* device, std::string filename);

	void FilterHeightMap();
	void CreateCollisionHeights(unsigned short* heights);
	void CalculateTangents();
	void ShutdownHeightMap();

	void CalculateTextureCoordinates();

private:
	int m_terrainWidth, m_terrainLength;
	float m_MaxHeight;
	float m_MinHeight;
	static int m_VertexCount;
	static unsigned m_IndexCount[LODCOUNT+1];
	float m_TileScale;
	// stores only heightmaps for use by the physics engine.
	float* m_HeightmapHeights;
	// stores everything, for use by graphics engine.
	TerrainVertexType* m_HeightMap;
	ResourceHandle m_TextureSplatHandle;
	ResourceHandle m_TextureTintHandle;
	Texture* m_HeightMapTexture;
	int m_xPosition;
	int m_zPosition;
	int m_LodLevel;


	static ID3D11Buffer *m_VertexBuffer;
	static ID3D11Buffer *m_IndexBuffer[LODCOUNT+1];
	static bool m_NeedsIndexBuffer;
};
#endif