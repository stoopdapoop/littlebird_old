#ifndef _REPLICATION_FLAGS_H_
#define _REPLICATION_FLAGS_H_

enum ReplicationFlags : char {
	LocalOnly = 0,
	Replicated,
	Authoritative, // probably a bad name, but implies replication with this as master copy
	ReplicateAndMakeLocal,


	ReplicationFlagCount,
};

#endif