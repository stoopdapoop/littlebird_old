#ifndef _MODEL_RENDER_SYSTEM_H_
#define _MODEL_RENDER_SYSTEM_H_

#include "component_system.h"
#include "ModelComponent.h"
#include "PositionComponent.h"
#include "renderer.h"

class Camera;
class TerrainManager;
namespace DirectX
{
struct BoundingOrientedBox;
struct BoundingFrustum;
}
class OrthographicCamera;

class ModelRenderSystem : public ComponentSystem
{
public:
	ModelRenderSystem(Renderer* rend);
	void AddEntry(unsigned ID, PositionComponent *pos, ModelComponent *mod);
	void RenderShadows(Camera* viewCamera, Camera& lightCamera, DirectX::XMVECTOR lightPos, DirectX::BoundingSphere& camSphere, TerrainManager* terrManager);
	void RenderSolids(Camera* camera, TerrainManager* terrManager);
	void EndFrame();

private:
	void RenderSolidGeometry( Camera* camera, size_t count, TerrainManager* terrManager); 

	void SolidRenderStaticMeshes( size_t count, DirectX::BoundingFrustum* camFrustum, Camera* camera );
	void RenderShadowMap(Camera* viewCamera, Camera& lightCamera, DirectX::XMVECTOR lightPos, size_t count, DirectX::BoundingSphere& camSphere, TerrainManager* terrManager);
	void ShadowRenderStaticMeshes( size_t count, DirectX::BoundingOrientedBox* lightBox, Camera* lightCamera );
	void ShadowRenderTerrainMeshes( DirectX::BoundingOrientedBox* lightBox, Camera* lightCamera, TerrainManager* terrManager, DirectX::XMVECTOR playerPosition );

	void SolidRenderTerrainMeshes( DirectX::BoundingFrustum* camFrustum, Camera* camera, TerrainManager* terrManager );

private:
	std::vector<PositionComponent*> m_Positions;
	std::vector<ModelComponent*> m_Models;
	Renderer* m_Renderer;
};

#endif