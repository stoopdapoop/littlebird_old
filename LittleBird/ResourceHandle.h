#ifndef _RESOURCE_HANDLE_H_
#define _RESOURCE_HANDLE_H_

typedef int ResourceID;

class ResourceHandle
{
public:
	ResourceHandle();

private:
	ResourceID ID;
	friend class ResourceManager;
};

#endif