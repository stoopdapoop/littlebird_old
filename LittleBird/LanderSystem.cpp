#include "LanderSystem.h"
#include "input.h"
#include "RigidBodyComponent.h"
#include "LanderComponent.h"
#include "physics.h"
#include "services.h"
#include "DebugDrawer.h"
#include "camera.h"
#include "MathUtilities.h"

using namespace DirectX;

class LanderInputAdapter 
{
public:
	LanderInputAdapter(RotationCoordinateSpace space);

	void SetRotationCoordinateSpace(RotationCoordinateSpace newSpace) { m_RotationCoordinateSpace = newSpace; }
	RotationCoordinateSpace GetRotationCoordinateSpace() { return m_RotationCoordinateSpace; }

	void CalculateDesiredMovementForces(float &xRot, float &yRot, float &zRot, float &throttle);
	void GetCameraRotation(float &xRot, float &yRot);

private:
	float						m_MouseSensitivity			= 0.001;
	float						m_ThrottleSensitivity		= 0.00025f;
	float						m_ThrottleAmount;
	float						m_CamRot_X					= 0.0f;
	float						m_CamRot_Y					= 0.0f;
	RotationCoordinateSpace		m_RotationCoordinateSpace;
	bool						m_UseController				= true;
};

LanderInputAdapter::LanderInputAdapter(RotationCoordinateSpace space) : m_RotationCoordinateSpace(space)
{

}

// has side effects, only call once per frame per adapter
void LanderInputAdapter::CalculateDesiredMovementForces(float &forward, float &right, float &yawRight, float &throttle)
{
	Input* input = Services::GetInput();

	if (!m_UseController)
	{
		bool upPressed = input->KeyDown(DIK_W);
		bool downPressed = input->KeyDown(DIK_S);
		bool leftPressed = input->KeyDown(DIK_A);
		bool rightPressed = input->KeyDown(DIK_D);
		bool rightYawPressed = input->KeyDown(DIK_E);
		bool leftYawPressed = input->KeyDown(DIK_Q);
		int throttleDelta = input->GetMouseWheelDelta();

		m_ThrottleAmount = clamp(m_ThrottleAmount + (m_ThrottleSensitivity * throttleDelta), 0.0f, 1.0f);

		float rotForward = 0.0f;
		float rotRight = 0.0f;
		float yawRight = 0.0f;
		forward += upPressed ? 1.0f : 0.0f;
		forward -= downPressed ? 1.0f : 0.0f;
		right -= rightPressed ? 1.0f : 0.0f;
		right += leftPressed ? 1.0f : 0.0f;
		yawRight += rightYawPressed ? 1.0f : 0.0f;
		yawRight -= leftYawPressed ? 1.0f : 0.0f;
		throttle = m_ThrottleAmount;
	}
	else
	{
		ControllerState &conState = input->GetControllerState();
		forward = conState.StickLeftCircular[1];
		right = -conState.StickLeftCircular[0];
		yawRight = 0.0f;
		yawRight += conState.ButtonDown(RIGHT_BUMPER) ? 1.0f : 0.0f;
		yawRight -= conState.ButtonDown(LEFT_BUMPER) ? 1.0f : 0.0f;
		throttle = conState.TriggerRight;
		conState.VibrationLeft = throttle - 0.7f;
	}
}

void LanderInputAdapter::GetCameraRotation(float &xRot, float &yRot)
{
	Input* input = Services::GetInput();
	float delta = Services::GetTimer()->CheckDeltaTime();
	if (!m_UseController)
	{
		int x, y;
		input->GetMouseDelta(x, y);
		m_CamRot_X += x * m_MouseSensitivity;
		m_CamRot_X = fmodf(m_CamRot_X, XM_2PI);
		m_CamRot_Y -= y * m_MouseSensitivity;
	}
	else
	{
		ControllerState conState = input->GetControllerState();
		// generalize this and move to input
		const float axisDeadzone = 0.2f;
		float deadZonedAxis = fmaxf(abs(conState.StickRightRectangular[0]) - axisDeadzone, 0.0f) * sign(conState.StickRightRectangular[0]) / (1.0f - axisDeadzone);
		m_CamRot_X += deadZonedAxis * 1.5f * delta;
		deadZonedAxis = fmaxf(abs(conState.StickRightRectangular[1]) - axisDeadzone, 0.0f) * sign(conState.StickRightRectangular[1]) / (1.0f - axisDeadzone);
		m_CamRot_Y -= deadZonedAxis * 1.5f * delta;
	}

	// small number is to prevent the view matrix from exploding when looking straight up or down
	const float smallNumber = 0.001f;
	m_CamRot_Y = clamp(m_CamRot_Y, -XM_PIDIV2 + smallNumber, XM_PIDIV2 - smallNumber);

	xRot = m_CamRot_X;
	yRot = m_CamRot_Y;
}


LanderSystem::LanderSystem() : m_UseLanderCam(true)
{
	DECLARE_CONSOLECOMMAND("camera_uselandercam", LanderSystem::Console_SetUseCamera);

	m_ControlAdapter = new LanderInputAdapter(CAMERA_SPACE);
}


LanderSystem::~LanderSystem()
{
	delete m_ControlAdapter;
}


void LanderSystem::AddEntry( unsigned ID, LanderComponent* lander, RigidBodyComponent* rigid )
{
	m_Roster.push_back(ID);
	m_Landers.push_back(lander);
	m_RigidBodies.push_back(rigid);
}

void LanderSystem::Run(Camera* cam)
{
	size_t landerCount = m_Roster.size();

	Input* input = Services::GetInput();
	Timer* GameTimer = Services::GetTimer();
	float deltaTime = GameTimer->CheckDeltaTime();

	static bool ObjectSpaceRotation = false;
	if (input->KeyPressed(DIK_COMMA))
	{
		ObjectSpaceRotation = !ObjectSpaceRotation;
		m_ControlAdapter->SetRotationCoordinateSpace(ObjectSpaceRotation ? OBJECT_SPACE : CAMERA_SPACE);
	}


	Services::GetDebugDrawer()->drawText(500.0f, 500.0f, ObjectSpaceRotation ? "ObjectSpace" : "CameraSpace", 1.0f);
	Services::GetDebugDrawer()->drawText(500.0f, 550.0f, to_string(m_Landers[0]->Throttle*100.0f) + "%", 1.0f);
	Services::GetDebugDrawer()->drawText(500.0f, 580.0f, to_string(m_Landers[0]->FuelAmount) + "Gallons", 1.0f);
	for(int i = 0; i < landerCount; ++i)
	{
		LanderComponent* lander = m_Landers[i];
		RigidBodyComponent* rbc = m_RigidBodies[i];
		btRigidBody* rigid = rbc->RigidBody;
		rigid->activate(true);
		rigid->setDamping(rigid->getLinearDamping(), 0.5f);
		btTransform trans;
		rigid->getMotionState()->getWorldTransform(trans);
	
		if(lander->PlayerControlled)
		{
		
			float rotForward, rotRight, yawRight;
			float desiredForward, desiredRight, desiredYaw, desiredThrottle;

			m_ControlAdapter->CalculateDesiredMovementForces(desiredForward, desiredRight, desiredYaw, desiredThrottle);
			
			lander->Throttle = desiredThrottle;
			
			rotForward = lander->TiltSpeed * desiredForward;
			rotRight = lander->TiltSpeed * desiredRight;
			yawRight = lander->TiltSpeed * desiredYaw;
			btVector3 torqueVec(rotForward, yawRight, rotRight);
			
			XMMATRIX camView = XMMATRIX(cam->GetViewMatrix());
			XMFLOAT3X3 camRotation3x3;
			XMStoreFloat3x3(&camRotation3x3, camView);
			XMMATRIX XMCamRotation = XMLoadFloat3x3(&camRotation3x3);

			btMatrix3x3 localBasis = m_ControlAdapter->GetRotationCoordinateSpace() == OBJECT_SPACE ? trans.getBasis() : *(btMatrix3x3*)&XMCamRotation;
			torqueVec = localBasis * torqueVec;
			XMMATRIX XMRigidBasis = XMLoadFloat3x3((XMFLOAT3X3*)&localBasis);
			XMVECTOR XMTorqueVector = XMLoadFloat3((XMFLOAT3*)&torqueVec);
			torqueVec.set128(XMTorqueVector);

			rigid->applyTorque(torqueVec);

			// using this curve for rocket efficiency, completely arbitrary for now I want good efficiency up to around 50% and
			// for rapid falloff to 50% after that.
			// http://www.wolframalpha.com/input/?i=x+*+%281-%281+-+pow%28x%2C+6%29%29+%2F+2+%2B+0.5%29+from+x+%3D+0+to+1+y+from+0+to+1.5
			float burnInefficiencyCoeff = 1.0f - (1.0f - pow(lander->Throttle, 6.0f)) / 2 + 0.5;
			float fuelConsumption = lander->Throttle * burnInefficiencyCoeff * 5.0f * deltaTime;
			lander->FuelAmount -= fuelConsumption;
			lander->FuelAmount = std::fmaxf(lander->FuelAmount, 0.0f);

			Services::GetDebugDrawer()->drawText(500.0f, 600.0f, to_string((1.0f - burnInefficiencyCoeff) * 100.0f) + "% burn efficiency", 1.0f);
			Services::GetDebugDrawer()->drawText(500.0f, 610.0f, to_string(fuelConsumption / deltaTime) + "gallons per second", 1.0f);

			btVector3 up = trans.getBasis().getColumn(1);
			up *= lander->Throttle * lander->LiftStrength;
			if (lander->FuelAmount > 0.0f)
				rigid->applyForce(up, btVector3(0.0f, 0.0f, 0.0f));

			if (m_UseLanderCam)
				PositionCamera(cam, lander, rigid);
		}
	}

	m_Roster.clear();
	m_RigidBodies.clear();
	m_Landers.clear();
}

void LanderSystem::PositionCamera(Camera* cam, LanderComponent* lander, btRigidBody* rigid)
{

	float camRotX, camRotY;
	m_ControlAdapter->GetCameraRotation(camRotX, camRotY);

	btVector3 forwardVec(0.0f, 0.0f, 20.0f);
	btVector3 upVec(0.0f, 1.0f, 0.0f);
	btVector3 rightVec(1.0f, 0.0f, 0.0f);
	forwardVec = forwardVec.rotate(rightVec, camRotY);
	forwardVec = forwardVec.rotate(upVec, camRotX);
	
	btTransform trans;
	rigid->getMotionState()->getWorldTransform(trans);
	btVector3 landerPos(trans.getOrigin());
	btVector3 eyePos = landerPos + forwardVec;

	cam->SetLookAt(*reinterpret_cast<XMVECTOR*>(&eyePos), *reinterpret_cast<XMVECTOR*>(&landerPos), XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f));
}

wstring LanderSystem::Console_SetUseCamera( wstring args )
{
	if(args == L"1")
		SetUseCamera(true);
	else if( args == L"0")
		SetUseCamera(false);

	else
		return L"Invalid input";

	return args;
}

