
#include "WorldQuery.h"
#include "terrain.h"
#include "Entity.h"

WorldQuery::WorldQuery( std::vector<Entity*>* entList ) : m_EntityList(entList)
{
}

WorldQuery::~WorldQuery()
{
}

void WorldQuery::AddEntity( Entity* ent )
{
	m_EntityList->push_back(ent);
}
