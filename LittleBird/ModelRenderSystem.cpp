
#include <DirectXCollision.h>
#include "ModelRenderSystem.h"
#include "camera.h"
#include "services.h"
#include "texture.h"
#include "model.h"
#include "timer.h"
#include "services.h"
#include "DebugDrawer.h"
#include "TerrainManager.h"
#include "terrain.h"
#include "shaders\SharedShaderDefines.h"

#include <float.h>
using namespace DirectX;

ModelRenderSystem::ModelRenderSystem( Renderer* rend ) : m_Renderer(rend)
{
}

void ModelRenderSystem::AddEntry( unsigned ID, PositionComponent *pos, ModelComponent *mod )
{
	m_Roster.push_back(ID);
	m_Positions.push_back(pos);
	m_Models.push_back(mod);
}

void ModelRenderSystem::RenderSolids(Camera* camera, TerrainManager* terrManager)
{
	size_t count = m_Roster.size();

	m_Renderer->GetImmediateContext()->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	RenderSolidGeometry(camera, count, terrManager);
}


void ModelRenderSystem::RenderShadows(Camera* viewCamera, Camera& lightCamera, XMVECTOR lightPos, DirectX::BoundingSphere& camSphere, TerrainManager* terrManager)
{
	size_t count = m_Roster.size();
	RenderShadowMap(viewCamera, lightCamera, lightPos, count, camSphere, terrManager);
}


void ModelRenderSystem::EndFrame()
{
	m_Models.clear();
	m_Positions.clear();
	m_Roster.clear();
}



void ModelRenderSystem::RenderShadowMap( Camera* viewCamera, Camera& lightCamera, XMVECTOR lightPos, size_t count, DirectX::BoundingSphere& camSphere, TerrainManager* terrManager) 
{
	m_Renderer->BeginShadowMapShader();

	XMFLOAT3 floatLightPos;
	XMStoreFloat3(&floatLightPos, lightPos);

	///////////// badddddddddddddddddddddddd
	const float lightFarClip = SHADOW_FAR_CLIP;
	const float lightNearClip = SHADOW_NEAR_CLIP;
	XMFLOAT3 floatLightExtents(camSphere.Radius, camSphere.Radius, lightFarClip);
	XMFLOAT4 floatLightOrientation;
	XMStoreFloat4(&floatLightOrientation, lightCamera.Orientation());
	BoundingOrientedBox lightBox(floatLightPos, floatLightExtents, floatLightOrientation);
	
	ShadowRenderStaticMeshes(count, &lightBox, &lightCamera);

	m_Renderer->BeginShadowMapTerrainShader();
	ShadowRenderTerrainMeshes( &lightBox, &lightCamera, terrManager, viewCamera->GetPosition());
}

// todo: move terrain rendering to terrain system
void ModelRenderSystem::RenderSolidGeometry( Camera* camera, size_t count, TerrainManager* terrManager) 
{
	m_Renderer->BeginGBufferShader();

	const XMVECTOR camPosition = camera->GetPosition();
	BoundingFrustum camFrustum;
	BoundingFrustum::CreateFromMatrix(camFrustum, camera->GetProjectionMatrix());
	camFrustum.Transform(camFrustum, 1.0f, camera->Orientation(), camPosition);
	
	SolidRenderStaticMeshes(count, &camFrustum, camera);
	
	string terrainSection = "terrain";
	m_Renderer->StartProfileSection(terrainSection);
	m_Renderer->BeginTerrainShader();
	SolidRenderTerrainMeshes(&camFrustum, camera, terrManager);
	m_Renderer->EndProfileSection(terrainSection);
}

void ModelRenderSystem::ShadowRenderStaticMeshes( size_t count, BoundingOrientedBox* lightBox, Camera* lightCamera ) 
{
	ResourceManager* res = Services::GetResourceManager();

	int meshCount = 0;
	for(size_t i = 0; i < count; ++i)
	{
		PositionComponent* pos = m_Positions[i];
		ModelComponent* currentModelComponent = m_Models[i];
		if(!currentModelComponent->CastsShadows)
			continue;

		XMVECTOR position = pos->Position;
		XMVECTOR rotation = pos->Orientation;
		XMFLOAT4 floatRot;
		XMFLOAT3 floatPos;
		XMStoreFloat3(&floatPos, position);
		XMStoreFloat4(&floatRot, rotation);		

		Model* currentModel = res->GetModel(currentModelComponent->ModelHandle);
		XMFLOAT3 center(currentModel->m_BoundingBoxCenter);
		XMFLOAT3 extents(currentModel->m_BoundingBoxHalfExtents);
		XMVECTOR cent = XMLoadFloat3(&center);
		cent = XMVectorAdd(cent, position);
		XMStoreFloat3(&center, cent);
		DirectX::BoundingOrientedBox thisBox(center, extents, floatRot);

		if(lightBox->Intersects(thisBox))
		{
			XMMATRIX modelMatrix = XMMatrixRotationQuaternion(rotation);
			modelMatrix = XMMatrixMultiply(modelMatrix, XMMatrixTranslationFromVector(position));
			m_Renderer->UpdatePerObjectBuffer(lightCamera, &modelMatrix);

			currentModel->Render(m_Renderer->GetImmediateContext());
			m_Renderer->GetImmediateContext()->DrawIndexed(currentModel->GetIndexCount(), 0, 0);
			++meshCount;
		}
	}
}

void ModelRenderSystem::ShadowRenderTerrainMeshes(BoundingOrientedBox* lightBox, Camera* lightCamera, TerrainManager* terrManager, XMVECTOR playerPosition ) 
{
	int x,z;
	terrManager->CalculateTilePosition(playerPosition, x, z);

	int tileCount = 0;
	Terrain::PreRender(m_Renderer->GetImmediateContext());

	const std::vector<TerrainCollisionPair>* TerrainList = terrManager->GetVisibleTiles();
	size_t listSize = TerrainList->size();
	for(size_t i = 0; i < listSize; ++i)
	{
		Terrain* currentTerrain = TerrainList->at(i).terrain;
		if(currentTerrain)
		{
			float terX, terY, terZ;
			currentTerrain->GetTerrainPosition(terX, terY, terZ);

			float HalfWidth = (currentTerrain->GetTerrainWidth() * currentTerrain->GetTileScale()) * 0.5f;
			XMFLOAT3 tPos(terX + HalfWidth, ((currentTerrain->GetMaxHeight() - currentTerrain->GetMinHeight()) * 0.5f) + currentTerrain->GetMinHeight(), terZ + HalfWidth);

			float halfHeight = (currentTerrain->GetMaxHeight() - currentTerrain->GetMinHeight()) * 0.5f;
			XMFLOAT3 halfExtents(HalfWidth, halfHeight, HalfWidth);
			BoundingBox bb(tPos, halfExtents);

			if(lightBox->Contains(bb))
			{
				++tileCount;
				XMMATRIX modelMatrix = XMMatrixTranslation(terX, terY, terZ);
				m_Renderer->UpdatePerObjectBuffer(lightCamera, &modelMatrix);
				ResourceHandle temp = currentTerrain->GetSplatTextureHandle();
				ID3D11ShaderResourceView* tempHeights = currentTerrain->GetHeightMapTexture()->GetResourceView();
				m_Renderer->GetImmediateContext()->VSSetShaderResources(0, 1, &tempHeights);
				currentTerrain->Render(m_Renderer->GetImmediateContext());

				m_Renderer->GetImmediateContext()->DrawIndexed(currentTerrain->GetIndexCount(), 0, 0);
			}
		}
	}
}

void ModelRenderSystem::SolidRenderStaticMeshes( size_t count, DirectX::BoundingFrustum* camFrustum, Camera* camera )
{
	ResourceManager* res = Services::GetResourceManager();
	for(size_t i = 0; i < count; ++i)
	{
		PositionComponent* pos = m_Positions[i];
		ModelComponent* currentModelComponent = m_Models[i];
		XMVECTOR position = pos->Position;
		XMVECTOR rotation = pos->Orientation;
		XMFLOAT4 floatRot;
		XMFLOAT3 floatPos;
		XMStoreFloat3(&floatPos, position);
		XMStoreFloat4(&floatRot, rotation);		
		Model* currentModel = res->GetModel(currentModelComponent->ModelHandle);
		XMFLOAT3 center(currentModel->m_BoundingBoxCenter);
		XMFLOAT3 extents(currentModel->m_BoundingBoxHalfExtents);
		XMVECTOR cent = XMLoadFloat3(&center);
		cent = XMVectorAdd(cent, position);
		XMStoreFloat3(&center, cent);
		DirectX::BoundingOrientedBox thisBox(center, extents, floatRot);

		if(camFrustum->Intersects(thisBox))
		{
			XMMATRIX modelMatrix = XMMatrixRotationQuaternion(rotation);
			modelMatrix = XMMatrixMultiply(modelMatrix, XMMatrixTranslationFromVector(position));
			m_Renderer->UpdatePerObjectBuffer(camera, &modelMatrix);

			// 4 good number
			ID3D11ShaderResourceView* tex[4];
			Texture* currentTexture[4];
			currentTexture[0] = res->GetTexture(currentModelComponent->DiffuseTextureHandle);
			currentTexture[1] = res->GetTexture(currentModelComponent->NormalTextureHandle);
			currentTexture[2] = res->GetTexture(currentModelComponent->RoughnessTextureHandle);
			currentTexture[3] = res->GetTexture(currentModelComponent->SpecularTextureHandle);

			tex[0] = currentTexture[0]->GetResourceView();
			tex[1] = currentTexture[1]->GetResourceView();
			tex[2] = currentTexture[2]->GetResourceView();
			tex[3] = currentTexture[3]->GetResourceView();
			m_Renderer->GetImmediateContext()->PSSetShaderResources(0, 4, tex);
			currentModel->Render(m_Renderer->GetImmediateContext());
			m_Renderer->GetImmediateContext()->DrawIndexed(currentModel->GetIndexCount(), 0, 0);
		}
	}
}

void ModelRenderSystem::SolidRenderTerrainMeshes(BoundingFrustum* camFrustum, Camera* camera, TerrainManager* terrManager)
{
	ResourceManager* res = Services::GetResourceManager();
	const int texCount = 7;
	ID3D11ShaderResourceView* terrtex[texCount];
	ResourceHandle temp = res->GetTextureHandle("terraindetail_BC7.dds");
	terrtex[0] = res->GetTexture(temp)->GetResourceView();
	temp = res->GetTextureHandle("detailgravel_BC7.dds");
	terrtex[1] = res->GetTexture(temp)->GetResourceView();
	temp = res->GetTextureHandle("woodgrain_BC7.dds");
	terrtex[2] = res->GetTexture(temp)->GetResourceView();
	temp = res->GetTextureHandle("subtlenormals_BC7.dds");
	terrtex[3] = res->GetTexture(temp)->GetResourceView();
	temp = res->GetTextureHandle("conc_slabs01_c_BC7.dds");
	terrtex[5] = res->GetTexture(temp)->GetResourceView();

	int x,z;
	terrManager->CalculateTilePosition(camera->GetPosition(), x, z);

	Terrain::PreRender(m_Renderer->GetImmediateContext());

	const std::vector<TerrainCollisionPair>* TerrainList = terrManager->GetVisibleTiles();
	size_t listSize = TerrainList->size();
	for(size_t i = 0; i < listSize; ++i)
	{
		Terrain* currentTerrain = TerrainList->at(i).terrain;
		if(currentTerrain)
		{
			float terX, terY, terZ;
			currentTerrain->GetTerrainPosition(terX, terY, terZ);

			float HalfWidth = (currentTerrain->GetTerrainWidth() * currentTerrain->GetTileScale()) * 0.5f;
			XMFLOAT3 tPos(terX + HalfWidth, ((currentTerrain->GetMaxHeight() - currentTerrain->GetMinHeight()) * 0.5f) + currentTerrain->GetMinHeight(), terZ + HalfWidth);

			float halfHeight = (currentTerrain->GetMaxHeight() - currentTerrain->GetMinHeight()) * 0.5f;
			XMFLOAT3 halfExtents(HalfWidth, halfHeight, HalfWidth);
			BoundingBox bb(tPos, halfExtents);

			if(camFrustum->Contains(bb))
			{
				XMMATRIX modelMatrix = XMMatrixTranslation(terX, terY, terZ);
				m_Renderer->UpdatePerObjectBuffer(camera, &modelMatrix);
				temp = currentTerrain->GetSplatTextureHandle();
				terrtex[4] = res->GetTexture(temp)->GetResourceView();
				ID3D11ShaderResourceView* tempHeights = currentTerrain->GetHeightMapTexture()->GetResourceView();
				temp = currentTerrain->GetTintTextureHandle();
				terrtex[6] = res->GetTexture(temp)->GetResourceView();

				m_Renderer->GetImmediateContext()->PSSetShaderResources(0,texCount, terrtex);
				m_Renderer->GetImmediateContext()->VSSetShaderResources(0, 1, &tempHeights);
				//m_Renderer->GetImmediateContext()->VSSetShaderResources(1, texCount, terrtex);
				currentTerrain->Render(m_Renderer->GetImmediateContext());

				m_Renderer->GetImmediateContext()->DrawIndexed(currentTerrain->GetIndexCount(), 0, 0);
			}
		}
	}
}
