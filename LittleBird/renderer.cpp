#include "renderer.h"
#include <d3dcommon.h>
#include "model.h"
#include "shader.h"
#include "FPSCounter.h"
#include <memory>
#include "MemoryUtilities.h"
#include "camera.h"
#include "services.h"
#include "texture.h"
#include "GPUProfiler.h"
#include <assert.h>

#include "DirectxTK\SpriteBatch.h"

#include "Shaders/SharedShaderDefines.h"

#include "Shaders/terrainConstants.h"

using namespace std;
using namespace DirectX;

DXGI_FORMAT RenderTargetFormats[] = { 
	DXGI_FORMAT_R8G8B8A8_UNORM,  //diffuse 
	DXGI_FORMAT_R16G16B16A16_FLOAT,  // normal / spec rough
	DXGI_FORMAT_R8G8B8A8_UNORM,	// specreflectance  
};

D3D11_INPUT_ELEMENT_DESC SingleLightLayout[] =
{
	{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	{ "TANGENT", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	{ "BINORMAL", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
};

D3D11_INPUT_ELEMENT_DESC TerrainLayout[] =
{
	{ "TEXCOORD", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
};

D3D11_INPUT_ELEMENT_DESC WaterLayout[] =
{
	{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
};

D3D11_INPUT_ELEMENT_DESC InstanceLayout[] =
{
	{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	{ "TANGENT", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	{ "BINORMAL", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	// instance stuffs
	{ "WORLDMATRIX", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
	{ "WORLDMATRIX", 1, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
	{ "WORLDMATRIX", 2, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
	{ "WORLDMATRIX", 3, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
};

const int ShadowMapRes = 2048;
bool showShadowMap = false;
SpriteBatch* sb;

// TODO:need to get formatted error messages

Renderer::Renderer( uint32_t screenheight, uint32_t screenwidth, HWND hwnd, bool fullScreen, bool vSync): m_FullScreen(fullScreen), m_VsyncEnabled(vSync), m_HWND(hwnd)
{
	// make sure that the number of render targets we specify the format of matches the number that the rest of the renderer expects to see.
	assert(sizeof(RenderTargetFormats)/sizeof(DXGI_FORMAT) == RENDERTARGET_COUNT);

	m_D3dInfoQueue = nullptr;
	m_D3dDebug = nullptr;
	m_SingleLightLayout = nullptr;
	m_DefaultSampler = nullptr;
	m_TerrainSampler = nullptr;
	m_Profiler = nullptr;
	INIReader* ini = Services::GetINIReader();
	ini->SetSection("Graphics_Options");
	m_MSAASampleCount = ini->GetValue("MSAA_Samples", 1);
	m_Use32BitDepth = ini->GetValue("Extended_Depth_Precision", false);
	m_MSAAQuality = 0;
	Create(screenheight, screenwidth);

	sb = new SpriteBatch(m_ImmediateContext);
}

Renderer::~Renderer()
{
	SafeDelete(m_FPSCounter);
	SafeDelete(m_Profiler);
	Shutdown();
}

void Renderer::Create( uint32_t screenheight, uint32_t screenwidth)
{
	// pretty straightforward
	CreateDevice();

	CreateSwapChain(screenwidth, screenheight);

	CreateBackBuffers(screenwidth, screenheight);

	CreateDepthStencil(screenwidth, screenheight);

	CreateViewports(screenwidth, screenheight);

	CreateRasterizers();

	CreateBlendState();

	CreateSamplers();

	CreateConstantBuffers();

	CreateStructuredBuffers(screenwidth, screenheight);

	LoadShaders();

	m_FPSCounter = new FPSCounter();

}

void Renderer::Initialize()
{
	m_FPSCounter->Initialize();
	m_Profiler = new GPUProfiler(m_Device, m_ImmediateContext);
	CreateLightMenu();
	CreateShadowMenu();
	CreateSkyMenu();
	CreateWaterMenu();
}

void Renderer::CreateDevice()
{
	UINT createDeviceFlags = 0;

#if defined(DEBUG) || defined(_DEBUG)
	createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

	D3D_FEATURE_LEVEL featureLevel;
	HRESULT hr = D3D11CreateDevice(
		0,                  // default adapter
		D3D_DRIVER_TYPE_HARDWARE,
		0,                  // no software device
		createDeviceFlags,
		0, 0,               // default feature level array
		D3D11_SDK_VERSION,
		&m_Device,
		&featureLevel,
		&m_ImmediateContext);

	if(FAILED(hr))
	{
		throw L"Could not create device.";
	}
	if(featureLevel < D3D_FEATURE_LEVEL_11_0)
	{
		throw L"Machine does not support D3D11";
	}

	HR(m_Device->CreateDeferredContext(0, &m_DebugContext));
}


void Renderer::CreateSwapChain( uint32_t screenwidth, uint32_t screenheight)
{
	
	IDXGIDevice* dxgiDevice = nullptr;
	HR(m_Device->QueryInterface(__uuidof(IDXGIDevice),
		(void**)&dxgiDevice));

	IDXGIAdapter* dxgiAdapter = nullptr;
	HR(dxgiDevice->GetParent(__uuidof(IDXGIAdapter),
		(void**)&dxgiAdapter));

	// Finally got the IDXGIFactory interface.
	IDXGIFactory* dxgiFactory = nullptr;
	HR(dxgiAdapter->GetParent(__uuidof(IDXGIFactory),
		(void**)&dxgiFactory));

	IDXGIOutput* adapterOutput = nullptr;
	HR(dxgiAdapter->EnumOutputs(0, &adapterOutput));

	DXGI_FORMAT format = DXGI_FORMAT_R8G8B8A8_UNORM;

	uint32_t numModes;
	HR(adapterOutput->GetDisplayModeList(format, DXGI_ENUM_MODES_INTERLACED, &numModes, NULL));

	unique_ptr<DXGI_MODE_DESC[]> displayModeList(new DXGI_MODE_DESC[numModes]);

	adapterOutput->GetDisplayModeList(format, DXGI_ENUM_MODES_INTERLACED, &numModes, displayModeList.get());
	// Now go through all the display modes and find the one that matches the screen width and height.
	// When a match is found store the numerator and denominator of the refresh rate for that monitor.
	unsigned int numerator = 1;
	unsigned int denominator = 0;
	for(uint32_t i=0; i<numModes; i++)
	{
		if(displayModeList[i].Width == screenwidth)
		{
			if(displayModeList[i].Height == screenheight)
			{
				numerator = displayModeList[i].RefreshRate.Numerator;
				denominator = displayModeList[i].RefreshRate.Denominator;
			}
		}
	}

	DXGI_SWAP_CHAIN_DESC sd;
	sd.BufferDesc.Width = screenwidth; // use window's client area dims
	sd.BufferDesc.Height = screenheight;

	// Set the refresh rate of the back buffer.
	if(m_VsyncEnabled)
	{
		sd.BufferDesc.RefreshRate.Numerator = numerator;
		sd.BufferDesc.RefreshRate.Denominator = denominator;
	}
	else
	{
		sd.BufferDesc.RefreshRate.Numerator = 1;
		sd.BufferDesc.RefreshRate.Denominator = 0;
	}

	sd.BufferDesc.Format = format;
	sd.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	sd.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;

	UINT levels;
	//msaa
	HR(m_Device->CheckMultisampleQualityLevels(format, m_MSAASampleCount,  &levels));
	if(levels)
	{
		sd.SampleDesc.Count = m_MSAASampleCount;
		sd.SampleDesc.Quality = m_MSAAQuality;
	}
	else
	{
		sd.SampleDesc.Count = 1;
		sd.SampleDesc.Quality = 0;
		throw string("MSAA level " + to_string(m_MSAASampleCount) + " is not supported by your hardware");
	}

	sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT | DXGI_USAGE_UNORDERED_ACCESS;
	sd.BufferCount = 1;
	sd.OutputWindow = m_HWND;
	// Set to full screen or windowed mode.
	if(m_FullScreen)
	{
		sd.Windowed = false;
	}
	else
	{
		sd.Windowed = true;
	}
	sd.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
	sd.Flags = 0;

	if( SUCCEEDED( m_Device->QueryInterface( __uuidof(ID3D11Debug), (void**)&m_D3dDebug ) ) )
	{
		if( SUCCEEDED( m_D3dDebug->QueryInterface( __uuidof(ID3D11InfoQueue), (void**)&m_D3dInfoQueue ) ) )
		{
#ifdef _DEBUG
			m_D3dInfoQueue->SetBreakOnSeverity( D3D11_MESSAGE_SEVERITY_CORRUPTION, true );
			m_D3dInfoQueue->SetBreakOnSeverity( D3D11_MESSAGE_SEVERITY_ERROR, true );
			m_D3dInfoQueue->SetBreakOnSeverity( D3D11_MESSAGE_SEVERITY_WARNING, true );
#endif
			D3D11_MESSAGE_ID hide [] =
			{
				D3D11_MESSAGE_ID_SETPRIVATEDATA_CHANGINGPARAMS,
				D3D11_MESSAGE_ID_DEVICE_DRAW_CONSTANT_BUFFER_TOO_SMALL,
				D3D11_MESSAGE_ID_DEVICE_OMSETRENDERTARGETS_HAZARD,
				D3D11_MESSAGE_ID_DEVICE_PSSETSHADERRESOURCES_HAZARD,

				// Add more message IDs here as needed
			};

			D3D11_INFO_QUEUE_FILTER filter;
			memset( &filter, 0, sizeof(filter) );
			filter.DenyList.NumIDs = _countof(hide);
			filter.DenyList.pIDList = hide;
			m_D3dInfoQueue->AddStorageFilterEntries( &filter );	
		}
	}

	// Now, create the swap chain.
	HR(dxgiFactory->CreateSwapChain(m_Device, &sd, &m_SwapChain));

	// Release our acquired COM interfaces (because we are done with them).
	SafeRelease(dxgiDevice);
	SafeRelease(dxgiAdapter);
	SafeRelease(dxgiFactory);
}

void Renderer::CreateRasterizers()
{
	D3D11_RASTERIZER_DESC rasterDesc;

	// Setup the raster description which will determine how and what polygons will be drawn.
	rasterDesc.AntialiasedLineEnable = false;
	rasterDesc.CullMode = D3D11_CULL_BACK;
	rasterDesc.DepthBias = 0;
	rasterDesc.DepthBiasClamp = 0.0f;
	rasterDesc.DepthClipEnable = true;
	rasterDesc.FillMode = D3D11_FILL_SOLID;
	rasterDesc.FrontCounterClockwise = false;
	rasterDesc.MultisampleEnable = m_MSAASampleCount > 1 ? true : false;
	rasterDesc.ScissorEnable = false;
	rasterDesc.SlopeScaledDepthBias = 0.0f;

	// Create the rasterizer state from the description we just filled out.
	HR(m_Device->CreateRasterizerState(&rasterDesc, &m_RasterState));
	m_CurrentSolidRasterstate = m_RasterState;
	m_ImmediateContext->RSSetState(m_RasterState);

	rasterDesc.CullMode = D3D11_CULL_NONE;
	HR(m_Device->CreateRasterizerState(&rasterDesc, &m_NoCullRasterState));
	rasterDesc.CullMode = D3D11_CULL_BACK;

	//create wireframe state
	rasterDesc.FillMode = D3D11_FILL_WIREFRAME;
	HR(m_Device->CreateRasterizerState(&rasterDesc, &m_WireframeRasterState));
}

void Renderer::CreateBackBuffers(uint32_t screenwidth, uint32_t screenheight)
{
	m_SwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D),
		reinterpret_cast<void**>(&m_BackBufferTexture));
	HR(m_Device->CreateRenderTargetView(m_BackBufferTexture, 0, &m_BackBufferRenderTargetView));

		
	for (int i = 0; i < RENDERTARGET_COUNT; ++i)
	{	

		// Setup the render target texture description.
		D3D11_TEXTURE2D_DESC textureDesc;
		ZeroMemory(&textureDesc, sizeof(textureDesc));
		textureDesc.Width = screenwidth;
		textureDesc.Height = screenheight;
		textureDesc.MipLevels = 1;
		textureDesc.ArraySize = 1;
		textureDesc.Format = RenderTargetFormats[i];
		textureDesc.SampleDesc.Count = m_MSAASampleCount;
		textureDesc.Usage = D3D11_USAGE_DEFAULT;
		textureDesc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
		textureDesc.CPUAccessFlags = 0;
		textureDesc.MiscFlags = 0;
		// Initialize the render target texture description.

		D3D11_RENDER_TARGET_VIEW_DESC renderTargetViewDesc;
		// Setup the description of the render target view.
		renderTargetViewDesc.Format = textureDesc.Format;
		renderTargetViewDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
		renderTargetViewDesc.Texture2D.MipSlice = 0;

		D3D11_SHADER_RESOURCE_VIEW_DESC shaderResourceViewDesc;
		// Setup the description of the shader resource view.
		shaderResourceViewDesc.Format = textureDesc.Format;
		shaderResourceViewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
		shaderResourceViewDesc.Texture2D.MostDetailedMip = 0;
		shaderResourceViewDesc.Texture2D.MipLevels = 1;

		HR( m_Device->CreateTexture2D(&textureDesc, NULL, &m_RenderTextures[i]));

		HR(m_Device->CreateRenderTargetView(m_RenderTextures[i], &renderTargetViewDesc, &m_RenderTargetViews[i]));

		HR(m_Device->CreateShaderResourceView(m_RenderTextures[i], &shaderResourceViewDesc, &m_RendertargetShaderResourceViews[i]));
	}	
}

void Renderer::CreateDepthStencil( uint32_t screenwidth, uint32_t screenheight )
{
	D3D11_TEXTURE2D_DESC depthStencilDesc;
	depthStencilDesc.Width     = screenwidth;
	depthStencilDesc.Height    = screenheight;
	depthStencilDesc.MipLevels = 1;
	depthStencilDesc.ArraySize = 1;
	depthStencilDesc.Format    =  m_Use32BitDepth ? DXGI_FORMAT_R32_TYPELESS : DXGI_FORMAT_R24G8_TYPELESS;

	depthStencilDesc.SampleDesc.Count = m_MSAASampleCount;
	depthStencilDesc.SampleDesc.Quality = m_MSAAQuality;

	depthStencilDesc.Usage          = D3D11_USAGE_DEFAULT;
	depthStencilDesc.BindFlags      = D3D11_BIND_DEPTH_STENCIL | D3D11_BIND_SHADER_RESOURCE;
	depthStencilDesc.CPUAccessFlags = 0;
	depthStencilDesc.MiscFlags      = 0;

	HR(m_Device->CreateTexture2D(&depthStencilDesc,	0, &m_DepthStencilBuffer));        


	depthStencilDesc.Width     = ShadowMapRes;
	depthStencilDesc.Height    = ShadowMapRes;
	depthStencilDesc.BindFlags      = D3D11_BIND_DEPTH_STENCIL | D3D11_BIND_SHADER_RESOURCE;
	depthStencilDesc.Format    = m_Use32BitDepth ? DXGI_FORMAT_R32_TYPELESS : DXGI_FORMAT_R24G8_TYPELESS;
	HR(m_Device->CreateTexture2D(&depthStencilDesc, 0, &m_ShadowDepthStencilTexture));

	D3D11_DEPTH_STENCIL_VIEW_DESC dsvd;
	ZeroMemory(&dsvd, sizeof(dsvd));
	dsvd.Format = m_Use32BitDepth ? DXGI_FORMAT_D32_FLOAT : DXGI_FORMAT_D24_UNORM_S8_UINT;
	dsvd.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	dsvd.Texture2D.MipSlice = 0;

	HR(m_Device->CreateDepthStencilView(m_DepthStencilBuffer, &dsvd, &m_DepthStencilView));
	HR(m_Device->CreateDepthStencilView(m_ShadowDepthStencilTexture, &dsvd, &m_ShadowDepthStencilView));

	dsvd.Flags = D3D11_DSV_READ_ONLY_DEPTH;
	HR(m_Device->CreateDepthStencilView(m_DepthStencilBuffer, &dsvd, &m_ReadOnlyDepthStencilView));

	m_ImmediateContext->OMSetRenderTargets(1, &m_BackBufferRenderTargetView, m_DepthStencilView);
	m_DebugContext->OMSetRenderTargets(1, &m_BackBufferRenderTargetView, m_DepthStencilView);

	D3D11_SHADER_RESOURCE_VIEW_DESC shaderResourceViewDesc;
	// Setup the description of the shader resource view.
	shaderResourceViewDesc.Format =m_Use32BitDepth ? DXGI_FORMAT_R32_FLOAT : DXGI_FORMAT_R24_UNORM_X8_TYPELESS;
	shaderResourceViewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	shaderResourceViewDesc.Texture2D.MostDetailedMip = 0;
	shaderResourceViewDesc.Texture2D.MipLevels = 1;

	// Create the shader resource view.
	HR(m_Device->CreateShaderResourceView(m_ShadowDepthStencilTexture, &shaderResourceViewDesc, &m_ShadowShaderResourceView));
	HR(m_Device->CreateShaderResourceView(m_DepthStencilBuffer, &shaderResourceViewDesc, &m_DepthStencilShaderResourceView));


	D3D11_DEPTH_STENCIL_DESC depthStencilStateDesc;
	// Initialize the description of the stencil state.
	ZeroMemory(&depthStencilStateDesc, sizeof(depthStencilStateDesc));

	// Set up the description of the stencil state.
	depthStencilStateDesc.DepthEnable = true;
	depthStencilStateDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	depthStencilStateDesc.DepthFunc = D3D11_COMPARISON_LESS;

	depthStencilStateDesc.StencilEnable = false;
	depthStencilStateDesc.StencilReadMask = 0xFF;
	depthStencilStateDesc.StencilWriteMask = 0xFF;

	// Stencil operations if pixel is front-facing.
	depthStencilStateDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthStencilStateDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	depthStencilStateDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthStencilStateDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	// Stencil operations if pixel is back-facing.
	depthStencilStateDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthStencilStateDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	depthStencilStateDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthStencilStateDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	// Create the depth stencil state.
	HR(m_Device->CreateDepthStencilState(&depthStencilStateDesc, &m_DepthStencilState));

	//depthStencilStateDesc.DepthEnable = false;
	depthStencilStateDesc.DepthFunc = D3D11_COMPARISON_LESS_EQUAL;

	HR(m_Device->CreateDepthStencilState(&depthStencilStateDesc, &m_SkyBoxDepthStencilState));

	// Set the depth stencil state.
	m_ImmediateContext->OMSetDepthStencilState(m_DepthStencilState, 1);
	m_DebugContext->OMSetDepthStencilState(m_DepthStencilState, 1);
}

void Renderer::CreateViewports( uint32_t screenwidth, uint32_t screenheight )
{
	m_CameraViewport.TopLeftX = 0.0f;
	m_CameraViewport.TopLeftY = 0.0f;
	m_CameraViewport.Width = static_cast<float>(screenwidth);
	m_CameraViewport.Height = static_cast<float>(screenheight);
	m_CameraViewport.MinDepth = 0.0f;
	m_CameraViewport.MaxDepth = 1.0f;

	m_ShadowViewport.TopLeftX = 0.0f;
	m_ShadowViewport.TopLeftY = 0.0f;
	m_ShadowViewport.Width = static_cast<float>(ShadowMapRes);
	m_ShadowViewport.Height = static_cast<float>(ShadowMapRes);
	m_ShadowViewport.MinDepth = 0.0f;
	m_ShadowViewport.MaxDepth = 1.0f;

	m_ImmediateContext->RSSetViewports(1, &m_CameraViewport);
	m_DebugContext->RSSetViewports(1, &m_CameraViewport);
}

void Renderer::CreateBlendState()
{
	float blendFactor[4] = {0.0f};

	D3D11_BLEND_DESC blendDesc;
	ZeroMemory(&blendDesc, sizeof(blendDesc));
	blendDesc.RenderTarget[0].BlendEnable = true;
	blendDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
	blendDesc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
	blendDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	blendDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_MAX;
	blendDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
	blendDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ONE;
	blendDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;



	m_Device->CreateBlendState(&blendDesc, &m_WaterBlendState);

	m_ImmediateContext->OMSetBlendState(nullptr, blendFactor, D3D11_COLOR_WRITE_ENABLE_ALL);
}

void Renderer::CreateStructuredBuffers(uint32_t screenwidth, uint32_t screenheight)
{
	unsigned count = screenwidth * screenheight;

	const unsigned lightBufferPixelDepth = 64;
	CD3D11_BUFFER_DESC desc(lightBufferPixelDepth * count, D3D11_BIND_UNORDERED_ACCESS | D3D11_BIND_SHADER_RESOURCE,
		D3D11_USAGE_DEFAULT, 0, D3D11_RESOURCE_MISC_BUFFER_STRUCTURED, lightBufferPixelDepth);

	m_Device->CreateBuffer(&desc, 0, &m_LightAccumulationBuffer);
	m_Device->CreateUnorderedAccessView(m_LightAccumulationBuffer, 0, &m_LightAccumulationBufferUnorderedAccessView);
	m_Device->CreateShaderResourceView(m_LightAccumulationBuffer, 0, &m_LightAccumulationResourceView);

	D3D11_UNORDERED_ACCESS_VIEW_DESC unorderedDesc;
	ZeroMemory(&unorderedDesc, sizeof(unorderedDesc));
	unorderedDesc.ViewDimension = D3D11_UAV_DIMENSION_TEXTURE2D;
	unorderedDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	//unorderedDesc.
	m_Device->CreateUnorderedAccessView(m_BackBufferTexture, &unorderedDesc, &m_BackBufferUnorderedAccessView);
}

void Renderer::CreateConstantBuffers()
{
	D3D11_BUFFER_DESC bufferDesc;

	// camera buffer
	bufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	bufferDesc.ByteWidth = sizeof(PerFrameBuffer);
	bufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	bufferDesc.MiscFlags = 0;
	bufferDesc.StructureByteStride = 0;

	// Create the constant buffer pointer so we can access the vertex shader constant buffer from within this class.
	HR(m_Device->CreateBuffer(&bufferDesc, nullptr, &m_PerFrameBuffer));


	bufferDesc.ByteWidth = sizeof(PerObjectBuffer);
	HR(m_Device->CreateBuffer(&bufferDesc, nullptr, &m_PerObjectBuffer));

	bufferDesc.ByteWidth = sizeof(LightBuffer);
	HR(m_Device->CreateBuffer(&bufferDesc, nullptr, &m_lightBuffer));

	bufferDesc.ByteWidth = sizeof(AmbientBuffer);
	HR(m_Device->CreateBuffer(&bufferDesc, nullptr, &m_AmbientBuffer));

	bufferDesc.ByteWidth = sizeof(SkyBuffer);
	HR(m_Device->CreateBuffer(&bufferDesc, nullptr, &m_SkyBuffer));

	bufferDesc.ByteWidth = sizeof(WaterBuffer);
	HR(m_Device->CreateBuffer(&bufferDesc, nullptr, &m_WaterBuffer));
}


void Renderer::LoadShaders()
{
	unsigned numElements = sizeof(SingleLightLayout) / sizeof(SingleLightLayout[0]);
	LoadShader("SingleLight", &m_SingleLightVertexShader, &m_SingleLightPixelShader, SingleLightLayout, numElements, &m_SingleLightLayout);

	numElements = sizeof(TerrainLayout) / sizeof(TerrainLayout[0]);
	LoadShader("terrain", &m_TerrainVertexShader, &m_TerrainPixelShader, TerrainLayout, numElements, &m_TerrainLayout);

	numElements = sizeof(InstanceLayout) / sizeof(InstanceLayout[0]);
	LoadShader("InstanceGBuffer", &m_InstanceGBufferVertexShader, &m_InstanceGBufferPixelShader, InstanceLayout, numElements, &m_InstanceLayout);
	LoadShader("InstanceGBufferAlphaTest", nullptr, &m_InstanceGBufferAlphaTestPixelShader, nullptr, 0, nullptr);
	LoadShader("InstanceShadowMap", &m_InstanceShadowMapVertexShader, nullptr, nullptr, 0, nullptr);

	LoadShader("Gbuffer", &m_GBufferVertexShader, &m_GBufferPixelShader, nullptr, 0, nullptr);
	LoadShader("GBuffer_AlphaTest", nullptr, &m_GBufferAlphaTestPixelShader, nullptr, 0, nullptr);
	LoadShader("SkySphere", &m_SkySphereVertexShader, &m_SkySpherePixelShader, nullptr, 0, nullptr);
	LoadShader("Shadowmap", &m_ShadowMapVertexShader, nullptr, nullptr, 0, nullptr);

	numElements = sizeof(WaterLayout) / sizeof(WaterLayout[0]);
	LoadShader("Water", &m_WaterVertexShader, &m_WaterPixelShader, nullptr, 0, nullptr);

	LoadDeferredShadingShader();

	LoadShader("fullscreentriangle", &m_FullScreenTriangleVertexShader, nullptr, nullptr, 0, nullptr);

}

void Renderer::BeginFrame( float red, float green, float blue, float alpha )
{
	DirectX::XMFLOAT4 color(red, green, blue, alpha);

	m_ImmediateContext->OMSetBlendState(nullptr, nullptr, 0xffffffff);
	//m_ImmediateContext->RSSetState(m_RasterState);
	m_ImmediateContext->OMSetDepthStencilState(m_DepthStencilState, 1);
	m_ImmediateContext->RSSetState(m_CurrentSolidRasterstate);

	//string name = "ClearingBuffers";
	//StartProfileSection(name);
	//for (int i = 0; i < RENDERTARGET_COUNT; ++i)
	//{
	//	m_ImmediateContext->ClearRenderTargetView(m_RenderTargetViews[i], (FLOAT*)&color);
	//}
	//	EndProfileSection(name);
	m_ImmediateContext->ClearDepthStencilView(m_DepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);
	m_ImmediateContext->ClearDepthStencilView(m_ShadowDepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);

}

void Renderer::EndFrame()
{
	if(showShadowMap)
	{
		sb->Begin();
		XMFLOAT2 drawPos(540.0f, 256.0f);
		RECT sourceRect = { 0, 0, ShadowMapRes, ShadowMapRes};
		RECT destRect = {540, 256, 540+ 256, 256+256};

		sb->Draw(m_ShadowShaderResourceView, destRect, &sourceRect );
		sb->End();
	}

	if(m_VsyncEnabled)
	{
		m_SwapChain->Present(1, 0);
	}
	else
	{
		m_SwapChain->Present(0, 0);
	}

	m_FPSCounter->Frame();
	if(m_Profiler)
		m_Profiler->EndFrame();
}

void Renderer::Shutdown()
{
	SafeRelease(m_ImmediateContext);
	SafeRelease(m_DepthStencilBuffer);
	SafeRelease(m_DepthStencilState);
	SafeRelease(m_DepthStencilView);
	SafeRelease(m_RenderTargetViews[0]);
	SafeRelease(m_RasterState);
	SafeRelease(m_SwapChain);
	SafeRelease(m_D3dInfoQueue);
	SafeRelease(m_D3dDebug);
	SafeRelease(m_Device);
	
}

void Renderer::CreateSamplers()
{
	D3D11_SAMPLER_DESC samplerDesc;

	//create default sampler
	samplerDesc.Filter = D3D11_FILTER_ANISOTROPIC;
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
	samplerDesc.MipLODBias = 0.0f;
	samplerDesc.MaxAnisotropy = 8;
	samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	samplerDesc.BorderColor[0] = 1.0f;
	samplerDesc.BorderColor[1] = 1.0f;
	samplerDesc.BorderColor[2] = 1.0f;
	samplerDesc.BorderColor[3] = 1.0f;
	samplerDesc.MinLOD = 0;
	samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;
	HR(m_Device->CreateSamplerState(&samplerDesc, &m_DefaultSampler));

	m_ImmediateContext->CSSetSamplers(0, 1, &m_DefaultSampler);

	// then create terrain sampler
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	HR(m_Device->CreateSamplerState(&samplerDesc, &m_TerrainSampler));
	
	//samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	HR(m_Device->CreateSamplerState(&samplerDesc, &m_NormalSampler));

	m_ImmediateContext->PSSetSamplers(1, 1, &m_NormalSampler);
	m_ImmediateContext->CSSetSamplers(1, 1, &m_NormalSampler);

	samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	//samplerDesc.MipLODBias = 3.0f;
	HR(m_Device->CreateSamplerState(&samplerDesc, &m_CubeSampler));

	m_ImmediateContext->PSSetSamplers(2, 1, &m_CubeSampler);
	m_ImmediateContext->CSSetSamplers(2, 1, &m_CubeSampler);

	// and then the one for shadows
	// todo: border?
	samplerDesc.Filter = D3D11_FILTER_COMPARISON_MIN_MAG_MIP_POINT;
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_BORDER;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_BORDER;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_BORDER;
	samplerDesc.ComparisonFunc = D3D11_COMPARISON_LESS_EQUAL;
	
	HR(m_Device->CreateSamplerState(&samplerDesc, &m_ShadowSampler));
	m_ImmediateContext->PSSetSamplers(3, 1, &m_ShadowSampler);
	m_ImmediateContext->CSSetSamplers(3, 1, &m_ShadowSampler);
}

void Renderer::UpdatePerObjectBuffer(Camera* cam, XMMATRIX* modelMatrix)
{
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	// Lock the camera constant buffer so it can be written to.
	HR(m_ImmediateContext->Map(m_PerObjectBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource));

	// Get a pointer to the data in the constant buffer.
	PerObjectBuffer* data;
	data = (PerObjectBuffer*)mappedResource.pData;

	// Copy the camera position into the constant buffer.
	data->WorldViewProjectionMatrix = XMMatrixTranspose(*modelMatrix * cam->GetViewProjectionMatrix());
	data->WorldViewMatrix = XMMatrixTranspose(*modelMatrix * cam->GetViewMatrix());
	data->WorldMatrix = XMMatrixTranspose(*modelMatrix);

	// Unlock the camera constant buffer.
	m_ImmediateContext->Unmap(m_PerObjectBuffer, 0);

	// Now set the camera constant buffer in the vertex shader with the updated values.
	m_ImmediateContext->VSSetConstantBuffers(1, 1, &m_PerObjectBuffer);
	m_ImmediateContext->PSSetConstantBuffers(1, 1, &m_PerObjectBuffer);
}

void Renderer::UpdatePerFrameBuffer( Camera* cam, Camera* lightCam, float time )
{
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	HR(m_ImmediateContext->Map(m_PerFrameBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource));

	PerFrameBuffer* data;
	data = (PerFrameBuffer*)mappedResource.pData;

	data->CameraWorldMatrix = XMMatrixTranspose(cam->GetWorldMatrix());
	data->ViewMatrix = XMMatrixTranspose(cam->GetViewMatrix());
	data->ViewProjectionMatrix = XMMatrixTranspose(cam->GetViewProjectionMatrix());
	data->ProjectionMatrix = XMMatrixTranspose(cam->GetProjectionMatrix());
	data->CameraPosition = cam->GetPosition();
	data->LightViewProjectionMatrix = XMMatrixTranspose(lightCam->GetViewProjectionMatrix());
	data->AbsoluteTime = time;

	m_ImmediateContext->Unmap(m_PerFrameBuffer, 0);

	m_ImmediateContext->VSSetConstantBuffers(0, 1, &m_PerFrameBuffer);
	m_ImmediateContext->PSSetConstantBuffers(0, 1, &m_PerFrameBuffer);
	m_ImmediateContext->CSSetConstantBuffers(0, 1, &m_PerFrameBuffer);
}

// todo: remove this
XMFLOAT4 lightPosition( 0.0f, -1.0f, 0.0f, 2.0f );
XMFLOAT4 lightDirection( sqrt(1.0f/3.0f), sqrt(1.0f/3.0f), sqrt(1.0f/3.0f), 0.0f );
XMFLOAT4 diffusecolor(0.5f, 0.5f, 0.5f, 1.0f);
XMFLOAT4 specularColor(0.03f, 0.03f, 0.03f, 0.05);
XMMATRIX directionalMatrix;

void Renderer::UpdateLightBuffer(Camera* cam, XMVECTOR orthExts)
{
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	// Lock the light constant buffer so it can be written to.
	HR(m_ImmediateContext->Map(m_lightBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource));

	// Get a pointer to the data in the constant buffer.
	LightBuffer* data;
	data = (LightBuffer*)mappedResource.pData;

	data->diffuseColor = diffusecolor;
	data->ambientColor = XMFLOAT4(0,0,0,0);
	//XMStoreFloat3(&data->lightDirection, XMVector3Rotate(XMVector3Normalize(XMVectorSet(-1.0f, -1.0f, -1.0f, 1.0f)), XMQuaternionRotationRollPitchYaw(-piss * 2.5f, -piss, piss * 0.25f)));
	//XMStoreFloat3(&data->lightDirection, XMVector3Normalize(XMVectorSet(-1.0f, -1.0f, -1.0f, 1.0f)));
	XMVECTOR tempVec = XMLoadFloat4(&lightDirection);
	tempVec = XMVector3Transform(tempVec, XMMatrixLookAtLH(XMVectorSet(0.0f, 0.0f, 0.0f, 1.0f), cam->GetForwardVector(), XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f)));
	XMStoreFloat4(&data->lightDirection, tempVec);
	//data->lightDirection = lightDirection;
	data->lightPosition = lightPosition;
	data->specularColor = specularColor;
	data->directionalMatrix = directionalMatrix;
	XMStoreFloat2(&data->orhographicExtents, orthExts);

	// Unlock the camera constant buffer.
	m_ImmediateContext->Unmap(m_lightBuffer, 0);

	// Now set the camera constant buffer in the vertex shader with the updated values.
	m_ImmediateContext->PSSetConstantBuffers(2, 1, &m_lightBuffer);
	m_ImmediateContext->VSSetConstantBuffers(2, 1, &m_lightBuffer);
	m_ImmediateContext->CSSetConstantBuffers(2, 1, &m_lightBuffer);
}

XMFLOAT4 skycolor(0.1f, 0.1f, 0.1f, 0.0f);
XMFLOAT4 groundcolor(0.03f, 0.03f, 0.03f, 0.0f);
XMFLOAT4 horizoncolor(0.05f, 0.05f, 0.05f, 0.0f);
XMFLOAT4 fogColor(0.5f,0.6f,0.7f,1.0f);
XMFLOAT4 sunFogColor(1.0f,0.9f,0.7f,1.0f);
XMFLOAT4 coefecients(0.05f, 0.004f, 000.0f, -0.0008f);
XMFLOAT4 testThings( 0.4f, 0.75f, -0.01f, 60.0f);

void Renderer::CreateLightMenu()
{
	DebugUI* deb = Services::GetDebugUI();

	string name = "lights";
	deb->CreateMenu(name);
	deb->AddReadWriteArrow(name, "position", &lightPosition);
	deb->AddReadWriteColorFloat3(name, "DirLightBrightness", &diffusecolor);
	deb->AddReadWriteColorFloat3(name, "SpecularAlbedo", &specularColor);
	deb->AddReadWriteFloat(name, "Fresnel F0", &specularColor.w, 0.001, 5, 0.001);
	deb->AddReadWriteColorFloat3(name, "skycolor", &skycolor);
	deb->AddReadWriteColorFloat3(name, "groundcolor", &groundcolor);
	deb->AddReadWriteColorFloat3(name, "horizoncolor", &horizoncolor);
	deb->AddReadWriteColorFloat3(name, "FogColor", &fogColor);
	deb->AddReadWriteColorFloat3(name, "SunFogColor", &sunFogColor);
	deb->AddReadWriteFloat(name, "exposure", &lightPosition.w, 0.001f, 5.0f, 0.01f);
	deb->AddReadWriteFloat(name, "c", &coefecients.x, 0.0f, 10.0f, 0.000001f);
	deb->AddReadWriteFloat(name, "b", &coefecients.y, 0.0f, 10.0f, 0.000001f);
	deb->AddReadWriteFloat(name, "yOffset", &coefecients.z, -8000.0f, 8000.0f, 1.0f);
	deb->AddReadWriteFloat(name, "sunPower", &coefecients.w, -1.0f, 1.0f, 0.0001f);
	deb->AddReadWriteFloat(name, "testX", &testThings.x, 0.0f, 100.0f, 0.1f);
	deb->AddReadWriteFloat(name, "testY", &testThings.y, 0.0f, 100.0f, 0.1f);
	deb->AddReadWriteFloat(name, "testZ", &testThings.z, 0.0f, 1000.0f, 0.1f);
	deb->AddReadWriteFloat(name, "testW", &testThings.w, 0.0f, 500.0f, 0.1f);

}

float RayleighFactor = 0.001f;
float MieFactor = 0.9999f;
float SurfaceHeight = 0.95f;
float SpotBrightness = 1.2f;
unsigned StepCount = 5;
float ScatterStrength = 1.0f;
float RayleighScatterStrength = 0.3f;
float MieScatterStrength = 0.4f;
float RayLeighCollectionPower = 0.03f;
float MieCollectionPower = 0.03f;

void Renderer::UpdateSkyBuffer()
{
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	// Lock the light constant buffer so it can be written to.
	HR(m_ImmediateContext->Map(m_SkyBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource));

	// Get a pointer to the data in the constant buffer.
	SkyBuffer* data;
	data = (SkyBuffer*)mappedResource.pData;

	data->RayleighFactor = RayleighFactor;
	data->MieFactor = MieFactor;
	data->SurfaceHeight = SurfaceHeight;
	data->SpotBrightness = SpotBrightness;
	data->StepCount = StepCount;
	data->ScatterStrength = ScatterStrength;
	data->RayleighScatterStrength = RayleighScatterStrength;
	data->MieScatterStrength = MieScatterStrength;
	data->RayLeighCollectionPower = RayLeighCollectionPower;
	data->MieCollectionPower = MieCollectionPower;

	// Unlock the camera constant buffer.
	m_ImmediateContext->Unmap(m_SkyBuffer, 0);

	m_ImmediateContext->CSSetConstantBuffers(4, 1, &m_SkyBuffer);
}

float AmpFactor = 1.0f;
float BaseFactor = 16.0f;
float WaveTimeScaler = 0.5f;
float WaterFadeFactor = 200.0f;
float BigWaveScrollDirection[2] = { 0.0f, 0.05f };
float SmallWaveScrollDirection[2] = { -0.2f, 0.0f};
XMFLOAT4 DeepColor(0.18867780436772762f, 0.5978442963618773f, 0.6616065586417131f, 0.0f);
XMFLOAT4 ShallowColor(0.18867780436772762f, 0.6978442963618773f, 0.6616065586417131f, 0.0f);

void Renderer::UpdateWaterBuffer()
{
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	// Lock the light constant buffer so it can be written to.
	HR(m_ImmediateContext->Map(m_WaterBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource));

	// Get a pointer to the data in the constant buffer.
	WaterBuffer* data;
	data = (WaterBuffer*)mappedResource.pData;
	
	data->AmpFactor = AmpFactor;
	data->BaseFactor = BaseFactor;
	data->WaveTimeScaler = WaveTimeScaler;
	data->WaterFadeFactor = WaterFadeFactor;
	data->BigWaveScrollDirection[0] = BigWaveScrollDirection[0];
	data->BigWaveScrollDirection[1] = BigWaveScrollDirection[1];
	data->SmallWaveScrollDirection[0] = SmallWaveScrollDirection[0];
	data->SmallWaveScrollDirection[1] = SmallWaveScrollDirection[1];
	data->DeepColor = DeepColor;
	data->ShallowColor = ShallowColor;

	m_ImmediateContext->Unmap(m_WaterBuffer, 0);

	m_ImmediateContext->VSSetConstantBuffers(4, 1, &m_WaterBuffer);
	m_ImmediateContext->PSSetConstantBuffers(4, 1, &m_WaterBuffer);
}


void Renderer::CreateWaterMenu()
{
	DebugUI* deb = Services::GetDebugUI();

	string name = "water";
	deb->CreateMenu(name);
	deb->AddReadWriteFloat(name, "AmpFactor", &AmpFactor, 0.0f, 10.0f, 0.1f);
	deb->AddReadWriteFloat(name, "BaseFactor", &BaseFactor, 0.0f, 1000.0f, 1.0f);
	deb->AddReadWriteFloat(name, "WaveTime Scaler", &WaveTimeScaler, 0.0f, 2.0, 0.01f);
	deb->AddReadWriteFloat(name, "WaterFadeFactor", &WaterFadeFactor, 0.0f, 600.0, 1.0f);
	deb->AddReadWriteFloat(name, "BigWaveScrollDirectionX", &BigWaveScrollDirection[0], -10.0f, 10.0f, 0.001f);
	deb->AddReadWriteFloat(name, "BigWaveScrollDirectionZ", &BigWaveScrollDirection[1], -10.0f, 10.0f, 0.001f);
	deb->AddReadWriteFloat(name, "SmallWaveScrollDirectionX", &SmallWaveScrollDirection[0], -10.0f, 10.0f, 0.1f);
	deb->AddReadWriteFloat(name, "SmallWaveScrollDirectionZ", &SmallWaveScrollDirection[1], -10.0f, 10.0f, 0.1f);
	deb->AddReadWriteColorFloat3(name, "DeepColor", &DeepColor);
	deb->AddReadWriteColorFloat3(name, "ShallowColor", &ShallowColor);
}


void Renderer::CreateSkyMenu()
{
	DebugUI* deb = Services::GetDebugUI();

	string name = "sky";
	deb->CreateMenu(name);
	deb->AddReadWriteFloat(name, "RayleighFactor", &RayleighFactor, -1.0f, 1.0f, 0.001f);
	deb->AddReadWriteFloat(name, "MieFactor", &MieFactor, -1.0f, 1.0f, 0.001f);
	deb->AddReadWriteFloat(name, "SurfaceHeight", &SurfaceHeight, -1.0f, 1.0f, 0.001f);
	deb->AddReadWriteFloat(name, "SpotBrightness", &SpotBrightness, 0.0f, 10.0f, 0.1f);
	deb->AddReadWriteUInt(name, "StepCount", &StepCount);
	deb->AddReadWriteFloat(name, "ScatterStrength", &ScatterStrength, 0.0f, 1.0f, 0.01f);
	deb->AddReadWriteFloat(name, "RayleighScatterStrength", &RayleighScatterStrength, 0.0f, 10.0f, 0.01f);
	deb->AddReadWriteFloat(name, "MieScatterStrength", &MieScatterStrength, 0.0f, 2.0f, 0.01f);
	deb->AddReadWriteFloat(name, "RayLeighCollectionPower", &RayLeighCollectionPower, 0.0f, 2.0f, 0.01f);
	deb->AddReadWriteFloat(name, "MieCollectionPower", &MieCollectionPower, 0.0f, 2.0f, 0.01f);
}

void Renderer::CreateShadowMenu()
{
	DebugUI* deb = Services::GetDebugUI();

	string name = "Shadows";
	deb->CreateMenu(name);
	deb->AddReadWriteArrow(name, "direction", &lightDirection);
	deb->AddReadWriteBool(name, "ShowShadowMap", &showShadowMap);

}

void Renderer::UpdateAmbientBuffer(XMFLOAT4 sky, XMFLOAT4 ground, XMFLOAT4 horizon, XMFLOAT4 fogC, XMFLOAT4 sunFogC, XMFLOAT4 Coefs)
{
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	// Lock the light constant buffer so it can be written to.
	HR(m_ImmediateContext->Map(m_AmbientBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource));

	AmbientBuffer* data;
	data = (AmbientBuffer*)mappedResource.pData;

	data->sky = sky;
	data->ground = ground;
	data->horizon = horizon;
	data->fogColor = fogC;
	data->sunFogColor = sunFogC;
	data->coefs = Coefs;

	m_ImmediateContext->Unmap(m_AmbientBuffer, 0);
	
	m_ImmediateContext->PSSetConstantBuffers(3, 1, &m_AmbientBuffer);
	m_ImmediateContext->CSSetConstantBuffers(3, 1, &m_AmbientBuffer);
}

void Renderer::UpdateAmbientBuffer()
{
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	// Lock the light constant buffer so it can be written to.
	HR(m_ImmediateContext->Map(m_AmbientBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource));

	AmbientBuffer* data;
	data = (AmbientBuffer*)mappedResource.pData;

	data->sky = skycolor;
	data->ground = groundcolor;
	data->horizon = horizoncolor;
	data->fogColor = fogColor;
	data->sunFogColor = sunFogColor;
	data->coefs = coefecients;
	data->testThing = testThings;


	m_ImmediateContext->Unmap(m_AmbientBuffer, 0);
	m_ImmediateContext->VSSetConstantBuffers(3, 1, &m_AmbientBuffer);
	m_ImmediateContext->PSSetConstantBuffers(3, 1, &m_AmbientBuffer);
	m_ImmediateContext->CSSetConstantBuffers(3, 1, &m_AmbientBuffer);
}

void Renderer::LoadShader( std::string name, ID3D11VertexShader** vertShader, ID3D11PixelShader** pixelShader,  D3D11_INPUT_ELEMENT_DESC* layoutDesc, unsigned layoutDescCount, ID3D11InputLayout** layout )
{
	string filename;
	FILE* fp;
	if(vertShader != nullptr)
	{
		filename = "../Assets/Shaders/" + name + "_VS.cso";
		FILE* fp = OpenShaderFile(filename);

		fseek(fp, 0, SEEK_END);
		long size = ftell(fp);
		fseek(fp, 0, SEEK_SET);

		std::vector<BYTE> vertexShaderBuffer(size);
		if (size > 0)
		{
			fread(&vertexShaderBuffer[0], 1, size, fp);

			m_Device->CreateVertexShader(&vertexShaderBuffer[0], size, NULL, vertShader);
		}
		if(layoutDesc != nullptr)
		{
			// Create the vertex input layout.
			HR(m_Device->CreateInputLayout(layoutDesc, layoutDescCount, &vertexShaderBuffer[0], vertexShaderBuffer.size(), layout));
		}
		fclose(fp);
	}
	
	if(pixelShader != nullptr)
	{
		filename = "../Assets/Shaders/" + name + "_PS.cso";
		fp = OpenShaderFile(filename);

		fseek(fp, 0, SEEK_END);
		long size = ftell(fp);
		fseek(fp, 0, SEEK_SET);
		std::vector<BYTE> pixelShaderBuffer(size);
		if (size > 0)
		{
			fread(&pixelShaderBuffer[0], 1, size, fp);

			m_Device->CreatePixelShader(&pixelShaderBuffer[0], size, NULL, pixelShader);
		}
		fclose(fp);
	}
}

void Renderer::LoadDeferredShadingShader()
{
	string filename("../Assets/Shaders/DeferredShading_CS.cso");
	FILE* fp = OpenShaderFile(filename);

	fseek(fp, 0, SEEK_END);
	long size = ftell(fp);
	fseek(fp, 0, SEEK_SET);

	std::vector<BYTE> vertexShaderBuffer(size);
	if (size > 0)
	{
		fread(&vertexShaderBuffer[0], 1, size, fp);

		m_Device->CreateComputeShader(&vertexShaderBuffer[0], size, NULL, &m_DeferredShadingComputeShader);
	}
	fclose(fp);
}

FILE* Renderer::OpenShaderFile( string &filename )
{
	FILE* fp;
	errno_t err = fopen_s(&fp, filename.c_str(), "rb");
	if(!fp || err)
	{
		throw string( "Could not find " + filename);
	}
	return fp;
}


void Renderer::BeginInstancedGbufferShader()
{
	m_ImmediateContext->VSSetShader(m_InstanceGBufferVertexShader, nullptr, 0);
	m_ImmediateContext->PSSetShader(m_InstanceGBufferPixelShader, nullptr, 0);
	m_ImmediateContext->PSSetSamplers(0, 1, &m_DefaultSampler);
	m_ImmediateContext->PSSetSamplers(1, 1, &m_NormalSampler);
	m_ImmediateContext->IASetInputLayout(m_InstanceLayout);
	m_ImmediateContext->OMSetRenderTargets(3, m_RenderTargetViews, m_DepthStencilView);
	m_ImmediateContext->RSSetState(m_CurrentSolidRasterstate);
	m_ImmediateContext->RSSetViewports(1, &m_CameraViewport);

	float blendFactor[4] = { 0.0f };
	m_ImmediateContext->OMSetBlendState(nullptr, blendFactor, 0xffffff);
}


void Renderer::BeginGBufferShader()
{
	m_ImmediateContext->VSSetShader(m_GBufferVertexShader, nullptr, 0);
	m_ImmediateContext->PSSetShader(m_GBufferPixelShader, nullptr, 0);
	m_ImmediateContext->PSSetSamplers(0, 1, &m_DefaultSampler);
	m_ImmediateContext->PSSetSamplers(1, 1, &m_NormalSampler);
	m_ImmediateContext->IASetInputLayout(m_SingleLightLayout);
	m_ImmediateContext->RSSetState(m_CurrentSolidRasterstate);
	m_ImmediateContext->OMSetRenderTargets(3, m_RenderTargetViews, m_DepthStencilView);
	m_ImmediateContext->RSSetViewports(1, &m_CameraViewport);
	float blendFactor[4] = {0.0f};
	m_ImmediateContext->OMSetBlendState(nullptr, blendFactor, 0xffffff);
}

void Renderer::BeginGBufferAlphaTestShader()
{
	m_ImmediateContext->VSSetShader(m_GBufferVertexShader, nullptr, 0);
	m_ImmediateContext->PSSetShader(m_GBufferAlphaTestPixelShader, nullptr, 0);
	m_ImmediateContext->PSSetSamplers(0, 1, &m_DefaultSampler);
	m_ImmediateContext->PSSetSamplers(1, 1, &m_NormalSampler);
	m_ImmediateContext->IASetInputLayout(m_SingleLightLayout);
	m_ImmediateContext->RSSetState(m_CurrentSolidRasterstate);
	//ID3D11RenderTargetView* viewTargs[3] = { m_BackBufferRenderTargetView, m_RenderTargetViews[1], m_RenderTargetViews[2] };
	m_ImmediateContext->OMSetRenderTargets(3, m_RenderTargetViews, m_DepthStencilView);
	m_ImmediateContext->RSSetViewports(1, &m_CameraViewport);
	float blendFactor[4] = { 0.0f };
	m_ImmediateContext->OMSetBlendState(nullptr, blendFactor, 0xffffff);
}

void Renderer::BeginShadowMapShader()
{
	m_ImmediateContext->VSSetShader(m_ShadowMapVertexShader, nullptr, 0);
	m_ImmediateContext->PSSetShader(nullptr, nullptr, 0);
	m_ImmediateContext->IASetInputLayout(m_SingleLightLayout);
	m_ImmediateContext->RSSetState(m_CurrentSolidRasterstate);
	ID3D11ShaderResourceView* unbind[1];
	unbind[0] = nullptr;
	m_ImmediateContext->PSSetShaderResources(4, 1, unbind);
	ID3D11RenderTargetView* nullView = nullptr;
	m_ImmediateContext->OMSetRenderTargets(1, &nullView, m_ShadowDepthStencilView);
	m_ImmediateContext->RSSetViewports(1, &m_ShadowViewport);
	m_ImmediateContext->OMSetDepthStencilState(m_DepthStencilState, 1);
}


void Renderer::BeginInstanceShadowMapShader()
{
	m_ImmediateContext->VSSetShader(m_InstanceShadowMapVertexShader, nullptr, 0);
	m_ImmediateContext->PSSetShader(nullptr, nullptr, 0);
	m_ImmediateContext->IASetInputLayout(m_InstanceLayout);
	m_ImmediateContext->RSSetState(m_CurrentSolidRasterstate);
	ID3D11ShaderResourceView* unbind[1];
	unbind[0] = nullptr;
	m_ImmediateContext->PSSetShaderResources(4, 1, unbind);
	ID3D11RenderTargetView* nullView = nullptr;
	m_ImmediateContext->OMSetRenderTargets(1, &nullView, m_ShadowDepthStencilView);
	m_ImmediateContext->RSSetViewports(1, &m_ShadowViewport);
	m_ImmediateContext->OMSetDepthStencilState(m_DepthStencilState, 1);
}


void Renderer::BeginShadingScene()
{
	m_ImmediateContext->VSSetShader(nullptr, nullptr, 0);
	m_ImmediateContext->PSSetShader(nullptr, nullptr, 0);
	m_ImmediateContext->CSSetShader(m_DeferredShadingComputeShader, nullptr, 0);
	m_ImmediateContext->OMSetRenderTargets(0, nullptr, nullptr);
	ID3D11ShaderResourceView* views[RENDERTARGET_COUNT + 1];
	for(int i = 0; i < RENDERTARGET_COUNT; ++i)
		views[i] = m_RendertargetShaderResourceViews[i];
	views[RENDERTARGET_COUNT] = m_DepthStencilShaderResourceView;
	m_ImmediateContext->CSSetShaderResources(0, RENDERTARGET_COUNT + 1, views);
	m_ImmediateContext->CSSetShaderResources(RENDERTARGET_COUNT + 1, 1, &m_ShadowShaderResourceView);
	m_ImmediateContext->CSSetUnorderedAccessViews(0, 1, &m_BackBufferUnorderedAccessView, 0);

	UpdateSkyBuffer();
	m_ImmediateContext->CSSetConstantBuffers(4, 1, &m_SkyBuffer);
	m_ImmediateContext->CSSetSamplers(3, 1, &m_ShadowSampler);

	ResourceManager* res = Services::GetResourceManager();
	ResourceHandle temp = res->GetTextureHandle("alt_uffizi_cross_bc6.dds");
	ID3D11ShaderResourceView* cube = res->GetTexture(temp)->GetResourceView();
	m_ImmediateContext->CSSetShaderResources(7,1, &cube);

	temp = res->GetTextureHandle("irracube_bc6.dds");
	cube = res->GetTexture(temp)->GetResourceView();
	m_ImmediateContext->CSSetShaderResources(8,1, &cube);

	unsigned bufferWidth, bufferHeight;
	GetDisplayResolution(bufferWidth, bufferHeight);
	unsigned int dispatchWidth = (bufferWidth + COMPUTE_SHADER_TILE_GROUP_DIM - 1) / COMPUTE_SHADER_TILE_GROUP_DIM;
	unsigned int dispatchHeight = (bufferHeight + COMPUTE_SHADER_TILE_GROUP_DIM - 1) / COMPUTE_SHADER_TILE_GROUP_DIM;
	m_ImmediateContext->Dispatch(dispatchWidth, dispatchHeight, 1);

	ID3D11ShaderResourceView* nullSRV[10] = {nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr};
	m_ImmediateContext->CSSetShaderResources(0, 10, nullSRV);
	ID3D11UnorderedAccessView* nullView = nullptr;
	m_ImmediateContext->CSSetUnorderedAccessViews(0, 1, &nullView, 0);
	m_ImmediateContext->OMSetRenderTargets(1, &m_BackBufferRenderTargetView, m_DepthStencilView);
}

void Renderer::BeginSingleLightShader()
{
	m_ImmediateContext->VSSetShader(m_SingleLightVertexShader, nullptr, 0);
	m_ImmediateContext->PSSetShader(m_ShadowTestPixelShader, nullptr, 0);
	m_ImmediateContext->PSSetSamplers(0, 1, &m_DefaultSampler);
	m_ImmediateContext->CSSetSamplers(0, 1, &m_DefaultSampler);
	m_ImmediateContext->IASetInputLayout(m_SingleLightLayout);
	m_ImmediateContext->OMSetRenderTargets(3, m_RenderTargetViews, m_DepthStencilView);
	m_ImmediateContext->PSSetShaderResources(4, 1, &m_ShadowShaderResourceView);
	m_ImmediateContext->RSSetState(m_CurrentSolidRasterstate);	
	m_ImmediateContext->RSSetViewports(1, &m_CameraViewport);
	float blendFactor[4] = {0.0f};
	m_ImmediateContext->OMSetBlendState(nullptr, blendFactor, 0xffffff);
}

void Renderer::BeginTerrainShader()
{
	m_ImmediateContext->VSSetShader(m_TerrainVertexShader, nullptr, 0);
	m_ImmediateContext->PSSetShader(m_TerrainPixelShader, nullptr, 0);
	m_ImmediateContext->PSSetSamplers(0, 1, &m_TerrainSampler);
	m_ImmediateContext->OMSetRenderTargets(3, m_RenderTargetViews, m_DepthStencilView);
	m_ImmediateContext->IASetInputLayout(m_TerrainLayout);
	m_ImmediateContext->RSSetState(m_CurrentSolidRasterstate);
	float blendFactor[4] = {0.0f};
	m_ImmediateContext->OMSetBlendState(nullptr, blendFactor, 0xffffff);
}

void Renderer::BeginShadowMapTerrainShader()
{
	m_ImmediateContext->VSSetShader(m_TerrainVertexShader, nullptr, 0);
	m_ImmediateContext->PSSetShader(nullptr, nullptr, 0);
	m_ImmediateContext->IASetInputLayout(m_TerrainLayout);
	m_ImmediateContext->RSSetState(m_CurrentSolidRasterstate);
}


void Renderer::BeginWaterShader()
{
	m_ImmediateContext->VSSetShader(m_WaterVertexShader, nullptr, 0);
	m_ImmediateContext->PSSetShader(m_WaterPixelShader, nullptr, 0);
	m_ImmediateContext->PSSetSamplers(0, 1, &m_DefaultSampler);
	m_ImmediateContext->PSSetSamplers(1, 1, &m_NormalSampler);
	m_ImmediateContext->IASetInputLayout(m_TerrainLayout);
	m_ImmediateContext->OMSetRenderTargets(1, &m_BackBufferRenderTargetView, m_ReadOnlyDepthStencilView);
	m_ImmediateContext->PSSetShaderResources(0, 1, &m_DepthStencilShaderResourceView);
	m_ImmediateContext->RSSetState(m_CurrentSolidRasterstate);	
	m_ImmediateContext->RSSetViewports(1, &m_CameraViewport);
	float blendFactor[4] = {0.0f};
	m_ImmediateContext->OMSetBlendState(m_WaterBlendState, blendFactor, 0xffffff);

	UpdateWaterBuffer();
}


void Renderer::ToggleWireframe()
{
	D3D11_RASTERIZER_DESC currentDesc;
	m_CurrentSolidRasterstate->GetDesc(&currentDesc);

	//notice that we don't actually set state here
	if(currentDesc.FillMode == D3D11_FILL_SOLID)
		m_CurrentSolidRasterstate = m_WireframeRasterState;
	else
		m_CurrentSolidRasterstate = m_RasterState;
}

void Renderer::GetDisplayResolution( unsigned& width, unsigned& height )const
{
	DXGI_SWAP_CHAIN_DESC desc;
	m_SwapChain->GetDesc(&desc);
	width = desc.BufferDesc.Width;
	height = desc.BufferDesc.Height;
}

void Renderer::CheckDriverSupport()
{
	D3D11_FEATURE_DATA_THREADING dat;
	m_Device->CheckFeatureSupport(D3D11_FEATURE_THREADING, &dat, sizeof(dat));
	if(!dat.DriverCommandLists)
		throw L"Your driver does not support command lists, tell poop to do things";
	if(!dat.DriverConcurrentCreates)
		throw L"Your driver does not support concurrent resource creation, tell poop to do things";
}

void Renderer::DrawSky(Camera* cam)
{
	m_ImmediateContext->VSSetShader(m_SkySphereVertexShader, nullptr, 0);
	m_ImmediateContext->PSSetShader(m_SkySpherePixelShader, nullptr, 0);
	m_ImmediateContext->IASetInputLayout(m_SingleLightLayout);

	ResourceManager* res = Services::GetResourceManager();

	//todo: depth stencil state stuff
	ResourceHandle skyTex = res->GetTextureHandle("alt_uffizi_cross_bc6.dds");
	ID3D11ShaderResourceView* cube = res->GetTexture(skyTex)->GetResourceView();
	m_ImmediateContext->PSSetShaderResources(7,1, &cube);

	m_ImmediateContext->RSSetViewports(1, &m_CameraViewport);

	m_ImmediateContext->OMSetDepthStencilState(m_SkyBoxDepthStencilState, 1);

	float farclip = 10.0f;
	XMMATRIX mat =XMMatrixScaling(farclip, farclip, farclip);
	mat	= XMMatrixMultiply(mat, XMMatrixTranslationFromVector(cam->GetPosition()));

	UpdatePerObjectBuffer(cam, &mat);
	
	ResourceHandle skyModel = res->GetModelHandle("skysphere.pbm");
	Model* mod = res->GetModel(skyModel);
	mod->Render(m_ImmediateContext);
	m_ImmediateContext->DrawIndexed(mod->GetIndexCount(), 0, 0);

	m_ImmediateContext->RSSetViewports(1, &m_CameraViewport);
	m_ImmediateContext->OMSetDepthStencilState(m_DepthStencilState, 1);
}


void Renderer::StartProfileSection( std::string& name )
{
	if(m_Profiler == nullptr)
		return;

	m_Profiler->StartProfile(name);
}

void Renderer::EndProfileSection( std::string& name )
{
	if(m_Profiler == nullptr)
		return;

	m_Profiler->EndProfile(name);
}

