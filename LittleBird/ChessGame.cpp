#include "ChessGame.h"

////////////////// reeeeeeeeemoveeeeeeeeeeeeeeeeeeeeee
#include <DirectXCollision.h>
#include "Iresource_manager.h"
#include "texture.h"

#include "renderer.h"
#include "DebugDrawer.h"
#include "debug_ui.h"
#include "input.h"
#include "services.h"
#include "timer.h"

#include "Entity.h"
#include "TerrainDecoratorSystem.h"
#include "UnitComponent.h"
#include "BaitComponent.h"
#include "UnitSystem.h"
#include "ModelRenderSystem.h"
#include "LanderSystem.h"
#include "LanderComponent.h"
#include "PlayerSystem.h"
#include "PlayerComponent.h"
#include "ModelComponent.h"
#include "PositionComponent.h"
#include "RigidBodyComponent.h"
#include "Networking.h"

#include "TerrainManager.h"
#include "terrain.h"
#include "shaders\terrainConstants.h"

#include "StringUtilities.h"

#include "camera.h"
#include "model.h"

using namespace std;
using namespace DirectX;


ChessGame::ChessGame( int screenheight, int screewidth, HWND hwnd, HINSTANCE hinstance, bool fullScreen, bool vSync ) : m_GameRunning(false)
{
	CreateSubsystems(screenheight, screewidth, hwnd, hinstance, fullScreen, vSync);

	InitializeSubsystems();

	DECLARE_CONSOLECOMMAND("hostgame", ChessGame::Console_HostGame);
	DECLARE_CONSOLECOMMAND("joingame", ChessGame::Console_ConnectGame);


}

ChessGame::~ChessGame()
{
	for(auto it = m_Entities.begin(); it != m_Entities.end(); ++it)
	{
		delete (*it);
	}
}

extern DirectX::XMFLOAT4 lightPosition;

bool ChessGame::Frame()
{

	float delta = static_cast<float>(m_GameTimer->DeltaTime());
	float elapsed = static_cast<float>(m_GameTimer->TotalTime());

	if(m_GameRunning && !m_Networking->IsHost() && !m_Networking->IsConnected())
	{
		throw string("lost connection to server, graceful DC not implemented yet.");
	}
		
	if(!HandleInput(delta))
		return false;

	m_Networking->Update();

	if(m_GameRunning)
	{		
		DispatchEntities();
		m_DebugDrawer->SetupCamera(m_PerspectiveCamera.get(), m_Renderer.get());
		m_LanderSystem->Run(m_PerspectiveCamera.get());
		m_Physics->Update(delta);
		m_TerrainManager->Update(m_PerspectiveCamera->GetPosition());
		m_TerrainDecoratorSystem->Run(m_PerspectiveCamera.get());
	}
	
	RenderFrame();
	return true;
}

void ChessGame::StartGame()
{
	CreateGameSystems();
	CreateGameObjects();
	m_GameRunning = true;
}

std::wstring ChessGame::Console_HostGame( std::wstring args )
{
	m_DeveloperConsole->WriteToConsole(L"Starting Host Game");
	StartGame();
	m_Networking->HostGame();
	m_DeveloperConsole->SetEnabled(false);
	
	return L"Game should be started";
}

std::wstring ChessGame::Console_ConnectGame( std::wstring args )
{
	m_DeveloperConsole->WriteToConsole(L"Starting Connection to game");
	
	m_Networking->Connect(args);
	StartGame();
	m_DeveloperConsole->SetEnabled(false);
	
	return L"Finished connection attempt";
}

void ChessGame::CreateSubsystems( int screenheight, int screenwidth, HWND hwnd, HINSTANCE hinstance, bool fullScreen, bool vSync )
{
	m_Renderer.reset(new Renderer(screenheight, screenwidth, hwnd, fullScreen, vSync));
	m_DeveloperConsole.reset(new DeveloperConsole(m_Renderer.get(), screenwidth, screenheight));
	Services::ProvideDeveloperConsole(m_DeveloperConsole.get());
	m_DebugUI.reset(new DebugUI(m_Renderer->GetDevice(), screenwidth, screenheight));
	Services::ProvideDebugUI(m_DebugUI.get());
	m_Input.reset(new Input(hinstance, hwnd, screenwidth, screenheight));
	Services::ProvideInput(m_Input.get());
	m_GameTimer.reset(new Timer);
	Services::ProvideTimer(m_GameTimer.get());
	m_ResourceManager.reset(new ResourceManager(m_Renderer.get(), m_Physics.get()));
	Services::ProvideResourceManager(m_ResourceManager.get());
	m_DebugDrawer.reset(new DebugDrawer(m_Renderer.get()));
	Services::ProvideDebugDrawer(m_DebugDrawer.get());
	m_Networking.reset(new Networking);
	Services::ProvideNetworking(m_Networking.get());
	//m_WorldQuery.reset(new WorldQuery(&m_Entities));
	//Services::ProvideWorldQuery(m_WorldQuery.get());

	ReplicateEntityFunction repl = std::bind( &ChessGame::ReplicateEntity, this, placeholders::_1,  placeholders::_2, placeholders::_3);
	m_Networking->SetReplicateEntityCallback(repl);

	ReplicateComponentFunction repComponent = std::bind(&ChessGame::AddReplicatedComponentToEntity, this, placeholders::_1, placeholders::_2, placeholders::_3);
	m_Networking->SetReplicateComponentCallback(repComponent);
}

void ChessGame::CreateGameSystems()
{
	m_Physics.reset(new Physics());
	Services::ProvidePhysics(m_Physics.get());
	m_Physics->SetDebugDrawer(m_DebugDrawer.get());
	m_TerrainManager.reset(new TerrainManager("testisland", 31, 30, m_Renderer->GetDevice()));
	m_TerrainDecoratorSystem.reset(new TerrainDecoratorSystem(m_Renderer.get(), m_TerrainManager.get()));
	m_LanderSystem.reset(new LanderSystem);
	m_ModelRenderSystem.reset(new ModelRenderSystem(m_Renderer.get()));
}

void ChessGame::CreateGameObjects()
{
	unsigned height, width;
	m_Renderer->GetDisplayResolution(width, height);

	float aspectRatio = static_cast<float>(width)/height;
	m_PerspectiveCamera.reset(new PerspectiveCamera(aspectRatio, XMConvertToRadians(60.0f), 1.15f, 10000.0f));

	XMVECTOR startPos = XMVectorSet(4000.0f* tileDistance, 200.0f, 4000.0f * tileDistance, 1.0f);
	XMVECTOR lookAt =  XMVectorZero();
	XMVECTOR up = XMVectorSet(0.0f, 1.0f, 0.0f, 1.0f);

	m_PerspectiveCamera->SetLookAt(startPos, lookAt, up);
	
	ResourceHandle mod = m_ResourceManager->GetModelHandle("tardplane.pbm");
	ResourceHandle diff = m_ResourceManager->GetTextureHandle("planeskin_bc7.dds");
	ResourceHandle normal = m_ResourceManager->GetTextureHandle("subtlenormals_BC7.dds");
	//ResourceHandle normal = res->GetTextureHandle("pondnormal_BC7.dds");
	ResourceHandle rough = m_ResourceManager->GetTextureHandle("woodgrain_BC7.dds");
	ResourceHandle spec = m_ResourceManager->GetTextureHandle("subtlenormals_BC7.dds");
	ResourceHandle board_col = m_ResourceManager->GetCollisionShapeHandle("board_col.pbm");
	ResourceHandle pawn_col = m_ResourceManager->GetCollisionShapeHandle("tardplane_col.pbm");
	ResourceHandle box_col = m_ResourceManager->GetCollisionShapeHandle(CollisionShape::BOX, 4.0f, 4.0f, 4.0f);

	string textureNames[] = { "subtlenormals_BC7.dds", "ft_stone01_n_BC7.dds", "redbrick01_n_BC7.dds", "ft_diagonal01_n_BC7.dds"};
	
	for(int i = 0; i < 2; ++i)
	{
		for(int j = 0; j < 2; ++j)
		{
			ModelComponent* peice = new ModelComponent(mod, diff, normal, rough, spec, LocalOnly);
			XMVECTOR posVec = XMVectorAdd(startPos, XMVectorSet(i*8.0f, 10.0f, j * 8.0f, 1.0f));
			PositionComponent* pos = new PositionComponent(posVec, XMQuaternionIdentity(), LocalOnly);
			RigidBodyComponent* rigid = new RigidBodyComponent(pawn_col, pos, 1.0f, LocalOnly);
			Entity* ent = new Entity(ReplicationFlags::LocalOnly);
			ent->AddComponent(peice);
			ent->AddComponent(pos);
			ent->AddComponent(rigid);
			m_Entities.push_back(ent);

			ResourceHandle BallResource = m_ResourceManager->GetModelHandle("matcube.pbm");
			ResourceHandle BallTexture = m_ResourceManager->GetTextureHandle("woodgrain_bc7.dds");
			ResourceHandle BallNormal = m_ResourceManager->GetTextureHandle(textureNames[i%4]);

			peice = new ModelComponent(BallResource, BallTexture, BallNormal, rough, spec, LocalOnly);
			pos = new PositionComponent(XMVectorAdd(posVec, XMVectorSet(3.0f, 7.0f, 3.0f, 1.0f)), XMQuaternionIdentity(), LocalOnly);
			rigid = new RigidBodyComponent(box_col, pos, 0.0f, LocalOnly);
			ent = new Entity(ReplicationFlags::LocalOnly);
			ent->AddComponent(peice);
			ent->AddComponent(pos);
			ent->AddComponent(rigid);
			m_Entities.push_back(ent);
		}	
	}

	mod = m_ResourceManager->GetModelHandle("pawn.pbm");
	pawn_col = m_ResourceManager->GetCollisionShapeHandle("tardplane_col.pbm");
	
	mod = m_ResourceManager->GetModelHandle("board.pbm");
	for(int i = -5; i < 5; ++i)
	{
		for(int j = -5; j < 5; ++j)
		{
			Entity* ent = new Entity(ReplicationFlags::LocalOnly);
					
			diff = m_ResourceManager->GetTextureHandle("conc_slabs01_c_BC7.dds");
			normal = m_ResourceManager->GetTextureHandle(textureNames[abs(i)%4]);
			//diff = m_ResourceManager->GetTextureHandle("woodgrain_BC7.dds");
			ModelComponent* board = new ModelComponent(mod, diff, normal, rough, spec, LocalOnly);
			PositionComponent* pos = new PositionComponent(XMVectorAdd(startPos,XMVectorSet(25.0f*i, -5.0f, 25.0f*j, 1.0f)), XMQuaternionIdentity(), LocalOnly);
			RigidBodyComponent* rigid = new RigidBodyComponent(board_col, pos, 0.0f, LocalOnly);
			btVector3 vec;
			vec.setZero();
			//rigid->RigidBody->setMassProps(0.0f, vec);
			ent->AddComponent(board);
			ent->AddComponent(pos);
			ent->AddComponent(rigid);
			m_Entities.push_back(ent);
		}
	}

	XMVECTOR playerStartPos = XMVectorAdd(startPos, XMVectorSet(25.0f, 10.0f, 25.0f, 1.0f));
	Entity* ent = new Entity(ReplicationFlags::LocalOnly);
	mod = m_ResourceManager->GetModelHandle("tardplane.pbm");
	ModelComponent* pawnModel = new ModelComponent(mod, diff, normal, rough, spec, LocalOnly);
	LanderComponent* lander = new LanderComponent(230.0f, 100.0f, 1000.0f, true, LocalOnly);
	PositionComponent* playerPos = new PositionComponent(playerStartPos, XMQuaternionIdentity(), LocalOnly);
	RigidBodyComponent* rigid = new RigidBodyComponent(pawn_col, playerPos, 10.0f, LocalOnly);
	ent->AddComponent(lander);
	ent->AddComponent(playerPos);
	ent->AddComponent(rigid);
	ent->AddComponent(pawnModel);
	m_Entities.push_back(ent);
}

void ChessGame::InitializeSubsystems()
{
	m_Renderer->Initialize();
	m_Input->Frame();
}

#include "shaders\SharedShaderDefines.h"
extern XMFLOAT4 lightDirection;
extern XMMATRIX directionalMatrix;

void ChessGame::RenderFrame()
{
	m_Renderer->BeginFrame(0.0f, 0.0f, 0.0f, 1.0f);
	m_Renderer->UpdateAmbientBuffer();

	if(m_GameRunning)
	{
		RenderFrameShadows();

		string solidSection = "Slow Path Solids";
		m_Renderer->StartProfileSection(solidSection);
		m_ModelRenderSystem->RenderSolids(m_PerspectiveCamera.get(), m_TerrainManager.get());
		m_Renderer->EndProfileSection(solidSection);

		string section = "Terrain Decorations";
		m_Renderer->StartProfileSection(section);
		m_TerrainDecoratorSystem->RenderToScreen();
		m_Renderer->EndProfileSection(section);

		section = "Deferred Shading";
		m_Renderer->StartProfileSection(section);
		m_Renderer->BeginShadingScene();
		m_Renderer->EndProfileSection(section);

		section = "Water";
		m_Renderer->StartProfileSection(section);
		RenderWater();
		m_Renderer->EndProfileSection(section);

		m_ModelRenderSystem->EndFrame();
	}
	
	m_DeveloperConsole->Display();
	string section = "DebugMenus";
	m_Renderer->StartProfileSection(section);
	m_DebugUI->Draw();
	m_Renderer->EndProfileSection(section);

	m_DebugDrawer->Present(m_Renderer.get());

	m_Renderer->EndFrame();
}

void ChessGame::RenderFrameShadows()
{
	const XMVECTOR camPosition = m_PerspectiveCamera->GetPosition();
	XMVECTOR xmLightDir;
	xmLightDir = XMLoadFloat4(&lightDirection);
	const float oldFar = m_PerspectiveCamera->GetFarClip();
	m_PerspectiveCamera->SetFarClip(75.0f);
	BoundingFrustum camFrustum;
	BoundingFrustum::CreateFromMatrix(camFrustum, m_PerspectiveCamera->GetProjectionMatrix());
	camFrustum.Transform(camFrustum, 1.0f, m_PerspectiveCamera->Orientation(), camPosition);
	m_PerspectiveCamera->SetFarClip(oldFar);
	BoundingSphere camSphere;
	BoundingSphere::CreateFromFrustum(camSphere, camFrustum);
	XMVECTOR camSphereCenter = XMLoadFloat3(&camSphere.Center);

	const float lightFarClip = SHADOW_FAR_CLIP;
	const float lightNearClip = SHADOW_NEAR_CLIP;
	XMVECTOR lightPos = XMVectorAdd(XMVectorScale(xmLightDir, lightFarClip / 3.0f), camSphereCenter);
	XMVECTOR up = XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);

	OrthographicCamera lightCamera(-camSphere.Radius, -camSphere.Radius, camSphere.Radius, camSphere.Radius, lightNearClip, lightFarClip);
	lightCamera.SetLookAt(lightPos, camSphereCenter, up);

	directionalMatrix = XMMatrixTranspose(lightCamera.GetViewMatrix());
	m_Renderer->UpdateLightBuffer(m_PerspectiveCamera.get(), XMVectorReplicate(camSphere.Radius));

	m_Renderer->UpdatePerFrameBuffer(m_PerspectiveCamera.get(), &lightCamera, float(m_GameTimer->TotalTime()));
	string shadowSection = "Slow Path Shadows";
	m_Renderer->StartProfileSection(shadowSection);
	m_ModelRenderSystem->RenderShadows(m_PerspectiveCamera.get(), lightCamera, lightPos, camSphere, m_TerrainManager.get());
	m_Renderer->EndProfileSection(shadowSection);

	string decorShadowSection = "Decoration Shadows";
	m_Renderer->StartProfileSection(decorShadowSection);
	m_TerrainDecoratorSystem->RenderToShadowMap();
	m_Renderer->EndProfileSection(decorShadowSection);
}

extern float BaseFactor;

void ChessGame::RenderWater()
{
	m_Renderer->BeginWaterShader();
	Texture* tex = m_ResourceManager->GetTextureFromName("waternormals_bc7.dds");
	ID3D11ShaderResourceView* texView = tex->GetResourceView();
	m_Renderer->GetImmediateContext()->PSSetShaderResources(1, 1, &texView);
	BoundingFrustum camFrustum;
	BoundingFrustum::CreateFromMatrix(camFrustum, m_PerspectiveCamera->GetProjectionMatrix());
	camFrustum.Transform(camFrustum, 1.0f, m_PerspectiveCamera->Orientation(), m_PerspectiveCamera->GetPosition());
	
	Terrain::PreRender(m_Renderer->GetImmediateContext());
	Terrain::Render(m_Renderer->GetImmediateContext(), 5);

	for(TerrainCollisionPair ter : *m_TerrainManager->GetVisibleTiles() )
	{
		if(ter.terrain->GetMinHeight() < BaseFactor)
		{
			float terX, terY, terZ;
			ter.terrain->GetTerrainPosition(terX, terY, terZ);

			float HalfWidth = (ter.terrain->GetTerrainWidth() * ter.terrain->GetTileScale()) * 0.5f;
			XMFLOAT3 tPos(terX + HalfWidth, ((ter.terrain->GetMaxHeight() - ter.terrain->GetMinHeight()) * 0.5f) + ter.terrain->GetMinHeight(), terZ + HalfWidth);

			float halfHeight = (ter.terrain->GetMaxHeight() - ter.terrain->GetMinHeight()) * 0.5f;
			XMFLOAT3 halfExtents(HalfWidth, halfHeight, HalfWidth);
			BoundingBox bb(tPos, halfExtents);

			if(camFrustum.Contains(bb))
			{
				XMMATRIX modelMatrix = XMMatrixTranslation(terX, terY, terZ);
				m_Renderer->UpdatePerObjectBuffer(m_PerspectiveCamera.get(), &modelMatrix);

				m_Renderer->GetImmediateContext()->DrawIndexed(ter.terrain->GetIndexCount(5), 0, 0);
			}
		}
	}
}

#define COMPONENT_MASK(m) 1 << (ComponentMask_t)(m)

void ChessGame::DispatchEntities()
{
	size_t entityCount = m_Entities.size();

	// todo: asks systems for their masks and maybe think of way to automate dispatch based on masks if I end up editing them a lot

	ComponentMask_t testingMasks[] = {
		//modelrender
		COMPONENT_MASK(ComponentID::Position) | COMPONENT_MASK(ComponentID::Model),
		//Lander
		COMPONENT_MASK(ComponentID::Lander) | COMPONENT_MASK(ComponentID::RigidBody),
	};

	for (size_t i = 0; i < entityCount; ++i)
	{
		Entity* ent = m_Entities[i];
		unsigned entID = ent->GetID();
		ComponentMask_t currentMask = ent->GetComponentMasks();

		//modelrender
		ComponentMask_t testingFor = testingMasks[0];
		if((currentMask & testingFor) == testingFor)
		{
			PositionComponent* entPos = (PositionComponent*)ent->GetComponent(ComponentID::Position);
			ModelComponent* entModel = (ModelComponent*)ent->GetComponent(ComponentID::Model);
			m_ModelRenderSystem->AddEntry(entID, entPos, entModel );
		}
		//helicopter
		testingFor = testingMasks[1];
		if ((currentMask & testingFor) == testingFor)
		{
			LanderComponent* entLander = (LanderComponent*)ent->GetComponent(ComponentID::Lander);
			RigidBodyComponent* entRigid = (RigidBodyComponent*)ent->GetComponent(ComponentID::RigidBody);
			m_LanderSystem->AddEntry(entID, entLander, entRigid);
		}
	}
}

bool ChessGame::HandleInput(float delta)
{
	m_Input->Frame();

	if(m_Input->KeyPressed(DIK_ESCAPE))
		return false;

	if(m_Input->KeyPressed(DIK_TAB))
		m_Input->ToggleCursor();

	if(m_Input->KeyPressed(DIK_GRAVE))
		m_DeveloperConsole->ToggleEnabled();

	if(m_DeveloperConsole->IsEnabled())
	{
		wstring bufferedInput = to_wstring(m_Input->GetBufferedChars());

		if(bufferedInput.length() > 0)
			m_DeveloperConsole->AddToInputBuffer(bufferedInput);

		if(m_Input->KeyPressed(DIK_UPARROW))
			m_DeveloperConsole->ShowPreviousCommand();

		return true;
	}

	if(m_Input->KeyPressed(DIK_F))
		m_DebugUI->SetEnabled(!m_DebugUI->GetEnabled());

	if(m_GameRunning)
		HandleGameInput(delta);

	if(m_Input->KeyPressed(DIK_I))
		m_Renderer->ToggleWireframe();

	return true;
}

void ChessGame::HandleGameInput( float delta ) 
{
	static float mupdown, mrightleft = 0.0f;
	int x,y,z;
	const float m_ViewSensitivity = 4.0f;
	m_Input->GetMouseDelta(x, y, z);
	if(!m_Input->IsCursorEnabled())
	{
		mrightleft += XMConvertToRadians(x/50.f) * m_ViewSensitivity;
		mupdown += XMConvertToRadians(y/50.f) * m_ViewSensitivity;
		if (!m_LanderSystem->UsingLanderCam())
			m_PerspectiveCamera->SetOrientation(XMQuaternionRotationRollPitchYaw(mupdown, mrightleft, 0.0f));
	}

	static float cameraSpeed = 30.0f;

	cameraSpeed = max<float>(0.0f, cameraSpeed += z/50.0f);
	float rightDelta = 0.0f;
	float forwardDelta = 0.0f;
	float upDelta = 0.0f;
	if(m_Input->KeyDown(DIK_D))
		rightDelta += 1.0f;
	if(m_Input->KeyDown(DIK_A))
		rightDelta -= 1.0f;
	if(m_Input->KeyDown(DIK_W))
		forwardDelta += 1.0f;
	if(m_Input->KeyDown(DIK_S))
		forwardDelta -= 1.0f;
	if(m_Input->KeyDown(DIK_E))
		upDelta += 1.0f;
	if(m_Input->KeyDown(DIK_Q))
		upDelta -= 1.0f;

	if(m_Input->KeyPressed(DIK_T))
	{
		if (m_DebugDrawer->getDebugMode() == DebugDrawModes::DBG_NoDebug)
			m_DebugDrawer->setDebugMode(DebugDrawModes::DBG_DrawAabb);
		else
			m_DebugDrawer->setDebugMode(DebugDrawModes::DBG_NoDebug);
	}

	rightDelta = rightDelta * cameraSpeed * delta;
	forwardDelta = forwardDelta * cameraSpeed * delta;
	upDelta = upDelta * cameraSpeed * delta;
	XMVECTOR forwardVec = m_PerspectiveCamera->GetForwardVector();
	forwardVec = XMVectorSetY(forwardVec, 0.0f);
	forwardVec = XMVector3Normalize(forwardVec);
	XMVECTOR rightVec = m_PerspectiveCamera->GetRightVector();
	rightVec = XMVectorSetY(rightVec, 0.0f);
	rightVec = XMVector3Normalize(rightVec);
	XMVECTOR rightPos = XMVectorScale(rightVec, rightDelta);
	XMVECTOR forwardPos = XMVectorScale(forwardVec, forwardDelta);
	XMVECTOR upPos = XMVectorScale(XMVectorSet(0.0f, 1.0f, 0.0f, 1.0f), upDelta);
	XMVECTOR& newpos = rightPos;
	newpos = XMVectorAdd(forwardPos, rightPos);
	newpos = XMVectorAdd(newpos, upPos);
	newpos = XMVectorAdd(m_PerspectiveCamera->GetPosition(), newpos);
	if (!m_LanderSystem->UsingLanderCam())
		m_PerspectiveCamera->SetPosition(newpos);

	if(m_Input->MouseButtonPressed(0) && !m_Input->IsCursorEnabled())
	{
		ResourceHandle mod = m_ResourceManager->GetModelHandle("pawn.pbm");
		ResourceHandle diff = m_ResourceManager->GetTextureHandle("detailgravel_BC7.dds");
		ResourceHandle normal = m_ResourceManager->GetTextureHandle("subtlenormals_BC7.dds");
		//ResourceHandle normal = res->GetTextureHandle("pondnormal_BC7.dds");
		ResourceHandle rough = m_ResourceManager->GetTextureHandle("woodgrain_BC7.dds");
		ResourceHandle spec = m_ResourceManager->GetTextureHandle("subtlenormals_BC7.dds");
		ModelComponent* peice = new ModelComponent(mod, diff, normal, rough, spec, Authoritative);
		XMVECTOR lookDir = m_PerspectiveCamera->GetForwardVector();
		PositionComponent* pos = new PositionComponent(XMVectorAdd(newpos, lookDir), XMQuaternionIdentity(), Authoritative);
		ResourceHandle pawn_col = m_ResourceManager->GetCollisionShapeHandle("pawn_col.pbm");
		RigidBodyComponent* rigid = new RigidBodyComponent(pawn_col, pos, 1.0f, LocalOnly);
		rigid->RigidBody->setLinearVelocity((*(btVector3*)&lookDir) * 50.0f);
		Entity* ent = new Entity(ReplicationFlags::Authoritative);
		ent->AddComponent(peice);
		ent->AddComponent(pos);
		ent->AddComponent(rigid);
		m_Entities.push_back(ent);
	}

}

void ChessGame::ReplicateEntity( uint64_t id, bool spawning, char replicationFlags )
{
	DeveloperConsole* dev = Services::GetDeveloperConsole();

	if(spawning)
	{
		char newReplictionFlag;
		newReplictionFlag = replicationFlags == ReplicationFlags::Replicated ? Replicated : LocalOnly;
		if(replicationFlags != Replicated && replicationFlags != ReplicateAndMakeLocal)
			throw string ("tried to instantiate entity with invalid replication flags");

		Entity* ent = new Entity(newReplictionFlag);
		ent->SetNetworkID(id);
		m_Entities.push_back(ent);
		dev->WriteToConsole(L"Created " + to_wstring(id) + L" " + to_wstring(replicationFlags));
	}
	else
	{
		throw string("implement entity destruction");
	}
}

void ChessGame::AddReplicatedComponentToEntity(ComponentID_t idType, Entity* ent, Component* com)
{

	// dooooohj
	switch ((ComponentID)idType)
	{
	case ComponentID::Position:
		ent->AddComponent<PositionComponent>((PositionComponent*)com);
		break;
	case ComponentID::Model:
		ent->AddComponent<ModelComponent>((ModelComponent*)com);
		break;
	case ComponentID::RigidBody:
		ent->AddComponent<RigidBodyComponent>((RigidBodyComponent*)com);
		break;
	case ComponentID::Lander:
		ent->AddComponent<LanderComponent>((LanderComponent*)com);
		break;
	case ComponentID::Player:
		ent->AddComponent<PlayerComponent>((PlayerComponent*)com);
		break;
	default:
		throw string("could not add replicated component to replicated entity");
		break;
	}
}



