
#ifndef _CAMERA_H_
#define _CAMERA_H_


#include <DirectXMath.h>

class Camera
{

public:

	Camera(float nearZ, float farZ);
	~Camera();

	const DirectX::XMMATRIX& GetViewMatrix() const { return m_ViewMatrix; };
	const DirectX::XMMATRIX& GetProjectionMatrix() const { return m_ProjectionMatrix; };
	const DirectX::XMMATRIX& GetViewProjectionMatrix() const { return m_ViewProjection; };
	const DirectX::XMMATRIX& GetWorldMatrix() const { return m_WorldMatrix; };
	const DirectX::XMVECTOR& GetPosition() const { return m_Position; };
	const DirectX::XMVECTOR& Orientation() const { return m_Orientation; };
	const float GetNearClip() const { return m_NearZ; };
	const float GetFarClip() const { return m_FarZ; };

	DirectX::XMVECTOR GetForwardVector() const;
	DirectX::XMVECTOR GetBackVector() const;
	DirectX::XMVECTOR GetUpVector() const;
	DirectX::XMVECTOR GetDownVector() const;
	DirectX::XMVECTOR GetRightVector() const;
	DirectX::XMVECTOR GetLeftVector() const;

	void SetLookAt(const DirectX::XMFLOAT3& eye, const DirectX::XMFLOAT3& lookAt, const DirectX::XMFLOAT3& up);
	void SetLookAt(const DirectX::XMVECTOR &eye, const DirectX::XMVECTOR &lookAt, const DirectX::XMVECTOR &up);
	void SetWorldMatrix(const DirectX::XMMATRIX& newWorld);
	void SetPosition(const DirectX::XMVECTOR& newPosition);
	void SetOrientation(const DirectX::XMVECTOR& newOrientation);
	void SetNearClip(float newNearClip);
	void SetFarClip(float newFarClip);
	void SetProjection(const DirectX::XMMATRIX& newProjection);

protected:

	DirectX::XMMATRIX m_ViewMatrix;
	DirectX::XMMATRIX m_ProjectionMatrix;
	DirectX::XMMATRIX m_ViewProjection;
	DirectX::XMMATRIX m_WorldMatrix;

	DirectX::XMVECTOR m_Position;
	DirectX::XMVECTOR m_Orientation;

	float m_NearZ;
	float m_FarZ;

	virtual void CreateProjection() = 0;
	void WorldMatrixChanged();
};

// Camera with an orthographic projection
class OrthographicCamera : public Camera
{

public:

	OrthographicCamera(float minX, float minY, float maxX, float maxY, float nearClip, float farClip);
	~OrthographicCamera();

	float GetMinX() const { return m_XMin; };
	float GetMinY() const { return m_YMin; };
	float GetMaxX() const { return m_XMax; };
	float GetMaxY() const { return m_YMax; };

	void SetMinX(float minX);
	void SetMinY(float minY);
	void SetMaxX(float maxX);
	void SetMaxY(float maxY);

protected:

	float m_XMin;
	float m_XMax;
	float m_YMin;
	float m_YMax;

	virtual void CreateProjection();
};

// Camera with a perspective projection
class PerspectiveCamera : public Camera
{

public:

	PerspectiveCamera(float aspectRatio, float fieldOfView, float nearClip, float farClip);
	~PerspectiveCamera();

	float GetAspectRatio() const { return m_AspectRatio; };
	float GetFieldOfView() const { return m_Fov; };

	void SetAspectRatio(float aspectRatio);
	void SetFieldOfView(float fieldOfView);

protected:

	float m_AspectRatio;
	float m_Fov;

	virtual void CreateProjection();
};

// Perspective camera that rotates about Z and Y axes
class FirstPersonCamera : public PerspectiveCamera
{

public:

	FirstPersonCamera(float aspectRatio, float fieldOfView, float nearClip, float farClip);
	~FirstPersonCamera();

	float GetXRotation() const { return m_XRotation; };
	float GetYRotation() const { return m_YRotation; };

	void SetXRotation(float xRotation);
	void SetYRotation(float yRotation);

protected:

	float m_XRotation;
	float m_YRotation;

};

#endif