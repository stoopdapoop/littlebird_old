#ifndef _RENDERER_H_
#define _RENDERER_H_

#pragma comment(lib, "dxgi.lib")
#pragma comment(lib, "d3d11.lib")


#pragma warning(disable:4005)
#include <windows.h>
#include <d3d11.h>
#pragma warning(default:4005)
#include <vector>
#include "constant_buffers.h"

class Model;
class Shader;
class FPSCounter;
class Camera;
class GPUProfiler;

//todo: do this at runtime
#define RENDERTARGET_COUNT 3

class Renderer
{
public:
	Renderer(uint32_t screenheight, uint32_t screenwidth, HWND hwnd, bool fullScreen, bool vSync);
	~Renderer();

	ID3D11Device* GetDevice()const { return m_Device; }
	ID3D11DeviceContext* GetImmediateContext()const {return m_ImmediateContext; }
	ID3D11DeviceContext* GetDebugContext()const {return m_DebugContext; }
	int GetFrameRate()const;
	void GetDisplayResolution(unsigned& width, unsigned& height)const;

	void Initialize();

	void BeginFrame(float red, float green, float blue, float alpha);
	void BeginInstancedGbufferShader();
	void BeginInstanceShadowMapShader();
	void BeginGBufferShader();
	void BeginGBufferAlphaTestShader();
	void BeginShadingScene();
	void BeginShadowMapShader();
	void BeginShadowMapTerrainShader();
	void BeginSingleLightShader();
	void BeginTerrainShader();
	void BeginWaterShader();
	
	

	void ToggleWireframe();

	void EndFrame();

	void UpdatePerObjectBuffer(Camera* cam, DirectX::XMMATRIX* modelMatrix);
	void UpdatePerFrameBuffer(Camera* cam, Camera* lightCam, float time);
	void UpdateLightBuffer(Camera* cam, DirectX::XMVECTOR orthExts);
	void UpdateAmbientBuffer(DirectX::XMFLOAT4 sky, DirectX::XMFLOAT4 ground, DirectX::XMFLOAT4 horizon, DirectX::XMFLOAT4 fogC, DirectX::XMFLOAT4 sunFogC, DirectX::XMFLOAT4 Coefs);
	void UpdateAmbientBuffer();
	void UpdateSkyBuffer();
	void UpdateWaterBuffer();

	void DrawSky(Camera* cam);

	void StartProfileSection(std::string& name);
	void EndProfileSection(std::string& name);

private:

	void CheckDriverSupport();

	void Create(uint32_t screenheight, uint32_t screenwidth);
	void CreateViewports( uint32_t screenwidth, uint32_t screenheight );
	void CreateDepthStencil( uint32_t screenwidth, uint32_t screenheight );
	void CreateBackBuffers(uint32_t screenwidth, uint32_t screenheight);
	void CreateSwapChain( uint32_t screenwidth, uint32_t screenheight);
	void CreateStructuredBuffers(uint32_t screenwidth, uint32_t screenheight);
	void CreateDevice();
	void CreateRasterizers();
	void CreateBlendState();
	void CreateSamplers();
	void CreateConstantBuffers();
	void LoadShaders();

	void LoadDeferredShadingShader();
	void LoadWaterShader();

	void LoadShader(std::string name, ID3D11VertexShader** vertShader, ID3D11PixelShader** pixelShader, D3D11_INPUT_ELEMENT_DESC* layoutDesc, unsigned layoutDescCount, ID3D11InputLayout** layout);

	FILE* OpenShaderFile(std::string &filename );

	void CreateLightMenu();
	void CreateShadowMenu();
	void CreateSkyMenu();
	void CreateWaterMenu();

	void Shutdown();


private:

	HWND m_HWND;

	bool m_FullScreen;
	bool m_VsyncEnabled;
	bool m_Use32BitDepth;

	ID3D11Device* m_Device;
	ID3D11Debug* m_D3dDebug;
	ID3D11Buffer* m_PerFrameBuffer;
	ID3D11Buffer* m_PerObjectBuffer;
	ID3D11InfoQueue* m_D3dInfoQueue;
	ID3D11BlendState* m_WaterBlendState;
	ID3D11SamplerState* m_DefaultSampler;
	ID3D11SamplerState* m_TerrainSampler;
	ID3D11SamplerState* m_NormalSampler;
	ID3D11SamplerState* m_CubeSampler;
	ID3D11SamplerState* m_ShadowSampler;
	ID3D11Texture2D* m_DepthStencilBuffer;
	ID3D11ShaderResourceView* m_DepthStencilShaderResourceView;
	ID3D11DeviceContext* m_ImmediateContext;
	ID3D11DeviceContext* m_DebugContext;
	ID3D11DepthStencilView* m_DepthStencilView;
	ID3D11DepthStencilView* m_ShadowDepthStencilView;
	ID3D11DepthStencilView* m_ReadOnlyDepthStencilView;
	ID3D11DepthStencilState* m_DepthStencilState;
	ID3D11DepthStencilState* m_SkyBoxDepthStencilState;
	ID3D11Texture2D* m_BackBufferTexture;
	ID3D11UnorderedAccessView* m_BackBufferUnorderedAccessView;
	ID3D11Texture2D* m_RenderTextures[RENDERTARGET_COUNT];
	ID3D11Texture2D* m_ShadowDepthStencilTexture;
	ID3D11RenderTargetView* m_RenderTargetViews[RENDERTARGET_COUNT];
	ID3D11RenderTargetView* m_ShadowRenderTargetView;
	ID3D11RenderTargetView* m_BackBufferRenderTargetView;
	ID3D11ShaderResourceView* m_RendertargetShaderResourceViews[RENDERTARGET_COUNT];
	ID3D11ShaderResourceView* m_ShadowShaderResourceView;
	ID3D11RasterizerState* m_RasterState;
	ID3D11RasterizerState* m_WireframeRasterState;
	ID3D11RasterizerState* m_NoCullRasterState;
	ID3D11RasterizerState* m_CurrentSolidRasterstate;
	ID3D11Buffer* m_LightAccumulationBuffer;
	ID3D11ShaderResourceView* m_LightAccumulationResourceView;
	ID3D11UnorderedAccessView* m_LightAccumulationBufferUnorderedAccessView;
	ID3D11Buffer* m_lightBuffer;
	IDXGISwapChain* m_SwapChain;
	FPSCounter* m_FPSCounter;
	ID3D11Buffer* m_AmbientBuffer;
	ID3D11Buffer* m_SkyBuffer;
	ID3D11Buffer* m_WaterBuffer;

	D3D11_VIEWPORT m_CameraViewport;
	D3D11_VIEWPORT m_ShadowViewport;

	unsigned m_MSAASampleCount;
	unsigned m_MSAAQuality;

	ID3D11VertexShader* m_SingleLightVertexShader;
	ID3D11PixelShader* m_SingleLightPixelShader;
	ID3D11InputLayout* m_SingleLightLayout;
	
	ID3D11VertexShader* m_TerrainVertexShader;
	ID3D11PixelShader* m_TerrainPixelShader;
	ID3D11InputLayout* m_TerrainLayout;

	ID3D11VertexShader* m_WaterVertexShader;
	ID3D11PixelShader* m_WaterPixelShader;
	ID3D11InputLayout* m_WaterLayout;

	ID3D11VertexShader* m_SkySphereVertexShader;
	ID3D11PixelShader* m_SkySpherePixelShader;

	ID3D11VertexShader* m_ShadowMapVertexShader;
	ID3D11PixelShader* m_ShadowTestPixelShader;

	ID3D11VertexShader* m_GBufferVertexShader;
	ID3D11PixelShader* m_GBufferPixelShader;
	ID3D11PixelShader* m_GBufferAlphaTestPixelShader;

	ID3D11VertexShader* m_FullScreenTriangleVertexShader;

	ID3D11ComputeShader* m_DeferredShadingComputeShader;

	ID3D11VertexShader* m_InstanceGBufferVertexShader;
	ID3D11VertexShader* m_InstanceShadowMapVertexShader;
	ID3D11PixelShader* m_InstanceGBufferPixelShader;
	ID3D11PixelShader* m_InstanceGBufferAlphaTestPixelShader;
	ID3D11InputLayout* m_InstanceLayout;

	GPUProfiler* m_Profiler;

};

#endif