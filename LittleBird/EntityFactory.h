#ifndef _ENTITY_FACTORY_H_
#define _ENTITY_FACTORY_H_

class Entity;
class PositionComponent;
#include <string>

class EntityFactory
{
public:
	static Entity* CreatePropEntity(std::string modelName, std::string textureName, PositionComponent *pos);
private:
	
};

#endif