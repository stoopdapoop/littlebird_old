#include "RigidBodySystem.h"
#include "physics.h"
#include "PositionComponent.h"
#include "RigidBodyComponent.h"

RigidBodySystem::RigidBodySystem( Physics* phys ) : m_Physics(phys)
{
	throw std::string("do not use this, physics objects use motionstates now");
}

void RigidBodySystem::AddEntry( int ID, RigidBodyComponent* rigid, PositionComponent* pos )
{
	m_Roster.push_back(ID);
	m_RigidBodies.push_back(rigid);
	m_Positions.push_back(pos);
}

void RigidBodySystem::Run()
{
	using namespace DirectX;
	size_t entryCount = m_Roster.size();

	for(size_t i = 0; i < entryCount; ++i)
	{
		btTransform trans;
		m_RigidBodies[i]->RigidBody->getMotionState()->getWorldTransform(trans);
		btVector3 vecPos = trans.getOrigin();
		btQuaternion quatOrientation = trans.getRotation();
		m_Positions[i]->Position = *(reinterpret_cast<XMVECTOR*>(&vecPos));
		m_Positions[i]->Orientation  = *(reinterpret_cast<XMVECTOR*>(&quatOrientation));
	}

	m_Roster.clear();
	m_RigidBodies.clear();
	m_Positions.clear();
}


