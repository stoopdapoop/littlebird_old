#include "model.h"
#pragma warning(disable:4005)
#include <d3d11.h>
#pragma warning(default:4005)
#include <string>
#include <memory>
#include <assert.h>
#include "MemoryUtilities.h"
using namespace std;
using namespace DirectX;

#include "lz4.h"
#include "lz4hc.h"

#ifdef NDEBUG
#pragma comment(lib, "lz4c64.lib")
#else
#pragma comment(lib, "lz4c64d.lib")
#endif


Model::Model(ID3D11Device* device, string modelFilename): m_vertexCount(0), m_indexCount(0)
{
	m_vertexBuffer = nullptr;
	m_indexBuffer = nullptr;

	Initialize(device, modelFilename);
}

Model::~Model()
{
	Shutdown();
}

void Model::Initialize(ID3D11Device* device, string modelFilename)
{
	//Load in the model data,
	LoadModelPBM(modelFilename, m_Verts, m_Indicies);


	// Initialize the vertex and index buffers.
	InitializeBuffers(device, m_Verts);
}

void Model::Shutdown()
{
	// Release the vertex and index buffers.
	ShutdownBuffers();

	// Release the model data.
	ReleaseModel();
}

void Model::Render(ID3D11DeviceContext* deviceContext)const
{
	// Put the vertex and index buffers on the graphics pipeline to prepare them for drawing.
	RenderBuffers(deviceContext);
}


void Model::InitializeBuffers(ID3D11Device* device, vector<VertexType>& vertices)
{
	D3D11_BUFFER_DESC vertexBufferDesc, indexBufferDesc;
	D3D11_SUBRESOURCE_DATA vertexData, indexData;

	// Set up the description of the static vertex buffer.
	vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	vertexBufferDesc.ByteWidth = sizeof(VertexType) * m_vertexCount;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = 0;
	vertexBufferDesc.MiscFlags = 0;
	vertexBufferDesc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the vertex data.
	vertexData.pSysMem = &vertices[0];
	vertexData.SysMemPitch = 0;
	vertexData.SysMemSlicePitch = 0;

	// Now create the vertex buffer.
	HRESULT result = device->CreateBuffer(&vertexBufferDesc, &vertexData, &m_vertexBuffer);
	if(FAILED(result))
		throw string("Could not create vertexbuffer");

	// Set up the description of the static index buffer.
	indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	indexBufferDesc.ByteWidth = sizeof(uint16_t) * m_indexCount;
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags = 0;
	indexBufferDesc.MiscFlags = 0;
	indexBufferDesc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the index data.
	indexData.pSysMem = &m_Indicies[0];
	indexData.SysMemPitch = 0;
	indexData.SysMemSlicePitch = 0;

	// Create the index buffer.
	result = device->CreateBuffer(&indexBufferDesc, &indexData, &m_indexBuffer);
	if(FAILED(result))
		throw string("could not create index buffer");
}


void Model::LoadModelOBJ(string filename, vector<VertexType>& modelTypes)
{
	struct int3
	{
		int posInt;
		int texInt;
		int normInt;
	};

	ifstream fin;

	// Open the model file.
	fin.open(filename);
	
	// If it could not open the file then exit.
	if(fin.fail())
	{
		throw "Could not open model file "+filename;
	}

	unsigned currentlineNumber = 0;
	char currentline[1024];
	float tempFloat3[3];
	vector<XMFLOAT3> positions;
	vector<XMFLOAT3> normals;
	vector<XMFLOAT2> texCoords;
	vector<int3> faces;
	
	int vertIndex[4], normalIndex[4], texCoordIndex[4];

	// First we gather up the raw infos
	while(!fin.eof())
	{
		++currentlineNumber;
		fin.getline(currentline,1023);

		// Positions
		if(sscanf_s(currentline,"v %f%f%f", &tempFloat3[0], &tempFloat3[1], &tempFloat3[2]) == 3)
		{
			XMFLOAT3 temp(tempFloat3);
			positions.push_back(temp);
		}
		
		// Normals
		else if(sscanf_s(currentline,"vn %f%f%f", &tempFloat3[0], &tempFloat3[1], &tempFloat3[2]) == 3)
		{	
			XMFLOAT3 temp(tempFloat3);
			normals.push_back(temp);	
		}

		// Texture Coords
		else if(sscanf_s(currentline,"vt %f%f", &tempFloat3[0], &tempFloat3[1]) == 2)
		{
			XMFLOAT2 temp(tempFloat3[0], 1.0f - tempFloat3[1]);
			texCoords.push_back(temp);
		}

		// put face indicies into seperate vector so we don't have to read the file again to assemble faces.
		// Can't do it now because some faces can require verts that haven't been defined yet.
		else if(sscanf_s(currentline, "f %d/%d/%d %d/%d/%d %d/%d/%d", &vertIndex[0], &texCoordIndex[0], &normalIndex[0], &vertIndex[1], &texCoordIndex[1], 
							&normalIndex[1], &vertIndex[2], &texCoordIndex[2], &normalIndex[2]) == 9)
		{
			int3 temp;
			for(int i = 0; i < 3; ++i)
			{
				temp.posInt = vertIndex[i] -1;
				temp.texInt = texCoordIndex[i]-1;
				temp.normInt = normalIndex[i]-1;
				faces.push_back(temp);
			}	
		}
		else if(sscanf_s(currentline, "f %d/%d/%d %d/%d/%d %d/%d/%d %d/%d/%d", &vertIndex[0], &texCoordIndex[0], &normalIndex[0], &vertIndex[1], &texCoordIndex[1], 
			&normalIndex[1], &vertIndex[2], &texCoordIndex[2], &normalIndex[2], &vertIndex[3], &texCoordIndex[3], &normalIndex[3]) == 12)
		{
			int3 temp;
			for(int i = 0; i < 4; ++i)
			{
				temp.posInt = vertIndex[i]-1;
				temp.texInt = texCoordIndex[i]-1;
				temp.normInt = normalIndex[i]-1;
				faces.push_back(temp);
			}
		}

		else if(!(currentline[0] == '#' || currentline[0] == 'o' || currentline[0] == 'm' || currentline[0] == 0 || currentline[0] == 'g' || currentline[0] == 'u' || currentline[0] == 's'))
		{
			throw string("unexpected .obj input in " + filename + ": \"" + currentline + "\" at line number " + to_string(currentlineNumber));
		}
	}

	// for now
	m_indexCount = (int)faces.size() * 3;
	m_vertexCount = m_indexCount;
	modelTypes.reserve(m_vertexCount);

	for(unsigned int i = 0; i < faces.size(); ++i)
	{
		VertexType current;
		current.position = XMFLOAT3(positions[faces[i].posInt].x, positions[faces[i].posInt].y, positions[faces[i].posInt].z);

		current.texture = XMFLOAT2(texCoords[faces[i].texInt].x, texCoords[faces[i].texInt].y);

		current.normal = XMFLOAT3(normals[faces[i].normInt].x, normals[faces[i].normInt].y, normals[faces[i].normInt].z);

		modelTypes.push_back(current);
	}

	// Close the model file.
	fin.close();
}

void Model::ShutdownBuffers()
{
	SafeRelease(m_indexBuffer);
	SafeRelease(m_vertexBuffer);
}

void Model::RenderBuffers(ID3D11DeviceContext* deviceContext)const
{
	unsigned int stride = sizeof(VertexType);
	unsigned int offset = 0;
	
	// Set the vertex buffer to active in the input assembler so it can be rendered.
	deviceContext->IASetVertexBuffers(0, 1, &m_vertexBuffer, &stride, &offset);

	// Set the index buffer to active in the input assembler so it can be rendered.
	deviceContext->IASetIndexBuffer(m_indexBuffer, DXGI_FORMAT_R16_UINT, 0);

}

void Model::ReleaseModel()
{

}


void Model::LoadModelPBM( std::string filename, std::vector<VertexType>& verticies, std::vector<uint16_t>& indicies)
{
	ifstream inFile(filename, ios::in | ios::binary);
	if(!inFile.is_open())
		throw string("could not open model file: " + filename);

	char buffer[128];

	inFile.read(buffer, 5);
	if(strcmp(buffer, "POOP"))
		throw string(filename + " missing pbm header");

	inFile.read(buffer, sizeof(uint32_t));
	uint32_t versionNumber = *reinterpret_cast<uint32_t*>(buffer);
	if(versionNumber != 7)
		throw string(filename + " has an invalid version number");
	
	inFile.read(buffer, sizeof(uint32_t));
	uint32_t vertexType = *reinterpret_cast<uint32_t*>(buffer);
	if(vertexType != 1)
		throw string(filename + " uses a different vertex encoding than expected");

	inFile.read(buffer, sizeof(uint32_t));
	uint32_t readSize = *reinterpret_cast<uint32_t*>(buffer);

	inFile.read(buffer, sizeof(uint32_t));
	uint32_t vertexCount = *reinterpret_cast<uint32_t*>(buffer);

	inFile.read(buffer, sizeof(uint32_t));
	uint32_t indexCount = *reinterpret_cast<uint32_t*>(buffer);

	
	inFile.read((char*)m_BoundingBoxCenter, sizeof(float) * 3);
	inFile.read((char*)m_BoundingBoxHalfExtents, sizeof(float) * 3);
	
	m_vertexCount = vertexCount;
	m_indexCount = indexCount;

	unique_ptr<char[]> readbuff(new char[readSize]);
	unique_ptr<char[]> output(new char[sizeof(VertexType) * vertexCount + sizeof(uint16_t) * indexCount]);

	inFile.read((char*)readbuff.get(), readSize);

	LZ4_decompress_fast(readbuff.get(), output.get(), sizeof(VertexType) * vertexCount + sizeof(uint16_t) * indexCount);

	verticies.insert( verticies.begin() , (VertexType*)output.get() , (VertexType*)output.get() + vertexCount ); 
	unsigned* indexOffset = (unsigned*)(output.get()+ vertexCount* sizeof(VertexType));
	indicies.insert(indicies.begin(), (unsigned short*)indexOffset, (unsigned short*)indexOffset+indexCount);
}

CollisionModel::CollisionModel( std::string modelName )
{
	ifstream inFile(modelName, ios::in | ios::binary);
	if(!inFile.is_open())
		throw string("could not open collision model file: " + modelName);

	char buffer[128];

	inFile.read(buffer, 5);
	if(strcmp(buffer, "POOP"))
		throw string(modelName + " collision model missing pbm header");

	inFile.read(buffer, sizeof(uint32_t));
	uint32_t versionNumber = *reinterpret_cast<uint32_t*>(buffer);
	if(versionNumber != 7)
		throw string(modelName + " collision model has an invalid version number");

	inFile.read(buffer, sizeof(uint32_t));
	uint32_t vertexType = *reinterpret_cast<uint32_t*>(buffer);
	if(vertexType != 2)
		throw string(modelName + " collision model uses a different vertex encoding than expected");

	inFile.read(buffer, sizeof(uint32_t));
	uint32_t readSize = *reinterpret_cast<uint32_t*>(buffer);

	inFile.read(buffer, sizeof(uint32_t));
	uint32_t vertexCount = *reinterpret_cast<uint32_t*>(buffer);
	m_VertexCount = vertexCount;

	inFile.read(buffer, sizeof(uint32_t));
	uint32_t indexCount = *reinterpret_cast<uint32_t*>(buffer);
	// silence warning
	indexCount;

	inFile.read(buffer, sizeof(uint32_t)*6);
	//throw string("implement bounds stuff");

	unique_ptr<char[]> readbuff(new char[readSize]);
	m_Verticies = new CollisionVertexType[vertexCount];

	inFile.read((char*)readbuff.get(), readSize);

	LZ4_decompress_fast(readbuff.get(), (char*)m_Verticies, sizeof(CollisionVertexType) * vertexCount);
}

CollisionModel::~CollisionModel()
{
	SafeDeleteArray(m_Verticies);
}
