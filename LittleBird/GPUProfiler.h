#ifndef _PROFILER_H_
#define _PROFILER_H_

#pragma warning(disable:4351)

#include <string>
#include <unordered_map>
#include "timer.h"

struct  ID3D11Device;
struct ID3D11DeviceContext;
struct ID3D11Query;

class GPUProfiler
{

public:
	GPUProfiler(ID3D11Device* device, ID3D11DeviceContext* immContext);
	void StartProfile(const std::string& name);
	void EndProfile(const std::string& name);
	float GetProfileTime(const std::string& name);

	void EndFrame();

private:

	void Initialize(ID3D11Device* device, ID3D11DeviceContext* immContext);
	void CreateDebugUI();

private:

	// Constants
	static const unsigned __int64 QueryLatency = 30;

	struct ProfileData
	{
		ID3D11Query* DisjointQuery[QueryLatency];
		ID3D11Query* TimestampStartQuery[QueryLatency];
		ID3D11Query* TimestampEndQuery[QueryLatency];
		bool QueryStarted;
		bool QueryFinished;

		ProfileData() : QueryStarted(false), QueryFinished(false), DisjointQuery(), TimestampStartQuery(), TimestampEndQuery(){}
	};

	typedef std::unordered_map<std::string, ProfileData> ProfileMap;
	typedef std::unordered_map<std::string, float> TimeMap;

	TimeMap m_Times;
	ProfileMap m_Profiles;
	unsigned __int64 m_CurrFrame;

	ID3D11Device* m_Device;
	ID3D11DeviceContext* m_Context;

	bool m_Enabled;

	Timer m_Timer;
};


#endif