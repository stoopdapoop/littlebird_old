
#include "PlayerSystem.h"
#include "PlayerComponent.h"
#include "RigidBodyComponent.h"
#include "BulletCollision\CollisionDispatch\btGhostObject.h"
#include "camera.h"
#include "services.h"
#include <DirectXMath.h>
using namespace DirectX;

PlayerSystem::PlayerSystem() : m_UsePlayerCam(false)
{
	DECLARE_CONSOLECOMMAND("player_use_playercam", PlayerSystem::Console_SetUseCamera);
}

void PlayerSystem::AddEntry( unsigned ID, PlayerComponent *player, PositionComponent* position )
{
	m_Roster.push_back(ID);
	m_Positions.push_back(position);
	m_Players.push_back(player);
}

void PlayerSystem::Run( float delta, Camera* camera )
{
	delta;

	// since we're moving relative to camera, we need to get camera vecs
	XMVECTOR forwardVec = camera->GetForwardVector();
	XMVECTOR rightVec = camera->GetRightVector();

	//then we gotta project and normalize them in world space to get the direction that the player should be moving in.
	forwardVec = XMVectorSetY(forwardVec, 0.0f);
	forwardVec = XMVector3Normalize(forwardVec);
	rightVec = XMVectorSetY(rightVec, 0.0f);
	rightVec = XMVector3Normalize(rightVec);
	
	
	//accumulate desired movement along local space axeseseses
	Input* input = Services::GetInput();
	float forwardInfluence = 0.0f;
	forwardInfluence += input->KeyDown(DIK_W) ? 1.0f : 0.0f;
	forwardInfluence -= input->KeyDown(DIK_S) ? 1.0f : 0.0f;
	float rightInfluence = 0.0f;
	rightInfluence += input->KeyDown(DIK_D) ? 1.0f : 0.0f;
	rightInfluence -= input->KeyDown(DIK_A) ? 1.0f : 0.0f;
	XMVECTOR directionalInfluence;
	

	directionalInfluence = XMVectorScale(rightVec, rightInfluence);
	directionalInfluence = XMVectorAdd(directionalInfluence, XMVectorScale(forwardVec, forwardInfluence));

	size_t count = m_Roster.size();
	for(int i = 0; i < count; ++i)
	{

		//btRigidBody* rb = rigid->RigidBody;
		PositionComponent* pos = m_Positions[i];
		PlayerComponent* player = m_Players[i];	
		btKinematicCharacterController* kineCon = player->kinematicController;
		kineCon->setJumpSpeed(30.0f);
		kineCon->setGravity(50.0f);

		btTransform trans = kineCon->getGhostObject()->getWorldTransform();

		btQuaternion tempQuat = trans.getRotation();

		XMVECTOR playerPos = *(XMVECTOR*)&trans.getOrigin();
		XMVECTOR playerRot = *(XMVECTOR*)&tempQuat;

		pos->Position = playerPos;
		pos->Orientation = playerRot;

		directionalInfluence = XMVector3Normalize(directionalInfluence);
		directionalInfluence = XMVectorScale(directionalInfluence, player->MaxMoveSpeed);
		if(kineCon->onGround())
		{
			kineCon->setWalkDirection(*(btVector3*)&directionalInfluence);
		}

		if(input->KeyPressed(DIK_SPACE))
			kineCon->jump();
		
		//btVector3 btFrom = *(btVector3*)&rigid->Position->Position;
		//btVector3 btTo = btFrom - btVector3(0.0f, 0.5f, 0.0f);
		//btCollisionWorld::ClosestRayResultCallback res(btFrom, btTo);

		//phys->GetWorld()->rayTest(btFrom, btTo, res); // m_btWorld is btDiscreteDynamicsWorld


		if(m_UsePlayerCam)
		{
			camera->SetPosition(XMVectorAdd(playerPos, XMVectorSet(0.0f, player->ViewHeight*0.5f, 0.0f, 0.0f)));
		}	
	}



	m_Roster.clear();
	m_Positions.clear();
	m_Players.clear();
}

std::wstring PlayerSystem::Console_SetUseCamera( std::wstring args )
{
	if(args == L"1")
		m_UsePlayerCam = true;
	else if( args == L"0")
		m_UsePlayerCam =false;

	else
		return L"Invalid input";

	return args;
}
