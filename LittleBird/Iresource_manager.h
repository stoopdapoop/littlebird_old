#ifndef _I_RESOURCE_MANAGER_
#define _I_RESOURCE_MANAGER_

#include "ResourceHandle.h"
#include <string>

class Texture;

class IResourceManager
{
public:
	virtual ResourceHandle GetModelHandle(std::string modelName) = 0;
	virtual ResourceHandle GetTextureHandle(std::string textureName) = 0;
	virtual Texture* GetTextureFromName(std::string textureName) = 0;
};

#endif
