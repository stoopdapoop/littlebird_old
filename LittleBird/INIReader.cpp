
#include "INIReader.h"
#include "MemoryUtilities.h"
#include <string>
using namespace std;

INIReader::INIReader()
{
	m_INI = new INI_t(settingsFileName);
}

INIReader::~INIReader()
{
	SafeDelete(m_INI);
}

void INIReader::SetSection( std::string sectionName )
{
	bool found = m_INI->select(sectionName);

	if(!found)
	{
		bool success = m_INI->create(sectionName);
		if(!success)
			throw string("could not create section" + sectionName);
	}
}

