
#include "FPSCounter.h"
#include "services.h"
#include <memory.h>
using namespace std;

FPSCounter::FPSCounter()
{
	m_fps = 0;
	m_count = 0;
	m_startTime = timeGetTime();
}


FPSCounter::~FPSCounter()
{
}

void FPSCounter::Initialize()
{
	CreateDebugMenu();
}

void FPSCounter::Frame()
{
	++m_count;

	if(timeGetTime() >= (m_startTime + 1000))
	{
		m_fps = m_count;
		m_count = 0;

		m_startTime = timeGetTime();
	}
}

int FPSCounter::GetFps() const
{
	return m_fps;
}

void FPSCounter::CreateDebugMenu()
{
	DebugUI* dGUI = Services::GetDebugUI();
	string name = "FrameStats";
	dGUI->CreateMenu(name);
	dGUI->AddReadOnlyVariable(name, "FPS", DEBUG_UI_TYPE::INT32, &m_fps);
	dGUI->SetMenuSize(name, 50, 50);
	dGUI->SetMenuRefreshRate(name, 0.5f);
}
