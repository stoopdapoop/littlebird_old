#include "component.h"
#include "services.h"
#include <string>

using namespace std;

Component::Component()
{
	throw std::string("Cannot call default constructor on component class");
}

Component::Component( ComponentID type, char replicationFlags ) : m_ID(type), m_ReplicationFlags(replicationFlags)
{
}

void Component::NetworkSerialize(RakNet::BitStream& bs)
{
	throw string("Network serializer not implemented for:" + to_string((ComponentID_t)m_ID));
}

void Component::NetworkDeserialize(RakNet::BitStream& bs)
{
	throw string("Network deserializer not implemented for:" + to_string((ComponentID_t)m_ID));
}
