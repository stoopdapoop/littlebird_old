#include "GPUProfiler.h"
#pragma warning(disable:4005)
#include <d3d11.h>
#pragma warning(default:4005)
#include <DirectXMath.h>
#include "services.h"
#include "DebugDrawer.h"
using std::string;
using std::map;
using namespace std;
using namespace DirectX;

GPUProfiler::GPUProfiler(ID3D11Device* device, ID3D11DeviceContext* immContext): m_Enabled(true)
{
	Initialize(device, immContext);
	CreateDebugUI();
}

void GPUProfiler::Initialize(ID3D11Device* device, ID3D11DeviceContext* immContext)
{
    this->m_Device = device;
    this->m_Context = immContext;
	m_CurrFrame = 0;
}

void GPUProfiler::StartProfile(const string& name)
{
	if(!m_Enabled)
		return;

    ProfileData& profileData = m_Profiles[name];
    _ASSERT(profileData.QueryStarted == FALSE);
    _ASSERT(profileData.QueryFinished == FALSE);
    
    if(profileData.DisjointQuery[m_CurrFrame] == NULL)
    {
        // Create the queries
        D3D11_QUERY_DESC desc;
        desc.Query = D3D11_QUERY_TIMESTAMP_DISJOINT;
        desc.MiscFlags = 0;
        m_Device->CreateQuery(&desc, &profileData.DisjointQuery[m_CurrFrame]);

        desc.Query = D3D11_QUERY_TIMESTAMP;
        m_Device->CreateQuery(&desc, &profileData.TimestampStartQuery[m_CurrFrame]);
        m_Device->CreateQuery(&desc, &profileData.TimestampEndQuery[m_CurrFrame]);
    }

    // Start a disjoint query first
    m_Context->Begin(profileData.DisjointQuery[m_CurrFrame]);

    // Insert the start timestamp    
    m_Context->End(profileData.TimestampStartQuery[m_CurrFrame]);

    profileData.QueryStarted = TRUE;
}

void GPUProfiler::EndProfile(const string& name)
{
	if(!m_Enabled)
		return;

    ProfileData& profileData = m_Profiles[name];
    _ASSERT(profileData.QueryStarted == TRUE);
    _ASSERT(profileData.QueryFinished == FALSE);

    // Insert the end timestamp    
    m_Context->End(profileData.TimestampEndQuery[m_CurrFrame]);

    // End the disjoint query
    m_Context->End(profileData.DisjointQuery[m_CurrFrame]);

    profileData.QueryStarted = FALSE;
    profileData.QueryFinished = TRUE;
}

static string menuText;
void GPUProfiler::EndFrame()
{
    if(!m_Enabled)
        return;

    m_CurrFrame = (m_CurrFrame + 1) % QueryLatency;    

    float queryTime = 0.0f;

    // Iterate over all of the profiles
    ProfileMap::iterator iter;
	menuText = "";
    for(iter = m_Profiles.begin(); iter != m_Profiles.end(); iter++)
    {
        ProfileData& profile = (*iter).second;
		menuText += (*iter).first + ":";
        if(profile.QueryFinished == FALSE)
            continue;

        profile.QueryFinished = FALSE;

        if(profile.DisjointQuery[m_CurrFrame] == NULL)
            continue;

        // Get the query data
        unsigned __int64 startTime = 0;
        while(m_Context->GetData(profile.TimestampStartQuery[m_CurrFrame], &startTime, sizeof(startTime), 0) != S_OK);

        unsigned __int64 endTime = 0;
        while(m_Context->GetData(profile.TimestampEndQuery[m_CurrFrame], &endTime, sizeof(endTime), 0) != S_OK);

        D3D11_QUERY_DATA_TIMESTAMP_DISJOINT disjointData;
        while(m_Context->GetData(profile.DisjointQuery[m_CurrFrame], &disjointData, sizeof(disjointData), 0) != S_OK);

        queryTime += (float)m_Timer.DeltaTime()*1000.0f;

        float time = 0.0f;
        if(disjointData.Disjoint == FALSE)
        {
            unsigned __int64 delta = endTime - startTime;
            float frequency = static_cast<float>(disjointData.Frequency);
            time = (delta / frequency) * 1000.0f;
        }  
		m_Times[(*iter).first] = time;
		menuText += to_string(time) + "\n";
    }
	DebugDrawer* deb = Services::GetDebugDrawer();
	deb->drawText(5.0f, 5.0f, menuText);
}

float GPUProfiler::GetProfileTime( const std::string& name )
{
	float t = m_Times[name];
	return t;
}

void GPUProfiler::CreateDebugUI()
{
	DebugUI* deb = Services::GetDebugUI();
	string name = "GPUQueries";
	deb->CreateMenu(name);
	deb->AddReadWriteBool(name, "Enabled", &m_Enabled);
}

