#ifndef _VERTEX_FORMATS_H
#define  _VERTEX_FORMATS_H

#include <D3D11.h>
#include <DirectXMath.h>

struct VertexType
{
	DirectX::XMFLOAT3 position;
	//DirectX::PackedVector::XMSHORT2 texture;
	DirectX::XMFLOAT3 normal;
	DirectX::XMFLOAT4 tangent;
	DirectX::XMFLOAT4 binormal;
	DirectX::XMFLOAT2 texture;
};

struct TerrainVertexType
{
	DirectX::XMFLOAT4 xy_texture;
};


struct CollisionVertexType
{
	DirectX::XMFLOAT3 position;
};

#endif // !_VERTEX_FORMATS_H
