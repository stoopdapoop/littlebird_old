#ifndef _TERRAIN_MANAGER_H_
#define _TERRAIN_MANAGER_H_

#include <string>
#include <DirectXMath.h>
#include <unordered_map>
#include "ResourceHandle.h"
#include ".\Shaders\terrainConstants.h"

class Terrain;
struct ID3D11Device;
class btRigidBody;

struct IWICImagingFactory;



struct TerrainCollisionPair {
	Terrain* terrain;
	btRigidBody* collider;
};

class TerrainManager
{
public:
	TerrainManager(std::string mapname, int terrainStride, int worldGridWidthCount, ID3D11Device* device);
	~TerrainManager();

	void Update(DirectX::XMVECTOR viewPosition);

	void RemoveDistantTiles();


	int GetTerrainStride() { return m_TerrainStride; }
	const std::vector<TerrainCollisionPair>* GetVisibleTiles() { return &m_VisibleTilesThisFrame; }
	static bool CompareTerrainForRendering(TerrainCollisionPair t1, TerrainCollisionPair t2);
	Terrain* GetTerrain(int x, int z);

	void CalculateTilePosition( DirectX::XMVECTOR viewPosition , int& x, int& z);
	
	void SetTerrainStride(int oddStride);

	
	
private:
	static std::string GenerateTerrainKey(int x, int z);
	std::string GenerateLongPositionString(int x, int z);
	bool IsTileWithinMapBoundary(int x, int z);
	int CalculateLOD(int terrainX, int terrainZ);
	void DecorateTerrain(Terrain* ter);
	
private:
	std::unordered_map<std::string, Terrain*> m_TerrainGrid;
	std::unordered_map<std::string, btRigidBody*> m_ColliderGrid;
	std::vector<TerrainCollisionPair> m_VisibleTilesThisFrame;
	std::string m_MapName;
	ID3D11Device* m_Device;
	int m_currentTileX;
	int m_currentTileY;
	// must be an odd number, currently used to figure out how many tiles are in the current working set.
	int m_TerrainStride;
	// number of tiles wide the map is in total
	int m_MaxGridWidthCount;
	//
	IWICImagingFactory* m_pIWICFactory;
};

#endif