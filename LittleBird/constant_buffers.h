#ifndef _CONSTANT_BUFFERS_H
#define _CONSTANT_BUFFERS_H

#include <DirectXMath.h>



struct PerFrameBuffer
{
	DirectX::XMMATRIX CameraWorldMatrix;
	DirectX::XMMATRIX ViewMatrix;
	DirectX::XMMATRIX ViewProjectionMatrix;
	DirectX::XMMATRIX ProjectionMatrix;
	DirectX::XMVECTOR CameraPosition;
	DirectX::XMMATRIX LightViewProjectionMatrix;
	float AbsoluteTime;
	float padding[3];
};

struct PerObjectBuffer
{
	DirectX::XMMATRIX WorldViewProjectionMatrix;
	DirectX::XMMATRIX WorldViewMatrix;
	DirectX::XMMATRIX WorldMatrix;
};


struct LightBuffer
{
	DirectX::XMFLOAT4 ambientColor;
	DirectX::XMFLOAT4 diffuseColor;
	DirectX::XMFLOAT4 specularColor;
	DirectX::XMFLOAT4 lightDirection;
	DirectX::XMFLOAT4 lightPosition;
	DirectX::XMMATRIX directionalMatrix;
	DirectX::XMFLOAT2 orhographicExtents;
};

struct AmbientBuffer
{
	DirectX::XMFLOAT4 sky;
	DirectX::XMFLOAT4 ground;
	DirectX::XMFLOAT4 horizon;
	DirectX::XMFLOAT4 fogColor;
	DirectX::XMFLOAT4 sunFogColor;
	DirectX::XMFLOAT4 coefs;
	DirectX::XMFLOAT4 testThing;
};

struct SkyBuffer
{
	float RayleighFactor;
	float MieFactor;
	float SurfaceHeight;
	float SpotBrightness;
	unsigned StepCount;
	float ScatterStrength;
	float RayleighScatterStrength;
	float MieScatterStrength;
	float RayLeighCollectionPower;
	float MieCollectionPower;
	float padding[2];
};

struct WaterBuffer
{
	float AmpFactor;
	float BaseFactor;
	float WaveTimeScaler;
	float WaterFadeFactor;
	float BigWaveScrollDirection[2];
	float SmallWaveScrollDirection[2];
	DirectX::XMFLOAT4 DeepColor;
	DirectX::XMFLOAT4 ShallowColor;
	//float padding[2];
};

#endif