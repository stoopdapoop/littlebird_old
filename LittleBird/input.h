#ifndef _INPUT_H_
#define _INPUT_H_

#define DIRECTINPUT_VERSION 0x0800

#include <dinput.h>
#include <Xinput.h>
#include <string>

#define KB_BUFFER_SIZE 10

enum ControllerButton
{
DPAD_UP =		0x0001,
DPAD_DOWN =		0x0002,
DPAD_LEFT =		0x0004,
DPAD_RIGHT =	0x0008,
START =			0x0010,
BACK =			0x0020,
LEFT_STICK =	0x0040,
RIGHT_STICK =	0x0080,
LEFT_BUMPER =	0x0100,
RIGHT_BUMPER =	0x0200,
A =				0x1000,
B =				0x2000,
X =				0x4000,
Y =				0x8000
};

// all values normalized
struct ControllerState
{
	//x and y -1 to 1
	float StickLeftCircular[2];
	float StickRightCircular[2];
	float StickLeftRectangular[2];
	float StickRightRectangular[2];
	float StickLeftMagnitude;
	float StickRightMagnitude;
	//0-1
	float TrifferLeft;
	float TriggerRight;

	float VibrationLeft;
	float VibrationRight;
	//0-1
	float DeadZone;
	float ValidRange;

	//0-whatever
	float StickLeftXSensitivity;
	float StickLeftYSensitivity;
	float StickRightXSensitivity;
	float StickRightYSensitivity;

	unsigned short CurrentButtons;
	unsigned short PreviousButtons;

	bool controllerConnected;

	bool ButtonDown(ControllerButton butt) { return (CurrentButtons & butt) != 0; }
	bool ButtonPressed(ControllerButton butt) { return (CurrentButtons & butt && !(PreviousButtons & butt)); }
};

class Input
{
public:
	Input( HINSTANCE hinstance, HWND hwnd, int screenWidth, int screenHeight );
	Input(const Input&);
	~Input();

	std::string GetBufferedChars() { return m_BufferedChars; }

	void GetMouseLocation(int&, int&) const;
	void GetMouseDelta(int& x, int& y) const;
	void GetMouseDelta(int& x, int& y, int& z) const;
	int GetMouseWheelDelta() const;

	ControllerState& GetControllerState() { return m_ControlerState; }

	bool MouseButtonDown(unsigned char button) const;
	bool MouseButtonPressed(unsigned char button) const;
	bool MouseButtonReleased(unsigned char button) const;

	bool KeyDown(const unsigned char key, bool consume = false);
	bool KeyPressed(unsigned char key, bool consume = false);
	bool KeyReleased(unsigned char key, bool consume = false);

	char Input::DIKToChar(DWORD scanCode);

	bool IsCursorEnabled()		{return m_CursorEnabled;}

	void ToggleCursor();

	void Shutdown();

	void Frame();

private:
	void Initialize(HINSTANCE hinstance, HWND hwnd, int screenWidth, int screenHeight);

	void CreateControllerMenu();

	bool ReadKeyboard();
	bool ReadMouse();
	void ReadController();

	void ProcessInput();

	void ProcessController();

	void ProcessKeyboard();

	void ProcessMouse();

	void NormalizeStick(float inX, float inY, float& outXCircular, float& outYCircular, float& outXRectangular, float& outYRectangular);

private:
	IDirectInput8* m_DirectInput;
	IDirectInputDevice8* m_Keyboard;
	IDirectInputDevice8* m_Mouse;
	XINPUT_STATE m_Controller;
	ControllerState m_ControlerState;

	unsigned char m_KeyboardState[256];
	unsigned char m_KeyboardPressed[256];
	unsigned char m_KeyboardReleased[256];
	DWORD	m_KeyboardBufferedEventCount;
	
	DIMOUSESTATE m_MouseState;
	unsigned char m_MousePressed[4];
	unsigned char m_MouseReleased[4];
	DWORD	m_MouseBufferedEventCount;

	DIDEVICEOBJECTDATA m_KeyboardBuffer[KB_BUFFER_SIZE];
	DIDEVICEOBJECTDATA m_MouseBuffer[KB_BUFFER_SIZE];

	int m_ScreenWidth, m_ScreenHeight;
	int m_MouseX, m_MouseY;
	bool m_CursorEnabled;
	HWND m_Hwnd;

	std::string m_BufferedChars;
};

#endif