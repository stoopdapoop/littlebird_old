#ifndef _ENTITY_H
#define _ENTITY_H

#include <unordered_map>
#include "component.h"
#include "Networking.h"
class Component;



class Entity : public RakNet::NetworkIDObject
{
public:
	Entity(char replicationFlags);
	~Entity();

	unsigned GetID() { return m_ID; }

	template<class T>
	void AddComponent(T* com);
	//int HasComponent(ComponentMask compMask) { return (m_ComponentMasks & (1 << (unsigned)compMask));}
	ComponentMask_t GetComponentMasks() {return m_ComponentMasks; }
	Component* GetComponent(ComponentID id) { return m_Components[id]; }

private:
	
private:
	char m_ReplicationFlags;
	unsigned m_ID;
	ComponentMask_t m_ComponentMasks;
	static unsigned m_NextID;

public:
	std::unordered_map<ComponentID, Component*> m_Components;
};

template<class T>
void Entity::AddComponent( T* com )
{

	ComponentMask_t mask = com->GetComponentMask();
	ComponentID compID = com->GetComponentID();
	m_ComponentMasks |= mask;

	auto existing = m_Components.find(compID);

	if(existing != m_Components.end())
	{
		delete (*existing).second;
		if((*existing).second == com)
			throw std::string("can't add same component to entity twice");
	}

	char replicationFlags = com->GetReplicationFlags();

	if( replicationFlags == Authoritative || replicationFlags == ReplicateAndMakeLocal )
	{
		// can't replicate component if entity isn't replicated
		if(m_ReplicationFlags == LocalOnly || m_ReplicationFlags == Replicated)
			throw string("can't add authoritative component to non authoritative entity");
		Networking* net = Services::GetNetworking();
		net->SendReplicateComponent<T>(GetNetworkID(), com);
	}
	
	m_Components[compID] = com;
}

#endif