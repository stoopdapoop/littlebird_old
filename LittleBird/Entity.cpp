#include "entity.h"
#include "services.h"
#include "component.h"

unsigned Entity::m_NextID;

Entity::Entity(char replicationFlags) : m_ID(m_NextID++), m_ReplicationFlags(replicationFlags)
{
	Networking* net = Services::GetNetworking();
	SetNetworkIDManager(net->GetNetworkIDManager());
	m_ComponentMasks = 0;
	if(replicationFlags != ReplicationFlags::LocalOnly && replicationFlags != ReplicationFlags::Replicated)
		net->SendReplicateEntity(GetNetworkID(), m_ReplicationFlags);
}

Entity::~Entity()
{
	for(auto it = m_Components.begin(); it != m_Components.end(); ++it)
	{
		delete (*it).second;
	}

}


