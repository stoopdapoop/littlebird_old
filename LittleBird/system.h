#ifndef _SYSTEM_H_
#define _SYSTEM_H_

#define NOMINMAX

#include <windows.h>
#include <memory>
//#include "game.h"

class INIReader;
class ChessGame;

class System
{
public:
	System();
	~System();

	void Run();

	LRESULT CALLBACK System::MessageHandler(HWND hwnd, UINT umsg, WPARAM wparam, LPARAM lparam);

private:
	void InitializeWindows(int& height, int& width);
	bool Frame();

private:
	LPCWSTR m_applicationName;
	HINSTANCE m_hinstance;
	HWND m_hwnd;

	bool m_FullScreen;
	bool m_VSync;
	int m_WindowedScreenWidth;
	int m_WindowedScreenHeight;

	std::unique_ptr<ChessGame> m_Game;
	std::unique_ptr<INIReader> m_INIReader;
};

static LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

static System* ApplicationHandle;


#endif