
#include "texture.h"
#pragma warning(disable:4005)
#include "DirectxTK/DDSTextureLoader.h"
#include "DirectxTK/WICTextureLoader.h"
#pragma warning(default:4005)
#include "renderer.h"
#include "StringUtilities.h"
#include "services.h"
using namespace std;
using namespace DirectX;

Texture::Texture(Renderer* rend,  std::string filename): m_ResourceView(nullptr), m_Resource(nullptr)
{
	InitializeFromFile(rend, filename);
}

Texture::Texture( ID3D11Device* device, UCHAR* data, unsigned format, unsigned width, unsigned height)
{
	// Create texture
	D3D11_TEXTURE2D_DESC desc;
	ZeroMemory(&desc, sizeof(desc));
	desc.Width = width;
	desc.Height = height;
	desc.MipLevels = 1;
	desc.ArraySize = 1;
	desc.Format = (DXGI_FORMAT)format;
	desc.SampleDesc.Count = 1;
	desc.SampleDesc.Quality = 0;
	desc.Usage = D3D11_USAGE_DEFAULT;
	desc.CPUAccessFlags = 0;

	desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
	desc.MiscFlags = 0;


	D3D11_SUBRESOURCE_DATA initData;
	initData.pSysMem = data;
	initData.SysMemPitch = static_cast<UINT>( width * sizeof(unsigned short) );
	initData.SysMemSlicePitch = static_cast<UINT>( width * height * sizeof(unsigned short) );
	
	ID3D11Texture2D* tempTex;

	HRESULT hr = device->CreateTexture2D( &desc, &initData, &tempTex );
	if(FAILED(hr))
		
	
	m_Resource = tempTex;

	D3D11_SHADER_RESOURCE_VIEW_DESC SRVDesc;
	memset( &SRVDesc, 0, sizeof( SRVDesc ) );
	SRVDesc.Format = desc.Format;

	SRVDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	SRVDesc.Texture2D.MipLevels = 1;

	hr = device->CreateShaderResourceView( tempTex, &SRVDesc, &m_ResourceView );
	if ( FAILED(hr) )
	{
		throw string("failed to make texture from memory");
	}
}


Texture::~Texture()
{
	Shutdown();
}

void Texture::InitializeFromFile(Renderer* rend, string filename)
{
	HRESULT result;
	string extn = filename.substr(filename.size()-4);
	wstring wideName = wstring(filename.begin(),filename.end());

	if(extn == ".dds")
	{
		result = CreateDDSTextureFromFile(rend->GetDevice(), wideName.c_str(), &m_Resource, &m_ResourceView);
	}
	else
	{
		result = CreateWICTextureFromFile( rend->GetDevice(), rend->GetImmediateContext(), wideName.c_str(), &m_Resource, &m_ResourceView );

	}

	if(FAILED(result))
	{
		string errordesc("Bad time loading " + filename) ;
		throw TextureException(errordesc);
	}
}

void Texture::Shutdown()
{
	// Release the texture resource.
	if(m_ResourceView)
	{
		m_ResourceView->Release();
		m_ResourceView = 0;
	}
}

ID3D11ShaderResourceView* Texture::GetResourceView()
{
	return m_ResourceView;
}


