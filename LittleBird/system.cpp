#include "system.h"
#include <string>
#include "StringUtilities.h"
#include "AntTweakBar.h"
#include "INIReader.h"
#include "ChessGame.h"
#include "services.h"
using namespace std;


System::System():m_FullScreen(false), m_VSync(true), m_WindowedScreenWidth(1920/2), m_WindowedScreenHeight(1080/2)
{
	int screenHeight = 0;
	int screenWidth = 0;

	m_INIReader.reset( new INIReader() );
	Services::ProvideINIReader(m_INIReader.get());
	m_INIReader->SetSection("Graphics_Options");
	m_FullScreen = m_INIReader->GetValue("FullScreen", m_FullScreen);
	m_WindowedScreenWidth = m_INIReader->GetValue("ResolutionWidth", m_WindowedScreenWidth);
	m_WindowedScreenHeight = m_INIReader->GetValue("ResolutionHeight", m_WindowedScreenHeight);
	m_VSync = m_INIReader->GetValue("VSync", m_VSync);
	InitializeWindows(screenHeight, screenWidth);
	m_Game.reset(new ChessGame(screenHeight, screenWidth, m_hwnd, m_hinstance, m_FullScreen, m_VSync));
}

System::~System()
{

}

void System::InitializeWindows(int& height, int& width)
{

	// Get the instance of this application.
	m_hinstance = GetModuleHandle(NULL);

	ApplicationHandle = this;

	// Give the application a name.
	m_applicationName = L"PoopEngine";

	// Setup the windows class with default settings.
	WNDCLASSEX wc;
	wc.style         = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc   = WndProc;
	wc.cbClsExtra    = 0;
	wc.cbWndExtra    = 0;
	wc.hInstance     = m_hinstance;
	wc.hIcon		 = LoadIcon(NULL, IDI_WINLOGO);
	wc.hIconSm       = wc.hIcon;
	wc.hCursor       = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wc.lpszMenuName  = NULL;
	wc.lpszClassName = m_applicationName;
	wc.cbSize        = sizeof(WNDCLASSEX);

	// Register the window class.
	RegisterClassEx(&wc);

	// Determine the resolution of the clients desktop screen.
	width  = GetSystemMetrics(SM_CXSCREEN);
	height = GetSystemMetrics(SM_CYSCREEN);

	int posX, posY;
	// Setup the screen settings depending on whether it is running in full screen or in windowed mode.
	if(m_FullScreen)
	{
		// If full screen set the screen to maximum size of the users desktop and 32bit.
		DEVMODE dmScreenSettings;
		memset(&dmScreenSettings, 0, sizeof(dmScreenSettings));
		dmScreenSettings.dmSize       = sizeof(dmScreenSettings);
		dmScreenSettings.dmPelsWidth  = (unsigned long)width;
		dmScreenSettings.dmPelsHeight = (unsigned long)height;
		dmScreenSettings.dmBitsPerPel = 32;			
		dmScreenSettings.dmFields     = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;

		// Change the display settings to full screen.
		ChangeDisplaySettings(&dmScreenSettings, CDS_FULLSCREEN);

		// Set the position of the window to the top left corner.
		posX = posY = 0;

		// Create the window with the screen settings and get the handle to it.
		m_hwnd = CreateWindowEx(WS_EX_APPWINDOW, m_applicationName, m_applicationName, 
			WS_CLIPSIBLINGS | WS_CLIPCHILDREN | WS_POPUP,
			posX, posY, width, height, NULL, NULL, m_hinstance, NULL);
	}
	else
	{
		// If windowed then set it to some resolution
		width  = m_WindowedScreenWidth;
		height = m_WindowedScreenHeight;

		// Place the window in the middle of the screen.
		posX = (GetSystemMetrics(SM_CXSCREEN) - width)  / 2;
		posY = (GetSystemMetrics(SM_CYSCREEN) - height) / 2;

		RECT rc = { 0, 0, width, height };
		AdjustWindowRect( &rc, WS_OVERLAPPEDWINDOW, FALSE );
		// Create the window with the screen settings and get the handle to it.
		m_hwnd = CreateWindow( m_applicationName, m_applicationName, WS_OVERLAPPEDWINDOW,
			CW_USEDEFAULT, CW_USEDEFAULT,rc.right - rc.left, rc.bottom - rc.top , NULL, NULL, m_hinstance, NULL);
	}

	
	// Bring the window up on the screen and set it as main focus.
	ShowWindow(m_hwnd, SW_SHOW);
	SetForegroundWindow(m_hwnd);
	SetFocus(m_hwnd);

}

void System::Run()
{
	MSG msg;
	bool done, result;

	// Initialize the message structure.
	ZeroMemory(&msg, sizeof(MSG));

	// Loop until there is a quit message from the window or the user.
	done = false;
	while(!done)
	{
		// Handle the windows messages.
		if(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		// If windows signals to end the application then exit out.
		if(msg.message == WM_QUIT)
		{
			done = true;
		}
		else
		{
			// Otherwise do the frame processing.
			result = Frame();
			if(!result)
			{
				done = true;
			}
		}
	}
}

bool System::Frame()
{
	bool result = m_Game->Frame();
	if(!result)
		return false;

	return true;
}

LRESULT CALLBACK System::MessageHandler(HWND hwnd, UINT umsg, WPARAM wparam, LPARAM lparam)
{		
	if( TwEventWin(hwnd, umsg, wparam, lparam) ) // send event message to AntTweakBar
		return 0; // event has been handled by AntTweakBar

	return DefWindowProc(hwnd, umsg, wparam, lparam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT umessage, WPARAM wparam, LPARAM lparam)
{
	switch(umessage)
	{
		// Check if the window is being destroyed.
	case WM_DESTROY:
		{
			PostQuitMessage(0);
			return 0;
		}

		// Check if the window is being closed.
	case WM_CLOSE:
		{
			PostQuitMessage(0);		
			return 0;
		}

		// All other messages pass to the message handler in the system class.
	default:
		{
			return ApplicationHandle->MessageHandler(hwnd, umessage, wparam, lparam);
		}
	}
}
