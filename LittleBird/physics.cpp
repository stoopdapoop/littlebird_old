
#ifdef NDEBUG
#pragma comment(lib, "BulletDynamics.lib")
#pragma comment(lib, "BulletCollision.lib")
#pragma comment(lib, "LinearMath.lib")
#pragma comment(lib, "BulletSoftBody.lib")
#endif

#ifdef _DEBUG
#pragma comment(lib, "BulletDynamics_debug.lib")
#pragma comment(lib, "BulletCollision_debug.lib")
#pragma comment(lib, "LinearMath_debug.lib")
#pragma comment(lib, "BulletSoftBody_debug.lib")
#endif

#include "PositionComponent.h"
#include "DebugDrawer.h"
#include "MemoryUtilities.h"
#include "terrain.h"
#include "BulletCollision/CollisionShapes/btHeightfieldTerrainShape.h"
#include "BulletCollision/CollisionDispatch/btSimulationIslandManager.h"

#include "RigidBodyComponent.h"

#include "Shaders\terrainConstants.h"

#include "physics.h"

#include "services.h"


Physics::Physics() : m_Dispatcher(nullptr)
{
	///collision configuration contains default setup for memory, collision setup. Advanced users can create their own configuration.
	m_CollisionConfiguration = new btDefaultCollisionConfiguration();

	m_Dispatcher = new	btCollisionDispatcher(m_CollisionConfiguration);
	
	///btDbvtBroadphase is a good general purpose broadphase. You can also try out btAxis3Sweep.
	m_OverlappingPairCache = new btDbvtBroadphase();

	///the default constraint solver. For parallel processing you can use a different solver (see Extras/BulletMultiThreaded)
	m_Solver = new btSequentialImpulseConstraintSolver;

	m_DynamicsWorld = new btDiscreteDynamicsWorld(m_Dispatcher,m_OverlappingPairCache,m_Solver,m_CollisionConfiguration);

	m_DynamicsWorld->setGravity(btVector3(0,-10,0));

	m_DynamicsWorld->getSimulationIslandManager()->setSplitIslands(true);
	m_DynamicsWorld->getSolverInfo().m_splitImpulse = false;
	m_DynamicsWorld->getSolverInfo().m_solverMode = SOLVER_SIMD+SOLVER_USE_WARMSTARTING;//+SOLVER_RANDMIZE_ORDER;
	m_DynamicsWorld->getDispatchInfo().m_enableSPU = true;
	m_DynamicsWorld->getDispatchInfo().m_useContinuous = true;
	m_DynamicsWorld->getDispatchInfo().m_allowedCcdPenetration = 0.0001f;
	m_DynamicsWorld->getSolverInfo().m_numIterations = 2;
}

Physics::~Physics()
{
	const int collcount = m_DynamicsWorld->getNumCollisionObjects();
	btCollisionObjectArray collWorld = m_DynamicsWorld->getCollisionObjectArray();
	for(int i = collcount-1; i >= 0; --i)
	{
		btCollisionObject* current = collWorld[i];
		btRigidBody* body = btRigidBody::upcast(current);
		if (body && body->getMotionState())
		{
			delete body->getMotionState();
		}
		m_DynamicsWorld->removeCollisionObject( current );
		delete current;
	}

	SafeDelete(m_DynamicsWorld);
	SafeDelete(m_Solver);
	SafeDelete(m_OverlappingPairCache);
	SafeDelete(m_Dispatcher);
}

void Physics::Update( float delta )
{
	m_DynamicsWorld->stepSimulation(delta,10, 1.0f/60.0f);
	m_DynamicsWorld->debugDrawWorld();
}

btRigidBody* Physics::CreateTerrainCollisionMesh( float* mesh, float maxHeight, float minHeight, float tileScale, int length, int width, int xpos, int zpos )
{
	btHeightfieldTerrainShape* terrainBody;

	terrainBody = new btHeightfieldTerrainShape(width, length, mesh, 1.0f, minHeight, maxHeight, 1, PHY_FLOAT, false);
	terrainBody->setLocalScaling(btVector3(tileScale,1.0f,tileScale));


	btTransform midpoint;
	midpoint = btTransform::getIdentity();

	float cellLength = tileScale * length - tileScale;
	float halfCell = cellLength * 0.5f;
	float xCoord = (cellLength * xpos) + (halfCell);
	float yCoord = ((maxHeight) *0.5f) + (minHeight * 0.5f);
	//float yCoord = minHeight + (maxHeight - minHeight) * 0.5f;
	float zCoord = (cellLength * zpos) + (halfCell);


	
	midpoint.setOrigin(btVector3(xCoord, yCoord, zCoord));

	return CreateRigidBody(0.0f, midpoint, terrainBody);
}

btRigidBody*	Physics::CreateRigidBody(float mass, RigidBodyComponent* rbc, PHYSICS_GROUP group, PHYSICS_GROUP col)
{
	btRigidBody* body = InternalCreateRigidBody(mass, rbc);

	m_DynamicsWorld->addRigidBody(body, group, col);

	return body;
}

btRigidBody* Physics::CreateRigidBody( float mass, RigidBodyComponent* rbc )
{
	btRigidBody* body = InternalCreateRigidBody(mass, rbc);
	
	m_DynamicsWorld->addRigidBody(body);

	return body;
}

btRigidBody*	Physics::CreateRigidBody(float mass, const btTransform& startTransform,btCollisionShape* shape, PHYSICS_GROUP group, PHYSICS_GROUP col)
{
	btRigidBody* body = InternalCreateRigidBody(mass, startTransform, shape);

	m_DynamicsWorld->addRigidBody(body, group, col);

	return body;
}

btRigidBody* Physics::CreateRigidBody( float mass, const btTransform& startTransform,btCollisionShape* shape )
{
	btRigidBody* body = InternalCreateRigidBody(mass, startTransform, shape);

	m_DynamicsWorld->addRigidBody(body);

	return body;
}

void Physics::SetDebugDrawer( DebugDrawer* deb )
{
	m_DynamicsWorld->setDebugDrawer(deb);
}

btRigidBody* Physics::InternalCreateRigidBody( float mass, RigidBodyComponent* rbc )
{
	
	//rigidbody is dynamic if and only if mass is non zero, otherwise static
	bool isDynamic = (mass != 0.f);

	ResourceManager* res = Services::GetResourceManager();

	btCollisionShape* shape = res->GetCollisionShape(rbc->ShapeHandle);

	//btCollisionShape* shape = new btSphereShape(0.5f);

	btVector3 localInertia(0,0,0);
	if (isDynamic)
	{
		shape->calculateLocalInertia(mass,localInertia);
	}

	PoopEngineMotionState* myMotionState = new PoopEngineMotionState(rbc);

	btRigidBody::btRigidBodyConstructionInfo cInfo(mass,myMotionState,shape,localInertia);

	btRigidBody* body = new btRigidBody(cInfo);
	//body->setContactProcessingThreshold(m_defaultContactProcessingThreshold);

	//body->setDamping(0.1, 0.1);

	//body->setFriction(1.3f);

	if (m_DynamicsWorld->getDispatchInfo().m_useContinuous)
	{
		btScalar rad;
		btVector3 center;
		shape->getBoundingSphere(center, rad);
		rad *= 0.2f;
		body->setCcdMotionThreshold(rad);
		rad *= 0.5f;
		body->setCcdSweptSphereRadius(rad);
	}

	return body;
}

btRigidBody* Physics::InternalCreateRigidBody( float mass, const btTransform& startTransform,btCollisionShape* shape )
{
	btAssert((!shape || shape->getShapeType() != INVALID_SHAPE_PROXYTYPE));

	//rigidbody is dynamic if and only if mass is non zero, otherwise static
	bool isDynamic = (mass != 0.f);

	btVector3 localInertia(0,0,0);
	if (isDynamic)
	{
		shape->calculateLocalInertia(mass,localInertia);
	}

	btDefaultMotionState* myMotionState = new btDefaultMotionState(startTransform);

	btRigidBody::btRigidBodyConstructionInfo cInfo(mass,myMotionState,shape,localInertia);

	btRigidBody* body = new btRigidBody(cInfo);
	//body->setContactProcessingThreshold(m_defaultContactProcessingThreshold);

	body->setDamping(0.0, 0.0);

	//body->setFriction(0.2f);

	if (m_DynamicsWorld->getDispatchInfo().m_useContinuous)
	{
		btScalar rad;
		btVector3 center;
		shape->getBoundingSphere(center, rad);
		rad *= 0.20f;
		body->setCcdMotionThreshold(rad);
		rad *= 0.5f;
		body->setCcdSweptSphereRadius(rad);
	}

	return body;
}

void Physics::RemoveRigidBody( btRigidBody* body )
{
	if(!body)
		throw string("tried to remove invalid rigidbody");

	if(body->getCollisionShape()->getShapeType() == TERRAIN_SHAPE_PROXYTYPE)
		delete body->getCollisionShape();
	
	m_DynamicsWorld->removeCollisionObject(body);

	btMotionState* motState = body->getMotionState();
	delete motState;

	delete body;
}

void Physics::ClosestCollision(btVector3 start, btVector3 end, bool& hit, btVector3& hitPoint)
{
	btCollisionWorld::ClosestRayResultCallback rayCallback(start, end);

	m_DynamicsWorld->rayTest(start, end, rayCallback);

	hit = rayCallback.hasHit();
	hitPoint = rayCallback.m_hitPointWorld;
}

void PoopEngineMotionState::getWorldTransform( btTransform& worldTrans ) const
{
	worldTrans.setOrigin(*(btVector3*)&m_Owner->Position->Position);
	worldTrans.setRotation(*(btQuaternion*)&m_Owner->Position->Orientation);
}

void PoopEngineMotionState::setWorldTransform( const btTransform& worldTrans )
{
	m_Owner->Position->Position = worldTrans.getOrigin().get128();
	m_Owner->Position->Orientation = worldTrans.getRotation().get128();
}

PoopEngineMotionState::PoopEngineMotionState( RigidBodyComponent* rbc ) : m_Owner(rbc)
{

}
