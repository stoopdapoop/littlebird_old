
#if defined(DEBUG) || defined(_DEBUG)
#pragma comment(lib, "DirectXTKd.lib")
#else
#pragma comment(lib, "DirectXTK.lib")
#endif

#include "DeveloperConsole.h"
#include "texture.h"
#include "renderer.h"
#include "services.h"
#include "StringUtilities.h"
#include "MemoryUtilities.h"
#pragma warning(disable:4005)
#include "DirectxTK/SpriteBatch.h"
#include "DirectxTK/SpriteFont.h"
#pragma warning(default:4005)
using namespace DirectX;

DeveloperConsole::DeveloperConsole( Renderer* rend, int screenWidth, int screenHeight ): m_Enabled(true), m_ScreenWidth(screenWidth), m_ScreenHeight(screenHeight)
{
	m_SpriteBatch = new SpriteBatch(rend->GetImmediateContext());
	m_SpriteFont = new SpriteFont(rend->GetDevice(), L"../Assets/Fonts/poopoo.spritefont");
	m_SpriteFont->SetDefaultCharacter(L'?');

	// this has to be done the old fasioned way because the dev console won't be provided until after this method ends.
	RegisterCommand(L"write", bind(&DeveloperConsole::WriteCommand, this, placeholders::_1));
	//DECLARE_CONSOLECOMMAND("write", DeveloperConsole::WriteCommand);

	m_ConsoleHeight = m_ScreenHeight / 2;

	m_LookBackCount = 0;

	m_InputLog.push_back(wstring(L"hostgame"));

	WriteToConsole(L"Go ahead");
}

DeveloperConsole::~DeveloperConsole()
{
	SafeDelete(m_SpriteBatch);
	SafeDelete(m_SpriteFont);
}

void DeveloperConsole::Display() const
{
	if(!m_Enabled)
		return;

	ResourceManager* res = Services::GetResourceManager();
	Texture* consoleGradient = res->GetTextureFromName("consoleGradient.dds");
	ID3D11ShaderResourceView* consoleGradientTexture = consoleGradient->GetResourceView();
	int consoleHeight = m_ConsoleHeight;
	float lineSpacing = m_SpriteFont->GetLineSpacing();
	RECT logRect = { 0, 0, m_ScreenWidth,  consoleHeight };
	RECT inputRect = {0, consoleHeight, m_ScreenWidth, static_cast<long>(lineSpacing + consoleHeight) };


	m_SpriteBatch->Begin(SpriteSortMode_Deferred);
		m_SpriteBatch->Draw(consoleGradientTexture, logRect);
		m_SpriteBatch->Draw(consoleGradientTexture, inputRect);

		int lineCount = 1;
		for(auto it = m_Log.crbegin(); it != m_Log.crend(); ++it, ++lineCount)
		{
			m_SpriteFont->DrawString(m_SpriteBatch, (*it).text.c_str(), XMVectorSet(0.0f, static_cast<float>(consoleHeight - (lineSpacing * lineCount)), 1, 1), (*it).color);
		}
		m_SpriteFont->DrawString(m_SpriteBatch, m_InputBuffer.c_str(), XMVectorSet(0.0f, static_cast<float>(consoleHeight), 1, 1));
	m_SpriteBatch->End();
}

void DeveloperConsole::AddToInputBuffer( std::wstring input )
{

	// read each character in input one letter at at time and ignore characters that aren't part of spritefont and characters that have special meaning
	for(auto it = input.begin(); it != input.end(); ++it)
	{
		wchar_t current = (*it);

		if(current == '\b' && m_InputBuffer.size() > 0)
		{
			m_InputBuffer.pop_back();
		}
		else if(current == '\r')
		{
			SubmitInputBuffer();
		}
		else if(m_SpriteFont->ContainsCharacter(current))
		{
			// just to prevent tilde from getting into the command stream
			if(current != '`')
				m_InputBuffer += current;
		}
	}
}

void DeveloperConsole::SubmitInputBuffer()
{
	if(m_InputBuffer.empty())
		return;

	WriteToConsole(m_InputBuffer);

	size_t commandEnd = m_InputBuffer.find_first_of(L" ");
	wstring command = m_InputBuffer.substr(0, commandEnd);
	wstring args = m_InputBuffer.substr(commandEnd+1);

	if(m_Commands[command])
	{
		WriteToConsole(m_Commands[command](args));
	}
	else
	{
		WriteToConsole(command + L" is an unknown command", Colors::Red);
	}

	// no need to store more than can be displayed
	if(m_Log.size() > GetVisibleLineCount())
		m_Log.pop_front();

	m_InputLog.push_back(m_InputBuffer);
	
	m_InputBuffer.clear();

	m_LookBackCount = 0;
}

int DeveloperConsole::GetVisibleLineCount() const
{
	return static_cast<int>(GetConsoleHeight() / m_SpriteFont->GetLineSpacing());
}

int DeveloperConsole::GetConsoleHeight() const
{
	return m_ConsoleHeight;
}

void DeveloperConsole::WriteToConsole( std::wstring str, XMVECTORF32 color)
{
	m_Log.push_back(DeveloperConsoleLogEntry(str, color));
}

std::wstring DeveloperConsole::WriteCommand( std::wstring str)
{
	return str;
}

void DeveloperConsole::RegisterCommand( std::wstring command, std::function<std::wstring(std::wstring arg)> functionCallback )
{
	command = wstringToLower(command);
	if(m_Commands[command])
		throw L"The command \"" + command + L"\" is attempting to be redefined";

	m_Commands[command] = functionCallback;
}

void DeveloperConsole::ShowPreviousCommand()
{
	m_InputBuffer.clear();

	if(m_InputLog.size() < 1)
		return;

	m_LookBackCount++;
	if(m_LookBackCount > m_InputLog.size())
		m_LookBackCount = 1;
	
	AddToInputBuffer(m_InputLog[m_InputLog.size() - m_LookBackCount].text);
}

