#ifndef _INI_READER_H_
#define _INI_READER_H_

#pragma warning(disable:4310)
#pragma warning(disable:4996)
#pragma warning(disable:4996)
#include "INI1.28.h"
#pragma warning(default:4310)
#pragma warning(default:4996)
#pragma warning(disable:4996)
#include <string>

typedef INI <std::string, std::string, std::string> INI_t;

const std::string settingsFileName = "settings.ini";

class INIReader
{
public:
	INIReader();
	~INIReader();

	void SetSection(std::string sectionName);
	

	template<class T>
	void SetValue(std::string keyName, T keyValue)
	{
		m_INI->set(keyName, keyValue);
		m_INI->save(settingsFileName);
	}

	template<class T>
	T GetValue(std::string key, T defaultValue) 
	{ 
		T returner = m_INI->get<T>(key, defaultValue);
		if(returner == defaultValue)
			m_INI->set(key, defaultValue);
		m_INI->save(settingsFileName);
		return returner;
	}

private:
	INI_t* m_INI;

};


#endif