

#ifndef UnitSystem_h__
#define UnitSystem_h__

#include <vector>
#include "component_system.h"

class UnitComponent;
class PlayerComponent;
class ModelComponent;
class PositionComponent;
class BaitComponent;

class UnitSystem : public ComponentSystem
{
public:
	UnitSystem();
	void AddEntry(unsigned ID, UnitComponent* unit, ModelComponent* mod, PositionComponent* pos);
	void AddBait(unsigned ID, BaitComponent* bait, PositionComponent* pos);
	void Run();

private:
	std::vector<UnitComponent*> m_Units;
	std::vector<ModelComponent*> m_Models;
	std::vector<PositionComponent*> m_Positions;

	std::vector<unsigned> m_BaitRoster;
	std::vector<PositionComponent*> m_BaitPositions;
	std::vector<BaitComponent*> m_Bait;
};

#endif // UnitSystem_h__