#ifndef _UI_VAR_TYPE_H_
#define _UI_VAR_TYPE_H_

#include <string>

enum class DEBUG_UI_TYPE
{
	UNDEF   = 0,
	BOOLCPP = 1,
	BOOL8   = 2,
	BOOL16,
	BOOL32,
	CHAR,
	INT8,
	UINT8,
	INT16,
	UINT16,
	INT32,
	UINT32,
	FLOAT,
	DOUBLE,
	COLOR32,    // 32 bits color. Order is RGBA if API is OpenGL or Direct3D10, and inversed if API is Direct3D9 (can be modified by defining 'colorOrder=...', see doc)
	COLOR3F,    // 3 floats color. Order is RGB.
	COLOR4F,    // 4 floats color. Order is RGBA.
	CDSTRING,   // Null-terminated C Dynamic String (pointer to an array of char dynamically allocated with malloc/realloc/strdup)
# if defined(_MSC_VER) && (_MSC_VER == 1600)
	STDSTRING = (0x2ffe0000+sizeof(std::string)),  // VS2010 C++ STL string (std::string)
# else
	STDSTRING = (0x2fff0000+sizeof(std::string)),  // C++ STL string (std::string)
# endif
	QUAT4F = CDSTRING+2, // 4 floats encoding a quaternion {qx,qy,qz,qs}
	QUAT4D,     // 4 doubles encoding a quaternion {qx,qy,qz,qs}
	DIR3F,      // direction vector represented by 3 floats
	DIR3D       // direction vector represented by 3 doubles
};

#endif