#define NOMINMAX

//#pragma warning(disable:4005)
#include "VertexFormats.h"
#include "services.h"
//#pragma warning(default:4005)
#include "Terrain.h"
#include <memory>
#include "texture.h"
#include "timer.h"
#include <limits>
#include <iostream>
#include "MemoryUtilities.h"
#include "Shaders\terrainConstants.h"
using namespace std;
using namespace DirectX;

ID3D11Buffer* Terrain::m_IndexBuffer[LODCOUNT+1] = {nullptr};
ID3D11Buffer* Terrain::m_VertexBuffer = {nullptr};
int Terrain::m_VertexCount = 0;
bool Terrain::m_NeedsIndexBuffer = true;
unsigned Terrain::m_IndexCount[LODCOUNT+1] = {0};

Terrain::Terrain(ID3D11Device* device, std::string heightMapFilename, ResourceHandle textureSplatHandle, ResourceHandle textureTintHandle, float tileScale, int xPos, int zPos, int lodLevel):m_TileScale(tileScale), m_TextureSplatHandle(textureSplatHandle),
	m_TextureTintHandle(textureTintHandle), m_xPosition(xPos), m_zPosition(zPos), m_LodLevel(lodLevel)
{
	m_HeightMap = nullptr;
	m_HeightmapHeights = nullptr;
	m_MaxHeight = numeric_limits<float>::min();
	m_MinHeight = numeric_limits<float>::max();
	//m_VertexBuffer = nullptr;
	m_terrainWidth = m_terrainLength = SZ;

	Initialize(device, heightMapFilename);
}


Terrain::~Terrain()
{
	Shutdown();
}

void Terrain::Initialize( ID3D11Device* device, string heightMapFilename)
{

	// Load in the height map for the terrain.
	LoadHeightMap(device, heightMapFilename);
	
	//CreateCollisionHeights();

	if(m_NeedsIndexBuffer)
		CalculateTextureCoordinates();

	if(m_NeedsIndexBuffer)
		InitializeBuffers(device);

}

void Terrain::Shutdown()
{

	// Release the height map data.
	ShutdownHeightMap();

	// Release the vertex array.
	ShutdownBuffers();	
}

void Terrain::InitializeBuffers(ID3D11Device* device)
{	
	

	// Calculate the number of vertices in the terrain mesh.
	m_VertexCount = (m_terrainWidth) * (m_terrainLength);
	int cellCount = (m_terrainWidth - 1) * (m_terrainWidth - 1);


	for (int lodCount = 0; lodCount <= LODCOUNT; ++lodCount)
	{
		int indexStride = 1 << lodCount;
		m_IndexCount[lodCount] = (cellCount * 6) / (indexStride*indexStride);
		unique_ptr<unsigned long[]> indices(new unsigned long[m_IndexCount[lodCount]]);

		// Initialize the index to the vertex buffer.
		unsigned index = 0;

		index = 0;
		unsigned index1 = 0;      // Bottom left.
		unsigned index2 = indexStride;      // Bottom right.
		unsigned index3 = m_terrainWidth*indexStride;     // Upper left.
		unsigned index4 = m_terrainWidth*indexStride + indexStride;  // Upper right.

		int colCount = (m_terrainLength / indexStride) - (lodCount == 0 ? 1 : 0);
		int rowCount = colCount;
		for (int j = 0; j < colCount; ++j)
		{
			for (int i = 0; i < rowCount; ++i)
			{
				indices[index] = index1;
				++index;
				indices[index] = index3;
				++index;
				indices[index] = index2;
				++index;
				indices[index] = index2;
				++index;
				indices[index] = index3;
				++index;
				indices[index] = index4;
				++index;
				index1 += indexStride;
				index2 += indexStride;
				index3 += indexStride;
				index4 += indexStride;
			}
			int indexHeightStride = m_terrainLength * (indexStride - 1) + 1;
			index1 += indexHeightStride;
			index2 += indexHeightStride;
			index3 += indexHeightStride;
			index4 += indexHeightStride;
		}
		D3D11_BUFFER_DESC indexBufferDesc;
		D3D11_SUBRESOURCE_DATA indexData;
		// Set up the description of the index buffer.
		indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
		indexBufferDesc.ByteWidth = sizeof(unsigned long) * m_IndexCount[lodCount];
		indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
		indexBufferDesc.CPUAccessFlags = 0;
		indexBufferDesc.MiscFlags = 0;
		indexBufferDesc.StructureByteStride = 0;

		// Give the subresource structure a pointer to the index data.
		indexData.pSysMem = indices.get();
		indexData.SysMemPitch = 0;
		indexData.SysMemSlicePitch = 0;

		// Create the index buffer.
		HRESULT result = device->CreateBuffer(&indexBufferDesc, &indexData, &m_IndexBuffer[lodCount]);

		if (FAILED(result))
			throw string("failed to create terrain index buffer");

	}
	m_NeedsIndexBuffer = false;
	

	D3D11_BUFFER_DESC vertexBufferDesc;
	D3D11_SUBRESOURCE_DATA vertexData;

	// Set up the description of the vertex buffer.
	vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	vertexBufferDesc.ByteWidth = sizeof(TerrainVertexType) * m_VertexCount;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = 0;
	vertexBufferDesc.MiscFlags = 0;
	vertexBufferDesc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the vertex data.
	//vertexData.pSysMem = vertices.get();
	vertexData.pSysMem = m_HeightMap;
	vertexData.SysMemPitch = 0;
	vertexData.SysMemSlicePitch = 0;

	// Now finally create the vertex buffer.
	HRESULT result = device->CreateBuffer(&vertexBufferDesc, &vertexData, &m_VertexBuffer);
	if(FAILED(result))
		throw string("Failed to create terrain vertex buffer");
}

void Terrain::ShutdownBuffers()
{
	//SafeRelease(m_VertexBuffer);
	//SafeRelease(m_IndexBuffer);
}

void Terrain::LoadHeightMap(ID3D11Device* device, string filename )
{
	Timer t;
	FILE* filePtr;
	int error;
	size_t count;
	int i, j, index;
	unique_ptr<unsigned short[]> bitmapImage(new unsigned short[SZ*SZ]);

	// Open the height map file in binary.
	error = fopen_s(&filePtr, filename.c_str(), "rb");
	if(error != 0)
		throw string("could not open file: " + filename);

	// Read in the file header.
	count = fread(bitmapImage.get(), sizeof(unsigned short), SZ*SZ, filePtr);
	if(count != SZ*SZ)
		throw string("improper data size in: " + filename);

	// Close the file.
	error = fclose(filePtr);
	if(error != 0)
		throw string ("failed to close:" + filename);

	// Create the structure to hold the height map data.
	m_HeightMap = new TerrainVertexType[m_terrainWidth * m_terrainLength];

	CreateCollisionHeights(bitmapImage.get());

	m_HeightMapTexture = new Texture(device, (unsigned char*)bitmapImage.get(), DXGI_FORMAT_R16_UNORM, SZ, SZ);

	// Initialize the position in the image data buffer, start from the end because dumbprogram reverses output.

	// Read the image data into the height map.
	for(j=0; j<m_terrainWidth; ++j)
	{
		for(i=0; i<m_terrainLength; ++i)
		{

			index = ((m_terrainLength * j) + i);

			m_HeightMap[index].xy_texture.x = m_TileScale * i;
			m_HeightMap[index].xy_texture.y = m_TileScale * j;
		}
	}	
}

void Terrain::ShutdownHeightMap()
{
	SafeDeleteArray(m_HeightMap);

	SafeDeleteArray(m_HeightmapHeights);
}


void Terrain::CalculateTextureCoordinates()
{

	// Loop through the entire height map and calculate the tu and tv texture coordinates for each vertex.
	for(int j=0; j<m_terrainLength; j++)
	{
		for(int i=0; i<m_terrainWidth; i++)
		{
			// Store the texture coordinate in the height map.
			m_HeightMap[(m_terrainLength * j) + i].xy_texture.z = (float)i/TEXTURE_SCALE;
			m_HeightMap[(m_terrainLength * j) + i].xy_texture.w = (float)j/TEXTURE_SCALE;
		}
	}
}


void Terrain::CreateCollisionHeights(unsigned short* heights)
{
	m_HeightmapHeights = new float[m_terrainWidth * m_terrainLength];

	// todo: flatten these
	for(int i = 0; i < m_terrainLength; ++i)
	{
		for(int j = 0; j < m_terrainWidth; ++j)
		{
			int index = (m_terrainLength * i) + j;

			m_HeightmapHeights[index] = heights[index] / (65535.0f / ampConstant);

			if(m_HeightmapHeights[index] > m_MaxHeight)
				m_MaxHeight = m_HeightmapHeights[index];
			if(m_HeightmapHeights[index] < m_MinHeight)
				m_MinHeight = m_HeightmapHeights[index];
		}
	}
}

void Terrain::PreRender( ID3D11DeviceContext* deviceContext )
{
	// Set vertex buffer stride and offset.
	unsigned int stride = sizeof(TerrainVertexType); 
	unsigned int offset = 0;

	// Set the vertex buffer to active in the input assembler so it can be rendered.
	deviceContext->IASetVertexBuffers(0, 1, &m_VertexBuffer, &stride, &offset);
}


void Terrain::Render( ID3D11DeviceContext* deviceContext )const
{
	// Set the index buffer to active in the input assembler so it can be rendered.
	deviceContext->IASetIndexBuffer(m_IndexBuffer[m_LodLevel], DXGI_FORMAT_R32_UINT, 0);
}

void Terrain::Render( ID3D11DeviceContext* deviceContext, int lodLevel )
{
	deviceContext->IASetIndexBuffer(m_IndexBuffer[lodLevel], DXGI_FORMAT_R32_UINT, 0);
}

ResourceHandle Terrain::GetSplatTextureHandle()const
{
	return m_TextureSplatHandle;
}

XMFLOAT3 AddFloat3(XMFLOAT3 x, XMFLOAT3 y)
{
	x.x += y.x;
	x.y += y.y;
	x.z += y.z;
	return x;
}


void Terrain::GetTerrainPosition( float& x, float& y, float& z )const
{
	float terrainWidth = m_TileScale * m_terrainWidth - m_TileScale;
	x = m_xPosition * terrainWidth;
	y = 0.0f;
	z = m_zPosition * terrainWidth;
}

void Terrain::SetLod( unsigned lod )
{
	if(lod > LODCOUNT || lod < 0)
		throw string("Tried to create tile with invalid LOD value");
	m_LodLevel = lod;
}

ResourceHandle Terrain::GetTintTextureHandle() const
{
	return m_TextureTintHandle;
}
