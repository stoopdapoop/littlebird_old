
#ifndef _COMPONENT_FACTORY_H_
#define _COMPONENT_FACTORY_H_

#include "component.h"
#include "PositionComponent.h"
#include "RigidBodyComponent.h"
#include "PlayerComponent.h"
#include "ModelComponent.h"
#include "LanderComponent.h"

Component* GetComponentFromBitstream(ComponentID_t id, RakNet::BitStream& bs)
{
	switch ( id )
	{
	case ComponentID::Position:
		{
			PositionComponent* pos = new PositionComponent(Replicated);
			pos->NetworkDeserialize(bs);
			return (Component*)pos;
		}	
		break;
	case ComponentID::Model:
		{
			ModelComponent* mod = new ModelComponent(Replicated);
			mod->NetworkDeserialize(bs);
			return (Component*)mod;
		}
		break;
	case ComponentID::RigidBody:
		{
			//RigidBodyComponent* rig = new RigidBodyComponent()
		}
		//break;
	case ComponentID::Lander:
		{

		}
		//break;
	case ComponentID::Player:
		{

		}
		//break;
	default:
		throw std::string("Need to implement specialization for this type");
		break;
	}
}

#endif