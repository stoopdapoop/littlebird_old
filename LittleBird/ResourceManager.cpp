

#include "ResourceHandle.h"
#include "services.h"
#include "model.h"
#include "texture.h"
#include <memory>
#include "StringUtilities.h"
#include "renderer.h"
#include "ResourceManager.h"
using namespace std;

static const string  ShapeStrings[ShapeCount] = { "Capsule", "Cone", "Cylinder", "Sphere", "Box" };

ResourceHandle::ResourceHandle(): ID(0) {}

ResourceManager::ResourceManager(Renderer* rend, Physics* phys):m_NextResourceID(1), m_Renderer(rend), m_Physics(phys)
{
}

ResourceManager::~ResourceManager()
{
	ReleaseMapAssets(begin(m_Textures), end(m_Textures));
	ReleaseMapAssets(begin(m_Models), end(m_Models));
	ReleaseMapAssets(begin(m_CollisionShapes), end(m_CollisionShapes));
}

ResourceHandle ResourceManager::GetTextureHandle( std::string textureName )
{
	ResourceHandle handle = m_TextureHandles[textureName];

	//  handle not found, we need to create one and load associated asset
	if(handle.ID == 0)
	{
		int newID = m_NextResourceID + 1;
		m_TextureHandles[textureName].ID = newID;
		try
		{
			m_Textures[newID] = new Texture(m_Renderer, "../Assets/Textures/" + textureName);
		}
		catch(TextureException& e)
		{
			DeveloperConsole* dev = Services::GetDeveloperConsole();
			if(dev)
			{
				wstring errMsg = to_wstring(e.what());
				dev->WriteToConsole(to_wstring(textureName) + L": " + errMsg, DirectX::Colors::Red);
			}
			return GetTextureHandle("error.jpg");
		}
		++m_NextResourceID;
	}

	return m_TextureHandles[textureName];
}

Texture* ResourceManager::GetTexture( ResourceHandle textureHandle )
{
	Texture* tex = nullptr;
	try
	{
		tex = m_Textures.at(textureHandle.ID);
	}
	catch (out_of_range& e)
	{
		string errmsg = string(e.what()) + " Could not find texture with ID of " + to_string(textureHandle.ID);
		throw errmsg;
	}

	return tex;
}

ResourceHandle ResourceManager::GetModelHandle( std::string modelName )
{
	ResourceHandle handle = m_ModelHandles[modelName];

	//  handle not found, we need to create one and load associated asset
	if(handle.ID == 0)
	{
		m_ModelHandles[modelName].ID = m_NextResourceID++;
		m_Models[m_ModelHandles[modelName].ID] = new Model(m_Renderer->GetDevice(), "../Assets/Models/" + modelName);
	}

	return m_ModelHandles[modelName];
}

ResourceHandle ResourceManager::GetCollisionShapeHandle( std::string CollisionShapeName )
{
	ResourceHandle handle = m_CollisionShapeHandles[CollisionShapeName];

	//  handle not found, we need to create one and load associated asset
	if(handle.ID == 0)
	{
		m_CollisionShapeHandles[CollisionShapeName].ID = m_NextResourceID++;
		unique_ptr<CollisionModel> mod(new CollisionModel("../Assets/Models/" + CollisionShapeName));
		if(mod->GetVertexCount() > 128)
			throw string("Collision model " + CollisionShapeName + " has more than 128 verticies");

		m_CollisionShapes[m_CollisionShapeHandles[CollisionShapeName].ID] = new btConvexHullShape((btScalar*)mod->GetVertexArray(), mod->GetVertexCount(), sizeof(CollisionVertexType));
		//m_CollisionShapes[m_CollisionShapeHandles[CollisionShapeName].ID] = new btBoxShape(btVector3(9,3,9));
	}

	return m_CollisionShapeHandles[CollisionShapeName];
}

ResourceHandle ResourceManager::GetCollisionShapeHandle( CollisionShape shape, float x /*= 1.0f*/, float y /*= 1.0f*/, float z /*= 1.0f*/ )
{
	string shapeIdentifier = ShapeStrings[shape]+to_string(x)+ " " + to_string(y) + " " +to_string(z);
	ResourceHandle handle = m_CollisionShapeHandles[shapeIdentifier];

	//  handle not found, we need to create one and load associated asset
	if(handle.ID == 0)
	{

		m_CollisionShapeHandles[shapeIdentifier].ID = m_NextResourceID++;

		btCollisionShape* colShape = nullptr;
		switch(shape)
		{
		case CAPSULE:
			colShape = new btCapsuleShape(x, y);
			break;
		case CONE:
			colShape = new btConeShape(x,y);
			break;
		case CYLINDER:
			colShape = new btCylinderShape(btVector3(x,y,z));
			break;
		case SPHERE:
			colShape = new btSphereShape(x);
			break;
		case BOX:
			colShape = new btBoxShape(btVector3(x, y, z));
			break;
		}

		if (colShape == nullptr)
			throw string(shapeIdentifier + " was not created properly");

		m_CollisionShapes[m_CollisionShapeHandles[shapeIdentifier].ID] = colShape;
		//m_CollisionShapes[m_CollisionShapeHandles[shapeIdentifier].ID] = new btBoxShape(btVector3(9,3,9));
	}

	return m_CollisionShapeHandles[shapeIdentifier];
}

Model* ResourceManager::GetModel( ResourceHandle modelHandle )
{
	Model* mod = nullptr;
	try
	{
		mod = m_Models.at(modelHandle.ID);
	}
	catch (out_of_range& e)
	{
		string errmsg = string(e.what()) + " Could not find model with ID of " + to_string(modelHandle.ID);
		throw errmsg;
	}

	return mod;
}

Texture* ResourceManager::GetTextureFromName( std::string textureName )
{
	return GetTexture(GetTextureHandle(textureName));
}

btCollisionShape* ResourceManager::GetCollisionShape( ResourceHandle collisionShapeHandle )
{
	btCollisionShape* shape = nullptr;
	try
	{
		shape = m_CollisionShapes.at(collisionShapeHandle.ID);
	}
	catch (out_of_range& e)
	{
		string errmsg = string(e.what()) + " Could not find texture with ID of " + to_string(collisionShapeHandle.ID);
		throw errmsg;
	}

	return shape;
}
