#define WIN32_LEAN_AND_MEAN

#define NOMINMAX
#include "system.h"
#include <windows.h>
#include <memory>
#include <string>
#include "vld.h"
#include <xmmintrin.h>
using namespace std;

void RunSystem();
#pragma warning(disable:4100)
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PSTR pScmdline, int iCmdshow)
{
	// no denormals allowed in this grill
	_MM_SET_FLUSH_ZERO_MODE(_MM_FLUSH_ZERO_ON);
	_mm_setcsr(_mm_getcsr() | 0x8040);

	try
	{
		RunSystem();
	}
	catch(exception& e)
	{
		string temp(e.what());
		wstring what(temp.begin(), temp.end());
		MessageBox(0, what.c_str(), 0, 0);
	}
	catch (wchar_t*  msg)
	{
		MessageBox(0, msg, 0, 0);
	}
	catch(std::string msg)
	{
		std::wstring stemp = std::wstring(msg.begin(), msg.end());
		LPCWSTR sw = stemp.c_str();
		MessageBox(0, sw, 0, 0);
	}
	catch(...)
	{
		return 1;
	}

	return 0;
}
#pragma warning(default:4100)

void RunSystem()
{
	std::unique_ptr<System> pSystem(new System);

	pSystem->Run();
}