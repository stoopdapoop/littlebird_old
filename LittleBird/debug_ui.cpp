#include "debug_ui.h"
#include "AntTweakBar.h"
//#include "DebugUI_callbacks.h"
//#include <windows.h>
#pragma warning(disable:4005)
#include <d3d11.h>
#pragma warning(default:4005)
#include "input.h"
#include <string>
#include "StringUtilities.h"
using namespace std;

DebugUI::DebugUI(ID3D11Device* device, int width, int height): m_Enabled(true)
{
	Initialize(device, width, height);
}

DebugUI::~DebugUI()
{
	Shutdown();
}

void DebugUI::Initialize( ID3D11Device* device, int width, int height )
{
	//init
	TwInit(TW_DIRECT3D11, device);
	TwWindowSize(width,height);
	//disable help bar
	TwDefine("TW_HELP visible=false");
}

void DebugUI::Draw()
{
	if(m_Enabled)
		TwDraw();
}

void DebugUI::Shutdown()
{
	TwTerminate();
}

void DebugUI::CreateMenu( std::string name )
{
	TwNewBar(name.c_str());
	TwDefine( string(name +" alpha=200 size='130 100' iconified=true").c_str()); // opaque bar
	//TwDefine( string(name +" valueswidth=fit").c_str());
}

void DebugUI::AddReadOnlyVariable( std::string menuName, std::string varName, DEBUG_UI_TYPE type, void* data)
{
	TwBar* bar = TwGetBarByName(menuName.c_str());
	int success = TwAddVarRO(bar, varName.c_str(), (TwType)type, data, NULL);
	if(!success)
		throw string(TwGetLastError());

	// need to convert RHS coordinates to LHS if arrow variable
	if(type == DEBUG_UI_TYPE::DIR3F)
	{
		string convert = string(menuName + "/" + varName + " axisx=x axisy=y axisz=-z");
		success = TwDefine( convert.c_str());
		if(!success)
			throw string(TwGetLastError());
	}
}

void DebugUI::SetMenuRefreshRate( std::string name, float rate )
{
	string refresh = string(name + " refresh=" + to_string(rate));
	int success = TwDefine(refresh.c_str());
	if(!success)
		throw string(TwGetLastError());
}

void DebugUI::SetMenuSize( std::string name, int x, int y )
{
	name += " size='" + to_string(x) + " " + to_string(y) +"'";
	int success = TwDefine(name.c_str());
	if(!success)
		throw string(TwGetLastError());
}

void DebugUI::AddReadWriteFloat( std::string menuName, std::string varName, void* data, float min, float max, float step )
{
	TwBar* bar = TwGetBarByName(menuName.c_str());
	string params = "min=" + to_string(min) + " max=" + to_string(max) + " step=" + to_string(step);
	int success = TwAddVarRW(bar, varName.c_str(), TwType::TW_TYPE_FLOAT, data, params.c_str());
	if(!success)
		throw string(TwGetLastError());
}

void DebugUI::AddReadWriteColorFloat3( std::string menuName, std::string varName, void* data )
{
	TwBar* bar = TwGetBarByName(menuName.c_str());
	//string params = "min=" + to_string(min) + " max=" + to_string(max) + " step=" + to_string(step);
	int success = TwAddVarRW(bar, varName.c_str(), TwType::TW_TYPE_COLOR3F, data, NULL);
	if(!success)
		throw string(TwGetLastError());
}

void DebugUI::AddReadWriteArrow(std::string menuName, std::string varName, void* data)
{
	TwBar* bar = TwGetBarByName(menuName.c_str());
	
	int success = TwAddVarRW(bar, varName.c_str(), TwType::TW_TYPE_DIR3F, data, NULL);
	string convert = string(menuName + "/" + varName + " axisx=x axisy=y axisz=-z");
	success = TwDefine( convert.c_str());
	if(!success)
		throw string(TwGetLastError());
}

void DebugUI::AddReadWriteBool( std::string menuName, std::string varName, void* data )
{
	TwBar* bar = TwGetBarByName(menuName.c_str());
	int success = TwAddVarRW(bar, varName.c_str(), TwType::TW_TYPE_BOOLCPP, data, NULL);
	if(!success)
		throw string(TwGetLastError());
}

void DebugUI::AddReadWriteUInt( std::string menuName, std::string varName, void* data )
{
	TwBar* bar = TwGetBarByName(menuName.c_str());
	int success = TwAddVarRW(bar, varName.c_str(), TwType::TW_TYPE_UINT32, data, NULL);
	if(!success)
		throw string(TwGetLastError());
}


void DebugUI::SetEnabled( bool enabled )
{
	m_Enabled = enabled;
}
