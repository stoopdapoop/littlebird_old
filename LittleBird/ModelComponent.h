#ifndef _MODEL_COMPONENT_H_
#define _MODEL_COMPONENT_H_

#include "ResourceHandle.h"
#include "component.h"

class ModelComponent : public Component
{
public:
	ModelComponent(ResourceHandle modelHand, ResourceHandle diffuseTex, ResourceHandle normTex, ResourceHandle roughTex, ResourceHandle specTex, char replicationFlags) : 
		ModelHandle(modelHand), DiffuseTextureHandle(diffuseTex), NormalTextureHandle(normTex), RoughnessTextureHandle(roughTex), SpecularTextureHandle(specTex),
		CastsShadows(true) ,Component(ComponentID::Model, replicationFlags) {}

	ModelComponent(char replicationFlags) : CastsShadows(true), Component(ComponentID::Model, replicationFlags) {}
	
	void NetworkSerialize(RakNet::BitStream& bs);
	void NetworkDeserialize(RakNet::BitStream& bs);
	
	ResourceHandle ModelHandle;
	ResourceHandle DiffuseTextureHandle;
	ResourceHandle NormalTextureHandle;
	ResourceHandle RoughnessTextureHandle;
	ResourceHandle SpecularTextureHandle;
	bool CastsShadows;
};

#endif