#define NOMINMAX

#include "MemoryUtilities.h"
#include "TerrainManager.h"
#include "terrain.h"
#include "wincodec.h"
#include "atlbase.h"
#include <random>
#include <algorithm>
#include "ModelComponent.h"
#include "PositionComponent.h"
#include "StringUtilities.h"
#include "Entity.h"
#include "services.h"

#include <limits>
using namespace std;
using namespace DirectX;

TerrainManager::TerrainManager( string mapname, int terrainStride, int worldGridWidthCount, ID3D11Device* device ) : m_MapName(mapname), m_MaxGridWidthCount(worldGridWidthCount-1), m_Device(device), m_currentTileX(INT_MIN), m_currentTileY(INT_MIN)
{
		SetTerrainStride(terrainStride);

		// Create a decoder
		//HRESULT hr = CoCreateInstance(CLSID_WICImagingFactory, NULL, CLSCTX_INPROC_SERVER, IID_PPV_ARGS(&m_pIWICFactory));
		//if (FAILED(hr))
		//	throw string("failed creating WIC instance");
}

TerrainManager::~TerrainManager()
{
	for(auto it = m_TerrainGrid.begin(); it != m_TerrainGrid.end(); ++it)
	{
		delete (*it).second;
	}
}

bool TerrainManager::CompareTerrainForRendering(TerrainCollisionPair t1, TerrainCollisionPair t2)
{
	unsigned t1Lod = t1.terrain->GetLod();
	unsigned t2Lod = t2.terrain->GetLod();
	if(t1Lod < t2Lod)
		return true;
	else
		return false;
}

void TerrainManager::Update( XMVECTOR viewPosition )
{
	// first check player's current grid position against the position from last frame
	int x,z;
	CalculateTilePosition(viewPosition, x, z);

	bool currentTileNeedsUpdate = false;
	if(x != m_currentTileX)
		currentTileNeedsUpdate = true;
	if(z != m_currentTileY)
		currentTileNeedsUpdate = true;

	m_VisibleTilesThisFrame.clear();

	m_currentTileX = x;
	m_currentTileY = z;
	int startOffset = -m_TerrainStride / 2;
	int endOffset = m_TerrainStride / 2;

	ResourceManager* res = Services::GetResourceManager();
	Physics* phys = Services::GetPhysics();
	for(int rowOffset = startOffset; rowOffset <= endOffset; ++rowOffset)
	{
		for(int columnOffset = startOffset; columnOffset <= endOffset; ++columnOffset)
		{
			int currentX = x+rowOffset;
			int currentZ = z+columnOffset;
			if(!IsTileWithinMapBoundary(currentX, currentZ))
				continue;

			
			string terrainKey = GenerateTerrainKey(currentX, currentZ);
			Terrain* currentTerrain = nullptr;
			auto terIterator = m_TerrainGrid.find(terrainKey);
			if(terIterator != m_TerrainGrid.end())
				currentTerrain = terIterator->second;

			if(currentTerrain == nullptr)
			{
				string positionString = GenerateLongPositionString(currentX, currentZ);
				string namePositionString = m_MapName + positionString;
				ResourceHandle currentTex = res->GetTextureHandle("./Terrain/"+ m_MapName+ "/" + m_MapName+ "_texture"+positionString+"_bc7.dds");
				ResourceHandle currentTintTex = res->GetTextureHandle("./Terrain/"+ m_MapName+ "/" + m_MapName+ "_tint"+positionString+"_bc7.dds");
				Terrain* newTerrain = new Terrain(m_Device, "../Assets/Terrain/" + namePositionString + ".r16", currentTex, currentTintTex, tileDistance, currentX, currentZ, CalculateLOD(currentX, currentZ));
				btRigidBody* newRigidbody = phys->CreateTerrainCollisionMesh(newTerrain->GetHeights(), newTerrain->GetMaxHeight(), newTerrain->GetMinHeight(), newTerrain->GetTileScale(), newTerrain->GetTerrainLength(), newTerrain->GetTerrainWidth(), currentX, currentZ);
				m_TerrainGrid[terrainKey] = newTerrain;
				m_ColliderGrid[terrainKey] = newRigidbody;
				
				m_VisibleTilesThisFrame.push_back({ newTerrain, newRigidbody });
			}
			else
			{

				unsigned lod = CalculateLOD(currentX, currentZ);
				currentTerrain->SetLod(lod);
				btRigidBody* tileRb = m_ColliderGrid[terrainKey];
				m_VisibleTilesThisFrame.push_back({ currentTerrain, tileRb });
			}
		}
	}
	std::sort(m_VisibleTilesThisFrame.begin(), m_VisibleTilesThisFrame.end(), &TerrainManager::CompareTerrainForRendering);

	RemoveDistantTiles();
}

void TerrainManager::CalculateTilePosition( XMVECTOR viewPosition, int& x, int& z)
{
	XMFLOAT3 position;
	XMStoreFloat3(&position, viewPosition);

	position.x /= (SZ-1) * tileDistance;
	position.z /= (SZ-1) * tileDistance;

	x =  static_cast<int>(position.x);
	z =  static_cast<int>(position.z);
}

std::string TerrainManager::GenerateLongPositionString(int x, int z)
{
	//need to work out padding for 3 digit numbers
	assert(x < 100 && z < 100);
	if (x > 100 || z > 100)
		throw string("Need to work out padding for 3 digit numbers in terrain manager.");

	string positionString = "_x";
	if (x < 10)
		positionString += "0" + to_string(x);
	else
		positionString += to_string(x);

	positionString += "_y";

	if (z < 10)
		positionString += "0" + to_string(z);
	else
		positionString += to_string(z);

	return positionString;
}

std::string TerrainManager::GenerateTerrainKey( int x, int z )
{
	return to_string(x) + "_" + to_string(z);
}

Terrain* TerrainManager::GetTerrain( int x, int z )
{
	// just a bounds check
	if(!IsTileWithinMapBoundary(x,z))
		return nullptr;

	string terrainIndex = GenerateTerrainKey(x, z);
	Terrain* returner = m_TerrainGrid[terrainIndex];
	if(returner == nullptr)
		throw string("bad terrain at ") + to_string(x) + " " + to_string(z) + ":" + terrainIndex;
	return returner;
}

// needs to be an odd number so that the player occupies a "center" tile with an equal number of tiles in each direction.
void TerrainManager::SetTerrainStride( int oddStride )
{
	if(oddStride % 2 == 0)
		throw string("terrainStride must be an odd number");

	m_TerrainStride = oddStride;
}

bool TerrainManager::IsTileWithinMapBoundary( int x, int z )
{
	if(x < 0 || x > m_MaxGridWidthCount  || z < 0 || z > m_MaxGridWidthCount)
		return false;

	return true;
}

int TerrainManager::CalculateLOD( int terrainX, int terrainZ )
{
	int dx = abs(m_currentTileX - terrainX);
	int dz = abs(m_currentTileY - terrainZ);
		
	return max(0, min(max(dx, dz), LODCOUNT));
}

void TerrainManager::RemoveDistantTiles()
{
	Physics* phys = Services::GetPhysics();
	vector<string> keys;
	for (auto it = m_TerrainGrid.begin(); it != m_TerrainGrid.end(); ++it)
	{
		int tX, tZ;
		Terrain* ter = (*it).second;
		int halfStride = m_TerrainStride / 2;
		ter->GetTilePosition(tX, tZ);
		if (abs(m_currentTileX - tX) > halfStride || abs(m_currentTileY - tZ) > halfStride)
		{
			keys.push_back((*it).first);
			delete ter;
			phys->RemoveRigidBody(m_ColliderGrid[GenerateTerrainKey(tX, tZ)]);
		}
	}

	for (int i = 0; i < keys.size(); ++i)
	{
		m_TerrainGrid.erase(keys[i]);
		m_ColliderGrid.erase(keys[i]);
	}
}

//void TerrainManager::DecorateTerrain(Terrain* ter)
//{
//	CComPtr<IWICBitmapDecoder> pDecoder;
//	int tileX, tileZ;
//	ter->GetTilePosition(tileX, tileZ);
//
//	string texPath = "../Assets/Textures/Terrain/"+ m_MapName+ "/" + m_MapName+ "_texture" + GenerateTilePositionString(tileX,tileZ)+".png";
//	wstring widePath = to_wstring(texPath);
//
//	HRESULT hr = m_pIWICFactory->CreateDecoderFromFilename(
//		widePath.c_str(),                      // Image to be decoded
//		NULL,                            // Do not prefer a particular vendor
//		GENERIC_READ,                    // Desired read access to the file
//		WICDecodeMetadataCacheOnDemand,  // Cache metadata when needed
//		&pDecoder                        // Pointer to the decoder
//		);
//
//	CComPtr<IWICBitmapFrameDecode> pFrame;
//
//	if (FAILED(hr))
//		throw string("Failed creating decoder for terrain map");
//	hr = pDecoder->GetFrame(0, &pFrame);
//	if (FAILED(hr))
//		throw string("Failed getting frame from terrain decoder");
//	unsigned x, y;
//	hr = pFrame->GetSize(&x, &y);
//	if (FAILED(hr))
//		throw string("Failed getting WIC frame size");
//	WICPixelFormatGUID guid;
//	pFrame->GetPixelFormat(&guid);
//	if(GUID_WICPixelFormat48bppRGB != guid)
//		throw string("TerrainTexture has unsupported Pixelformat");
//
//	unsigned imageFootprint = 6 * x * y;
//
//	unique_ptr<unsigned char> pixx(new unsigned char[imageFootprint]);
//	unsigned char* pix = pixx.get();
//	hr = pFrame->CopyPixels(nullptr, 6 *x, imageFootprint, pix);
//	if (FAILED(hr))
//		throw string("failed to copy pixels from WIC frame decoder");
//
//	unsigned pixelCount = x * y;
//	string terrainKey = m_MapName + GenerateTilePositionString(tileX, tileX);
//
//	std::default_random_engine randE((tileZ*m_MaxGridWidthCount) + tileX);
//	std::uniform_int_distribution<unsigned short> uniform_dist(0, std::numeric_limits<unsigned short>::max());
//	float posX, posY, posZ;
//	ter->GetTerrainPosition(posX, posY, posZ);
//	float tileScale = ter->GetTileScale();
//	ResourceManager* res = Services::GetResourceManager();
//	ResourceHandle modHandle = res->GetModelHandle("pawn.pbm");
//	WorldQuery* wq = Services::GetWorldQuery();
//	RakNet::BitStream bs;
//	unsigned count = 0;
//	Physics* phys = Services::GetPhysics();
//	for(unsigned i = 0; i < y; ++i)
//	{
//		for(unsigned j = 0; j < x; ++j)
//		{
//			unsigned short blueVal = (unsigned short)pix[((y * i *6) + j * 6)];
//			unsigned short randChar = uniform_dist(randE);
//			if( randChar < std::numeric_limits<unsigned short>::max()/16)
//			{
//				count++;
//				float decX = posX + j * tileScale;
//				float decY = 10.0f;
//				float decZ = posZ + i * tileScale;
//				ModelComponent* mod = new ModelComponent(LocalOnly);
//				
//				mod->NetworkDeserialize(bs);
//				PositionComponent* pos = new PositionComponent(XMVectorSet(decX, decY, decZ, 1.0f), XMVectorSet(0.0f, 0.0f, 0.0f, 1.0f), LocalOnly);
//				Entity* ent = new Entity(LocalOnly);
//				ent->AddComponent(mod);
//				ent->AddComponent(pos);
//				wq->AddEntity(ent);
//			}
//		}
//	}
//	Services::GetDeveloperConsole()->WriteToConsole(to_wstring(count));
//}
