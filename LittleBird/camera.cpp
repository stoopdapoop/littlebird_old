#include "camera.h"
#include "MathUtilities.h"
#include <assert.h>

using namespace DirectX;
//=================================================================================================
// Camera
//=================================================================================================
Camera::Camera(float nearClip, float farClip) : m_NearZ(nearClip),
	m_FarZ(farClip)
{
	assert(m_NearZ > 0 && m_NearZ < m_FarZ);
	assert(m_FarZ > 0 && m_FarZ > m_NearZ);

	m_WorldMatrix = XMMatrixIdentity();
	m_ViewMatrix = XMMatrixIdentity();
	m_Position = XMVectorZero();
	m_Orientation = XMQuaternionIdentity();
}

Camera::~Camera()
{
}

void Camera::WorldMatrixChanged()
{
	XMVECTOR det;
	m_ViewMatrix = XMMatrixInverse(&det, m_WorldMatrix);
	m_ViewProjection = XMMatrixMultiply(m_ViewMatrix, m_ProjectionMatrix);
	m_Orientation = XMQuaternionRotationMatrix(m_WorldMatrix);
}

XMVECTOR Camera::GetForwardVector() const
{
	return ForwardVec(m_WorldMatrix);
}

XMVECTOR Camera::GetBackVector() const
{
	return BackVec(m_WorldMatrix);
}

XMVECTOR Camera::GetUpVector() const
{
	return UpVec(m_WorldMatrix);
}

XMVECTOR Camera::GetDownVector() const
{
	return DownVec(m_WorldMatrix);
}

XMVECTOR Camera::GetRightVector() const
{
	return RightVec(m_WorldMatrix);
}

XMVECTOR Camera::GetLeftVector() const
{
	return LeftVec(m_WorldMatrix);
}

void Camera::SetLookAt(const XMFLOAT3 &eye, const XMFLOAT3 &lookAt, const XMFLOAT3 &up)
{
	XMVECTOR det;
	m_ViewMatrix = XMMatrixLookAtLH(XMLoadFloat3(&eye), XMLoadFloat3(&lookAt), XMLoadFloat3(&up));
	m_WorldMatrix = XMMatrixInverse(&det, m_ViewMatrix);
	m_Position = XMLoadFloat3(&eye);

	WorldMatrixChanged();
}

void Camera::SetLookAt(const XMVECTOR &eye, const XMVECTOR &lookAt, const XMVECTOR &up)
{
	XMVECTOR det;
	m_ViewMatrix = XMMatrixLookAtLH(eye, lookAt, up);
	m_WorldMatrix = XMMatrixInverse(&det, m_ViewMatrix);
	m_Position = eye;

	WorldMatrixChanged();
}

void Camera::SetWorldMatrix(const XMMATRIX& newWorld)
{
	m_WorldMatrix = newWorld;
	m_Position = TranslationVec(m_WorldMatrix);

	WorldMatrixChanged();
}

void Camera::SetPosition(const XMVECTOR& newPosition)
{
	m_Position = newPosition;
	SetTranslationVec(m_WorldMatrix, newPosition);

	WorldMatrixChanged();
}

void Camera::SetOrientation(const XMVECTOR& newOrientation)
{
	m_WorldMatrix = XMMatrixRotationQuaternion(newOrientation);
	//m_Orientation = newOrientation;
	SetTranslationVec(m_WorldMatrix, m_Position);

	WorldMatrixChanged();
}

void Camera::SetNearClip(float newNearClip)
{
	m_NearZ = newNearClip;
	CreateProjection();
}

void Camera::SetFarClip(float newFarClip)
{
	m_FarZ = newFarClip;
	CreateProjection();
}

void Camera::SetProjection(const XMMATRIX& newProjection)
{
	m_ProjectionMatrix = newProjection;
	m_ViewProjection = XMMatrixMultiply(m_ViewMatrix, m_ProjectionMatrix);
}

//=================================================================================================
// OrthographicCamera
//=================================================================================================

OrthographicCamera::OrthographicCamera(float minX, float minY, float maxX,
									   float maxY, float nearClip, float farClip) : Camera(nearClip, farClip),
									   m_XMin(minX),
									   m_YMin(minY),
									   m_XMax(maxX),
									   m_YMax(maxY)
{
	assert(m_XMax > m_XMin && m_YMax > m_YMin);

	CreateProjection();
}

OrthographicCamera::~OrthographicCamera()
{
}

void OrthographicCamera::CreateProjection()
{
	m_ProjectionMatrix = XMMatrixOrthographicOffCenterLH(m_XMin, m_XMax, m_YMin, m_YMax, m_NearZ, m_FarZ);
	m_ViewProjection = XMMatrixMultiply(m_ViewMatrix, m_ProjectionMatrix);
}

void OrthographicCamera::SetMinX(float minX)
{
	m_XMin = minX;
	CreateProjection();
}

void OrthographicCamera::SetMinY(float minY)
{
	m_YMin = minY;
	CreateProjection();
}

void OrthographicCamera::SetMaxX(float maxX)
{
	m_XMax = maxX;
	CreateProjection();
}

void OrthographicCamera::SetMaxY(float maxY)
{
	m_YMax = maxY;
	CreateProjection();
}

//=================================================================================================
// PerspectiveCamera
//=================================================================================================

PerspectiveCamera::PerspectiveCamera(float aspectRatio, float fieldOfView,
									 float nearClip, float farClip) :   Camera(nearClip, farClip),
									 m_AspectRatio(aspectRatio),
									 m_Fov(fieldOfView)
{
	assert(aspectRatio > 0);
	assert(fieldOfView > 0 && fieldOfView <= 3.14159f);
	CreateProjection();
}

PerspectiveCamera::~PerspectiveCamera()
{
}

void PerspectiveCamera::SetAspectRatio(float aspectRatio)
{
	m_AspectRatio = aspectRatio;
	CreateProjection();
}

//rads
void PerspectiveCamera::SetFieldOfView(float fieldOfView)
{
	m_Fov = fieldOfView;
	CreateProjection();
}

void PerspectiveCamera::CreateProjection()
{
	m_ProjectionMatrix = XMMatrixPerspectiveFovLH(m_Fov, m_AspectRatio, m_NearZ, m_FarZ);
	m_ViewProjection = XMMatrixMultiply(m_ViewMatrix, m_ProjectionMatrix);
}

//=================================================================================================
// FirstPersonCamera
//=================================================================================================

FirstPersonCamera::FirstPersonCamera(float aspectRatio, float fieldOfView,
									 float nearClip, float farClip) : PerspectiveCamera(aspectRatio, fieldOfView,
									 nearClip, farClip),
									 m_XRotation(0),
									 m_YRotation(0)
{
}

FirstPersonCamera::~FirstPersonCamera()
{
}

void FirstPersonCamera::SetXRotation(float xRotation)
{
	m_XRotation = XMScalarModAngle(xRotation);
	SetOrientation(XMQuaternionRotationRollPitchYaw(m_YRotation, m_XRotation, 0));
}

void FirstPersonCamera::SetYRotation(float yRotation)
{
	m_YRotation = clamp(yRotation, -XM_PIDIV2, XM_PIDIV2);
	SetOrientation(XMQuaternionRotationRollPitchYaw(m_YRotation, m_XRotation, 0));
}
