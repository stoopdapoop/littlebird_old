#ifndef _MATH_UTILITIES_H_
#define _MATH_UTILITIES_H_

#include <DirectXMath.h>

template <typename T> T clamp(T value, T low, T high)
{
	return (value < low) ? low : ((value > high) ? high : value);
}

template <typename T> T saturate(T value)
{
	return (value < 0) ? 0 : ((value > 1) ? 1 : value);
}

template <typename T> int sign(T val) {
	return (T(0) < val) - (val < T(0));
}

// Returns the forward vector from a transform matrix
inline DirectX::XMVECTOR ForwardVec(const DirectX::XMMATRIX& matrix)
{
	return matrix.r[2];
}

// Returns the forward vector from a transform matrix
inline DirectX::XMVECTOR BackVec(const DirectX::XMMATRIX& matrix)
{
	return DirectX::XMVectorNegate(matrix.r[2]);
}

// Returns the forward vector from a transform matrix
inline DirectX::XMVECTOR RightVec(const DirectX::XMMATRIX& matrix)
{
	return matrix.r[0];
}

// Returns the forward vector from a transform matrix
inline DirectX::XMVECTOR LeftVec(const DirectX::XMMATRIX& matrix)
{
	return DirectX::XMVectorNegate(matrix.r[0]);
}

// Returns the forward vector from a transform matrix
inline DirectX::XMVECTOR UpVec(const DirectX::XMMATRIX& matrix)
{
	return matrix.r[1];
}

// Returns the forward vector from a transform matrix
inline DirectX::XMVECTOR DownVec(const DirectX::XMMATRIX& matrix)
{
	return DirectX::XMVectorNegate(matrix.r[1]);
}

// Returns the translation vector from a transform matrix
inline DirectX::XMVECTOR TranslationVec(const DirectX::XMMATRIX& matrix)
{
	return matrix.r[3];
}

// Sets the translation vector in a transform matrix
inline void SetTranslationVec(DirectX::XMMATRIX& matrix, const DirectX::XMVECTOR& translation)
{
	const DirectX::XMVECTOR vecOne = DirectX::XMVectorSplatOne();
	matrix.r[3] = DirectX::XMVectorPermute(translation, vecOne, 0,1,2,4);
}

#endif