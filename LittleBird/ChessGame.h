#include <windows.h>
#include <memory>
#include <vector>

#include "component.h"

class Entity;
class PerspectiveCamera;
class Camera;
class DebugUI;
class Input;
class Physics;
class Renderer;
class Timer;
class DeveloperConsole;
class DebugDrawer;
class ResourceManager;
class PerspectiveCamera;
class Entity;
class ModelRenderSystem;
class LanderSystem;
class TerrainManager;
class Networking;
class WorldQuery;
class TerrainDecoratorSystem;
class UnitSystem;
class PlayerSystem;

class ChessGame
{
public:
	ChessGame(int screenheight, int screewidth, HWND hwnd, HINSTANCE hinstance, bool fullScreen, bool vSync);
	~ChessGame();

	bool Frame();

private:

	void CreateSubsystems(int screenheight, int screenwidth, HWND hwnd, HINSTANCE hinstance, bool fullScreen, bool vSync);
	void CreateGameSystems();
	void CreateGameObjects();
	void InitializeSubsystems();
	void StartGame();

	bool HandleInput(float delta);

	void HandleGameInput( float delta );

	void RenderFrame();

	void RenderFrameShadows();

	void RenderWater();

	void DispatchEntities();

	void ReplicateEntity(uint64_t id, bool spawning, char replicationFlags);
	void AddReplicatedComponentToEntity(ComponentID_t idType, Entity* ent, Component* com);

	std::wstring Console_HostGame(std::wstring args);
	std::wstring Console_ConnectGame(std::wstring args);
private:
	bool m_GameRunning;

	std::unique_ptr<Input> m_Input;
	std::unique_ptr<Renderer> m_Renderer;
	std::unique_ptr<Timer> m_GameTimer;
	std::unique_ptr<DeveloperConsole> m_DeveloperConsole;
	std::unique_ptr<DebugUI> m_DebugUI;
	std::unique_ptr<Physics> m_Physics;
	std::unique_ptr<ResourceManager> m_ResourceManager;
	std::unique_ptr<DebugDrawer> m_DebugDrawer;
	std::unique_ptr<PerspectiveCamera> m_PerspectiveCamera;
	std::unique_ptr<ModelRenderSystem> m_ModelRenderSystem;
	std::unique_ptr<TerrainManager> m_TerrainManager;
	std::unique_ptr<Networking> m_Networking;
	//std::unique_ptr<WorldQuery> m_WorldQuery;
	std::unique_ptr<TerrainDecoratorSystem> m_TerrainDecoratorSystem;
	//std::unique_ptr<UnitSystem> m_UnitSystem;
	std::unique_ptr<LanderSystem> m_LanderSystem;
	//std::unique_ptr<PlayerSystem> m_PlayerSystem;

	std::vector<Entity*> m_Entities;
};