#ifndef _HELICOPTER_SYSTEM_H_
#define _HELICOPTER_SYSTEM_H_

#include "component_system.h"

class Input;
class LanderComponent;
class RigidBodyComponent;
class btRigidBody;
class Camera;
class LanderInputAdapter;

enum RotationCoordinateSpace
{
	CAMERA_SPACE,
	OBJECT_SPACE
};

class LanderSystem : public ComponentSystem
{
public:
	LanderSystem();
	~LanderSystem();
	void AddEntry(unsigned ID, LanderComponent* lander, RigidBodyComponent* rigid);
	void Run(Camera* cam);
	void SetUseCamera(bool enabled) { m_UseLanderCam = enabled; }
	bool UsingLanderCam() { return m_UseLanderCam; }
	
private:
	void PositionCamera(Camera* cam, LanderComponent* lander, btRigidBody* rigid);
	std::wstring Console_SetUseCamera(std::wstring args);

private:
	LanderInputAdapter*			m_ControlAdapter;
	bool								m_UseLanderCam;
	std::vector<LanderComponent*>		m_Landers;
	std::vector<RigidBodyComponent*>	m_RigidBodies;
};

#endif