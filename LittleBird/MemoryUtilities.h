#ifndef _MEMORY_UTILITIES_H_
#define _MEMORY_UTILITIES_H_

#ifndef HR
#define HR(x)		                                          \
{						                                      \
	  HRESULT hr = (x);                                       \
	  if(FAILED(hr))                                          \
	  {                                                       \
		throw string("DX error");    \
	  }                                                       \
}
#endif

template <typename T>
void SafeDelete(T*& ptr) 
{
	if(ptr)
	{
		delete ptr;
		ptr = nullptr;
	}
}

template <typename T>
void SafeDeleteArray(T*& ptr) 
{
	if(ptr)
	{
		delete[] ptr;
		ptr = nullptr;
	}
}

template <typename T>
void SafeRelease(T*& ptr) 
{ 
	if(ptr)
	{
		ptr->Release();
		ptr = nullptr;
	}
}

#endif