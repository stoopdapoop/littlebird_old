#ifndef _COMPONENT_H_
#define _COMPONENT_H_

#include "ReplicationFlags.h"
#pragma warning(disable:4005)
#include "BitStream.h"
#pragma warning(default:4005)

typedef unsigned char ComponentID_t;
typedef unsigned ComponentMask_t;

namespace RakNet{
	class BitStream;
};

enum class ComponentID : ComponentID_t
{
	Position = 0,
	Model,
	RigidBody,
	Lander,
	Player,
	Unit,
	Bait,

	ComponentCount,
};

static_assert( (int)ComponentID::ComponentCount < 33, "Too many components to be identified by 32 bit mask");

// NO VIRTUAL DESTRUCTOR DON'T FORGET, ELSE BAD THINGS
class Component
{
public:
	ComponentID GetComponentID() { return m_ID; }
	ComponentMask_t GetComponentMask() { return 1 << (ComponentID_t)m_ID; }
	unsigned char GetReplicationFlags() { return m_ReplicationFlags; }

	void NetworkSerialize(RakNet::BitStream& bs);
	void NetworkDeserialize(RakNet::BitStream& bs);

protected:
	Component(ComponentID type, char replicationFlags);
	unsigned char m_ReplicationFlags;
	ComponentID m_ID;
	unsigned m_padding[3];
private:
	// do not try to use
	Component();
};

#endif