#ifndef _RESOURCE_MANAGER_H_
#define _RESOURCE_MANAGER_H_

#include <unordered_map>
#include <string>
#include "ResourceHandle.h"
#include "physics.h"


class Model;
class Texture;
class Renderer;


typedef std::unordered_map<ResourceID, Model*> ModelMap;
typedef std::unordered_map<ResourceID, Texture*> TextureMap;
typedef std::unordered_map<ResourceID, btCollisionShape*> CollisionShapeMap;
typedef std::unordered_map<std::string, ResourceHandle> HandleMap;

enum CollisionShape {
	CAPSULE,
	CONE,
	CYLINDER,
	SPHERE,
	BOX,
	ShapeCount,
};



class ResourceManager
{
public:
	ResourceManager(Renderer* rend, Physics* phys);
	~ResourceManager();

	ResourceHandle GetModelHandle(std::string modelName);
	ResourceHandle GetTextureHandle(std::string textureName);
	ResourceHandle GetCollisionShapeHandle(std::string CollisionShapeName);

	// capsule: radius, height
	// cone:radius, height
	// cylinder:vector3(halfextents)
	// sphere: radius
	ResourceHandle GetCollisionShapeHandle(CollisionShape shape, float x = 1.0f, float y = 1.0f, float z = 1.0f);

	Model* GetModel(ResourceHandle modelHandle);
	Texture* GetTexture(ResourceHandle textureHandle);
	Texture* GetTextureFromName(std::string textureName);

	btCollisionShape* GetCollisionShape(ResourceHandle collisionShapeHandle);

private:
	template<class Iter>
	void ReleaseMapAssets(Iter begin, Iter end);

private:
	ModelMap m_Models;
	TextureMap m_Textures;
	CollisionShapeMap m_CollisionShapes;
	HandleMap m_TextureHandles;
	HandleMap m_ModelHandles;
	HandleMap m_CollisionShapeHandles;

	ResourceID m_NextResourceID;
	
	Renderer* m_Renderer;
	Physics* m_Physics;
};

template<class Iter>
void ResourceManager::ReleaseMapAssets( Iter begin, Iter end )
{
	for(; begin != end ; ++begin )
	{
		delete (*begin).second;
	}
}

#endif