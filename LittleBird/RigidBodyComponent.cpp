
#include "RigidBodyComponent.h"
#include "PositionComponent.h"
#include "physics.h"
#include "services.h"



RigidBodyComponent::RigidBodyComponent( ResourceHandle rigidHandle, PositionComponent* pos, float weight, char replicationFlags ) : Component(ComponentID::RigidBody, replicationFlags), 
	ShapeHandle(rigidHandle), Position(pos)
{
	Physics* phys = Services::GetPhysics();
	btTransform trans = btTransform::getIdentity();
	trans.setOrigin(*(btVector3*)&pos->Position);
	RigidBody = phys->CreateRigidBody(weight, this);
}

RigidBodyComponent::~RigidBodyComponent()
{
	Physics* phys = Services::GetPhysics();
	phys->RemoveRigidBody(RigidBody);
}
