#ifndef _DEBUG_DRAWER_H_
#define _DEBUG_DRAWER_H_


#pragma warning(disable:4005)
#include <PrimitiveBatch.h>
#include <DirectxTK/Effects.h>
#include <VertexTypes.h>
#pragma warning(default:4005)
#include <memory>
#include <string>
#include "LinearMath/btIDebugDraw.h"

class Renderer;
class Camera;
namespace DirectX
{
	class SpriteBatch;
	class SpriteFont;
};

enum	DebugDrawModes
{
	DBG_NoDebug=0,
	DBG_DrawWireframe = 1,
	DBG_DrawAabb=2,
	DBG_DrawFeaturesText=4,
	DBG_DrawContactPoints=8,
	DBG_NoDeactivation=16,
	DBG_NoHelpText = 32,
	DBG_DrawText=64,
	DBG_ProfileTimings = 128,
	DBG_EnableSatComparison = 256,
	DBG_DisableBulletLCP = 512,
	DBG_EnableCCD = 1024,
	DBG_DrawConstraints = (1 << 11),
	DBG_DrawConstraintLimits = (1 << 12),
	DBG_FastWireframe = (1<<13),
	DBG_DrawNormals = (1<<14),
	DBG_MAX_DEBUG_DRAW_MODE
};


class DebugDrawer : public btIDebugDraw
{
public:
	DebugDrawer(Renderer* rend);
	~DebugDrawer();

public:
	void drawText(float x, float y, std::string text, float scale = 1.0f);

	void drawLine( const btVector3& from,const btVector3& to,const btVector3& color );

	void drawContactPoint( const btVector3& PointOnB,const btVector3& normalOnB,btScalar distance,int lifeTime,const btVector3& color ) ;

	void reportErrorWarning( const char* warningString );

	void draw3dText( const btVector3& location,const char* textString );

	void setDebugMode( int debugMode );

	int getDebugMode() const;

	void Present(Renderer* rend);
	void SetupCamera(Camera* cam, Renderer* rend);

private:
	int m_DebugMode;
	bool m_BatchStarted;
	std::unique_ptr<DirectX::PrimitiveBatch<DirectX::VertexPositionColor>> m_PrimativeBatch;
	std::unique_ptr<DirectX::BasicEffect> m_Effect;
	ID3D11CommandList* m_DebugCommandList;
	ID3D11InputLayout* m_InputLayout;
	DirectX::SpriteBatch* m_SpriteBatch;
	DirectX::SpriteFont* m_SpriteFont;
	Renderer* m_Renderer;
};

#endif