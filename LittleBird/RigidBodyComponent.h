#ifndef _RIGID_BODY_COMPONENT_H_
#define _RIGID_BODY_COMPONENT_H_

#include "component.h"
#include "ResourceHandle.h"
#include <DirectXMath.h>

class btRigidBody;
class PositionComponent;

class RigidBodyComponent : public Component
{
public:
	RigidBodyComponent(ResourceHandle rigidHandle, PositionComponent* pos, float weight, char replicationFlags);
	~RigidBodyComponent();
	
	ResourceHandle ShapeHandle;
	btRigidBody* RigidBody;
	//todo: probably shouldn't know about it's own position
	// but the alternative is to create a system and supply it during dispatch, which seems wasteful for physics.
	PositionComponent* Position;
};

#endif