
#include "services.h"
#include "UnitComponent.h"
#include "ModelComponent.h"
#include "UnitSystem.h"
#include "BulletCollision\CollisionDispatch\btGhostObject.h"
using namespace DirectX;

UnitSystem::UnitSystem()
{

}

void UnitSystem::AddEntry(unsigned ID, UnitComponent* unit, ModelComponent* mod, PositionComponent* pos)
{
	m_Roster.push_back(ID);
	m_Units.push_back(unit);
	m_Models.push_back(mod);
	m_Positions.push_back(pos);
}

void UnitSystem::AddBait(unsigned ID, BaitComponent* bait, PositionComponent* pos)
{
	m_BaitRoster.push_back(ID);
	m_Bait.push_back(bait);
	m_BaitPositions.push_back(pos);
}

void UnitSystem::Run()
{
	size_t unitCount = m_Roster.size();
	for (size_t i = 0; i < unitCount; ++i)
	{
		PositionComponent* pos = m_Positions[i];
		UnitComponent* unit = m_Units[i];
		btKinematicCharacterController* kineCon = unit->kinematicController;

		btTransform trans = kineCon->getGhostObject()->getWorldTransform();

		btQuaternion tempQuat = trans.getRotation();

		XMVECTOR unitPos = *(XMVECTOR*)&trans.getOrigin();
		XMVECTOR unitRot = *(XMVECTOR*)&tempQuat;

		pos->Position = unitPos;
		pos->Orientation = unitRot;

		if (kineCon->onGround())
			kineCon->setWalkDirection(btVector3(0.0f, 0.0f, unit->MoveSpeed));
	}

	m_Roster.clear();
	m_Units.clear();
	m_Models.clear();
	m_Positions.clear();

	m_BaitRoster.clear();
	m_Bait.clear();
	m_BaitPositions.clear();
}
