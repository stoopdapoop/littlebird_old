

#include "renderer.h"
#include "camera.h"
#include "StringUtilities.h"
#pragma warning(disable:4005)
#include "DirectxTK/SpriteBatch.h"
#include "DirectxTK/CommonStates.h"
#include "DirectxTK/SpriteFont.h"
#pragma warning(default:4005)
#include "DebugDrawer.h"
#include "services.h"
using namespace DirectX;
using namespace std;

DebugDrawer::DebugDrawer(Renderer* rend) : m_BatchStarted(false), m_SpriteBatch(nullptr), m_SpriteFont(nullptr), m_Renderer(rend)
{
	m_PrimativeBatch.reset(new PrimitiveBatch<VertexPositionColor>(rend->GetDebugContext()));
	m_Effect.reset(new BasicEffect(rend->GetDevice()));

	m_Effect->SetVertexColorEnabled(true);

	void const* shaderByteCode;
	size_t byteCodeLength;

	m_Effect->GetVertexShaderBytecode(&shaderByteCode, &byteCodeLength);

	rend->GetDevice()->CreateInputLayout(VertexPositionColor::InputElements,
		VertexPositionColor::InputElementCount,
		shaderByteCode, byteCodeLength,
		&m_InputLayout);

	rend->GetDebugContext()->IASetInputLayout(m_InputLayout);
	m_Effect->SetWorld(XMMatrixIdentity());
	m_Effect->SetView(XMMatrixIdentity());
	m_Effect->SetProjection(XMMatrixIdentity());
	m_Effect->Apply(rend->GetDebugContext());

	m_SpriteBatch = new SpriteBatch(rend->GetDebugContext());
	m_SpriteFont = new SpriteFont(rend->GetDevice(), L"../Assets/Fonts/poopoo.spritefont");
	m_SpriteFont->SetDefaultCharacter(L'?');

	m_DebugMode = 0;
}

DebugDrawer::~DebugDrawer()
{
	delete m_SpriteBatch;
	delete m_SpriteFont;
}

void DebugDrawer::drawLine( const btVector3& from,const btVector3& to,const btVector3& color ) 
{
	if(!m_BatchStarted)
	{
		m_PrimativeBatch->Begin();
		m_BatchStarted = true;
	}

	XMVECTOR xmfrom, xmto, xmcolor;
	xmfrom = XMLoadFloat3((XMFLOAT3*)&from);
	xmto = XMLoadFloat3((XMFLOAT3*)&to);
	xmcolor = XMLoadFloat3((XMFLOAT3*)&color);
	VertexPositionColor v1(xmfrom, xmcolor);
	VertexPositionColor v2(xmto, xmcolor);
	m_PrimativeBatch->DrawLine(v1, v2);
}

void DebugDrawer::drawContactPoint( const btVector3& PointOnB,const btVector3& normalOnB,btScalar distance,int lifeTime,const btVector3& color ) 
{
	color;
	lifeTime;
	distance;
	normalOnB;
	PointOnB;
}

void DebugDrawer::reportErrorWarning( const char* warningString ) 
{
	DeveloperConsole* dev = Services::GetDeveloperConsole();

	dev->WriteToConsole(to_wstring(string(warningString)));
}

void DebugDrawer::draw3dText( const btVector3& location,const char* textString ) 
{
	textString;
	location;
	throw L"implement 3d debug text";
}

void DebugDrawer::setDebugMode( int debugMode ) 
{
	m_DebugMode = debugMode;
}

int DebugDrawer::getDebugMode() const
{
	return m_DebugMode;
}

void DebugDrawer::Present(Renderer* rend)
{
	if(m_BatchStarted)
	{	
		m_PrimativeBatch->End();

		m_BatchStarted = false;
	}

	rend->GetDebugContext()->FinishCommandList(true, &m_DebugCommandList);

	// todo: remove these slow calls with restore
	rend->GetImmediateContext()->ExecuteCommandList(m_DebugCommandList, true);

	m_DebugCommandList->Release();
}

void DebugDrawer::SetupCamera( Camera* cam, Renderer* rend )
{
	rend->GetDebugContext()->IASetInputLayout(m_InputLayout);
	m_Effect->SetWorld(XMMatrixIdentity());
	m_Effect->SetView(cam->GetViewMatrix());
	m_Effect->SetProjection(cam->GetProjectionMatrix());
	m_Effect->Apply(rend->GetDebugContext());
}

void DebugDrawer::drawText( float x, float y, std::string text, float scale)
{
	m_SpriteBatch->Begin(SpriteSortMode_Deferred);
	m_SpriteFont->DrawString(m_SpriteBatch, to_wstring(text).c_str(), XMVectorSet(x,y,1,1), Colors::White, 0.0f, g_XMZero, XMVectorSet(scale, scale, scale, scale));
	m_SpriteBatch->End();
}

