
#include "camera.h"
#include <DirectXCollision.h>
#include "MemoryUtilities.h"
#include "model.h"
#include <random>
#include "renderer.h"
#include "services.h"
#include "terrain.h"
#include "TerrainDecoratorSystem.h"
#include "TerrainManager.h"
#include "texture.h"

#include "DebugDrawer.h"

using namespace DirectX;

// todo: make sure live instance count doesn't exceed buffer size

#define MAX_INSTANCE_COUNT 80000

TerrainDecoratorSystem::TerrainDecoratorSystem(Renderer* rend, TerrainManager* terr) : m_Renderer(rend), m_TerrainManager(terr)
{
	ID3D11Device* device = m_Renderer->GetDevice();
	
	D3D11_BUFFER_DESC bufferDesc;
	ZeroMemory(&bufferDesc, sizeof(bufferDesc));

	// camera buffer
	bufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	bufferDesc.ByteWidth = sizeof(DecorationInstanceData) * MAX_INSTANCE_COUNT;
	bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	bufferDesc.MiscFlags = 0;
	bufferDesc.StructureByteStride = 0;

	// Create the constant buffer pointer so we can access the vertex shader constant buffer from within this class.
	HR(device->CreateBuffer(&bufferDesc, NULL, &m_InstanceBuffer));

	m_VisibleInstanceCount = 0;
}

void TerrainDecoratorSystem::Run(Camera* cam)
{
	m_VisibleInstanceCount = 0;
	const XMVECTOR xmViewPos = cam->GetPosition();
	
	BoundingFrustum camFrustum;
	BoundingFrustum::CreateFromMatrix(camFrustum, cam->GetProjectionMatrix());
	camFrustum.Transform(camFrustum, 1.0f, cam->Orientation(), xmViewPos);

	const float distCutoff = 800.0f * 3.0f;
	const float distCutoffSq = distCutoff * distCutoff;

	// broadphase filtering
	float halfTileLength = Terrain::CalculateTileTerrainSideLength() * 0.5f;
	XMVECTOR xmOffset = XMVectorSet(halfTileLength, 0.0f, halfTileLength, 0.0f);
	// todo: use distcutoff in the narrowPhase distance check
	XMVECTOR xmDistCutoff = XMVectorReplicate(distCutoff);
	XMVECTOR xmDistCutoffSq = XMVectorReplicate(distCutoffSq);
	
	vector<TerrainCollisionPair> narrowPhaseTiles;
	PerformBroadPhaseTileFiltering(narrowPhaseTiles, xmViewPos, xmOffset, xmDistCutoffSq, camFrustum);

	Services::GetDebugDrawer()->drawText(400, 400, to_string(narrowPhaseTiles.size()));

	// narrowphase and write
	PopulateInstanceBuffer(narrowPhaseTiles, xmViewPos, xmDistCutoff, cam);

	CleaupOldDecorations();

	Services::GetDebugDrawer()->drawText(400, 500, to_string(m_VisibleInstanceCount));
}

void TerrainDecoratorSystem::PerformBroadPhaseTileFiltering(std::vector<TerrainCollisionPair>& narrowPhaseTiles, XMVECTOR xmViewPos, XMVECTOR xmOffset, 
	XMVECTOR xmDistCutoffSq, BoundingFrustum& camFrustum)
{
	auto visibleTiles = m_TerrainManager->GetVisibleTiles();
	int x, z;
	m_TerrainManager->CalculateTilePosition(xmViewPos, x, z);

	XMVECTOR xmDoubleDistCutoffSq = XMVectorMultiply(XMVectorReplicate(2.5f), xmDistCutoffSq);
	XMVECTOR xmOffsetSq = XMVectorMultiply(xmOffset, xmOffset);
	float halfTileLength = Terrain::CalculateTileTerrainSideLength() * 0.5f;

	for (auto tile : *visibleTiles)
	{
		Terrain* ter = tile.terrain;
		XMFLOAT3 terPos;
		ter->GetTerrainPosition(terPos.x, terPos.y, terPos.z);
		XMVECTOR xmTilePos = XMLoadFloat3(&terPos);
		XMVECTOR xmTileCenter = XMVectorAdd(xmTilePos, xmOffset);
		XMVECTOR xmToTerrainRay = XMVectorSubtract(xmTileCenter, xmViewPos);
		XMVECTOR xmToTerrainLenSq = XMVector3LengthSq(xmToTerrainRay);
		uint32_t inRange = XMVector3GreaterR(XMVectorAdd(xmDoubleDistCutoffSq, xmOffsetSq), xmToTerrainLenSq);
		if (XMComparisonAnyTrue(inRange))
		{
			//todo: use bounding box after rangecheck, calculate BB in terrain class
			// no need to vectorize this because the number of terrains that pass the range check will be tiny.
			XMFLOAT3 terCenter;
			XMStoreFloat3(&terCenter, xmTileCenter);
			float terHeightMax = ter->GetMaxHeight();
			float terHeightMin = ter->GetMinHeight();
			float terHeightExtent = (terHeightMax - terHeightMin) * 0.5f;
			float terHeightMid = terHeightExtent + terHeightMin;
			BoundingBox bb;
			bb.Center = { terCenter.x, terHeightMid, terCenter.z };
			bb.Extents = { halfTileLength, terHeightExtent, halfTileLength };
			if (camFrustum.Contains(bb))
				narrowPhaseTiles.push_back(tile);
		}
	}
}

void TerrainDecoratorSystem::PopulateInstanceBuffer(vector<TerrainCollisionPair>& narrowPhaseTiles, const DirectX::XMVECTOR xmViewPos, DirectX::XMVECTOR xmDistCutoff, Camera* cam)
{
	ID3D11DeviceContext* context = m_Renderer->GetImmediateContext();

	D3D11_MAPPED_SUBRESOURCE mappedResource;
	context->Map(m_InstanceBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);

	DecorationInstanceData* data;
	data = (DecorationInstanceData*)mappedResource.pData;

	ResourceManager* res = Services::GetResourceManager();

	for (auto tile : narrowPhaseTiles)
	{
		auto terIterator = m_TileDecorationPairs.find((size_t)tile.terrain);
		if (terIterator != m_TileDecorationPairs.end())
		{
			// there is a decoration for this

			// should coarse lod changes happen?

			// should there still be decoration for this?
			for (const TerrainDecorationList& decList : (*terIterator).second.tileDecorationLists)
			{
				//todo: do a little book keeping to keep track of where each decoration starts and ends.
				Model* mod = res->GetModel(decList.modelHandle);
				float boundingDist = std::fmaxf(mod->m_BoundingBoxHalfExtents[0], mod->m_BoundingBoxHalfExtents[1]);
				boundingDist = std::fmaxf(boundingDist, mod->m_BoundingBoxHalfExtents[2]);
				BoundingSphere bs;
				bs.Radius = boundingDist;
				for (auto decor : decList.instances)
				{
					XMVECTOR decorPos = decor.worldMatrix.r[3];
					XMStoreFloat3(&bs.Center, decorPos);
					XMVECTOR decorRay = XMVectorSubtract(decorPos, xmViewPos);
					XMVECTOR decorRayLenSq = XMVector3LengthEst(decorRay);
					uint32_t inRange = XMVector3GreaterR(xmDistCutoff, decorRayLenSq);
					if (XMComparisonAnyTrue(inRange))
					{
						XMVECTOR camForward = cam->GetForwardVector();
						XMVECTOR camDecorDot = XMVector3Dot(camForward, XMVectorDivide(decorRay, decorRayLenSq));
						// todo derive this number from camera fov
						uint32_t aboveZero = XMVector3GreaterR(camDecorDot, XMVectorReplicate(0.55f));
						if (XMComparisonAnyTrue(aboveZero))
						{
							data[m_VisibleInstanceCount++].worldMatrix = XMMatrixTranspose(decor.worldMatrix);
							if (m_VisibleInstanceCount == MAX_INSTANCE_COUNT)
								throw(string("too many instances, tell poop to manage buffers smarterly"));
						}
					}
				}
			}
		}
		else
		{
			GenerateDecorationListForTile(&tile);
		}
	}

	context->Unmap(m_InstanceBuffer, 0);
}


void TerrainDecoratorSystem::RenderToShadowMap()
{
	ID3D11DeviceContext* context = m_Renderer->GetImmediateContext();
	ResourceManager* res = Services::GetResourceManager();

	m_Renderer->BeginInstanceShadowMapShader();

	if (m_TileDecorationPairs.size() > 0)
	{
		// here we iterate through the bookkeeping we made and do our appropriate instanced calls
		Model* mod = res->GetModel((*m_TileDecorationPairs.begin()).second.tileDecorationLists[0].modelHandle);
		ResourceHandle diff = res->GetTextureHandle("rock_bc7.dds");
		ResourceHandle normal = res->GetTextureHandle("rock_normal_BC7.dds");
		ResourceHandle rough = res->GetTextureHandle("woodgrain_BC7.dds");
		ResourceHandle spec = res->GetTextureHandle("subtlenormals_BC7.dds");
		// 4 good number
		// u feel?
		ID3D11ShaderResourceView* tex[4];
		Texture* currentTexture[4];
		currentTexture[0] = res->GetTexture(diff);
		currentTexture[1] = res->GetTexture(normal);
		currentTexture[2] = res->GetTexture(rough);
		currentTexture[3] = res->GetTexture(spec);

		tex[0] = currentTexture[0]->GetResourceView();
		tex[1] = currentTexture[1]->GetResourceView();
		tex[2] = currentTexture[2]->GetResourceView();
		tex[3] = currentTexture[3]->GetResourceView();
		context->PSSetShaderResources(0, 4, tex);

		ID3D11Buffer* vertStreams[] = { mod->GetVertexBuffer(), m_InstanceBuffer };

		context->IASetIndexBuffer(mod->GetIndexBuffer(), DXGI_FORMAT_R16_UINT, 0);

		unsigned int strides[] = { sizeof(VertexType), sizeof(DecorationInstanceData) };
		unsigned int offsets[] = { 0, 0 };
		context->IASetVertexBuffers(0, 2, vertStreams, strides, offsets);

		context->DrawIndexedInstanced(mod->GetIndexCount(), m_VisibleInstanceCount, 0, 0, 0);

		vertStreams[0] = vertStreams[1] = nullptr;
		context->IASetVertexBuffers(0, 2, vertStreams, strides, offsets);
	}
}


void TerrainDecoratorSystem::RenderToScreen()
{
	ID3D11DeviceContext* context = m_Renderer->GetImmediateContext();
	ResourceManager* res = Services::GetResourceManager();

	m_Renderer->BeginInstancedGbufferShader();
	
	if (m_TileDecorationPairs.size() > 0)
	{
		// here we iterate through the bookkeeping we made and do our appropriate instanced calls
		Model* mod = res->GetModel((*m_TileDecorationPairs.begin()).second.tileDecorationLists[0].modelHandle);
		ResourceHandle diff = res->GetTextureHandle("rock_bc7.dds");
		ResourceHandle normal = res->GetTextureHandle("rock_normal_BC7.dds");
		ResourceHandle rough = res->GetTextureHandle("woodgrain_BC7.dds");
		ResourceHandle spec = res->GetTextureHandle("subtlenormals_BC7.dds");
		// 4 good number
		// u feel?
		ID3D11ShaderResourceView* tex[4];
		Texture* currentTexture[4];
		currentTexture[0] = res->GetTexture(diff);
		currentTexture[1] = res->GetTexture(normal);
		currentTexture[2] = res->GetTexture(rough);
		currentTexture[3] = res->GetTexture(spec);

		tex[0] = currentTexture[0]->GetResourceView();
		tex[1] = currentTexture[1]->GetResourceView();
		tex[2] = currentTexture[2]->GetResourceView();
		tex[3] = currentTexture[3]->GetResourceView();
		context->PSSetShaderResources(0, 4, tex);

		ID3D11Buffer* vertStreams[] = { mod->GetVertexBuffer(), m_InstanceBuffer };

		context->IASetIndexBuffer(mod->GetIndexBuffer(), DXGI_FORMAT_R16_UINT, 0);

		unsigned int strides[] = { sizeof(VertexType), sizeof(DecorationInstanceData) };
		unsigned int offsets[] = { 0, 0 };
		context->IASetVertexBuffers(0, 2, vertStreams, strides, offsets);

		context->DrawIndexedInstanced(mod->GetIndexCount(), m_VisibleInstanceCount, 0, 0, 0);

		vertStreams[0] = vertStreams[1] = nullptr;
		context->IASetVertexBuffers(0, 2, vertStreams, strides, offsets);
	}
}

void TerrainDecoratorSystem::GenerateDecorationListForTile(const TerrainCollisionPair* tile)
{
	//no decoration
	// find out if there should be decoration
	// for the moment, lets just assume that if the decoration is described, then it belongs.
	// the following code should be predicated on lodlevel of the tile and the decoration in the future
	const Terrain* thisTerrain = tile->terrain;

	// thisPair.tileDecorationLists.push_back()
	// soon we're going to need a description of all decorations on a given tile, but for now lets assume every tile has one list
	// and it's a list of rocks.
	auto res = Services::GetResourceManager();

	// uh oh
	ResourceHandle rockHandle = res->GetModelHandle("rock.pbm");
	TerrainDecorationList rockList(rockHandle);

	Physics* phys = Services::GetPhysics();

	const size_t gridWidthCount = 30;
	const float sideLength = thisTerrain->CalculateTileTerrainSideLength();
	const float maxHeight = thisTerrain->GetMaxHeight();
	const float maxHeightWithOffset = maxHeight + 20.0f;
	const float minHeight = thisTerrain->GetMinHeight();
	const float minHeightWithOffset = minHeight - 20.0f;
	const float gridSpacing = (sideLength / gridWidthCount);
	const float halfGrid = gridSpacing * 0.5f;
	std::uniform_real_distribution<float> gridDistro(-halfGrid, halfGrid);
	std::uniform_real_distribution<float> rotateDistro(0, XM_2PI);
	std::uniform_real_distribution<float> scaleDistro(4.8f, 12.6f);
	int tX, tZ;
	thisTerrain->GetTilePosition(tX, tZ);
	std::mt19937 rgen((unsigned)thisTerrain->GetMinHeight() + (unsigned)thisTerrain->GetMaxHeight() + tX);
	float terrainXOffset, terrainYOffset, terrainZOffset;
	thisTerrain->GetTerrainPosition(terrainXOffset, terrainYOffset, terrainZOffset);
	//float centerHeight = (thisTerrain->GetMaxHeight() - thisTerrain->GetMinHeight()) * 0.5f + thisTerrain->GetMinHeight();

	//replace this with something that makes sense
	size_t decorationCount = 1;
	for (size_t dI = 0; dI < decorationCount; ++dI)
	{
		// here we have logic for spawning rocks, probably grid based with randomized offsets for even distrobution where each cell has a likelyhood of spawning a decoration
		// based on some external decorationmap, that'll be fun to manage.

		// maybe use (bounding box size) * (some arbitrarliy defined density factor) to determine grid size.
		for (size_t rI = 0; rI < gridWidthCount; rI++)
		{
			const float zPos = rI * gridSpacing + halfGrid + terrainZOffset;
			for (size_t cI = 0; cI < gridWidthCount; cI++)
			{
				const float xPos = cI * gridSpacing + halfGrid + gridDistro(rgen) + terrainXOffset;
				float newZPos = zPos + gridDistro(rgen);
				btVector3 rayStart(xPos, maxHeightWithOffset, newZPos);
				btVector3 rayEnd(xPos, minHeightWithOffset, newZPos);
				btVector3 hitPoint;
				bool hit;
				phys->ClosestCollision(rayStart, rayEnd, hit, hitPoint);
				// probably will never be worth it to hand optimize these
				XMVECTOR xmPos = XMVectorSet(xPos, hitPoint.getY(), newZPos, 1.0f);
				XMMATRIX wMatrix = XMMatrixScalingFromVector(XMVectorReplicate(scaleDistro(rgen)));
				wMatrix = XMMatrixMultiply(wMatrix, XMMatrixRotationRollPitchYaw(rotateDistro(rgen), rotateDistro(rgen), rotateDistro(rgen)));
				wMatrix = XMMatrixMultiply(wMatrix, XMMatrixTranslationFromVector(xmPos));
				rockList.instances.push_back({ wMatrix });
			}
		}
	}


	TileDecorationPair thisPair(tile->terrain);
	thisPair.tileDecorationLists.push_back(rockList);

	// todo: move instead of assign FOR MINIMUM COPYING
	m_TileDecorationPairs.emplace((size_t)tile->terrain, thisPair);
}

void TerrainDecoratorSystem::CleaupOldDecorations()
{
	// here we make sure that every decorationpair and its lists are still relevant.
	// todo: make make sure collector is less agressive than generator so that tiles don't get generated and immediately collected
	for (auto pairIter = m_TileDecorationPairs.begin(); pairIter != m_TileDecorationPairs.end();)
	{
		if ((*pairIter).second.terrainTile->GetLod() > 8)
		{
			pairIter = m_TileDecorationPairs.erase(pairIter);
		}
		else
		{
			++pairIter;
		}
	}
	// want to make sure that we don't have more decorated tiles than we have decoration lists by the end of this function.
	//assert(visibleTiles->size() > m_TileDecorationPairs.size());
}
