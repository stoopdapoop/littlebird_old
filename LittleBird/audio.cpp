#include "audio.h"
#include <string>
#include "memory_utilities.h"
#include "services.h"
#pragma warning(disable:4005)
// lol
#pragma warning(default:4005)
using namespace std;
using namespace DirectX;

#pragma comment(lib, "X3DAudio.lib")

#define NUM_PRESETS 30

// Specify sound cone to add directionality to listener for artistic effect:
// Emitters behind the listener are defined here to be more attenuated,
// have a lower LPF cutoff frequency,
// yet have a slightly higher reverb send level.
static const X3DAUDIO_CONE Listener_DirectionalCone = { X3DAUDIO_PI*5.0f/6.0f, X3DAUDIO_PI*11.0f/6.0f, 1.0f, 0.75f, 0.0f, 0.25f, 0.708f, 1.0f };

// Specify LFE level distance curve such that it rolls off much sooner than
// all non-LFE channels, making use of the subwoofer more dramatic.
static const X3DAUDIO_DISTANCE_CURVE_POINT Emitter_LFE_CurvePoints[3] = { 0.0f, 1.0f, 0.25f, 0.0f, 1.0f, 0.0f };
static const X3DAUDIO_DISTANCE_CURVE       Emitter_LFE_Curve          = { (X3DAUDIO_DISTANCE_CURVE_POINT*)&Emitter_LFE_CurvePoints[0], 3 };

// Specify reverb send level distance curve such that reverb send increases
// slightly with distance before rolling off to silence.
// With the direct channels being increasingly attenuated with distance,
// this has the effect of increasing the reverb-to-direct sound ratio,
// reinforcing the perception of distance.
static const X3DAUDIO_DISTANCE_CURVE_POINT Emitter_Reverb_CurvePoints[3] = { 0.0f, 0.5f, 0.75f, 1.0f, 1.0f, 0.0f };
static const X3DAUDIO_DISTANCE_CURVE       Emitter_Reverb_Curve          = { (X3DAUDIO_DISTANCE_CURVE_POINT*)&Emitter_Reverb_CurvePoints[0], 3 };

XAUDIO2FX_REVERB_I3DL2_PARAMETERS g_PRESET_PARAMS[ NUM_PRESETS ] =
{
	XAUDIO2FX_I3DL2_PRESET_FOREST,
	XAUDIO2FX_I3DL2_PRESET_DEFAULT,
	XAUDIO2FX_I3DL2_PRESET_GENERIC,
	XAUDIO2FX_I3DL2_PRESET_PADDEDCELL,
	XAUDIO2FX_I3DL2_PRESET_ROOM,
	XAUDIO2FX_I3DL2_PRESET_BATHROOM,
	XAUDIO2FX_I3DL2_PRESET_LIVINGROOM,
	XAUDIO2FX_I3DL2_PRESET_STONEROOM,
	XAUDIO2FX_I3DL2_PRESET_AUDITORIUM,
	XAUDIO2FX_I3DL2_PRESET_CONCERTHALL,
	XAUDIO2FX_I3DL2_PRESET_CAVE,
	XAUDIO2FX_I3DL2_PRESET_ARENA,
	XAUDIO2FX_I3DL2_PRESET_HANGAR,
	XAUDIO2FX_I3DL2_PRESET_CARPETEDHALLWAY,
	XAUDIO2FX_I3DL2_PRESET_HALLWAY,
	XAUDIO2FX_I3DL2_PRESET_STONECORRIDOR,
	XAUDIO2FX_I3DL2_PRESET_ALLEY,
	XAUDIO2FX_I3DL2_PRESET_CITY,
	XAUDIO2FX_I3DL2_PRESET_MOUNTAINS,
	XAUDIO2FX_I3DL2_PRESET_QUARRY,
	XAUDIO2FX_I3DL2_PRESET_PLAIN,
	XAUDIO2FX_I3DL2_PRESET_PARKINGLOT,
	XAUDIO2FX_I3DL2_PRESET_SEWERPIPE,
	XAUDIO2FX_I3DL2_PRESET_UNDERWATER,
	XAUDIO2FX_I3DL2_PRESET_SMALLROOM,
	XAUDIO2FX_I3DL2_PRESET_MEDIUMROOM,
	XAUDIO2FX_I3DL2_PRESET_LARGEROOM,
	XAUDIO2FX_I3DL2_PRESET_MEDIUMHALL,
	XAUDIO2FX_I3DL2_PRESET_LARGEHALL,
	XAUDIO2FX_I3DL2_PRESET_PLATE,
};

//class VoiceCallback : public IXAudio2VoiceCallback
//{
//public:
//	IXAudio2SourceVoice* m_Owner;
//	VoiceCallback(IXAudio2SourceVoice* pOwner): m_Owner(pOwner){}
//	~VoiceCallback(){}
//
//	//Called when the voice has just finished playing a contiguous audio stream.
//	void OnStreamEnd() { m_Owner->Stop(); }
//
//	//Unused methods are stubs
//	void OnVoiceProcessingPassEnd() { }
//	void OnVoiceProcessingPassStart(UINT32 SamplesRequired) {    }
//	void OnBufferEnd(void * pBufferContext)    { }
//	void OnBufferStart(void * pBufferContext) {    }
//	void OnLoopEnd(void * pBufferContext) {    }
//	void OnVoiceError(void * pBufferContext, HRESULT Error) { }
//};

#pragma warning(disable:4100)
class VoiceCallback : public IXAudio2VoiceCallback
{
public:
	HANDLE hBufferEndEvent;
	VoiceCallback(): hBufferEndEvent( CreateEvent( NULL, FALSE, FALSE, NULL ) ){}
	~VoiceCallback(){ CloseHandle( hBufferEndEvent ); }

	//Called when the voice has just finished playing a contiguous audio stream.
	void OnStreamEnd() { SetEvent( hBufferEndEvent ); }

	//Unused methods are stubs
	void OnVoiceProcessingPassEnd() { }
	void OnVoiceProcessingPassStart(UINT32 SamplesRequired) {    }
	void OnBufferEnd(void * pBufferContext)    { }
	void OnBufferStart(void * pBufferContext) {    }
	void OnLoopEnd(void * pBufferContext) {    }
	void OnVoiceError(void * pBufferContext, HRESULT Error) { }
};
#pragma warning(default:4100)

Audio::Audio():  m_XAudio2(0), m_MasteringVoice(0), m_SourceVoice(0), m_SubmixVoice(0), m_ReverbEffect(0), m_SampleData(0), m_MasterVolume(1.0f),
	m_VoiceCallback(0)
{
	using std::string;

	ZeroMemory(&m_EmitterAzimuths, sizeof(float)*m_MaxInputChannels);

	// Clear struct
	//
	// Initialize XAudio2
	//
	CoInitializeEx( NULL, COINIT_MULTITHREADED );

	UINT32 flags = 0;
#ifdef _DEBUG
	flags |= XAUDIO2_DEBUG_ENGINE;
#endif

	if( FAILED(XAudio2Create( &m_XAudio2, flags ) ) )
		throw string("could not create XAudio");
	//
	// Create a mastering voice
	//
	if( FAILED( m_XAudio2->CreateMasteringVoice( &m_MasteringVoice ) ) )
	{
		SafeRelease( m_XAudio2 );
		throw string ("could not create masteringVoice");
	}

	// Check device details to make sure it's within our sample supported parameters
	XAUDIO2_DEVICE_DETAILS details;
	if( FAILED( m_XAudio2->GetDeviceDetails( 0, &details ) ) )
	{
		SafeRelease( m_XAudio2 );
		throw string ("audio device not supported");
	}

	if( details.OutputFormat.Format.nChannels > m_MaxOutputChannels )
	{
		SafeRelease( m_XAudio2 );
		throw string ("could not create output channels");
	}

	m_ChannelMask = details.OutputFormat.dwChannelMask;
	m_Channels = details.OutputFormat.Format.nChannels;


	flags = 0;
#ifdef _DEBUG
	flags |= XAUDIO2FX_DEBUG;
#endif

	if( FAILED(XAudio2CreateReverb( &m_ReverbEffect, flags ) ) )
	{
		SafeRelease( m_XAudio2 );
		throw string(__LINE__ + __FILE__);
	}

	//
	// Create a submix voice
	//

	// Performance tip: you need not run global FX with the sample number
	// of channels as the final mix.  For example, this sample runs
	// the reverb in mono mode, thus reducing CPU overhead.
	XAUDIO2_EFFECT_DESCRIPTOR effects[] = { { m_ReverbEffect, TRUE, 1 } };
	XAUDIO2_EFFECT_CHAIN effectChain = { 1, effects };

	if( FAILED( m_XAudio2->CreateSubmixVoice( &m_SubmixVoice, 1,
		details.OutputFormat.Format.nSamplesPerSec, 0, 0,
		NULL, &effectChain ) ) )
	{
		SafeRelease( m_XAudio2 );
		SafeRelease( m_ReverbEffect );
		throw string(__LINE__ + __FILE__);
	}

	// Set default FX params
	XAUDIO2FX_REVERB_PARAMETERS native;
	ReverbConvertI3DL2ToNative( &g_PRESET_PARAMS[0], &native );
	m_SubmixVoice->SetEffectParameters( 0, &native, sizeof( native ) );


	//
	// Initialize X3DAudio
	//  Speaker geometry configuration on the final mix, specifies assignment of channels
	//  to speaker positions, defined as per WAVEFORMATEXTENSIBLE.dwChannelMask
	//
	//  SpeedOfSound - speed of sound in user-defined world units/second, used
	//  only for doppler calculations, it must be >= FLT_MIN
	//
	const float SPEEDOFSOUND = X3DAUDIO_SPEED_OF_SOUND;

	X3DAudioInitialize( details.OutputFormat.dwChannelMask, SPEEDOFSOUND, m_X3DInstance );

	m_ListenerPos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	m_EmitterPos = D3DXVECTOR3( 1.0f, 1.0f, 1.0f);

	m_ListenerAngle = 0.0f;
	m_UseListenerCone = TRUE;
	m_UseInnerRadius = TRUE;
	m_UseRedirectToLFE = ((details.OutputFormat.dwChannelMask & SPEAKER_LOW_FREQUENCY) != 0);

	//
	// Setup 3D audio structs
	//
	m_Listener.Position = D3DXVECTOR3(m_ListenerPos.x, m_ListenerPos.y, m_ListenerPos.z);

	m_Listener.OrientFront = D3DXVECTOR3( 0, 0, 1 );
	m_Listener.OrientTop = D3DXVECTOR3( 0, 1, 0 );
	m_Listener.pCone = (X3DAUDIO_CONE*)&Listener_DirectionalCone;

	ZeroMemory(&m_Emitter, sizeof(m_Emitter));
	m_Emitter.pCone = &m_EmitterCone;
	m_Emitter.pCone->InnerAngle = 0.0f;
	// Setting the inner cone angles to X3DAUDIO_2PI and
	// outer cone other than 0 causes
	// the emitter to act like a point emitter using the
	// INNER cone settings only.
	m_Emitter.pCone->OuterAngle = 0.0f;
	// Setting the outer cone angles to zero causes
	// the emitter to act like a point emitter using the
	// OUTER cone settings only.
	m_Emitter.pCone->InnerVolume = 0.0f;
	m_Emitter.pCone->OuterVolume = 1.0f;
	m_Emitter.pCone->InnerLPF = 0.0f;
	m_Emitter.pCone->OuterLPF = 1.0f;
	m_Emitter.pCone->InnerReverb = 0.0f;
	m_Emitter.pCone->OuterReverb = 1.0f;

	m_Emitter.Position = m_EmitterPos;
	m_Emitter.OrientFront = D3DXVECTOR3( 0, 0, 1 );
	m_Emitter.OrientTop = D3DXVECTOR3( 0, 1, 0 );
	m_Emitter.ChannelCount = m_MaxInputChannels;
	m_Emitter.ChannelRadius = 1.0f;
	m_Emitter.pChannelAzimuths = m_EmitterAzimuths;

	// Use of Inner radius allows for smoother transitions as
	// a sound travels directly through, above, or below the listener.
	// It also may be used to give elevation cues.
	m_Emitter.InnerRadius = 2.0f;
	m_Emitter.InnerRadiusAngle = X3DAUDIO_PI/4.0f;;

	m_Emitter.pVolumeCurve = (X3DAUDIO_DISTANCE_CURVE*)&X3DAudioDefault_LinearCurve;
	m_Emitter.pLFECurve    = (X3DAUDIO_DISTANCE_CURVE*)&Emitter_LFE_Curve;
	m_Emitter.pLPFDirectCurve = NULL; // use default curve
	m_Emitter.pLPFReverbCurve = NULL; // use default curve
	m_Emitter.pReverbCurve    = (X3DAUDIO_DISTANCE_CURVE*)&Emitter_Reverb_Curve;
	m_Emitter.CurveDistanceScaler = 14.0f;
	m_Emitter.DopplerScaler = 1.0f;

	ZeroMemory(&m_DSPSettings, sizeof(m_DSPSettings));
	m_DSPSettings.SrcChannelCount = m_MaxInputChannels;
	m_DSPSettings.DstChannelCount = m_Channels;
	std::fill_n(m_MatrixCoefficients,m_MaxInputChannels * m_MaxOutputChannels, 0.0f);
	m_DSPSettings.pMatrixCoefficients = m_MatrixCoefficients;


	//
	// Done
	//

	FILE *fp = 0;

	errno_t err = fopen_s(&fp ,"../Assets/Audio/chime.wav","rb");
	if(!fp || err)
	{
		throw string("Audiofile Not found error number " + std::to_string(err));
	}

	char id[5]; //four bytes to hold 'RIFF'
	id[4] = '\0';
	DWORD size = 0; //32 bit value to hold file size
	DWORD format_length, data_size;
	WAVEFORMATEX wfx;
	
	fread_s(id,sizeof(BYTE)*4,sizeof(BYTE), 4, fp); //read in first four bytes
	if (strcmp(id,"RIFF"))
		throw string("File is not a proper Riff");
	
	//we had 'RIFF' let's continue
	fread_s(&size,sizeof(DWORD),sizeof(DWORD), 1, fp);

	fread_s(id,sizeof(BYTE)*4, sizeof(BYTE), 4, fp);
	if (strcmp(id,"WAVE"))
		throw string("File is not a proper WAVE");
	
	fread_s(id,sizeof(BYTE)*4, sizeof(BYTE),4,fp);
	if (strcmp(id,"fmt "))
		throw string("File does not have proper fmt ");

	size_t formatsize = sizeof(wfx);
	fread_s(&format_length, sizeof(DWORD), sizeof(DWORD),1,fp);
	fread_s(&wfx, formatsize, format_length,1,fp);
	if(wfx.nChannels != 1)
		throw string("Audio needs to be mono, yo");

	fread_s(id, sizeof(BYTE)*4, sizeof(BYTE), 4, fp);
	if (strcmp(id,"data"))
		throw string("expected 'data'");
	fread_s(&data_size, sizeof(DWORD), sizeof(DWORD), 1, fp); //how many bytes of sound data we have
	m_SampleData =  new BYTE[data_size];
	fread_s(m_SampleData, sizeof(BYTE)*data_size, sizeof(BYTE), data_size, fp); //read in our whole sound data chunk


	//
	// Play the wave using a source voice that sends to both the submix and mastering voices
	//
	XAUDIO2_SEND_DESCRIPTOR sendDescriptors[2];
	sendDescriptors[0].Flags = XAUDIO2_SEND_USEFILTER; // LPF direct-path
	sendDescriptors[0].pOutputVoice = m_MasteringVoice;
	sendDescriptors[1].Flags = XAUDIO2_SEND_USEFILTER; // LPF reverb-path -- omit for better performance at the cost of less realistic occlusion
	sendDescriptors[1].pOutputVoice = m_SubmixVoice;
	const XAUDIO2_VOICE_SENDS sendList = { 2, sendDescriptors };

	m_VoiceCallback = new VoiceCallback();
	// create the source voice
	if(FAILED( m_XAudio2->CreateSourceVoice( &m_SourceVoice, &wfx, 0,
		2.0f, m_VoiceCallback, &sendList ) ) )
	{
		throw std::string("bad sourcevoice");
	}


	// Submit the wave sample data using an XAUDIO2_BUFFER structure
	m_Buffer = new XAUDIO2_BUFFER();
	m_Buffer->pAudioData = m_SampleData;
	m_Buffer->Flags = XAUDIO2_END_OF_STREAM;
	m_Buffer->AudioBytes = data_size;
	m_Buffer->LoopCount = XAUDIO2_NO_LOOP_REGION;

	//if(FAILED(m_SourceVoice->SubmitSourceBuffer( m_Buffer )))
		//throw std::string("bad thing");

	if(FAILED(m_SourceVoice->Start( 0 )))
		throw std::string("bad badstart");


	m_FrameToApply3DAudio = 0;

	CreateVolumeMenu();

}


Audio::~Audio()
{
	m_XAudio2->StopEngine();
	SafeRelease(m_XAudio2);
	SafeDelete(m_SampleData);
	SafeDelete(m_Buffer);
	SafeDelete(m_VoiceCallback);
}


void Audio::Update( float elapsedTime )
{
	

	if( m_FrameToApply3DAudio == 0 )
	{
		//m_EmitterPos = D3DXVECTOR3( bad * 5.0f, 0.1f, bad2 * 5.0f);

		// Calculate listener orientation in x-z plane
		if( m_ListenerPos.x != m_Listener.Position.x
			|| m_ListenerPos.z != m_Listener.Position.z )
		{
			D3DXVECTOR3 vDelta = m_ListenerPos - m_Listener.Position;

			m_ListenerAngle = float( atan2( vDelta.x, vDelta.z ) );

			vDelta.y = 0.0f;
			D3DXVec3Normalize( &vDelta, &vDelta );

			m_Listener.OrientFront.x = vDelta.x;
			m_Listener.OrientFront.y = 0.f;
			m_Listener.OrientFront.z = vDelta.z;
		}

		if (m_UseListenerCone)
		{
			m_Listener.pCone = (X3DAUDIO_CONE*)&Listener_DirectionalCone;
		}
		else
		{
			m_Listener.pCone = NULL;
		}
		if (m_UseInnerRadius)
		{
			m_Emitter.InnerRadius = 2.0f;
			m_Emitter.InnerRadiusAngle = X3DAUDIO_PI/4.0f;
		}
		else
		{
			m_Emitter.InnerRadius = 0.0f;
			m_Emitter.InnerRadiusAngle = 0.0f;
		}

		if( elapsedTime > 0 )
		{
			D3DXVECTOR3 lVelocity = ( m_ListenerPos - m_Listener.Position ) / elapsedTime;
			m_Listener.Position = m_ListenerPos;
			m_Listener.Velocity = lVelocity;

			D3DXVECTOR3 eVelocity = ( m_EmitterPos - m_Emitter.Position ) / elapsedTime;
			m_Emitter.Position = m_EmitterPos;
			m_Emitter.Velocity = eVelocity;
		}

		DWORD dwCalcFlags = X3DAUDIO_CALCULATE_MATRIX | X3DAUDIO_CALCULATE_DOPPLER
			| X3DAUDIO_CALCULATE_LPF_DIRECT | X3DAUDIO_CALCULATE_LPF_REVERB
			| X3DAUDIO_CALCULATE_REVERB;
		if (m_UseRedirectToLFE)
		{
			// On devices with an LFE channel, allow the mono source data
			// to be routed to the LFE destination channel.
			dwCalcFlags |= X3DAUDIO_CALCULATE_REDIRECT_TO_LFE;
		}

		X3DAudioCalculate( m_X3DInstance, &m_Listener, &m_Emitter, dwCalcFlags,
			&m_DSPSettings );


		IXAudio2SourceVoice* voice = m_SourceVoice;
		if( voice )
		{
			// Apply X3DAudio generated DSP settings to XAudio2
			voice->SetFrequencyRatio( m_DSPSettings.DopplerFactor );
			//voice->SetFrequencyRatio(0.7);
			voice->SetOutputMatrix( m_MasteringVoice, m_MaxInputChannels, m_Channels,
				m_MatrixCoefficients );

			voice->SetOutputMatrix(m_SubmixVoice, 1, 1, &m_DSPSettings.ReverbLevel);

			XAUDIO2_FILTER_PARAMETERS FilterParametersDirect = { LowPassFilter, 2.0f * sinf(X3DAUDIO_PI/6.0f * m_DSPSettings.LPFDirectCoefficient), 1.0f }; // see XAudio2CutoffFrequencyToRadians() in XAudio2.h for more information on the formula used here
			voice->SetOutputFilterParameters(m_MasteringVoice, &FilterParametersDirect);
			XAUDIO2_FILTER_PARAMETERS FilterParametersReverb = { LowPassFilter, 2.0f * sinf(X3DAUDIO_PI/6.0f * m_DSPSettings.LPFReverbCoefficient), 1.0f }; // see XAudio2CutoffFrequencyToRadians() in XAudio2.h for more information on the formula used here
			voice->SetOutputFilterParameters(m_SubmixVoice, &FilterParametersReverb);
		}
	}

	if(WaitForSingleObjectEx( m_VoiceCallback->hBufferEndEvent, 0, false ) != WAIT_TIMEOUT)
		m_SourceVoice->Stop();

	//if(change)
	//{
	//	XAUDIO2FX_REVERB_PARAMETERS native;
	//	if(++currentnum >= NUM_PRESETS)
	//		currentnum = 0;
	//	ReverbConvertI3DL2ToNative( &g_PRESET_PARAMS[currentnum], &native );
	//	m_SubmixVoice->SetEffectParameters( 0, &native, sizeof( native ) );
	//	change = false;
	//}

	//previous condition is only true every nth frame. I think n needs to be (2^n)-1, but I don't want to think too hard
	m_FrameToApply3DAudio++;
	m_FrameToApply3DAudio &= 1;
}

void Audio::SetVolume( float volume )
{
	m_MasterVolume = volume;
	m_MasteringVoice->SetVolume(volume);
}

void Audio::CreateVolumeMenu()
{
	DebugUI* deb = Services::GetDebugUI();
	string name = "SoundMenu";
	deb->CreateMenu(name);
	deb->AddReadOnlyVariable(name, "Volume", DEBUG_UI_TYPE::FLOAT, &m_MasterVolume);
	deb->AddReadOnlyVariable(name, "SoundPosition", DEBUG_UI_TYPE::DIR3F, &m_EmitterPos);
	deb->SetMenuSize(name, 80, 50);
	deb->SetMenuRefreshRate(name, 0.3f);
}

void Audio::PlaySound()
{
	/*if(FAILED(m_SourceVoice->Start( 0 )))
	throw std::string("bad badstart");*/
	m_SourceVoice->Stop();
	m_SourceVoice->FlushSourceBuffers();
	
	if(FAILED(m_SourceVoice->SubmitSourceBuffer( m_Buffer )))
		throw std::string("bad thing");

	m_SourceVoice->Start();
}
