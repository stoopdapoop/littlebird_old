#ifndef _AUDIO_H_
#define _AUDIO_H_

#include <xaudio2.h>
#include <xaudio2fx.h>
#include <x3daudio.h>
#pragma warning(disable:4005)
#include <DirectXMath.h>
#include <D3DX10math.h>
#pragma warning(default:4005)

struct D3DXVECTOR3;
class VoiceCallback;

class Audio
{
public:
	Audio();
	~Audio();
	void Update( float elapsedTime);
	void SetVolume(float volume);
	void PlaySound();
private:

	void CreateVolumeMenu();

private:
	static const int m_MaxOutputChannels = 8;
	static const int m_MaxInputChannels = 1;

	IXAudio2* m_XAudio2;
	IXAudio2MasteringVoice* m_MasteringVoice;
	IXAudio2SourceVoice* m_SourceVoice;
	VoiceCallback* m_VoiceCallback;
	IXAudio2SubmixVoice* m_SubmixVoice;
	IUnknown* m_ReverbEffect;
	BYTE* m_SampleData;
	XAUDIO2_BUFFER* m_Buffer;

	X3DAUDIO_HANDLE m_X3DInstance;
	int m_FrameToApply3DAudio;

	DWORD m_ChannelMask;
	unsigned __int32 m_Channels;

	X3DAUDIO_DSP_SETTINGS m_DSPSettings;
	X3DAUDIO_LISTENER m_Listener;
	X3DAUDIO_EMITTER m_Emitter;
	X3DAUDIO_CONE m_EmitterCone;

	D3DXVECTOR3 m_ListenerPos;
	D3DXVECTOR3 m_EmitterPos;
	float m_ListenerAngle;
	bool  m_UseListenerCone;
	bool  m_UseInnerRadius;
	bool  m_UseRedirectToLFE;

	FLOAT32 m_EmitterAzimuths[m_MaxInputChannels];
	FLOAT32 m_MatrixCoefficients[m_MaxInputChannels * m_MaxOutputChannels];

	float m_MasterVolume;
};

#endif