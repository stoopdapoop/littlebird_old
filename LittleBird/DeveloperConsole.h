#ifndef _DEVELOPER_CONSOLE_H_
#define _DEVELOPER_CONSOLE_H_

#include <deque>
#include <functional>
#include <unordered_map>
#include <string>
#include <DirectXColors.h>

class Renderer;
namespace DirectX
{
	class SpriteBatch;
	class SpriteFont;
};


struct DeveloperConsoleLogEntry
{
	DeveloperConsoleLogEntry(std::wstring txt, DirectX::XMVECTORF32 clr = DirectX::Colors::SandyBrown): text(txt), color(clr) {}
	std::wstring text;
	DirectX::XMVECTORF32 color;
};

#define WIDEN(x) L ## x
#define DECLARE_CONSOLECOMMAND( name , mthd ) Services::GetDeveloperConsole()->RegisterCommand(WIDEN( name ), bind(& ## mthd, this, placeholders::_1))



class DeveloperConsole
{
public:
	DeveloperConsole(Renderer* rend, int screenWidth, int screenHeight);
	~DeveloperConsole();
	
	void Display() const;

	bool IsEnabled() const { return m_Enabled; } 

	void SetEnabled(bool enabled) { m_Enabled = enabled; }
	void ToggleEnabled() { m_Enabled = !m_Enabled; }

	void AddToInputBuffer(std::wstring input);
	void SubmitInputBuffer();

	void WriteToConsole(std::wstring str, DirectX::XMVECTORF32 color = DirectX::Colors::SandyBrown);

	// returned value will be printed to console
	void RegisterCommand(std::wstring command, std::function<std::wstring(std::wstring arg)> functionCallback);

	void ShowPreviousCommand();

private:
	int GetVisibleLineCount() const;
	int GetConsoleHeight() const;
	std::wstring WriteCommand(std::wstring str);
	
private:
	bool m_Enabled;
	std::unordered_map<std::wstring, std::function<std::wstring(std::wstring)>> m_Commands;
	std::deque<DeveloperConsoleLogEntry> m_Log;
	std::vector<DeveloperConsoleLogEntry> m_InputLog;
	std::wstring m_InputBuffer;
	int m_ScreenWidth, m_ScreenHeight, m_ConsoleHeight;
	int m_LookBackCount;
	DirectX::SpriteBatch* m_SpriteBatch;
	DirectX::SpriteFont* m_SpriteFont;
};

#endif
