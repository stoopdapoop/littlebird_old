
#include "PlayerComponent.h"
#include "services.h"
#include "BulletCollision/CollisionDispatch/btGhostObject.h"


PlayerComponent::PlayerComponent( float moveSpeed, float accel, float viewHeight, DirectX::XMVECTOR startPos, char replicationFlags): MaxMoveSpeed(moveSpeed), Accelleration(accel), CurrentVelocity(),
	ViewHeight(viewHeight), Component(ComponentID::Player, replicationFlags)
{
	Physics* phys = Services::GetPhysics();
	ResourceManager* res = Services::GetResourceManager();
	ResourceHandle hand = res->GetCollisionShapeHandle(CAPSULE, 1.0f, ViewHeight );
	btConvexShape* shape = (btConvexShape*)res->GetCollisionShape(hand);
	if( shape == nullptr)
		throw string("player shape is not derived from btConvexShape");

	btTransform startTransform;
	startTransform.setIdentity ();
	startTransform.setOrigin(*(btVector3*)&startPos);
	btPairCachingGhostObject* spooky = new btPairCachingGhostObject();
	spooky->setWorldTransform(startTransform);

	phys->GetWorld()->getBroadphase()->getOverlappingPairCache()->setInternalGhostPairCallback(new btGhostPairCallback());
	spooky->setCollisionShape(shape);
	spooky->setCollisionFlags (btCollisionObject::CF_CHARACTER_OBJECT);

	kinematicController = new btKinematicCharacterController(spooky, shape, ViewHeight * 0.1f);

	phys->GetWorld()->addCollisionObject(spooky,btBroadphaseProxy::CharacterFilter, btBroadphaseProxy::StaticFilter|btBroadphaseProxy::DefaultFilter);

	phys->GetWorld()->addAction(kinematicController);
}

void PlayerComponent::NetworkSerialize( RakNet::BitStream& bs )
{
}

void PlayerComponent::NetworkDeserialize( RakNet::BitStream& bs )
{
}


