#ifndef _WORLD_QUERY_H_
#define _WORLD_QUERY_H_

#include <vector>

class Terrain;
class Entity;

class WorldQuery
{
public:
	WorldQuery(std::vector<Entity*>* entList);
	~WorldQuery();

	void AddEntity(Entity* ent);
	bool RemoveEntity(Entity* ent);

private:
	std::vector<Entity*>* m_EntityList;
};

#endif