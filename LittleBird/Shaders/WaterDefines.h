
#ifndef _WATER_DEFINES_H_
#define _WATER_DEFINES_H_

#include "terrainConstants.h"

cbuffer WaterBuffer : register (b4)
{
	float AmpFactor;
	float BaseFactor;
	float WaveTimeScaler;
	float WaterFadeFactor;
	float2 BigWaveScrollDirection;
	float2 SmallWaveScrollDirection;
	float4 DeepColor;
	float4 ShallowColor;
};

#endif