#include "GBufferSharedStructures.h"

Texture2D shaderTexture : register(t0);
Texture2D normalTexture : register(t1);
Texture2D smoothnessTexture : register(t2);
Texture2D specTexture : register(t3);

MRT_Out main(GBufferPixelInputType input)
{
	MRT_Out outColor;
	float3 diffuseAlbedo;
	diffuseAlbedo = shaderTexture.Sample(regularSampler, input.tex).rgb;
	outColor.diffuse = float4(diffuseAlbedo, 1.0f);

	float3 normal = normalize(input.normal);
	float3 tangent = normalize(input.tangent);
	float3 bitangent = normalize(input.bitangent);
	float3x3 tbn = float3x3(tangent, bitangent, normal);
	float3 texnormal;
	texnormal = float3(0.0, 0.0, 1.0);
	texnormal = normalTexture.Sample(normalSampler, input.tex).xyz * 2.0f - 1.0f;
	normal = normalize(mul(texnormal, tbn));
	float roughTex = smoothnessTexture.Sample(regularSampler, input.tex).r;
	outColor.normal_rough = float4(EncodeSphereMap(normal), roughTex, 0.0f);
	outColor.spec = float4((float3)0.08f, 0.0f);

	return outColor;
}