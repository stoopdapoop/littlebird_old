
#ifndef _VERTEX_INPUT_TYPES_H_
#define _VERTEX_INPUT_TYPES_H_

struct VertexInputType
{
	float4 position : POSITION;
	float3 normal : NORMAL;
	float4 tangent : TANGENT;
	float4 binormal : BINORMAL;
	float2 tex : TEXCOORD0;

};

struct InstanceVertexInputType
{
	float4 position : POSITION;
	float3 normal : NORMAL;
	float4 tangent : TANGENT;
	float4 binormal : BINORMAL;
	float2 tex : TEXCOORD0;
	float4x4 worldMatrix : WORLDMATRIX;
};

struct TerrainVertexInputType
{
	float4 xy_tex : TEXCOORD0;
};

struct WaterVertexInputType
{
	float2 xy : TEXCOORD0;
};

#endif