
#include "shared_shader_structures.h"

SkySpherePixelInputType main( VertexInputType input)
{
	SkySpherePixelInputType output;
	output.wPosition = input.position.xyz;
	input.position.w = 1.0f;
	output.position = mul(input.position, worldViewProjectionMatrix);
	

	return output;
}