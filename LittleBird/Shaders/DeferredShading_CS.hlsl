
#include "GBufferSharedStructures.h"
#include "SharedShaderDefines.h"

cbuffer SkyBuffer : register(b4)
{
	float RayleighFactor;
	float MieFactor;
	float SurfaceHeight;
	float SpotBrightness;
	uint StepCount;
	float ScatterStrength;
	float RayleighScatterStrength;
	float MieScatterStrength;
	float RayLeighCollectionPower;
	float MieCollectionPower;
};

RWTexture2D<float4> gFramebuffer : register(u0);

Texture2D shadowDepthTex : register(t4);

// the sky stuff is mostly from http://codeflow.org/entries/2011/apr/13/advanced-webgl-part-2-sky-rendering/

float SkyPhase(float alpha, float g){
	float a = 3.0*(1.0 - g*g);
	float b = 2.0*(2.0 + g*g);
	float c = 1.0 + alpha*alpha;
	float d = pow(1.0 + g*g - 2.0*g*alpha, 1.5);
	return (a / b) * (c / d);
}

float AtmosphericDepth(float3 position, float3 dir){
	float a = dot(dir, dir);
	float b = 2.0*dot(dir, position);
	float c = dot(position, position) - 1.0;
	float det = b*b - 4.0*a*c;
	float detSqrt = sqrt(det);
	float q = (-b - detSqrt) / 2.0;
	float t1 = c / q;
	return t1;
}

float HorizonExtinction(float3 position, float3 dir, float radius){
	float u = dot(dir, -position);
	if (u < 0.0){
		return 1.0;
	}
	float3 near = position + u*dir;
		if (length(near) < radius){
		return 0.0;
		}
		else{
			float3 v2 = normalize(near)*radius - position;
			float diff = acos(dot(normalize(v2), dir));
			return smoothstep(0.0, 1.0, pow(diff*2.0, 3.0));
		}
}
static float3 Kr = float3(0.18867780436772762f, 0.4978442963618773f, 0.6616065586417131f);
float3 Absorb(float dist, float3 color, float factor){
	return color - color*pow(Kr, (factor / dist));
}

[numthreads(COMPUTE_SHADER_TILE_GROUP_DIM, COMPUTE_SHADER_TILE_GROUP_DIM, 1)]
void main(uint3 groupId				: SV_GroupID,
	uint3 dispatchThreadId : SV_DispatchThreadID,
	uint groupIndex : SV_GroupIndex)
{
	uint2 globalCoords = dispatchThreadId.xy;


	SurfaceData surfaceSample;
	surfaceSample = ComputeSurfaceDataFromGBufferSample(globalCoords);
	// this branch seems to add 1/10th of a ms on my GTX480 in the worst case, ends up saving a ton of work (almost 8/10ths of a ms) when there's a lot of sky on the screen.
	// todo: make sure this still saves performance when there are trees implemented
	[branch]if (gGBufferTextures[3].Load(uint3(globalCoords.xy, 0)).x == 1.0f)
	{
		//float3 Kr = pow(float3( 0.18867780436772762f, 0.4978442963618773f, 0.6616065586417131f), 2.2);
		// don't get rid of this normalize, it breaks alpha calculation for sky
		float3 viewEyeRay = normalize(surfaceSample.viewPosition);
		//float3 worldEyeRay = mul(viewEyeRay, (float3x3)cameraWorldMatrix);
		//float3 worldLightDirection = mul(lightDirection.xyz, (float3x3)cameraWorldMatrix);
		//float alpha = dot(viewEyeRay, lightDirection.xyz);
		//float rayleighFactor = SkyPhase(alpha, RayleighFactor) * 3;
		//float mieFactor = SkyPhase(alpha, MieFactor);
		//float spot = smoothstep(0.0, 15.0, SkyPhase(alpha, 0.9995))*SpotBrightness;
		//float surfaceHeight = SurfaceHeight;
		//float3 eye_position = float3(0.0f, surfaceHeight, 0.0f);
		//float eye_depth = AtmosphericDepth(eye_position, worldEyeRay);
		//uint stepCount = StepCount;
		//float step_length = eye_depth / float(stepCount);
		//float eye_extinction = HorizonExtinction(eye_position, worldEyeRay, surfaceHeight - 0.1);
		//float3 rayleigh_collected = 0.0f;
		//float3 mie_collected = 0.0f;
		//	for (int i = 0; i < stepCount; i++)
		//	{
		//		float sample_distance = step_length*float(i);
		//		float3 position = eye_position + worldEyeRay*sample_distance;
		//		float extinction = HorizonExtinction(position, worldLightDirection, surfaceHeight - 0.35);
		//		float sample_depth = AtmosphericDepth(position, worldLightDirection);

		//		float3 influx = Absorb(sample_depth, (float3)1, ScatterStrength)*extinction;
		//		rayleigh_collected += Absorb(sample_distance, Kr*influx, RayleighScatterStrength);
		//		mie_collected += Absorb(sample_distance, influx, MieScatterStrength);
		//	}
		//rayleigh_collected = (rayleigh_collected * eye_extinction * pow(eye_depth, RayLeighCollectionPower)) / float(stepCount);
		//mie_collected = (mie_collected * eye_extinction * pow(eye_depth, MieCollectionPower)) / float(stepCount);
		//float3 skyColor = float3(spot*mie_collected + mieFactor*mie_collected + rayleighFactor*rayleigh_collected);

		////skyColor *=
		//skyColor = skyColor / (skyColor + 1);
		//gFramebuffer[globalCoords] = float4(pow(skyColor, 0.45f), 1.0f);
		float alpha = pow(saturate(dot(viewEyeRay, lightDirection.xyz)), 90.0f);
		gFramebuffer[globalCoords] = lerp(float4(0.18867780436772762f, 0.4978442963618773f, 0.6616065586417131f, 1.0f), (float4)1.0f, alpha);
		//gFramebuffer[globalCoords] = float4(0.18867780436772762f, 0.4978442963618773f, 0.6616065586417131f, 1.0f);
		//gFramebuffer[globalCoords] = (float4)0.0f;
		return;
	}


	float3 viewEyeRay = normalize(surfaceSample.viewPosition);
	float3 worldEyeRay = mul(viewEyeRay, (float3x3)cameraWorldMatrix);
	float3 viewReflection = reflect(viewEyeRay, surfaceSample.normal);
	float3 worldReflection = mul(viewReflection, (float3x3)cameraWorldMatrix);
	float roughTex = pow((1.0f-surfaceSample.specularRoughness) * 10.0f, 5.0f);
	// todo: replace constant with getdimensions-miplevels
	float envLod = pow(roughTex, 0.1f)*7.0f;
	float3 envColor = reflectTex.SampleLevel(cubeSampler, worldReflection, envLod).rgb * 0.05;

	float3 diffuseAlbedo;
	float3 diffuseAccum = 0.0f;
	float3 specularAccum = 0.0f;
	diffuseAlbedo = surfaceSample.diffuseAlbedo.xyz;

	//float3 lightPos = float3(lightPosition.x*2.0f, lightPosition.y*5.0f, lightPosition.z*50.0f);

	//float3 lightDir = (lightPos - surfaceSample.viewPosition);
	//float lightDist = length(lightDir);
	//float3 lightAtten = 1 / (lightDist*lightDist);
	//lightAtten *= exp2(diffuseColor.xyz*10.0f);
	//lightDir /= lightDist;
	//diffuseAccum = max(0.0f, dot(surfaceSample.normal, lightDir));
	//diffuseAccum *= lightAtten;

	// do something about these constants

	/////////////////////////////// shadows

	float4 shadowPos = float4(surfaceSample.viewPosition, 1.0f);
	shadowPos = mul(shadowPos, cameraWorldMatrix);
	//doubt if it'll be worth it to factor this out, especially after adding other shadow cascades
	shadowPos = mul(shadowPos, DirectionalMatrix);
	float normalizedShadowDepth = (shadowPos.z - SHADOW_NEAR_CLIP) / ((SHADOW_FAR_CLIP - SHADOW_NEAR_CLIP));

	float2 ProjectedTexCoords;
	ProjectedTexCoords[0] = shadowPos.x / orthoExtents.x / 2.0f + 0.5f;
	ProjectedTexCoords[1] = -shadowPos.y / orthoExtents.y / 2.0f + 0.5f;
	//float shadowSample = shadowDepthTex.SampleLevel(shadowSampler, ProjectedTexCoords, 0).r;
	float4 shadowSamples;
	// I feel like this is what unrolled loops are for...
	shadowSamples = shadowDepthTex.GatherCmp(shadowSampler, ProjectedTexCoords, normalizedShadowDepth + coefs.w, uint2(-1, -1));
	float lightfactor = dot(shadowSamples, (float4)0.0652);
	shadowSamples = shadowDepthTex.GatherCmp(shadowSampler, ProjectedTexCoords, normalizedShadowDepth + coefs.w, uint2( 1, -1));
	lightfactor += dot(shadowSamples, (float4)0.0652);
	shadowSamples = shadowDepthTex.GatherCmp(shadowSampler, ProjectedTexCoords, normalizedShadowDepth + coefs.w, uint2(-1,  1));
	lightfactor += dot(shadowSamples, (float4)0.0652);
	shadowSamples = shadowDepthTex.GatherCmp(shadowSampler, ProjectedTexCoords, normalizedShadowDepth + coefs.w, uint2( 1,  1));
	lightfactor += dot(shadowSamples, (float4)0.0652);
		///////////////////////////////
	float dlightDotN = saturate(dot(normalize(lightDirection.xyz), surfaceSample.normal));
	
	if (normalizedShadowDepth > 1.0f)
		lightfactor = 1.0f;
	{
		diffuseAccum += saturate(dlightDotN) * lightfactor;
		float3 viewHalfVec = normalize(lightDirection.xyz - viewEyeRay.xyz);
		float NdotH = saturate(dot(surfaceSample.normal, viewHalfVec));
		specularAccum += pow(saturate(NdotH), (roughTex + 2.0f) / 8.0f) * surfaceSample.specularAlbedo * lightfactor;
	}
	diffuseAccum += GetAmbient(surfaceSample.normal);

	diffuseAccum *= diffuseAlbedo;
	specularAccum += envColor * surfaceSample.specularAlbedo;

	float exposure = 2.9f;
	float3 finalColor = diffuseAccum + specularAccum;
	finalColor *= exposure;
	finalColor = finalColor / (finalColor + 1);
	const float3 lumVec = float3(0.2126, 0.7152, 0.0722);
	float colorLum = dot(lumVec, finalColor);
	colorLum = colorLum / (colorLum + 1);

	finalColor = pow(finalColor, 0.45f);

	gFramebuffer[globalCoords] = float4(finalColor, 1.0f);
}