#include "GBufferSharedStructures.h"

GBufferPixelInputType main(InstanceVertexInputType input)
{
	GBufferPixelInputType output;

	input.position.w = 1.0f;

	matrix mvp = mul(input.worldMatrix, viewProjectionMatrix);

	output.position = mul(input.position, mvp);

	// todo: ask for the lord's forgiveness
	float3x3 inverseCameraWorld = mul((float3x3)input.worldMatrix,(float3x3)viewMatrix);

	output.normal = mul(input.normal, (float3x3)inverseCameraWorld);

	output.tangent.xyz = mul(input.tangent.xyz * input.tangent.w, (float3x3)inverseCameraWorld);

	output.bitangent = mul(input.binormal.xyz * input.binormal.w, (float3x3)inverseCameraWorld);

	output.tex = input.tex;

	return output;
}