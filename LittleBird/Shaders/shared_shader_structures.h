
#ifndef _SHARED_SHADER_STRUCTURES_H_
#define _SHARED_SHADER_STRUCTURES_H_

#include "VertexInputTypes.h"
#include "PixelInputTypes.h"

TextureCube reflectTex : register (t7);
TextureCube irraTex : register (t8);
SamplerState regularSampler :register(s0);
SamplerState normalSampler :register(s1);
SamplerState cubeSampler :register(s2);
SamplerComparisonState shadowSampler:register(s3);

cbuffer PerFrameBuffer: register (b0)
{
	matrix cameraWorldMatrix;
	matrix viewMatrix;
	matrix viewProjectionMatrix;
	matrix projectionMatrix;
	float4 cameraPosition;
	matrix lightViewProjectionMatrix;
	float absoluteTime;
};

cbuffer PerObjectBuffer : register(b1)
{
	matrix worldViewProjectionMatrix;
	matrix worldViewMatrix;
	matrix worldMatrix;
}

cbuffer LightBuffer : register (b2)
{
	float4 ambientColor;
	float4 diffuseColor;
	float4 specularColor;
	float4 lightDirection;
	float4 lightPosition;
	matrix DirectionalMatrix;
	float2 orthoExtents;
};

cbuffer AmbientBuffer : register (b3)
{
	float4 sky;
	float4 ground;
	float4 horizon;
	float4 fogColor;
	float4 sunFogColor;
	float4 coefs;
	float4 testThing;
};

float3 GetAmbient(float3 normal)
{
	return lerp(ground.xyz, sky.xyz, normal.y*0.5+0.5);
}

float4 GetTerrainAmbient(float3 normal)
{
	return lerp(horizon, sky, normal.y);
}

#endif




