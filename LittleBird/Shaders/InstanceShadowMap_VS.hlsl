#include "shared_shader_structures.h"

float4 main(InstanceVertexInputType input) : SV_POSITION
{
	matrix lightSpace = mul(input.worldMatrix, lightViewProjectionMatrix);
	float4 pos = mul(input.position, lightSpace);
	return pos;
}