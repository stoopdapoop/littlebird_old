#ifndef _TERRAIN_CONSTANTS_H_
#define _TERRAIN_CONSTANTS_H_

static const float ampConstant = 700.0f;
static const float tileDistance = 3.0f;
#define SZ (257)
static const float tileInterval = 1.0f / tileDistance;

#endif