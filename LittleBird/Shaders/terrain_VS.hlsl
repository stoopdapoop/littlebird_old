#include "shared_shader_structures.h"
#include <terrainConstants.h>

Texture2D vertHeightTexture : register(t0);
////////////////////////////////////////////////////////////////////////////////
// Vertex Shader
////////////////////////////////////////////////////////////////////////////////

TerrainPixelInputType main(TerrainVertexInputType input)
{
    TerrainPixelInputType output;

    //input.position.w = 1.0f;
	// why 2? what was I doing?
	uint2 coords = input.xy_tex.xy/tileDistance;
	float height = vertHeightTexture[coords].r*ampConstant;
	float4 position = float4(input.xy_tex.x, height, input.xy_tex.y, 1.0f);
    output.position = mul(position, worldViewProjectionMatrix);
    
    //output.normal = mul(input.normal, (float3x3)worldViewMatrix);
	//output.tangent = mul(input.tangent, (float3x3)worldViewMatrix);

	float4 h; 
	h[0] = vertHeightTexture[coords+uint2(0,-1)].r*ampConstant;
	[flatten]if(h[0] == 0.0f)
		h[0] = height;
	h[1] = vertHeightTexture[coords+uint2(-1,0)].r*ampConstant;
	[flatten]if(h[1] == 0.0f)
		h[1] = height;
	h[2] = vertHeightTexture[coords+uint2(1,0)].r*ampConstant;
	[flatten]if(h[2] == 0.0f)
		h[2] = height;
	h[3] = vertHeightTexture[coords+uint2(0,1)].r*ampConstant;
	[flatten]if(h[3] == 0.0f)
		h[3] = height;

	output.normal = float3(h[1] - h[2],2*tileDistance,h[0] - h[3]);
	float3 otherPosition = position.xyz + float3(tileDistance + position.x, h[2], position.z);
	output.tangent = normalize(otherPosition - position.xyz);

	output.normal = mul(output.normal, worldViewMatrix);
	output.tangent = mul(output.tangent, worldViewMatrix);
	output.bitangent = cross(output.tangent, output.normal);

	output.indextex = position.xz / (SZ / tileInterval);
	
	output.tex = input.xy_tex.zw;

    return output;
}