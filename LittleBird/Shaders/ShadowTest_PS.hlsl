#include "shared_shader_structures.h"

Texture2D shaderTexture : register(t0);
Texture2D shadowDepthTex : register(t4);

float4 main(SingleLightPixelInputType input) : SV_TARGET
{
	float2 ProjectedTexCoords;
	input.normal = normalize(input.normal);
    ProjectedTexCoords[0] = input.pos2D.x/input.pos2D.w/2.0f +0.5f;
    ProjectedTexCoords[1] = -input.pos2D.y/input.pos2D.w/2.0f +0.5f;
	float shadowSample = shadowDepthTex.SampleLevel(shadowSampler, ProjectedTexCoords, 0).r;
	float3 color = shaderTexture.Sample(regularSampler, input.tex).rgb;
	float3 colorAccum;
	float nDotL = saturate(dot(input.normal.xyz, lightDirection.xyz));

	colorAccum = float4(color * GetAmbient(input.normal), 1.0f);

	if ( shadowSample  >  input.pos2D.z)
	{
		colorAccum += nDotL * color;
	}

	const float3 lumVec = float3(0.2126, 0.7152, 0.0722);
	float colorlum = dot(lumVec, colorAccum);
	colorlum = colorlum/(colorlum+0.5);
	colorAccum *= colorlum;

	return float4(colorAccum, 1.0);

	//
	//return float4(ProjectedTexCoords,  0.0f, 1.0f);
}