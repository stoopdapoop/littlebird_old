
#ifndef _PIXEL_INPUT_TYPES_H_
#define _PIXEL_INPUT_TYPES_H_

struct TerrainPixelInputType
{
	float4 position : SV_POSITION;
	float3 normal : NORMAL;
	float3 tangent: TANGENT;
	float3 bitangent: BITANGENT;
	float2 tex : TEXCOORD0;
	float2 indextex : TEXCOORD2;
};

struct SingleLightPixelInputType
{
	float3 normal : NORMAL;
	float3 tangent: TANGENT;
	float3 bitangent: BITANGENT;
	float2 tex : TEXCOORD0;
	float3 wPosition : TEXCOORD1;
	float4 pos2D : TEXCOORD2;
	float4 position : SV_POSITION;
};

struct WaterPixelInputType
{
	float4 position : SV_POSITION;
	float4 viewPosition : TEXCOORD0;
	float3 worldPosition : TEXCOORD1;
	float3 normal : NORMAL;
	float3 tangent : TANGENT;
	float3 bitangent : BITANGENT;
};

struct SkySpherePixelInputType
{
	float4 position : SV_POSITION;
	float3 wPosition : TEXCOORD0;
};

#endif