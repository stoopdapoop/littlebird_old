
#include "shared_shader_structures.h"
#include "WaterDefines.h"

Texture2D sceneDepth : register(t0);
Texture2D normalMap : register(t1);

// sin(2.0 * pi * oilThickness / (dot(L, H) * wavelength)) * 0.5 + 0.5 try this for irridecscence

float3 ComputePositionViewFromZ(float2 positionScreen,
								float viewSpaceZ)
{
	float2 screenSpaceRay = float2(positionScreen.x / projectionMatrix._11, positionScreen.y / projectionMatrix._22);

	float3 positionView;
	positionView.z = viewSpaceZ;
	positionView.xy = screenSpaceRay.xy * positionView.z;

	return positionView;
}

float4 main(WaterPixelInputType input, float4 screenSpace : SV_Position) : SV_TARGET
{
	float3 normal = normalize(input.normal);
	float3 tangent = normalize(input.tangent);
	float3 bitangent = normalize(input.bitangent);
	float3x3 tbn = float3x3(tangent, bitangent, normal);
	float3 texNormal[2];
	// it's more accurate if I normalize texnormal before change of basis, but it looks better if I do it after :[
	texNormal[0] = normalize(normalMap.Sample(normalSampler, input.worldPosition.xz*0.01 + BigWaveScrollDirection * absoluteTime *WaveTimeScaler*0.5f).xyz * 2.0f - 1.0f);
	texNormal[1] = normalize(normalMap.Sample(normalSampler, input.worldPosition.xz*0.3 + SmallWaveScrollDirection * absoluteTime * WaveTimeScaler).xyz * 2.0f - 1.0f);
	float3 blendedNormal = normalize(float3(texNormal[0].xy + texNormal[1].xy, texNormal[0].z));
	input.normal = (mul(blendedNormal, tbn));

	float dep = sceneDepth.Load(uint3(screenSpace.x, screenSpace.y, 0)).r;
	float nDotL = dot(input.normal.xyz, lightDirection.xyz);
	//float3 deepColor = float3( 0.18867780436772762f, 0.5978442963618773f, 0.6616065586417131f);
	//float3 shallowColor = float3( 0.18867780436772762f, 0.6978442963618773f, 0.6616065586417131f);
	float3 finalColor;
	finalColor = lerp(ShallowColor.xyz, DeepColor.xyz, saturate(nDotL)*0.5f+0.5f);
	float3 eyeVec = normalize(input.viewPosition.xyz);
	float3 halfAngle = normalize(eyeVec.xyz - lightDirection.xyz);
	float nDotH = saturate(dot(-input.normal, halfAngle));
	float adjustedNDotH = pow(nDotH, 90);
	adjustedNDotH = adjustedNDotH / (adjustedNDotH + 1);
	finalColor += adjustedNDotH;

	float2 sceneDim;
	sceneDepth.GetDimensions(sceneDim.x, sceneDim.y);
	float2 screenPixelOffset = float2(2.0f, -2.0f) / sceneDim;
	float2 positionScreen = (float2(screenSpace.xy) + 0.5f) * screenPixelOffset.xy + float2(-1.0f, 1.0f);
	dep = projectionMatrix._43 / (dep - projectionMatrix._33);
	float underwaterDepth = ComputePositionViewFromZ(positionScreen, dep).z;
	float surfaceDepth = input.viewPosition.z;
	
	float transvalue = 0.8f;
	transvalue = lerp(0.1, 1, saturate(abs(underwaterDepth - surfaceDepth) * 1/WaterFadeFactor));

//	finalColor = pow(finalColor, 0.45f);
	return float4(finalColor,transvalue);
}