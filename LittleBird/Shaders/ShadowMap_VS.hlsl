#include "shared_shader_structures.h"


float4 main( float4 pos : POSITION ) : SV_POSITION
{
	pos = mul(pos, worldViewProjectionMatrix);
	return pos;
}