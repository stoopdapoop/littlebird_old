#include "GBufferSharedStructures.h"

GBufferPixelInputType main( VertexInputType input )
{
	GBufferPixelInputType output;

    input.position.w = 1.0f;

    output.position = mul(input.position, worldViewProjectionMatrix);

	output.normal = mul(input.normal, (float3x3)worldViewMatrix);

	output.tangent.xyz = mul(input.tangent.xyz * input.tangent.w, (float3x3)worldViewMatrix);

	output.bitangent = mul(input.binormal.xyz * input.binormal.w, (float3x3)worldViewMatrix);

    output.tex = input.tex;

	return output;
}