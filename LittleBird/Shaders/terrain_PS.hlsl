////////////////////////////////////////////////////////////////////////////////
// Filename: terrain.ps
////////////////////////////////////////////////////////////////////////////////
#include "shared_shader_structures.h"
#include "GBufferSharedStructures.h"

Texture2D terrainTexture[7] : register(t0);

float4 applyFog( in float4  rgb,      // original color of the pixel
				in float distance, // camera to point distance
				in float3  rayOri,   // camera position
				in float3  rayDir, // camera to point vector
				in float3 sunDir)  
{
	float c = coefs.x;
	float b = coefs.y;
	rayOri.y -= coefs.z;
	float fogAmount = c * exp(-rayOri.y*b) * (1.0-exp( -distance*rayDir.y*b ))/rayDir.y;
	fogAmount *= clamp(fogAmount, 0.0, 0.9);
	float sunAmount = saturate( dot( rayDir, sunDir ) );
	float4  thisfogColor;
	thisfogColor = lerp( fogColor, sunFogColor, pow(sunAmount,coefs.w) );
	//thisfogColor = reflectTex.SampleLevel(NormalSampler, reflect(rayDir, sunDir), 9);

	return lerp( rgb, thisfogColor, fogAmount );
}

//#define USE_NORMALMAP
////////////////////////////////////////////////////////////////////////////////
// Pixel Shader
////////////////////////////////////////////////////////////////////////////////
MRT_Out main(TerrainPixelInputType input)
{
	MRT_Out outColor;
	//return float4(0.0f, 1.0f, 0.0f, 1.0f);
	float3 normal = normalize(input.normal);

#ifdef USE_NORMALMAP
	float3 tangent = normalize(input.tangent);
	float3 bitangent = normalize(input.bitangent);
			
	float3x3 tbn = float3x3(tangent, bitangent, normal);
	float3 texnormal = normalize(terrainTexture[3].Sample(normalSampler, input.tex*.8f).xyz * 2.0f - 1.0f);
	normal = mul(texnormal, tbn);
#endif
	outColor.normal_rough = float4(EncodeSphereMap(normal), 0.65, 0.0f);
	
	float2 detailCoords = input.tex;
	float4 denseDetailSample = terrainTexture[0].Sample(regularSampler, detailCoords);
	
	detailCoords /= testThing.x;
	float4 sparseDetailSample = terrainTexture[0].Sample(regularSampler, detailCoords);

	//splat
	float4 splatMask = terrainTexture[4].Sample(regularSampler, float2(input.indextex.x, input.indextex.y));
	splatMask = pow(splatMask, 0.45f);
	//float4 textureColor = textureColor5;
	//textureColor = pow(textureColor, testThing.x);
	
	float4 tintSample = terrainTexture[6].Sample(regularSampler, float2(input.indextex.x, input.indextex.y));
	float denseDetailAccum;
	float sparseDetailAccum;

	denseDetailAccum = denseDetailSample.a * splatMask.r;
	sparseDetailAccum = sparseDetailSample.a * splatMask.r;
	denseDetailAccum += denseDetailSample.g * splatMask.g;
	sparseDetailAccum += sparseDetailSample.g * splatMask.g;
	denseDetailAccum += denseDetailSample.b * splatMask.b;
	sparseDetailAccum += sparseDetailSample.b * splatMask.b;
	//whatever's left goes to last channel
	float alphaBlendFactor = 1-(splatMask.r + splatMask.g + splatMask.b);
	denseDetailAccum += denseDetailSample.r * alphaBlendFactor;
	sparseDetailAccum += sparseDetailSample.r * alphaBlendFactor;

	float detailBlend = sparseDetailAccum * denseDetailAccum;
	tintSample *= detailBlend * 2.0f;

	outColor.diffuse = tintSample;
	outColor.spec = 0.01f;

	return outColor;
}


