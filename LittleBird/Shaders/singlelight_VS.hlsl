////////////////////////////////////////////////////////////////////////////////
// Filename: singlelight.vsh
////////////////////////////////////////////////////////////////////////////////
#include "shared_shader_structures.h"


////////////////////////////////////////////////////////////////////////////////
// Vertex Shader
////////////////////////////////////////////////////////////////////////////////
SingleLightPixelInputType main(VertexInputType input)
{
    SingleLightPixelInputType output;
    

    // Change the position vector to be 4 units for proper matrix calculations.
    input.position.w = 1.0f;

    // Calculate the position of the vertex against the world, view, and projection matrices.
    output.position = mul(input.position, worldViewProjectionMatrix);

    // Calculate the normal vector against the world matrix only.
    output.normal = mul(input.normal, (float3x3)worldMatrix);
	
    // Normalize the normal vector.
    //output.normal = normalize(output.normal);

	output.tangent.xyz = mul(input.tangent.xyz * input.tangent.w, (float3x3)worldMatrix);
	//output.tangent.xyz = input.tangent.w;

	output.bitangent = mul(input.binormal.xyz * input.binormal.w, (float3x3)worldMatrix);
	//output.bitangent.xyz = input.binormal.w;

	// Calculate the position of the vertex in the world.
    output.wPosition = mul(input.position, worldMatrix);

	output.pos2D = mul(input.position, mul(worldMatrix, DirectionalMatrix));

    // Normalize the viewing direction vector.
     //output.viewDirection = normalize(output.viewDirection);

	// Store the texture coordinates for the pixel shader.
    output.tex = input.tex;

    return output;
}