
#include "shared_shader_structures.h"

float4 main(SkySpherePixelInputType input) : SV_TARGET
{
	float3 toWorld = input.wPosition;
	//float3 envColor = reflectTex.SampleLevel(cubeSampler, toWorld, 0) *.05;
	//float3 tardColor = float3((1.0f-(input.wPosition.y/4.0f))*.3, 1.0f-(input.wPosition.y/4.0f), input.wPosition.y/4.0f);
	float3 base = float3(0.25f, 0.45f, 0.5f);
	float3 top = float3(0.3f, 0.6f, 0.6f);
	//envColor = envColor / (envColor+1);
	base = lerp(base, top, input.wPosition.y/4.0f);
	return float4(base, 1.0f);
}