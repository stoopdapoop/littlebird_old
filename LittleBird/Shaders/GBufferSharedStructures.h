#ifndef _GBUFFER_SHARED_STRUCTURES_H_
#define _GBUFFER_SHARED_STRUCTURES_H_

#include "shared_shader_structures.h"

//todo: figure out if I can remove this
Texture2D gGBufferTextures[4] : register(t0);
Texture2D shadowMap : register(t4);

float2 EncodeSphereMap(float3 n)
{
	float oneMinusZ = 1.0f - n.z;
	float p = sqrt(n.x * n.x + n.y * n.y + oneMinusZ * oneMinusZ);
	return n.xy / p * 0.5f + 0.5f;
}

float3 DecodeSphereMap(float2 e)
{
	float2 tmp = e - e * e;
	float f = tmp.x + tmp.y;
	float m = sqrt(4.0f * f - 1.0f);

	float3 n;
	n.xy = m * (e * 4.0f - 2.0f);
	n.z  = 3.0f - 8.0f * f;
	return n;
}

struct MRT_Out
{
	float4 diffuse : SV_Target0;
	float4 normal_rough : SV_Target1;
	float4 spec : SV_Target2;
};

struct GBufferPixelInputType
{
	float4 position : SV_POSITION;
	float3 normal : NORMAL;
	float3 tangent: TANGENT;
	float3 bitangent: BITANGENT;
	float2 tex : TEXCOORD0;	
};

// Data that we can read or derive from the surface shader outputs
struct SurfaceData
{
	float3 viewPosition;         // View space position
	float3 normal;               // View space normal
	float4 diffuseAlbedo;
	float3 specularAlbedo;
	float specularRoughness;
};

float3 ComputePositionViewFromZ(float2 positionScreen,
								float viewSpaceZ)
{
	float2 screenSpaceRay = float2(positionScreen.x / projectionMatrix._11, positionScreen.y / projectionMatrix._22);

	float3 positionView;
	positionView.z = viewSpaceZ;
	positionView.xy = screenSpaceRay.xy * positionView.z;

	return positionView;
}

SurfaceData ComputeSurfaceDataFromGBufferSample(uint2 positionViewport)
{
	// Load the raw data from the GBuffer
	MRT_Out rawData;
	rawData.diffuse = gGBufferTextures[0].Load(uint3(positionViewport.xy, 0)).xyzw;
	rawData.normal_rough = gGBufferTextures[1].Load(uint3(positionViewport.xy, 0)).xyzw;
	rawData.spec = gGBufferTextures[2].Load(uint3(positionViewport.xy, 0)).xyzw;
	float zBuffer = gGBufferTextures[3].Load(uint3(positionViewport.xy, 0)).x;

	float2 gbufferDim;
	gGBufferTextures[0].GetDimensions(gbufferDim.x, gbufferDim.y);

	float2 screenPixelOffset = float2(2.0f, -2.0f) / gbufferDim;
	float2 positionScreen = (float2(positionViewport.xy) + 0.5f) * screenPixelOffset.xy + float2(-1.0f, 1.0f);

	// Decode into reasonable outputs
	SurfaceData data;

	float viewSpaceZ = projectionMatrix._43 / (zBuffer - projectionMatrix._33);

	data.viewPosition = ComputePositionViewFromZ(positionScreen, viewSpaceZ);

	data.normal = DecodeSphereMap(rawData.normal_rough.xy);
	data.diffuseAlbedo = rawData.diffuse;

	data.specularRoughness = rawData.normal_rough.z;
	data.specularAlbedo = rawData.spec.xyz;

	return data;
}

#endif


