////////////////////////////////////////////////////////////////////////////////
// Filename: singlelight.psh
////////////////////////////////////////////////////////////////////////////////
#include "shared_shader_structures.h"

Texture2D shaderTexture : register(t0);
Texture2D normalTexture : register(t1);
Texture2D smoothnessTexture : register(t2);
Texture2D specTexture : register(t3);
Texture2D shadowDepthTex : register(t4);


////////////////////////////////////////////////////////////////////////////////
// Pixel Shader
////////////////////////////////////////////////////////////////////////////////
float4 main(SingleLightPixelInputType input) : SV_TARGET
{  
	float3 normal = normalize(input.normal);
	float3 tangent = normalize(input.tangent);
	float3 bitangent = normalize(input.bitangent);
	float3x3 tbn = float3x3(tangent, bitangent, normal);
	float3 texnormal;
	texnormal = float3(0.0, 0.0, 1.0);
	texnormal = normalTexture.Sample(normalSampler, input.tex).xyz * 2.0f - 1.0f;
	normal = normalize(mul(texnormal, tbn));

	float rlen = 1.0/saturate(length(texnormal));
    float gloss = 1.0/(1.0 + (rlen - 1.0f));

	float3 diffuseAlbedo;
    diffuseAlbedo = shaderTexture.Sample(regularSampler, input.tex);
	//diffuseAlbedo = (float4)((0.2126*diffuseAlbedo.r) + (0.7152*diffuseAlbedo.g) + (0.0722*diffuseAlbedo.b));
	//diffuseAlbedo = (float4)0.01f;
	
	float3 viewDir = normalize(cameraPosition.xyz - input.wPosition.xyz);
		
	//perlight ------
	float3 lightPos = float3(lightPosition.x*20.0f, lightPosition.y*50.0f+5.0, lightPosition.z*5.0f);

    float3 lightDir = (lightPos - input.wPosition);
	float lightDist = length(lightDir);
	float4 lightAtten = 1/(lightDist*lightDist);
	lightAtten *=  exp2(diffuseColor*20);
	lightDir /= lightDist;

	float3  half_vec = normalize( viewDir + lightDir );
	float NdotL    = saturate(dot( normal, lightDir ));
	float NdotV    = saturate(dot( normal, viewDir ));
	float NdotH    = max( dot( normal, half_vec ), 1.0e-7 );
	float VdotH    = saturate(dot( viewDir,   half_vec ));
	

	float geometric = 2.0 * NdotH / VdotH;
	geometric = min( 1.0, geometric * min(NdotV, NdotL) );
	
	float roughTex = smoothnessTexture.Sample(regularSampler, input.tex).r;
	//roughTex = lightPosition.w;
	float r_sq          = (roughTex * roughTex);
	float NdotH_sq      = NdotH * NdotH;
	float NdotH_sq_r    = 1.0 / (NdotH_sq * r_sq);
	float roughness_exp = (NdotH_sq - 1.0) * ( NdotH_sq_r );
	float roughness     = exp(roughness_exp) * NdotH_sq_r / (4.0 * NdotH_sq );
	float3 fresnel       = 1.0 / (1.0 + NdotV);
	fresnel = specularColor + (1.0f - specularColor) * pow(1.0f - saturate(dot(viewDir, half_vec)), 5);
	float Rs = saturate((fresnel * geometric * roughness) / (NdotV * NdotL + 1.0e-7));

	float3 color = NdotL  * (diffuseAlbedo * (1.0-specularColor) + 1.0f * Rs) * lightAtten;
	float3 reflectVec = reflect(-viewDir, normal);
	fresnel = specularColor + (1.0f - specularColor) * pow(1.0f - saturate(dot(viewDir, normal)), 5);
	// constant is to prevent aliasing on reflective surfaces or bright lights
	float envLod = min(reflectTex.CalculateLevelOfDetail(regularSampler, reflectVec), 5);
	envLod = pow(roughTex, 0.1)*7.0f;
	float3 envColor = reflectTex.SampleLevel(cubeSampler, reflectVec, envLod) * 0.05;
	//color += envColor * fresnel;
	//color += diffuseAlbedo * irraTex.SampleLevel(cubeSampler, normal, 2) * 0.05f;


	color = diffuseAlbedo * dot(normal, -lightDirection) * 0.3;


	//color += diffuseAlbedo;
	// adjust exposure
	color *= lightPosition.w;

	const float3 lumVec = float3(0.2126, 0.7152, 0.0722);
	float colorlum = dot(lumVec, color);
	colorlum = colorlum/(colorlum+1);
	color *= colorlum;


	//color = (float4)0.0f;
	//color.r = 1-shadowDepthTex.Load(int3(input.position.x*2.1333, input.position.y*3.792, 0)) ;
	//color.r = shadowDepthTex.Sample(regularSampler, float2(input.position.x/960, input.position.y/540));
	//color = color/(color+1);
	//color = NdotL * lightAtten * (diffuseAlbedo);
	//float nDotL = saturate(dot(normal, lightDir));

	//float3 halfAngle = normalize(lightDir + viewDir);
	//float nDotH = saturate(dot(normal, halfAngle));
 //   float texPower = smoothnessTexture.Sample(regularSampler, float2(input.tex.x,input.tex.y)).r;
	//// use dot for lum
	////powerLum = ((0.2126*power.r) + (0.7152*power.g) + (0.0722*power.b));
	////return powerLum;
	////powerLum = powerLum*2;
	//float power = exp2(lightPosition.w * texPower + 1) ;
	//power = lerp(1.0f, 2048.0f, texPower);
 //   float spec = pow(nDotH, power)*((power + 2.0)/8.0);

	////return spec * lightAtten;

	//float4 newspecularColor = specularColor + (1.0f - specularColor) * pow(1.0f - saturate(dot(-viewDir, halfAngle)), 5);

	//float4 specAccum = newspecularColor * spec;
	////envColor = shaderTexture.Sample(RegularSampler, float2(input.tex.y,input.tex.x)).zxyw;
	//	
	//// fudgefactor
	////envMapColor /= 10;
	////return envMapColor;

	////float4 val = specAccum + (max(power, specAccum) - specAccum) * pow(1 - saturate(dot(viewDir, normal)), 5);
	////float4 val =  specAccum + (1.0f - specAccum) * pow(1.0f - saturate(dot(viewDir, halfAngle)), 5);
	////float4 fresnel = specularColor + (1.0f - specularColor) * pow(1.0f - saturate(dot(viewDir, halfAngle)), 5);
	////specAccum *= fresnel;
	////return val;

	//
	////float4 us = specAccum * pow(1 - saturate(dot(viewDir, normal)), 5) * (envMapColor);

	////diffuseAlbedo = (1-specAccum)*diffuseAlbedo;
	////diffuseAlbedo += diffuseAlbedo * GetAmbient(normal);
	//
	//float4 surfaceColor = (diffuseAlbedo + specAccum);

	//float4 envMapColor;
	//float3 reflectVec = reflect(-viewDir, normal);
	//float envLod = reflectTex.CalculateLevelOfDetail(regularSampler, reflectVec);//7-(texPower*7)
	//envLod = max(7-(texPower*7), envLod);
	//envMapColor = reflectTex.SampleLevel(cubeSampler, reflectVec, envLod) * 0.3f;
	//float4 us;
	//us = specularColor + (max(texPower, specularColor) - specularColor) * pow(1 - saturate(dot(viewDir, normal)), 5);
	//us *= envMapColor;
	//

	//color = (surfaceColor * nDotL * lightAtten);
	////color += diffuseAlbedo * GetAmbient(normal);
	////color += us;
	//color = color/(color+1.0);
	//float3 lumVec = float3(0.2126, 0.7152, 0.0722);
	//float colorlum = dot(lumVec, color);
	////color *= colorlum/(colorlum+1);
	//outColor.rt0 = (float4)(gloss/5);
	//outColor.rt1.r = lightAtten * nDotL;
	//outColor.rt1.g = spec * specAccum * nDotL;
	//outColor.rt1 = (float4)1;
	//outColor.rt2 =1;
    return float4(color, 1.0f);
}