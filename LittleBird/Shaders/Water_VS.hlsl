
#include "shared_shader_structures.h"
#include "WaterDefines.h"

static const float waterInterval = 1.0f/SZ;
static const float phaseFactor = 3.14*2.0f;

float rand(float2 co)
{
    return frac(sin(dot(co.xy ,float2(12.9898f,78.233f))) * 43758.5453f);
}

float GetHeightAtCoords(float2 waterCoords)
{
	return sin((absoluteTime+ rand(waterCoords)*phaseFactor)*WaveTimeScaler) * AmpFactor + BaseFactor;
}

WaterPixelInputType main(TerrainVertexInputType input)
{
	WaterPixelInputType output;

	float2 localCoords = input.xy_tex.xy;
	float2 worldOffset = float2(worldMatrix._41, worldMatrix._43);
	float height = GetHeightAtCoords(localCoords + worldOffset);

	float4 h; 
	h[0] = GetHeightAtCoords(float2(localCoords.x, localCoords.y-waterInterval) + worldOffset);
	h[1] = GetHeightAtCoords(float2(localCoords.x-waterInterval, localCoords.y) + worldOffset);
	h[2] = GetHeightAtCoords(float2(localCoords.x+tileInterval, localCoords.y) + worldOffset);
	h[3] = GetHeightAtCoords(float2(localCoords.x, localCoords.y+waterInterval) + worldOffset);


	float4 position = float4(input.xy_tex.x, height, input.xy_tex.y, 1.0f);
    output.position = mul(position, worldViewProjectionMatrix);
	output.normal = normalize(float3(h[1] - h[2],2*tileDistance,h[0] - h[3]));
	output.normal = (mul(output.normal, (float3x3)worldViewMatrix));
	output.tangent = normalize(float3(2*tileDistance, h[1] - h[2], h[0] - h[3]));
	output.tangent = (mul(output.tangent, (float3x3)worldViewMatrix));
	output.bitangent = cross(output.normal, output.tangent);

	output.viewPosition = mul(position, worldViewMatrix);
	output.worldPosition = mul(position, worldMatrix);


	return output;
}