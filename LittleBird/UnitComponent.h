#ifndef UnitComponent_h__
#define UnitComponent_h__

#include <DirectXMath.h>
#include "component.h"

class UnitComponent : public Component
{
public:
	UnitComponent(DirectX::XMVECTOR startPos, float unitHeight, float moveSpeed);

	btKinematicCharacterController* kinematicController;
	float UnitHeight;
	float MoveSpeed;
};


#endif // UnitComponent_h__
