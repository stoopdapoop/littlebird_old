#ifndef _TEXTURE_H_
#define _TEXTURE_H_

#include <string>
#include "TextureException.h"

struct ID3D11ShaderResourceView;
struct ID3D11Resource;
struct ID3D11Device;
class Renderer;

class Texture
{
public:
	Texture(Renderer* rend, std::string filename);
	Texture(ID3D11Device* device, unsigned char* data, unsigned format, unsigned width, unsigned height);
	~Texture();

	ID3D11ShaderResourceView* GetResourceView();

private:
	void InitializeFromFile(Renderer* rend, std::string filename);
	void Shutdown();

private:

	ID3D11ShaderResourceView* m_ResourceView;
	ID3D11Resource* m_Resource;
};

#endif