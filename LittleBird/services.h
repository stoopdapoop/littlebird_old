#ifndef _SERVICES_H_
#define _SERVICES_H_

#include "Debug_ui.h"
#include "INIReader.h"
//#include "WorldQuery.h"
#include "DeveloperConsole.h"
#include "Networking.h"
#include "input.h"
#include "ResourceManager.h"
#include "timer.h"

#define MEMBERVARNAME( classname ) m_ ## classname
#define DECLARE_SERVICE( clsname ) public: \
	static clsname ## * Get ## clsname ## () { return MEMBERVARNAME(clsname) ; } \
	static void Provide ## clsname ## ( ## clsname ## * service ) \
	{ \
		MEMBERVARNAME(clsname) = service; \
	} \
	private: \
	static clsname ## * MEMBERVARNAME(clsname);

class Services
{
	DECLARE_SERVICE(DebugUI)
	//DECLARE_SERVICE(WorldQuery)
	DECLARE_SERVICE(ResourceManager)
	DECLARE_SERVICE(INIReader)
	DECLARE_SERVICE(DeveloperConsole)
	DECLARE_SERVICE(Networking)
	DECLARE_SERVICE(Physics)
	DECLARE_SERVICE(Input)
	DECLARE_SERVICE(DebugDrawer)
	DECLARE_SERVICE(Timer)
};

#endif